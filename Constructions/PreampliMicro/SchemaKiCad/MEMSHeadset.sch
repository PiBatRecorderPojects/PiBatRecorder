EESchema Schematic File Version 2
LIBS:MEMSHeadset-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:Gajda_amplif
LIBS:MEMSHeadset-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Pré-ampli micro MEMS vers entrée Headset"
Date "2016-01-17"
Rev "A"
Comp "PiBatRecorder"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LM358-RESCUE-MEMSHeadset U1
U 1 1 569BC3FE
P 6400 3100
F 0 "U1" H 6350 3300 60  0000 L CNN
F 1 "OPA2344" H 6350 2850 60  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm" H 6400 3100 60  0001 C CNN
F 3 "" H 6400 3100 60  0000 C CNN
	1    6400 3100
	1    0    0    -1  
$EndComp
$Comp
L LM358-RESCUE-MEMSHeadset U1
U 2 1 569BC47B
P 4000 3100
F 0 "U1" H 3950 3300 60  0000 L CNN
F 1 "OPA2344" H 3950 2850 60  0000 L CNN
F 2 "Sockets:H08" H 4000 3100 60  0001 C CNN
F 3 "" H 4000 3100 60  0000 C CNN
	2    4000 3100
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 569BC4EA
P 3400 3650
F 0 "R3" V 3480 3650 50  0000 C CNN
F 1 "10k" V 3400 3650 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 3330 3650 30  0001 C CNN
F 3 "" H 3400 3650 30  0000 C CNN
	1    3400 3650
	1    0    0    -1  
$EndComp
$Comp
L C C2
U 1 1 569BC5CF
P 5100 2800
F 0 "C2" H 5125 2900 50  0000 L CNN
F 1 "12pF" H 5125 2700 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 5138 2650 30  0001 C CNN
F 3 "" H 5100 2800 60  0000 C CNN
	1    5100 2800
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR L1
U 1 1 569BCAFB
P 8200 2100
F 0 "L1" V 8150 2100 50  0000 C CNN
F 1 "100mH" V 8300 2100 50  0000 C CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 8200 2100 60  0001 C CNN
F 3 "" H 8200 2100 60  0000 C CNN
	1    8200 2100
	0    1    1    0   
$EndComp
$Comp
L R R4
U 1 1 569BD05B
P 3400 2550
F 0 "R4" V 3480 2550 50  0000 C CNN
F 1 "10k" V 3400 2550 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 3330 2550 30  0001 C CNN
F 3 "" H 3400 2550 30  0000 C CNN
	1    3400 2550
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 569BD338
P 2050 3200
F 0 "C1" V 1900 3150 50  0000 L CNN
F 1 "47nF" V 2200 3100 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 2088 3050 30  0001 C CNN
F 3 "" H 2050 3200 60  0000 C CNN
	1    2050 3200
	0    1    1    0   
$EndComp
$Comp
L C C3
U 1 1 569BD3DC
P 5350 3100
F 0 "C3" V 5200 3050 50  0000 L CNN
F 1 "56pF" V 5500 3000 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 5388 2950 30  0001 C CNN
F 3 "" H 5350 3100 60  0000 C CNN
	1    5350 3100
	0    1    1    0   
$EndComp
$Comp
L R R1
U 1 1 569BD48E
P 5600 2800
F 0 "R1" V 5680 2800 50  0000 C CNN
F 1 "100k" V 5600 2800 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 5530 2800 30  0001 C CNN
F 3 "" H 5600 2800 30  0000 C CNN
	1    5600 2800
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 569BE0C6
P 5100 3650
F 0 "R2" V 5180 3650 50  0000 C CNN
F 1 "15k" V 5100 3650 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 5030 3650 30  0001 C CNN
F 3 "" H 5100 3650 30  0000 C CNN
	1    5100 3650
	1    0    0    -1  
$EndComp
$Comp
L C C1
U 1 1 569BE2E5
P 4750 3100
F 0 "C1" V 4600 3050 50  0000 L CNN
F 1 "56pF" V 4900 3000 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 4788 2950 30  0001 C CNN
F 3 "" H 4750 3100 60  0000 C CNN
	1    4750 3100
	0    1    1    0   
$EndComp
$Comp
L C C5
U 1 1 569BE348
P 7200 3100
F 0 "C5" V 7050 3050 50  0000 L CNN
F 1 "47nF" V 7350 3000 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 7238 2950 30  0001 C CNN
F 3 "" H 7200 3100 60  0000 C CNN
	1    7200 3100
	0    1    1    0   
$EndComp
$Comp
L R R7
U 1 1 569BEC0F
P 5800 3650
F 0 "R7" V 5880 3650 50  0000 C CNN
F 1 "10k" V 5800 3650 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 5730 3650 30  0001 C CNN
F 3 "" H 5800 3650 30  0000 C CNN
	1    5800 3650
	1    0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 569BEC92
P 5800 2450
F 0 "R8" V 5880 2450 50  0000 C CNN
F 1 "10k" V 5800 2450 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 5730 2450 30  0001 C CNN
F 3 "" H 5800 2450 30  0000 C CNN
	1    5800 2450
	1    0    0    -1  
$EndComp
$Comp
L R R5
U 1 1 569BED0D
P 2750 3200
F 0 "R5" V 2830 3200 50  0000 C CNN
F 1 "4.7k" V 2750 3200 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 2680 3200 30  0001 C CNN
F 3 "" H 2750 3200 30  0000 C CNN
	1    2750 3200
	0    1    1    0   
$EndComp
$Comp
L R R6
U 1 1 569BEFA5
P 4150 2750
F 0 "R6" V 4230 2750 50  0000 C CNN
F 1 "100k" V 4150 2750 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 4080 2750 30  0001 C CNN
F 3 "" H 4150 2750 30  0000 C CNN
	1    4150 2750
	0    1    1    0   
$EndComp
$Comp
L C C6
U 1 1 569BFA68
P 7500 3450
F 0 "C6" H 7525 3550 50  0000 L CNN
F 1 "47nF" H 7525 3350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 7538 3300 30  0001 C CNN
F 3 "" H 7500 3450 60  0000 C CNN
	1    7500 3450
	1    0    0    -1  
$EndComp
$Comp
L C C7
U 1 1 569BFAA9
P 8900 3450
F 0 "C7" H 8925 3550 50  0000 L CNN
F 1 "47nF" H 8925 3350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 8938 3300 30  0001 C CNN
F 3 "" H 8900 3450 60  0000 C CNN
	1    8900 3450
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 P1
U 1 1 569CCCB5
P 9250 2100
F 0 "P1" H 9250 2200 50  0000 C CNN
F 1 "+5V" H 9400 2100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 9250 2100 60  0001 C CNN
F 3 "" H 9250 2100 60  0000 C CNN
	1    9250 2100
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 P3
U 1 1 569CD3D9
P 9250 4000
F 0 "P3" H 9250 4100 50  0000 C CNN
F 1 "GND" H 9450 4000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 9250 4000 60  0001 C CNN
F 3 "" H 9250 4000 60  0000 C CNN
	1    9250 4000
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X01 P4
U 1 1 569CE250
P 1600 2100
F 0 "P4" H 1600 2200 50  0000 C CNN
F 1 "+3V3 MEMS" H 1900 2100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 1600 2100 60  0001 C CNN
F 3 "" H 1600 2100 60  0000 C CNN
	1    1600 2100
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 P5
U 1 1 569CEAD3
P 1600 3200
F 0 "P5" H 1600 3300 50  0000 C CNN
F 1 "Entrée Micro" H 1900 3200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 1600 3200 60  0001 C CNN
F 3 "" H 1600 3200 60  0000 C CNN
	1    1600 3200
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 P6
U 1 1 569CEB26
P 1600 4000
F 0 "P6" H 1600 4100 50  0000 C CNN
F 1 "GND MEMS" H 1900 4000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 1600 4000 60  0001 C CNN
F 3 "" H 1600 4000 60  0000 C CNN
	1    1600 4000
	-1   0    0    1   
$EndComp
$Comp
L CONN_01X01 P2
U 1 1 569CD34C
P 9250 3100
F 0 "P2" H 9250 3200 50  0000 C CNN
F 1 "E Micro Heatset" H 9650 3100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01" H 9250 3100 60  0001 C CNN
F 3 "" H 9250 3100 60  0000 C CNN
	1    9250 3100
	1    0    0    -1  
$EndComp
$Comp
L CP C8
U 1 1 569C6269
P 7700 2450
F 0 "C8" H 7725 2550 50  0000 L CNN
F 1 "10µF" H 7725 2350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 7738 2300 50  0001 C CNN
F 3 "" H 7700 2450 50  0000 C CNN
	1    7700 2450
	1    0    0    -1  
$EndComp
$Comp
L CP C9
U 1 1 569C7DC9
P 8700 2450
F 0 "C9" H 8725 2550 50  0000 L CNN
F 1 "10µF" H 8725 2350 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 8738 2300 50  0001 C CNN
F 3 "" H 8700 2450 50  0000 C CNN
	1    8700 2450
	1    0    0    -1  
$EndComp
$Comp
L R R10
U 1 1 56BB7719
P 2850 2100
F 0 "R10" V 2950 2100 50  0000 C CNN
F 1 "100" V 2850 2100 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 2780 2100 30  0001 C CNN
F 3 "" H 2850 2100 30  0000 C CNN
	1    2850 2100
	0    1    1    0   
$EndComp
$Comp
L CP C10
U 1 1 56BB7893
P 2250 2900
F 0 "C10" H 2275 3000 50  0000 L CNN
F 1 "100µF" H 2275 2800 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D6_P5" H 2288 2750 50  0001 C CNN
F 3 "" H 2250 2900 50  0000 C CNN
	1    2250 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3400 2700 3400 3500
Wire Wire Line
	3500 3000 3400 3000
Connection ~ 3400 3000
Wire Wire Line
	2900 3200 3500 3200
Connection ~ 3100 3200
Wire Wire Line
	3100 2750 3100 3200
Wire Wire Line
	3400 4000 3400 3800
Connection ~ 3400 4000
Connection ~ 3900 4000
Wire Wire Line
	3900 2100 3900 2700
Connection ~ 3900 2100
Wire Wire Line
	3400 2400 3400 2100
Connection ~ 3400 2100
Connection ~ 5800 2100
Wire Wire Line
	5800 2600 5800 3500
Wire Wire Line
	5800 4000 5800 3800
Connection ~ 5800 4000
Wire Wire Line
	5900 3000 5800 3000
Connection ~ 5800 3000
Wire Wire Line
	4500 3100 4600 3100
Wire Wire Line
	4900 3100 5200 3100
Wire Wire Line
	6900 3100 7050 3100
Wire Wire Line
	6300 4000 6300 3500
Connection ~ 6300 4000
Wire Wire Line
	6300 2100 6300 2700
Connection ~ 6300 2100
Connection ~ 6950 3100
Wire Wire Line
	7700 2100 7700 2300
Connection ~ 7700 2100
Wire Wire Line
	8700 2300 8700 2100
Connection ~ 8700 2100
Wire Wire Line
	8900 2100 8900 3300
Connection ~ 8900 2100
Connection ~ 7500 2100
Wire Wire Line
	8900 4000 8900 3600
Connection ~ 8900 4000
Wire Wire Line
	7500 4000 7500 3600
Connection ~ 7500 4000
Wire Wire Line
	7700 4000 7700 2600
Connection ~ 7700 4000
Wire Wire Line
	8700 4000 8700 2600
Connection ~ 8700 4000
Wire Wire Line
	5800 2100 5800 2300
Wire Wire Line
	7500 2100 7500 3300
Wire Wire Line
	3900 4000 3900 3500
Wire Wire Line
	1900 3200 1800 3200
Wire Wire Line
	5100 2650 6950 2650
Wire Wire Line
	6950 2650 6950 3100
Connection ~ 5600 2650
Wire Wire Line
	5500 3100 5600 3100
Wire Wire Line
	5600 2950 5600 3200
Wire Wire Line
	5600 3200 5900 3200
Connection ~ 5600 3100
Wire Wire Line
	5100 2950 5100 3500
Connection ~ 5100 3100
Wire Wire Line
	5100 4000 5100 3800
Connection ~ 5100 4000
Wire Wire Line
	4500 3100 4500 2750
Wire Wire Line
	4500 2750 4300 2750
Wire Wire Line
	4000 2750 3100 2750
Wire Wire Line
	2600 3200 2200 3200
Wire Wire Line
	1800 2100 2700 2100
Connection ~ 2250 2100
Connection ~ 2250 4000
Wire Wire Line
	3000 2100 7900 2100
Wire Wire Line
	7350 3100 9050 3100
Wire Wire Line
	8500 2100 9050 2100
Wire Wire Line
	1800 4000 9050 4000
$Comp
L C C11
U 1 1 56BDEA32
P 2550 2850
F 0 "C11" H 2575 2950 50  0000 L CNN
F 1 "220nF" H 2575 2750 50  0000 L CNN
F 2 "Capacitors_ThroughHole:C_Disc_D3_P2.5" H 2588 2700 30  0001 C CNN
F 3 "" H 2550 2850 60  0000 C CNN
	1    2550 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 2700 2550 2100
Connection ~ 2550 2100
Wire Wire Line
	2550 3000 2550 4000
Connection ~ 2550 4000
Connection ~ 4500 3100
$Comp
L R R11
U 1 1 5706E667
P 1850 2750
F 0 "R11" V 1950 2750 50  0000 C CNN
F 1 "2.7k" V 1850 2750 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 1780 2750 30  0001 C CNN
F 3 "" H 1850 2750 30  0000 C CNN
	1    1850 2750
	-1   0    0    1   
$EndComp
Wire Wire Line
	2250 3050 2250 4000
Wire Wire Line
	2250 2100 2250 2750
Wire Wire Line
	1850 2900 1850 3200
Connection ~ 1850 3200
Wire Wire Line
	1850 2600 1850 2300
Wire Wire Line
	1850 2300 3200 2300
Wire Wire Line
	3200 2300 3200 2100
Connection ~ 3200 2100
Wire Notes Line
	1700 2200 2000 2200
Wire Notes Line
	2000 2200 2000 3000
Wire Notes Line
	2000 3000 1700 3000
Wire Notes Line
	1700 3000 1700 2200
Wire Notes Line
	2100 1950 2100 3050
Wire Notes Line
	2100 3050 3050 3050
Wire Notes Line
	3050 3050 3050 1950
Wire Notes Line
	3050 1950 2100 1950
Text Label 2150 1900 0    60   ~ 0
Pour_micro_MEMS
Text Label 800  2800 0    60   ~ 0
Pour_micro_Electret
$EndSCHEMATC
