/* 
 * File:   AboutParam.cpp
 * Author: Jean-Do
 * 
 * Created on 22 septembre 2015, 20:20
 */
//-------------------------------------------------------------------------
// Classe d'affichage des copyright

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "AboutParam.h"
#include "Version.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
AboutParam::AboutParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	strcpy( indicModif, "000000000000000000000");
	strcat( sCopyright, " https://frama.link/PiBatRecSynth ");
	strcat( sCopyright, " Copyrights: PiBatRecorder ");
	strcat( sCopyright, VERSION);
	strcat( sCopyright, " JDV 2015;");
	strcat( sCopyright, " Wiring Pi Drogon 2015;");
	strcat( sCopyright, " Port Audio R Bencina P Burk 2011;");
	strcat( sCopyright, " GPU FFT Andrew Holme 2014;");
	strcat( sCopyright, " GFX Adafruit P Burgess   ");
	iAff = 0;
}

//-------------------------------------------------------------------------
// Destructeur
AboutParam::~AboutParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void AboutParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	GenericParam::ReceiveKey(iKey);
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void AboutParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	//printf("AboutParam::AffParam %d\n", iLine);
	// Préparation de la ligne
	// Affichage en boucle des copyrights
	int iLenCopyright = strlen(sCopyright);
	if (iAff < (iLenCopyright-21))
		strncpy(&(lineParam[1]), &(sCopyright[iAff]), 21);
	else
	{
		strncpy(&(lineParam[1]), &(sCopyright[iAff]), iLenCopyright-iAff-1);
		lineParam[iLenCopyright-iAff-1] = 0;
		strncpy(&lineParam[strlen(lineParam)], &(sCopyright[0]), 21-(iLenCopyright-iAff-1));
	}
	lineParam[ 0] = ' ';
	lineParam[21] = 0;
	//printf("AboutParam::AffParam (%s)\n", lineParam);
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
	// Préparation index prochain affichage
	iAff++;
	if (iAff >= iLenCopyright)
		iAff = 0;
}
