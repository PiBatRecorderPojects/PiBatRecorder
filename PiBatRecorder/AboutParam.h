/* 
 * File:   AboutParam.h
 * Author: Jean-Do
 *
 * Created on 22 septembre 2015, 20:20
 */

#include "GenericParam.h"

#ifndef ABOUTPARAM_H
#define	ABOUTPARAM_H

//-------------------------------------------------------------------------
// Classe d'affichage des copyright
class AboutParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	AboutParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~AboutParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

private:
	// Chaine de l'ensemble des copyright
	char sCopyright[512];
	// Indice du 1er caractère
	int iAff;
};

#endif	/* ABOUTPARAM_H */

