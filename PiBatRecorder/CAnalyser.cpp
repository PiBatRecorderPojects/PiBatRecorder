/* 
 * File:   CAnalyser.cpp
 * Author: Jean-Do
 * 
 * Created on 22 août 2015, 16:18
 */
//-------------------------------------------------------------------------
// Classe de gestion de l'analyse des échantillons
// Gère un thread qui exécute les FFT et analyse les résultats
// Gère une liste de cris avec la moyenne des derniers cris

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <wiringPi.h>
#include "CAnalyser.h"
#include "CParameters.h"
#include "GpuFFT/mailbox.h"
#include "GpuFFT/gpu_fft.h"

// Pour debug de l'analyse ou des FFT
//#define DEBUG_ANALYSE
//#define DEBUG_FFT
//#define DEBUG_LEVEL

// Pour mémoriser le niveau de bruit dans un fichier en fin de thread
//#define MEMO_NOISE

// Temps minimum d'un cri en ms. En-dessous, ce n'est pas un cri.
#define DUR_MIN_CRY 1.7

// Pour un taux de recouvrement de 50%
#define DBL_FFT

// Pour une détection sur les cris, sinon sur le seuil
#define DETECT_CRY

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
CAnalyser::CAnalyser(
		CParameters *pPar,			// Pointeur sur les paramètres
		CMonitStatus *pMonitStatus	// Pointeur sur gestionnaire d'état du Monitoring
		)
{
	//printf("CAnalyser::CAnalyser\n");		
	// Mémorisation des paramètres
	pParams     = pPar;
	pGestMonitStatus = pMonitStatus;
	iThreshold  = pPar->GetThreshold();
	dRecMin     = pPar->GetDRecMin();
	bRunAnalyse = false;
	buffLengthA = 0;
	buffLengthB = 0;
	iNbCry      = 0;
	// Précision fréquentielle en Hz de la FFT
	fFAccuracy  = (192000.0 / 2.0) / (FFTLEN / 2.0);
	// Précision temporelle en secondes de la FFT
#ifdef DBL_FFT
	fTAccuracy  = (FFTLEN * 1.0 / 192000) / 2;
#else
	fTAccuracy  = (FFTLEN * 1.0 / 192000);
#endif
	// Durée maximale d'un trou
	dMaxTrou    = fTAccuracy * 4.2;
	// Largeur max d'une QFC en Hz
	iMaxQFC     = 4000;
	// Préparation index FMin et FMax
	idFMin = pPar->GetFMin() / fFAccuracy;
	idFMax = pPar->GetFMax() / fFAccuracy;
	int iFFTLen = FFTLEN;
	long lChunk = BUFF_LENGTH;
	LogFile::AddLog(LINFO, "CAnalyser::CAnalyser FFT %d points, précisions %3.3fHz, %1.3fms, seuil %ddB\n", iFFTLen, fFAccuracy, fTAccuracy*1000.0, iThreshold);
#ifdef DBL_FFT
	LogFile::AddLog(LINFO, "BUFF_LENGTH %d, %d FFT par trame (recouvrement 50%), dMaxTrou %2.3lfms, idFMin %d, idFMax %d\n", lChunk, lChunk/(iFFTLen/2), dMaxTrou*1000.0, idFMin, idFMax);
#else
	LogFile::AddLog(LINFO, "BUFF_LENGTH %d, %d FFT par trame (pas de recouvrement), dMaxTrou %2.3lfms, idFMin %d, idFMax %d\n", lChunk, lChunk/iFFTLen, dMaxTrou*1000.0, idFMin, idFMax);
#endif
	// Préparation GPU_FFT
	mb = mbox_open();
	int ret = gpu_fft_prepare(mb, LOG2FFTLEN, GPU_FFT_FWD, 1, &fft);
    switch(ret) {
        case -1: LogFile::AddLog(LLOG, "Unable to enable V3D. Please check your firmware is up to date.\n");	break;
        case -2: LogFile::AddLog(LLOG, "log2_N=%d not supported.  Try between 8 and 22.\n", LOG2FFTLEN);		break;
        case -3: LogFile::AddLog(LLOG, "Out of memory.  Try a smaller batch or increase GPU memory.\n");		break;
        case -4: LogFile::AddLog(LLOG, "Unable to map Videocore peripherals into ARM memory space.\n");			break;
        case -5: LogFile::AddLog(LLOG, "Can't open libbcm_host.\n");											break;
    }
	//printf("mbox_open OK\n");
	dataIn = fft->in;
	dataOut = fft->out;
	iNbTrames = 0;
	iNbAnalyses = 0;

	/*if (pParams->GetMikeType() == MEMS_SPU0410LR5HQB)
	{
		// Init du tableau de correction des niveaux
		int iF = fFAccuracy;
		double iC = 0.0;
		for (int i=0; i<FFTLEN/2; i++)
		{
			fTbCorectLevel[i] = 0;
			if (iF <= 9750)
				// De 750Hz à 9750Hz courbe à 0
				fTbCorectLevel[i] = 0;
			else if (iF > 9750 and iF <= 23250)
				// De +9750 à 23250 la courbe monte de 11 dB en 18 pas
				fTbCorectLevel[i] = (-11.0 / 18) * (((iF - 9750) / 750) + 1);
			else if (iF > 23500 and iF <= 55000)
				// De +23500 à 55000 la courbe baisse de +11 à -8dB en 42 pas
				fTbCorectLevel[i] = -11.0 + (19.0 / 42) * (((iF - 23500) / 750) + 1);
			else if (iF > 55000 and iF <= 65250)
				// De +55000 à 65250 la courbe est stable à -8dB
				fTbCorectLevel[i] = +8.0;
			else if (iF > 65250 and iF <= 79500)
				// De +65000 à 79500 la courbe monte de -8 à -4dB en 19 pas
				fTbCorectLevel[i] = 8.0 - (4.0 / 19) * (((iF - 65250) / 750) + 1);
			else if (iF > 79500)
				// Au-dessus de 79500 la courbe est à priori stable à -4dB
				fTbCorectLevel[i] = +4.0;
			//printf("F %d, correction %lf\n", iF, fTbCorectLevel[i]);
			// Pas suivant
			iF += fFAccuracy;
		}
	}
	else*/
	{
		for (int i=0; i<FFTLEN/2; i++)
			fTbCorectLevel[i] = 0;
	}
	//printf("CAnalyser::CAnalyser OK\n");		
}

//-------------------------------------------------------------------------
// Destructeur
CAnalyser::~CAnalyser()
{
	// Relache les ressources FFT_GPU
	gpu_fft_release(fft);
}

//-------------------------------------------------------------------------
// Lance le thread
void CAnalyser::Start()
{
	LogFile::AddLog(LDEBUG, "CAnalyser::Start\n");
	// Init des paramètres
	iThreshold  = pParams->GetThreshold();
	dRecMin     = pParams->GetDRecMin();
	// Préparation index FMin et FMax (au cas ou les paramètres aient changés)
	idFMin = pParams->GetFMin() / fFAccuracy;
	idFMax = pParams->GetFMax() / fFAccuracy;
	// RAZ buffers trames
	buffLengthA = 0;
	buffLengthB = 0;
	// RAZ du cri courant
	RAZCurrentCry();
	iDetectPos = 0;
	iDetectNeg = 0;
	iNivMoyen = 30;
	for (int i=0; i<FFTLEN/2; i++)
	{
		iTbNoiseLevels[i] = 30;
		iTbLevels[i] = 0;
		iTbNbLevels[i] = 0;
	}
	// Lance le thread
	bRunAnalyse = true;
	int err = pthread_create(&thread, NULL, &RunAnalyse, (void *)this);
	if (err)
		LogFile::AddLog(LLOG, "pthread_create erreur : %d\n", err);
	iNbTrames = 0;
	iNbAnalyses = 0;
}

//-------------------------------------------------------------------------
// Stoppe le thread
void CAnalyser::Stop()
{
	// On prend la main sur les données
	pthread_mutex_lock (&mutexDatas);
	// Stoppe le thread
	bRunAnalyse = false;
#ifdef MEMO_NOISE
	// Memo des niveaux moyen de chaque canaux FFT dans un fichier log
	MemoLogNoise();
#endif
	// On relache les données
	pthread_mutex_unlock(&mutexDatas);
	LogFile::AddLog(LDEBUG, "CAnalyser::Stop\n");
}

//-------------------------------------------------------------------------
// Initialise le seuil de détection
void CAnalyser::SetThreshold(
	int iTh	// Seuil de détection
	)
{
	// On prend la main sur les données
	pthread_mutex_lock (&mutexDatas);
	// Init du nouveau seuil
	iThreshold = iTh;
	// On relache les données
	pthread_mutex_unlock(&mutexDatas);
}

//-------------------------------------------------------------------------
// RAZ du cri courant
void CAnalyser::RAZCurrentCry()
{
	RAZCry(&currentCry);
	lstFreq.clear();
}

//-------------------------------------------------------------------------
// RAZ d'un cri
void CAnalyser::RAZCry( 
	CryParams *pCry	// CRI à mettre à 0
	)
{
	pCry->Dur    = 0;
	pCry->FI     = 0;
	pCry->FME    = 0;
	pCry->FT     = 0;
	pCry->NivMax = 0;
	pCry->TDeb   = 0;
	pCry->TFin   = 0;
	strcpy(pCry->Struct, "      ");
}
	
//-------------------------------------------------------------------------
// Décodage de la structure du dernier cri
void CAnalyser::DecodageStruct()
{
#ifdef DEBUG_ANALYSE
	printf("\n*** CAnalyser::DecodageStruct\n");
	int x = 0;
#endif		
	// Un cri de plus
	iNbCry++;
    // Il faut au moins 5 FFTs pour définir un cri
	int iNb = lstFreq.size();
	if (iNb > 5)
	{
		// Calcul Moyenne, Ecart Type et Variance
		double dETypeF, dVarF, dMoyF = 0.0, dS1 = 0.0, dS2 = 0.0;
		int iNb = lstFreq.size();
		int i = 0;
		for (std::list<int>::iterator it=lstFreq.begin(); it != lstFreq.end(); ++it)
		{
			int iF = *it;
#ifdef DEBUG_ANALYSE
			printf("F %02d %dHz, ", i, iF);
			if (x >= 7)
			{
				printf("\n");
				x = 0;
			}
			else
				x++;
			i++;
#endif		
			dS1 += iF;
			dS2 = dS2 + pow(iF,2);
		}
		// Calcul moyenne
		dMoyF = dS1 / iNb;
		// Calcul de la variance
		dVarF = (dS2 / iNb) - pow(dMoyF, 2);
		// Calcul Ecart Type
		dETypeF = sqrt(dVarF);
#ifdef DEBUG_ANALYSE
		printf("\nMoyenne %lf, Variance %lf, Ecart type %lf\n", dMoyF, dVarF, dETypeF);
#endif		
		// Recherche FMin et FMax pour l'ensemble du cri
		// et pour des bandes temporelles de 1/5 du cri
		int FMinT[5];
		int FMaxT[5];
		int MoyFT[5];
		int NbFT[5];
		int LBT[5];
		int FMin = lstFreq.front();
		int FMax = FMin;
		i = 0;
		int j = 0;
		int iMax = lstFreq.size() - 1;
		for (j=0; j<5; j++)
		{
			FMinT[j] = 500000;
			FMaxT[j] = 0;
			MoyFT[j] = 0;
			NbFT [j] = 0;
		}
		// Recherche des FMin FMax des 5 bandes
		int iMoyF = 0;
		int iETypeF, iVarF, S1 = 0, S2 = 0;
		for (std::list<int>::iterator it=lstFreq.begin(); it != lstFreq.end(); ++it)
		{
			int iF = *it;
			// Si la fréquence n'est pas trop éloignée par rapport
			// à la moyenne et à l'écart type
			if (abs(dMoyF - iF) < dETypeF)
			{
				j = i * 5 / iMax;
				if (j > 4)
					j = 4;
				NbFT [j] += 1;
				MoyFT[j] += iF;
				if (iF < FMin)
					FMin = iF;
				if (iF > FMax)
					FMax = iF;
				if (iF < FMinT[j])
					FMinT[j] = iF;
				if (iF > FMaxT[j])
					FMaxT[j] = iF;
			}
			i++;
		}
		// Calcul largeur de bande et des moyennes des 5 parties
		for (j=0; j<5; j++)
		{
			LBT[j] = FMaxT[j] - FMinT[j];
			if (NbFT[j] == 0)
				MoyFT[j] = 0;
			else
				MoyFT[j] = MoyFT[j] / NbFT[j];
		}
		// Calcul FMin et FMax 3/5 centraux
		int FMin3_5 = 500000;
		int FMax3_5 = 0;
		for (j=1; j<4; j++)
		{
			if (FMinT[j] < FMin3_5)
				FMin3_5 = FMinT[j];
			if (FMaxT[j] > FMax3_5)
				FMax3_5 = FMaxT[j];
		}
#ifdef DEBUG_ANALYSE
		//printf("\n");
		printf("FMin globale %d FMax globale %d LB %d\n", FMin, FMax, FMax-FMin);
		printf("Bande 3/5 centrale FMin %d, FMax %d, LB %d\n", FMin3_5, FMax3_5, FMax3_5 - FMin3_5);
		for (i=0; i<5; i++)
			printf("Bande %d, FMin %d, FMax %d, LB %d, Moy %d\n", i, FMinT[i], FMaxT[i], LBT[i], MoyFT[i]);
#endif		
		// Si largeur de bande des 3/5 centraux inférieure à 2kHz et la LB des moustaches > 4kHz
		if ((FMax3_5 - FMin3_5) <= 2000 and ((FMaxT[0]-FMinT[0]) > iMaxQFC or (FMaxT[4]-FMinT[4]) > iMaxQFC))
		{
			// Si FI et FT plus petit que FME (moustache à l'endroit)
			if (currentCry.FI < currentCry.FME or currentCry.FT < currentCry.FME)
			{
				// SI FC dans la bande du grand Rhino
				if (currentCry.FME >= 76000 and currentCry.FME <= 85000)
					strcpy(currentCry.Struct, "G.RHINO");
				else
					strcpy(currentCry.Struct, "FM/FC/FM");
			}
			// Si FI et FT plus grand que FME (moustache à l'envers)
			else if (currentCry.FI > currentCry.FME and currentCry.FT > currentCry.FME)
			{
				// Si FC dans la bande "image" de l'Eurial (100/102kHz)
				if (currentCry.FME >= 90000 and currentCry.FME <= 92000)
					strcpy(currentCry.Struct, "RHINO.E");
				// Si FC dans la bande "image" du petit Rhino (107/118kHz)
				else if (currentCry.FME >= 74000 and currentCry.FME <= 85000)
					strcpy(currentCry.Struct, "P.RHINO");
				else
					strcpy(currentCry.Struct, "FM/FC/FM");
			}
			else
				strcpy(currentCry.Struct, "FM/FC/FM");
		}
		// Si largeur de bande inférieure à 4kHz
		else if ((FMax - FMin) <= iMaxQFC)
			strcpy(currentCry.Struct, "QFC");
		else
		{
			// Test si des parties de 1/5ème sont en QFC
			bool bQFCT[5];
			for (j=0; j<5; j++)
				if (LBT[j] <= iMaxQFC)
					bQFCT[j] = true;
				else
					bQFCT[j] = false;
			// Si QFC sur les 2 1er temps puis pas de QFC
			if (bQFCT[0] and bQFCT[1] and not bQFCT[2] and not bQFCT[3] and not bQFCT[4])
				strcpy(currentCry.Struct, "QFC/FM");
			// Si QFC sur le 1er temps puis pas de QFC
			else if (bQFCT[0] and not bQFCT[1] and not bQFCT[2] and not bQFCT[3] and not bQFCT[4])
				strcpy(currentCry.Struct, "QFC?/FM");
			// Si QFC sur les 2 dernier temps puis pas de QFC
			else if (not bQFCT[0] and not bQFCT[1] and not bQFCT[2] and bQFCT[3] and bQFCT[4])
				strcpy(currentCry.Struct, "FM/QFC");
			// Si QFC sur le dernier temps puis pas de QFC
			else if (not bQFCT[0] and not bQFCT[1] and not bQFCT[2] and not bQFCT[3] and bQFCT[4])
				strcpy(currentCry.Struct, "FM/QFC?");
			else
				strcpy(currentCry.Struct, "FM");
		}
	}
	else
		// Pas assez de pas, FM? par défaut
		strcpy(currentCry.Struct,     "FM?");
#ifdef DEBUG_ANALYSE
	printf("FI %d, FME %d, FT %d, NbF %d, Niveau %d\n",
			currentCry.FI, currentCry.FME, currentCry.FT, lstFreq.size(), currentCry.NivMax);
	printf("*** Fin DecodageStruct HDeb %lf, HFin %lf, Dur %f, Structure %s\n\n", 
			currentCry.TDeb, currentCry.TFin, currentCry.Dur, currentCry.Struct);
#endif		
}

//-------------------------------------------------------------------------
// Mémo d'un cri
void CAnalyser::MemoCri()
{
	// Cri valide, Décodage de la structure
	DecodageStruct();
	// Etat courant du monitoring
	MonitoringStatus MonitStatus;
	// Récupération du dernier état
	pGestMonitStatus->GetStatus( &MonitStatus);
	// Test si niveau plus fort ou si Structure différente ou si FME différente de plus de 10%
	if (   currentCry.NivMax > MonitStatus.lastCry.NivMax 
		or strcmp(currentCry.Struct, MonitStatus.lastCry.Struct) != 0
		or abs(currentCry.FME - MonitStatus.lastCry.FME) > (MonitStatus.lastCry.FME / 10))
	{
#ifdef DEBUG_ANALYSE
		printf("Mémorisation du cri");
		if (currentCry.NivMax > MonitStatus.lastCry.NivMax)
			printf(", niveau plus fort");
		if (strcmp(currentCry.Struct, MonitStatus.lastCry.Struct) != 0)
			printf(", nouvelle structure");
		if (abs(currentCry.FME - MonitStatus.lastCry.FME) > (MonitStatus.lastCry.FME / 10))
			printf(", FME différente de + de 10%");					
		printf("\n");
#endif		
		// Mémorisation du dernier cri
		pGestMonitStatus->SetLastCry( &currentCry);
		//printf("AddMesure new cry FI %d, FME %d, FT %d, NivMax %d, %s\n", lastCry.FI, lastCry.FME, lastCry.FT, lastCry.NivMax, lastCry.Struct);
		//printf("AddMesure pos  curtime %lf, currentCry TDeb %lf, TFin %lf, FI %d, FME %d, FT %d\n", curtime, currentCry.TDeb, currentCry.TFin, currentCry.FI/1000, currentCry.FME/1000, currentCry.FT/1000);
		//printf("AddMesure new cry curtime %lf, lastCry TDeb %lf, TFin %lf, FI %d, FME %d, FT %d\n", curtime, lastCry.TDeb, lastCry.TFin, lastCry.FI/1000, lastCry.FME/1000, lastCry.FT/1000);
	}
	// Mémo du cri dans la liste
	//lstCry.push_front(currentCry);
}

//-------------------------------------------------------------------------
// Test de présence d'une détection
void CAnalyser::TestDetect(
	bool bSeuil	// Indique si un niveau est au-dessus du seuil
	)
{
#ifdef DETECT_CRY
	// Si un cri en cours et d'une durée supérieure à la durée min
	if (currentCry.FME > 0 and currentCry.Dur > DUR_MIN_CRY)
	{
		// Il y a détection
		pGestMonitStatus->SetDetection( true);
		//printf("CAnalyser::TestDetect true\n");
	}	
	else
	{
		// Pas de détection
		pGestMonitStatus->SetDetection( false);
		//printf("CAnalyser::TestDetect false\n");
	}
#else
	if (bSeuil)
	{
		if (not bDetect)
		{
			// Il faut 4 FFT positives consécutives pour une détection (4x0.666=2.66ms)
			iDetectPos++;
			if (iDetectPos >= 4)
			{
				bDetect = true;
				iDetectPos = 0;
				iDetectNeg = 0;
			}
		}
		else
			// RAZ du compteur des détections négatives
			iDetectNeg = 0;			
	}
	else
	{
		if (bDetect)
		{
			// Il faut 2 FFT négatives consécutives pour stopper une détection
			iDetectNeg++;
			if (iDetectNeg >= 2)
			{
				bDetect = true;
				iDetectPos = 0;
				iDetectNeg = 0;
			}
		}
		else
			// RAZ du compteur des détections positives
			iDetectPos = 0;
	}
#endif
}

//-------------------------------------------------------------------------
// Ajout d'une mesure avec les résultats d'une FFT
void CAnalyser::AddMesure()
{
#ifdef DEBUG_ANALYSE
	printf("AddMesure iNivMoyen %ddB, iThreshold %ddB, iMaxPowerFFT %ddB, iFreqMaxFFT %dHz\n", iNivMoyen, iThreshold, iMaxPowerFFT, iFreqMaxFFT);
#endif		
	//printf("AddMesure curtime %lf\n", curtime);
	// Si niveau au-dessus du seuil
	if (iMaxPowerFFT > iNivMoyen+iThreshold and iFreqMaxFFT > 0)
	{
		// Mesure positive
#ifdef DEBUG_LEVEL
		printf("AddMesure positive curtime %lfs, iNivMoyen %ddB, iThreshold %ddB, iMaxPowerFFT %ddB, iFreqMaxFFT %dHz\n", 
				curtime, iNivMoyen, iThreshold, iMaxPowerFFT, iFreqMaxFFT);
#endif
		// Init du cri courant
		if (currentCry.FI == 0)
		{
			// Début d'un cri on met toutes les valeurs à jour
			currentCry.FI     = iFreqMaxFFT;
			currentCry.FME    = iFreqMaxFFT;
			currentCry.TDeb   = curtime;
			currentCry.NivMax = iMaxPowerFFT;
		}
		if (iMaxPowerFFT > currentCry.NivMax)
		{
			// Mémo du maximum d'énergie
			currentCry.NivMax = iMaxPowerFFT;
			currentCry.FME    = iFreqMaxFFT;
		}
		// Mémo des infos de fin
		currentCry.FT   = iFreqMaxFFT;
		currentCry.TFin = curtime;
		currentCry.Dur  = (currentCry.TFin - currentCry.TDeb) * 1000;
		lstFreq.push_back(iFreqMaxFFT);
		if (currentCry.Dur > 100.0)
		{
			// Cri de plus de 100ms ! On force la fin du cri
			MemoCri();
			// RAZ du cri courant
			RAZCurrentCry();
		}
		//printf("AddMesure pos curtime %lfs, currentCry TDeb %lfs, TFin %lfs, Dur %lfms, FI %dkHz, FME %dkHz, FT %dkHz\n", curtime, currentCry.TDeb, currentCry.TFin, currentCry.Dur, currentCry.FI/1000, currentCry.FME/1000, currentCry.FT/1000);
		// Mesure positive, test si détection
		TestDetect(true);
	}
	else
	{
#ifdef DEBUG_LEVEL
		printf("AddMesure négative, curtime %lfs, iMoyPowerFFT %d, iMaxPowerFFT %d, iFreqMaxFFT %d\n",
			curtime, iMoyPowerFFT, iMaxPowerFFT, iFreqMaxFFT);
#endif		
		// Mesure négative, test si le trou est suffisament important pour terminer le cri courant
		if (currentCry.FME > 0 and (curtime - currentCry.TFin) > dMaxTrou)
		{
			//printf("Fin d'un cri, trou de %lfms, durée %fms\n", (curtime - currentCry.TFin)*1000.0, currentCry.Dur);
			// Fin d'un cri, si sa durée est supérieure à la durée min d'un cri
			if (currentCry.Dur > DUR_MIN_CRY)
				// Mémorisation du cri
				MemoCri();
			// RAZ du cri courant
			RAZCurrentCry();
		}
		// Mesure négative, test si fin de détection
		TestDetect(false);
	}
}
	
//-------------------------------------------------------------------------
// Initialisation d'un buffer d'échantillons
void CAnalyser::SetData(
		short *pData,			// Pointeur sur les échantillons
		unsigned long length,	// Taille des échantilllons
		double timeData			// Temps des échantillons en s
		)
{
	// On prend la main sur les données
	pthread_mutex_lock (&mutexDatas);
	pGestMonitStatus->SetPertAnalyse(iNbTrames - iNbAnalyses);
#ifdef DEBUG_ANALYSE
	printf("CAnalyser::SetData NbTrames %d, NbAnalyse %d, Pertes %d\n", iNbTrames, iNbAnalyses, iNbTrames - iNbAnalyses);
#endif
	iNbTrames++;
	if (buffLengthA == 0)
	{		
		// Copie des infos dans le buffer et mémo de la taille
		memcpy(BufferA, pData, length*sizeof(short));
		buffLengthA = length;
		curtimeA = timeData;
	}
	else
	{
		// Copie des infos dans le buffer et mémo de la taille
		memcpy(BufferB, pData, length*sizeof(short));
		buffLengthB = length;
		curtimeB = timeData;
	}
	// On relache les données
	pthread_mutex_unlock(&mutexDatas);
}
	
//-------------------------------------------------------------------------
// Analyse des trames
void CAnalyser::Analyse()
{
	LogFile::AddLog(LDEBUG, "CAnalyser::Analyse debut thread\n");
	// Boucle infinie d'analyse
	while (true)
	{
		// On prend la main sur les données
		pthread_mutex_lock (&mutexDatas);
		// Test si on doit sortir
		if (not bRunAnalyse)
		{
			// On relache les données
			pthread_mutex_unlock(&mutexDatas);
			// Fin de la boucle du thread
			break;
		}
		// Test si des infos sont disponible
		short Buffer[BUFF_LENGTH];
		long lBuffLength = 0;
		if (buffLengthA > 0)
		{
			// Copie des infos dans le buffer et mémo de la taille
			memcpy(Buffer, BufferA, buffLengthA*sizeof(short));
			lBuffLength  = buffLengthA;
			curtime      = curtimeA;
			// On libère le buffer
			buffLengthA  = 0;
		}
		else if (buffLengthB > 0)
		{
			// Copie des infos dans le buffer et mémo de la taille
			memcpy(Buffer, BufferB, buffLengthB*sizeof(short));
			lBuffLength  = buffLengthB;
			curtime      = curtimeB;
			// On libère le buffer
			buffLengthB  = 0;
		}
		// On relache les données
		pthread_mutex_unlock(&mutexDatas);
		if (lBuffLength > 0)
		{
			//printf("Calcul FFT %d trames, %d analyses, curtime %lf\n", iNbTrames, iNbAnalyses, curtime);
			// Calcul du nombre de FFT pour le buffer courant
#ifdef DBL_FFT
			int nbFFT = (lBuffLength / (FFTLEN/2)) -1;
#else
			int nbFFT = lBuffLength / FFTLEN;
#endif
			// Pour chaque FFT
			unsigned long iMin = 0;
			for (int i=0; i<nbFFT; i++)
			{
				//printf("curtime %lf s\n", curtime);
#ifdef DEBUG_FFT
				printf("FFT %d, iMin %d, curtime %lf, ", i, iMin, curtime);
#endif
				// Calcul FFT
				CalculFFT(&(Buffer[iMin]));
				// On calcule le niveau moyen glissant
				if (iNivMoyen == 30)
					iNivMoyen = iMoyPowerFFT;
				else
					iNivMoyen = (iNivMoyen + iMoyPowerFFT) / 2;
#ifdef DEBUG_LEVEL
				if (iMaxPowerFFT > iNivMoyen+iThreshold)
					printf("FFT positive iNivMoyen %ddB, iMoyPowerFFT %ddB, NivMax %ddB, fMax %dHz, curtime %lfs\n",
							iNivMoyen, iMoyPowerFFT, iMaxPowerFFT, iFreqMaxFFT, curtime);
				else
					printf("FFT négative iNivMoyen %ddB, iMoyPowerFFT %ddB, NivMax %ddB, fMax %dHz, curtime %lfs\n",
							iNivMoyen, iMoyPowerFFT, iMaxPowerFFT, iFreqMaxFFT, curtime);
#endif
				// Ajout d'une mesure
				AddMesure();
				// FFT suivante
#ifdef DBL_FFT
				iMin += (FFTLEN / 2);
#else
				iMin += FFTLEN;
#endif
				curtime += fTAccuracy;
			}
			iNbAnalyses++;
			//printf("%d trames, %d analyses\n", iNbTrames, iNbAnalyses);
			// Init du bruit moyen
			pGestMonitStatus->SetNivMoy(iMoyPowerFFT);
			//printf("Niveau moyen %d dB, Max %d dB\n", iMoyPowerFFT, iMaxPowerFFT);
			// Etat courant du monitoring
			MonitoringStatus MonitStatus;
			// Récupération du dernier état
			pGestMonitStatus->GetStatus( &MonitStatus);
			// Test si le dernier cri est trop vieux
			if (MonitStatus.lastCry.FME > 0 and (curtime-MonitStatus.lastCry.TFin) > dRecMin)
			{
				//printf("RAZ lastCry curtime %lf, lastCry.TDeb %lf, lastCry.TFin %lf, dRecMin %f\n", curtime, lastCry.TDeb, lastCry.TFin, dRecMin);
				// Oui, RAZ du dernier cri
				pGestMonitStatus->RAZLastCry();
				// Absence de détection
				pGestMonitStatus->SetDetection( false);
			}
		}
		else
		{
			// Attente trames
			//printf("CAnalyser::Analyse attente trame\n");
			delay(1);
		}
			
	}
	LogFile::AddLog(LDEBUG, "CAnalyser::Analyse fin thread\n");
}

//-------------------------------------------------------------------------
// Calcul FFT
// Retourne l'indice du max de puissance
void CAnalyser::CalculFFT(
	short *pEch		// Pointeur sur les échantillons à analyser
	)
{
	// Init des données d'entrée
	for (int i=0; i<FFTLEN; i++)
	{
		dataIn[i].re = (float)(pEch[i]);
		dataIn[i].im = 0.0;
	}
	// Calcul de la FFT
	gpu_fft_execute(fft);
	// Récupération des résultats en dB pour les fréquences d'intérets
	// Seul le max compte, le reste n'est pas mémorisé sauf le niveau moyen
	int maxPower = -200;
	int moyPower = 0.0;
	int iMaxPower = -1;
	int nb = 0;
	for (int i=idFMin; i<idFMax; i++)
	{
		// Power = Log10( abs((out)) ^ 2
		//double power = pow(log10(abs((double)(dataOut[i].re))), 2);
		// Power = 20Log10( abs((out))
		//double power = (20.0 * log10(abs((double)(dataOut[i].re)))) - 120.0 + fTbCorectLevel[i];
		double dPower = (20.0 * log10(abs((double)(dataOut[i].re)))) - 120.0;
		if (dPower >= -120.0 and dPower < 0.0)
		{
			int power = (int)dPower;
			moyPower += power;
			// Mémorisation du niveau dans la table des niveaux pour le calcul du niveau d ebruit
			if (iTbNbLevels[i] == 0)
				iTbLevels[i] = power;
			else
				iTbLevels[i] += power;
			// Mémo du nombre de niveaux cumulées
			iTbNbLevels[i]++;
#define MAX_NBNOISE 100
			// Calcul du bruit sur les différents canaux
			if (iTbNbLevels[i] >= MAX_NBNOISE)
			{
				iTbNoiseLevels[i] = iTbLevels[i] / iTbNbLevels[i];
				if (iTbNoiseLevels[i] == 30)
					iTbNoiseLevels[i] = iTbLevels[i] / iTbNbLevels[i];
				else
					iTbNoiseLevels[i] = (iTbNoiseLevels[i] + (iTbLevels[i] / iTbNbLevels[i])) / 2;
				/*if (iTbNoiseLevels[i] > -40)
				{
					printf("iTbNoiseLevels[%d] = %d, Nb levels = %d, TbLevels %d\n", i, iTbNoiseLevels[i], iTbNbLevels[i], iTbLevels[i]);
				}*/
				iTbLevels[i] = 0;
				iTbNbLevels[i] = 0;
			}
			if (iTbNoiseLevels[i] < 30)
				// Correction du niveau en fonction du bruit moyen et du bruit du canal
				power = power + (iNivMoyen - iTbNoiseLevels[i]);
			if (power > maxPower)
			{
				maxPower = power;
				iMaxPower = i;
			}
			nb++;
		}
	}

	if (iMaxPower >= 0)
	{
		float fMax   = (float)iMaxPower * fFAccuracy;
		iFreqMaxFFT  = (int)fMax;
		iMaxPowerFFT = maxPower;
		iMoyPowerFFT = moyPower / nb;
#ifdef DEBUG_FFT
		printf("CalculFFT Nivmoy %ddB, NivMax %ddB, fMax %dHz\n", iMoyPowerFFT, iMaxPowerFFT, iFreqMaxFFT);
#endif
	}
	else
	{
		iFreqMaxFFT  = 0;
		iMoyPowerFFT = 0;
#ifdef DEBUG_FFT
		printf("CalculFFT Nivmoy 0\n");
#endif
	}
	/*for (int i=0; i<10; i++)
		printf("%d ", pEch[i]);
	printf("\n");*/
}

//-------------------------------------------------------------------------
// Fonction du thread de traitement
void *CAnalyser::RunAnalyse(void *threadarg)
{
	// Appel de la méthode de l'instance
	((CAnalyser *)threadarg)->Analyse();
	// Fin du thread
	pthread_exit( 0);
}

#ifdef MEMO_NOISE
//-------------------------------------------------------------------------
// Memo des niveaux moyen de chaque canaux FFT dans un fichier log
void CAnalyser::MemoLogNoise()
{
	FILE *f;
	f = NULL;
	// Création du nom du fichier de sauvegarde (/mnt/usbkey/LogNoise.csv)
	sprintf(sFilePath , "%sLogNoise.csv", pParams->GetLogPath());
	// Essai d'ouverture en lecture
	f = fopen (sFilePath, "r");
	if (f == NULL)
	{
		// Création du fichier
		f = fopen (sFilePath, "a+");
		if (f != NULL)
		{
			// Création de l'entête
			fprintf (f, "Date-Heure\tMoy\tHz\t");
			float Freq = fFAccuracy / 2;
			for (int i=0; i<FFTLEN/2; i++)
			{
				fprintf (f, "%d\t", (int)Freq);
				Freq += fFAccuracy;
			}
			fprintf (f, "\n");
			fflush  (f);
		}
		else
			LogFile::AddLog(LLOG, "LogFile::StopWavFile Echec création [%s]\n", sFilePath);
	}
	else
	{
		// Fermeture du fichier en lecture
		fclose(f);
		// Ouverture du fichier en ajout
		f = fopen (sFilePath, "a+");
	}
	// Mémo des informations dans le log
	if (f != NULL)
	{
		// Création de la date du log
		char timeStr[80];
		struct tm * timeinfo;
		time_t curtime;
		time(&curtime);
		timeinfo = localtime (&curtime);
		strftime(timeStr, 80, "%y%m%d_%H%M%S", timeinfo);
		// Ecriture de la ligne concernant le fichier wav
		fprintf (f, "%s\t%d\tdB\t", timeStr, iNivMoyen);
		for (int i=0; i<FFTLEN/2; i++)
			if (iTbNoiseLevels[i] >= 30)
				fprintf (f, "0\t");
			else
				fprintf (f, "%d\t", iTbNoiseLevels[i]);
		fprintf (f, "\n");
		fflush  (f);
		// Fermeture du fichier
		fclose(f);
	}
	else
		LogFile::AddLog(LLOG, "LogFile::StopWavFile Echec ouverture [%s]\n", sFilePath);
}
#endif