/* 
 * File:   CAnalyser.h
 * Author: Jean-Do
 *
 * Created on 22 août 2015, 16:18
 */
#include <list>
#include <pthread.h>
#include "CParameters.h"
#include "CStatus.h"
#include "GpuFFT/gpu_fft.h"

#ifndef CANALYSER_H
#define	CANALYSER_H

// Taille des buffers
#define BUFF_LENGTH 8192

// Taille de la FFT
#define FFTLEN 256
#define LOG2FFTLEN 8
//#define FFTLEN 512
//#define LOG2FFTLEN 9

//-------------------------------------------------------------------------
// Classe de gestion de l'analyse des échantillons
// Gère un thread qui exécute les FFT et analyse les résultats
// Gère une liste de cris avec la moyenne des derniers cris
class CAnalyser
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	CAnalyser(
			CParameters *pPar,			// Pointeur sur les paramètres
			CMonitStatus *pMonitStatus	// Pointeur sur gestionnaire d'état du Monitoring
			);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~CAnalyser();
	
	//-------------------------------------------------------------------------
	// Lance le thread
	void Start();
	
	//-------------------------------------------------------------------------
	// Stoppe le thread
	void Stop();
	
	//-------------------------------------------------------------------------
	// Mémo d'un cri
	void MemoCri();
	
	//-------------------------------------------------------------------------
	// RAZ du cri courant
	void RAZCurrentCry();
	
	//-------------------------------------------------------------------------
	// Décodage de la structure du dernier cri
	void DecodageStruct();
	
	//-------------------------------------------------------------------------
	// Ajout d'une mesure avec les résultats d'une FFT
	void AddMesure();
	
	//-------------------------------------------------------------------------
	// Analyse des trames
	void Analyse();
	
	//-------------------------------------------------------------------------
	// Calcul FFT
	// Retourne le niveua max, la fréquence du niveau max et le niveau moyen
	void CalculFFT(
		short *pEch		// Pointeur sur les échantillons à analyser
		);
	
	//-------------------------------------------------------------------------
	// Initialisation d'un buffer d'échantillons
	void SetData(
		short *pData,			// Pointeur sur les échantillons
		unsigned long length,	// Taille des échantilllons
		double timeData			// Temps des échantillons en s
		);
	
	//-------------------------------------------------------------------------
	// Fonction du thread de traitement
	static void *RunAnalyse(void *threadarg);
	
	//-------------------------------------------------------------------------
	// Test de présence d'une détection
	void TestDetect(
		bool bSeuil	// Indique si un niveau est au-dessus du seuil
		);
	
	//-------------------------------------------------------------------------
	// Memo des niveaux moyen de chaque canaux FFT dans un fichier log
	void MemoLogNoise();
	
	//-------------------------------------------------------------------------
	// Initialise le seuil de détection
	void SetThreshold(
		int iTh	// Seuil de détection
		);
	
private:
	//-------------------------------------------------------------------------
	// RAZ d'un cri
	void RAZCry( 
		CryParams *pCry	// CRI à mettre à 0
		);
	
	// Pointeur sur le thread
	pthread_t thread;
	// true pour une analyse en cours, false pour la stopper
	bool bRunAnalyse;
	// Pointeur sur la mémorisation des paramètres
	CParameters *pParams;
	// Mutex de controle d'accès aux infos de l'instance
	// entre le thread principal et la callback
	pthread_mutex_t mutexDatas;
	// Gestionnaire d'état du Monitoring
	CMonitStatus *pGestMonitStatus;
	// Durée min d'enregistrement (durée de garde des cris)
	float dRecMin;
	// Seuil de détection en dB au-dessus du niveau moyen (15 par défaut)
	int iThreshold;
	// Indice de la Frequence min d'interet
	int idFMin;
	// Indice de la Frequence max d'interet
	int idFMax;
	// Précision fréquentielle en Hz de la FFT
	float fFAccuracy;
	// Précision temporelle en s de la FFT
	double fTAccuracy;
	// Durée maximale d'un trou
	double dMaxTrou;
	// Largeur max d'une QFC
	int iMaxQFC;
	// Compteur de détections positives
	int iDetectPos;
	// Compteur de détections négatives
	int iDetectNeg;
	// Deux buffers permettent de traiter l'un pendant que l'autre est copié
	// Buffers de réception des échantillons
	short BufferA[BUFF_LENGTH];
	short BufferB[BUFF_LENGTH];
	// Temps du buffer d'échantillons en s
	double curtimeA;
	double curtimeB;
	double curtime;
	// Taille des données dans le buffer (0 si aucune)
	long buffLengthA;
	long buffLengthB;
	// Structures pour GPU_FFT
    struct GPU_FFT_COMPLEX *base;
    struct GPU_FFT *fft;
	GPU_FFT_COMPLEX *dataIn, *dataOut;
	int mb;
	// Résultats d'une mesure FFT
	int iFreqMaxFFT;	// Fréquence du niveau max (Hz, 0 si aucune)
	int iMaxPowerFFT;	// Niveau max de la FFT (dB)
	int iMoyPowerFFT;	// Niveau moyen de la FFT (dB)
	// Tableau des niveaux de bruit moyen glissant en dB de chaque canaux
	int iTbNoiseLevels[FFTLEN/2];
	// Tableau des niveaux cucmulés pour le calcul des niveaux de bruit glissant
	long iTbLevels[FFTLEN/2];
	// Tableau du nombre de mesures pour le calcul des niveaux de bruit glissant
	int iTbNbLevels[FFTLEN/2];
	// Tableau de correction des niveaux des canaux de FFT
	double fTbCorectLevel[FFTLEN/2];
	// Niveau moyen glissant (dB)
	int iNivMoyen;
	// Cri courant
	CryParams currentCry;
	// Liste des fréquences du cri courant
	std::list<int> lstFreq;
	// Liste des derniers cris
	//list<CryParams> lstCry;
	// Nombre de trame reçues
	int iNbTrames;
	// Nombre de trames analysées
	int iNbAnalyses;
	// Nombre de cris sur une détection
	int iNbCry;
	// Nom du fichier de sauvegarde des niveaux de bruit(/mnt/usbkey/Log_AAMMJJ_HHMMSS.csv)
	char sFilePath[80];

};

#endif	/* CANALYSER_H */

