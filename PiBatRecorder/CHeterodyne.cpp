/* 
 * File:   CHeterodyne.cpp
 * Author: Jean-Do
 * 
 * Created on 16 septembre 2015, 11:31
 */
//-------------------------------------------------------------------------
// Classe de gestion du signal hétérodyne
// En mode manuel, sur présence d'une FME, génère le signal hétérodyne


#include <cstdlib>
#include <stdio.h>
#include <math.h>
#include <climits>
#include "portaudio.h"
#include "CHeterodyne.h"

#define DEUXPI 6.2831853

// Pour enregistrer le résultat hétérodyne dans un fichier wave
//#define RECORD_WAVE

//-------------------------------------------------------------------------
// Constructeur
CHeterodyne::CHeterodyne(
	CParameters *pPar,			// Pointeur sur les paramètres
	CMonitStatus *pMonitStatus	// Pointeur sur gestionnaire d'état du Monitoring
	)
{
	// Mémorisation des paramètres
	pParams = pPar;
	pGestMonitStatus = pMonitStatus;
	// Init des variables
	bRunning   = false;
	bRecording = false;
	bWave      = true;
	// Par défaut, pas de données disponibles
	stream = NULL;
	iFreqH = 0;
	iDecH = 0;
	iNbEchA = 0;
	iNbEchB = 0;
	iFME = 0;
	ModeHeterodyne = HET_AUTO;
	TrigHeterodyne = HET_TRIGGER;
#ifdef TESTHETERODYNE
	pTestwav = NULL;
	bEndFile = false;
#endif
}

//-------------------------------------------------------------------------
// Destructeur
CHeterodyne::~CHeterodyne()
{
}

//-------------------------------------------------------------------------
// Lance l'hétérodyne en ouvrant le canal audio
void CHeterodyne::Start()
{
	LogFile::AddLog(LDEBUG, "CHeterodyne::Start\n");		
	// On prend la main sur les données
	pthread_mutex_lock (&mutexDatas);
	// Mémo des paramètres éventuellements modifiés par menu
	iDecH = pParams->GetDecHeter();
	// On initialise le niveau de lecture
	SetVolume();
	// RAZ des infos hétérodyne
	iFreqH = 0;
	iFME = 0;
	bRecording = false;
	iNbEchA = 0;
	iNbEchB = 0;
    // Ouverture du canal audio
    err = Pa_OpenDefaultStream( &stream,
                                0,				  // Pas d'enregistrement
                                1,				  // Lecture en mono
                                paInt16,		  // 16 bits
								48000,			  // 48kHz
                                CHUNK_LENGTHHET/4,// Nombre d'échantillons par callback
                                Callback,		  // Pointeur sur la callback
								this);			  // Pointeur sur l'instance de la classe
	if (err == paNoError)
		// Démarre le canal
		err = Pa_StartStream( stream );
	else
		LogFile::AddLog(LLOG, "CHeterodyne::Start Pa_OpenDefaultStream erreur [%s]\n", Pa_GetErrorText(err));	
	if (err != paNoError)
		LogFile::AddLog(LLOG, "CHeterodyne::Start Pa_StartStream erreur [%s]\n", Pa_GetErrorText(err));
	else
		bRunning = true;
	iNbTrames = 0;
	iNbAnalyses = 0;
	// On relache les données
	pthread_mutex_unlock(&mutexDatas);
}

//-------------------------------------------------------------------------
// Stoppe l'hétérodyne en fermant le canal audio
void CHeterodyne::Stop()
{
	// On prend la main sur les données
	pthread_mutex_lock (&mutexDatas);
	bRunning = false;
	// On relache les données
	pthread_mutex_unlock(&mutexDatas);
	// Arrêt du canal
	err = Pa_StopStream( stream );
	if (err == paNoError)
		// Fermeture du canal
		err = Pa_CloseStream( stream );
	else
		LogFile::AddLog(LLOG, "CHeterodyne::Stop Pa_StopStream erreur [%s]\n", Pa_GetErrorText(err));	
	if (err != paNoError)
		LogFile::AddLog(LLOG, "CHeterodyne::Stop Pa_CloseStream erreur [%s]\n", Pa_GetErrorText(err));	
	stream = NULL;
	LogFile::AddLog(LDEBUG, "CHeterodyne::Stop\n");		
}

//-------------------------------------------------------------------------
// Initialise le volume de sortie
void CHeterodyne::SetVolume()
{
	// Calcul du volume (paramètre 0-31, mixer 0-191)
	int iVol = pParams->GetVolPlay() * 191 / 31;
	if (iVol > 191)
		iVol = 191;
	// Set path gain to -6dB for safety. ie max 0.5Vrms output level. (0-191)
	// amixer -q -Dhw:sndrpiwsp cset name='HPOUT1 Digital Volume' 116
	char sCmd[160];
	// Sélection de la sortie utilisée
	switch (pParams->GetOutput())
	{
	default:
	case OUTHEADSET:
		// Volume sur la prise casque
		sprintf(sCmd, "amixer -q -Dhw:sndrpiwsp cset name='HPOUT1 Digital Volume' %d", iVol);
		LogFile::AddLog(LDEBUG, "CHeterodyne::SetVolume Headset %s\n", sCmd);
		system(sCmd);
		break;
	case OUTLINE:
		// Volume sur la sortie ligne
		sprintf(sCmd, "amixer -q -Dhw:sndrpiwsp cset name='HPOUT2 Digital Volume' %d", iVol);
		LogFile::AddLog(LDEBUG, "CHeterodyne::SetVolume Line %s\n", sCmd);
		system(sCmd);
		break;
	case OUTLS:
		// Volume sur la sortie HP
		sprintf(sCmd, "amixer -q -Dhw:sndrpiwsp cset name='Speaker Digital Volume' %d", iVol);
		LogFile::AddLog(LDEBUG, "CHeterodyne::SetVolume Headset %s\n", sCmd);
		system(sCmd);
	}
	system(sCmd);
}

//-------------------------------------------------------------------------
// Traitement des échantillons
// Attention, appel depuis un thread différent du programme principal
int CHeterodyne::TraiteEch(
	const void *inputBuffer,					// Buffer d'enregistrement
	void *outputBuffer,							// Buffer de lecture
	unsigned long framesPerBuffer,				// Taille des buffers
	const PaStreamCallbackTimeInfo* timeInfo,	// Info temporelles
	PaStreamCallbackFlags statusFlags			// Informations
	)
{
	//printf("CHeterodyne::TraiteEch\n");
	// On prend la main sur les données
	pthread_mutex_lock (&mutexDatas);
	//LogFile::AddLog(LDEBUG, "CHeterodyne::TraiteEch");
	int iReturn = paContinue;
	if (!bRunning)
	{
		LogFile::AddLog(LDEBUG, "CHeterodyne::TraiteEch paComplete\n");
		iReturn = paComplete;
	}
#ifdef TESTHETERODYNE
	// Lecture des échantillons dans le fichier wave
	printf("Avant lecture Wave %d ech\n", CHUNK_LENGTHHET);
	long lLength = pTestwav->WavfileRead(BufferEchA, CHUNK_LENGTHHET);
	iNbEchA = lLength;
	if (lLength < CHUNK_LENGTHHET)
	{
		printf("Lecture Wave erreur %d\n", lLength);
		iReturn = paComplete;
		bEndFile = true;
	}
	else
		printf("Lecture Wave %d ech\n", lLength);
#endif
	/*if (framesPerBuffer != CHUNK_LENGTHHET)
		printf("CHeterodyne::TraiteEch framesPerBuffer %ld\n", framesPerBuffer);*/
	iNbAnalyses++;
	// Calcul du signal hétérodyne
	CalculHeterodyne( (short *)outputBuffer);
#ifdef RECORD_WAVE
	// Test si on débute un enregistrement
	if (iFreqH != 0 and not bRecording)
		StartRecording();
	else if (iFreqH == 0 and bRecording)
		StopRecording();
	if (bRecording)
		wavefile.WavfileWrite( (short *)outputBuffer, framesPerBuffer);
#endif
	// On relache les données
	pthread_mutex_unlock(&mutexDatas);
	return iReturn;
}

//-------------------------------------------------------------------------
// Callback de traitement des échantillons
// Attention, appel depuis un thread différent du programme principal
int CHeterodyne::Callback(
	const void *inputBuffer,					// Buffer d'enregistrement
	void *outputBuffer,							// Buffer de lecture
	unsigned long framesPerBuffer,				// Taille des buffers
	const PaStreamCallbackTimeInfo* timeInfo,	// Info temporelles
	PaStreamCallbackFlags statusFlags,			// Informations
	void *userData								// Pointeur sur l'instance
	)
{
	// Appel de la méthode de traitement de l'instance
	return ((CHeterodyne *)userData)->TraiteEch( inputBuffer, outputBuffer,
			framesPerBuffer, timeInfo, statusFlags);
}

//-------------------------------------------------------------------------
// Modification du décalage de la fréquence hétérodyne
void CHeterodyne::SetHeterodyne()
{
	// On prend la main sur les données
	pthread_mutex_lock (&mutexDatas);
	// Test de la fréquence hétérodyne
	if (iFreqH != 0)
	{
		LogFile::AddLog(LDEBUG, "CHeterodyne::SetHeterodyne iFreqH : %d\n", iFreqH);
		// Préparation des infos de génération de la porteuse
		double dAmplitude = 32767.0 * 0.95;
		FrequencePorteuse = (double)iFreqH / 192000.0;
		ej2PiFoTe.Re  = cos( DEUXPI * FrequencePorteuse );
		ej2PiFoTe.Im  = sin( DEUXPI * FrequencePorteuse );
		ej2PiFoNTe.Re = cos( 0.0 ) * dAmplitude ;
		ej2PiFoNTe.Im = sin( 0.0 ) * dAmplitude ;
	}
	else
		LogFile::AddLog(LDEBUG, "CHeterodyne::SetHeterodyne iFreqH = 0\n");
	// On relache les données
	pthread_mutex_unlock(&mutexDatas);
}

//-------------------------------------------------------------------------
// Préparation de l'hétérodyne
void CHeterodyne::PrepareHeterodyne(
	int ifme,			// Fréquence (en Hz) FME pour le calcul de la fréquence hétérodyne
						// Ou fréquence de l'hétérodyne en mode manuel
	int ModeFonct,		// Mode fonctionnement de l'hétérodyne
	int TrigH			// Trigger de lancement de l'hétérodyne
	)
{
	// On prend la main sur les données
	pthread_mutex_lock (&mutexDatas);
	//LogFile::AddLog(LDEBUG, "CHeterodyne::PrepareHeterodyne ModeFonct: %d, TrigH: %d, ifme : %d\n", ModeFonct, TrigH, ifme);
	ModeHeterodyne = ModeFonct;
	TrigHeterodyne = TrigH;
	// Test du mode de fonctionnement de l'hétérodyne
	if (ModeHeterodyne == HET_SANS)
		// Pas d'hétérodyne
		iFreqH = 0;
	else if (ModeHeterodyne == HET_MANU)
		// Hétérodyne manuel, mémo de la fréquence
		iFreqH = ifme;
	else
	{
		// Mode automatique, Test FME
		if (ifme == 0)
		{
			// Pas de FME, donc pas d'hétérodyne
			iFME = 0;
			iFreqH = 0;
		}
		else
		{
			// Mémo de la nouvelle FME
			iFME = ifme;
			iDecH = pParams->GetDecHeter();
			iFreqH = ifme + iDecH;
		}
	}
	// On relache les données
	pthread_mutex_unlock(&mutexDatas);
	// Préparation de l'hétérodyne
	SetHeterodyne();
}

//-------------------------------------------------------------------------
// Mémorisation du buffer des échantillons reçus pour le calcul de l'hétérodyne
void CHeterodyne::SetEchantillons(
	short *pEchs,	// Buffer des échantillons
	int iLength		// Nombre d'échantillons
	)
{
	// On prend la main sur les données
	pthread_mutex_lock (&mutexDatas);
	pGestMonitStatus->SetPertHeterodyne(iNbTrames - iNbAnalyses);
	iNbTrames++;
	if (iNbEchA == 0)
	{
		// Mémorisation des échantillons
		iNbEchA = iLength;
		memcpy(BufferEchA, pEchs, iLength*sizeof(short));
	}
	else
	{
		// Mémorisation des échantillons
		iNbEchB = iLength;
		memcpy(BufferEchB, pEchs, iLength*sizeof(short));
	}
	// On relache les données
	pthread_mutex_unlock(&mutexDatas);
}
	
//-------------------------------------------------------------------------
// Génère le buffer de lecture du signal hétérodyne
void CHeterodyne::CalculHeterodyne(
	short *pOut // Buffer à initialiser pour la lecture
	)
{
	// Récupération des infos de la porteuse
	double Cos2PiFoTe = ej2PiFoTe.Re;
	double Sin2PiFoTe = ej2PiFoTe.Im;
	double CosTranspo = ej2PiFoNTe.Re;
	double SinTranspo = ej2PiFoNTe.Im;
	double VarTemp, VarTemp2;
	short sEch;
	short *pBuffer = NULL;
	int iNbEch;
	if (iNbEchA > 0)
	{
		pBuffer = BufferEchA;
		iNbEch  = iNbEchA;
		iNbEchA = 0;
	}
	else if (iNbEchB > 0)
	{
		pBuffer = BufferEchB;
		iNbEch  = iNbEchB;
		iNbEchB = 0;
	}
	if (iFreqH == 0 or pBuffer == NULL)
	{
		//LogFile::AddLog(LDEBUG, "CHeterodyne::CalculHeterodyne 0 %d", iFreqH);
		// Pas de fréquence hétérodyne, on met les échantillons à 0
		for (int i=0; i<CHUNK_LENGTHHET/4; i++)
			pOut[i] = 0;
	}
	else
	{
		//LogFile::AddLog(LDEBUG, "CHeterodyne::CalculHeterodyne %d", iFreqH);
		// Indices des échantillons pour décimation de 192kHz à 48kHz
		int j = 0;
		// Pour l'ensemble des échantillons, calcul hétérodyne
		for (int i=0; i<iNbEch; i++)
		{
			// Transposition de l'échantillon
			//Signal.Re = pBuffer[i];
			//Signal.Im = 0.0;
			VarTemp = pBuffer[i] * CosTranspo;// - Signal.Im * SinTranspo;
			//VarTemp   = Signal.Re * CosTranspo;// - Signal.Im * SinTranspo;
			//Signal.Im = Signal.Re * SinTranspo + Signal.Im * CosTranspo;

			// Calcul de la transpo suivante : A.ej2PiFoNTe = A.ej2PiFo(N-1)Te * ej2PiFoTe */
			VarTemp2   = Cos2PiFoTe * CosTranspo - Sin2PiFoTe * SinTranspo;
			SinTranspo = Cos2PiFoTe * SinTranspo + Sin2PiFoTe * CosTranspo;
			CosTranspo = VarTemp2;
			
			// Normalisation du résultat en short
			sEch = (short)(VarTemp / SHRT_MAX);
			
			// Décimation 192kHz vers 48kHz, prise d'un ech sur 4
			if (i % 4 == 0)
				// Mémorisation dans le buffer de lecture
				pOut[j++] = sEch;
		}
	}
	/* sauvegarde des valeurs a l'instant N pour calcul à N+1*/
	ej2PiFoNTe.Re = CosTranspo;
	ej2PiFoNTe.Im = SinTranspo;
}

//-------------------------------------------------------------------------
// Démarre un enregistrement
// Attention, appel depuis un thread différent du programme principal
void CHeterodyne::StartRecording()
{
	if (bWave)
	{
		char fileName[80];
		char filePath[160];
		char timeStr[80];
		char sPrefWav[80];
		struct tm * timeinfo;
		time_t curtime;
		time(&curtime);
		timeinfo = localtime (&curtime);
		strftime(timeStr, 80, "%y%m%d_%H%M%S", timeinfo);
		// Préfixe des fichier wav
		strcpy( sPrefWav, pParams->GetWaveName());
		// On passe en mode Record
		bRecording = true;
		// Préparation du nom du fichier wav
		sprintf(filePath, "%s%sH_%s.wav", pParams->GetWavPath(), sPrefWav, timeStr);
		sprintf(fileName, "%s_%s.wav", sPrefWav, timeStr);
		// Initialisation du fichier wav
		wavefile.OpenWaveFileForWrite(filePath, false);
	}
}

//-------------------------------------------------------------------------
// Stoppe un enregistrement
// Attention, appel depuis un thread différent du programme principal
void CHeterodyne::StopRecording()
{
	if (bWave)
		// Fermeture du fichier
		wavefile.CloseWavfile();
	bRecording = false;
}

#ifdef TESTHETERODYNE
//-------------------------------------------------------------------------
// Init du pointeur sur le wavefile à traiter
void CHeterodyne::SetWaveFile( 
	CWaveFile *testwav	// Pointeur sur le fichier à lire
	)
{
	pTestwav = testwav;
}
#endif
