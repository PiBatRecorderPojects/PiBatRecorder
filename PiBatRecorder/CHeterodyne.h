/* 
 * File:   CHeterodyne.h
 * Author: Jean-Do
 *
 * Created on 16 septembre 2015, 11:31
 */
#include <pthread.h>
#include "CStatus.h"
#include "portaudio.h"
#include "CWaveFile.h"
#include "CParameters.h"

#ifndef CHETERODYNE_H
#define	CHETERODYNE_H

// Taille des buffers
#define CHUNK_LENGTHHET 8192

//-------------------------------------------------------------------------
// Classe de gestion du signal hétérodyne
// En mode manuel, sur présence d'une FME, génère le signal hétérodyne
class CHeterodyne
{
public:
	//-------------------------------------------------------------------------
	// Constructeur
	CHeterodyne(
		CParameters *pPar,			// Pointeur sur les paramètres
		CMonitStatus *pMonitStatus	// Pointeur sur gestionnaire d'état du Monitoring
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~CHeterodyne();
	
	//-------------------------------------------------------------------------
	// Lance l'hétérodyne en ouvrant le canal audio
	void Start();
	
	//-------------------------------------------------------------------------
	// Stoppe l'hétérodyne en fermant le canal audio
	void Stop();
	
	//-------------------------------------------------------------------------
	// Traitement des échantillons
	// Attention, appel depuis un thread différent du programme principal
	int TraiteEch(
		const void *inputBuffer,					// Buffer d'enregistrement
		void *outputBuffer,							// Buffer de lecture
		unsigned long framesPerBuffer,				// Taille des buffers
		const PaStreamCallbackTimeInfo* timeInfo,	// Info temporelles
		PaStreamCallbackFlags statusFlags			// Informations
		);

	//-------------------------------------------------------------------------
	// Callback de traitement des échantillons
	// Attention, appel depuis un thread différent du programme principal
	static int Callback(
		const void *inputBuffer,					// Buffer d'enregistrement
		void *outputBuffer,							// Buffer de lecture
		unsigned long framesPerBuffer,				// Taille des buffers
		const PaStreamCallbackTimeInfo* timeInfo,	// Info temporelles
		PaStreamCallbackFlags statusFlags,			// Informations
		void *userData								// Pointeur sur l'instance
		);

	//-------------------------------------------------------------------------
	// Préparation de l'hétérodyne
	void PrepareHeterodyne(
		int ifme,	// Fréquence FME pour le calcul de la fréquence hétérodyne
		int ModeFonct=HET_AUTO,		// Mode fonctionnement de l'hétérodyne
		int TrigH=HET_TRIGGER		// Trigger de lancement de l'hétérodyne
		);
	
	//-------------------------------------------------------------------------
	// Modification du décalage de la fréquence hétérodyne
	void SetHeterodyne();

	//-------------------------------------------------------------------------
	// Mémorisation du buffer des échantillons reçus pour le calcul de l'hétérodyne
	void SetEchantillons(
		short *pEchs,	// Buffer des échantillons
		int iLength		// Nombre d'échantillons
		);
	
	//-------------------------------------------------------------------------
	// Génère le buffer de lecture du signal hétérodyne
	void CalculHeterodyne( 
		short *pOut // Buffer à initialiser pour la lecture
		);
	
	//-------------------------------------------------------------------------
	// Initialise le volume de sortie
	void SetVolume();
	
	//-------------------------------------------------------------------------
	// Démarre un enregistrement
	// Attention, appel depuis un thread différent du programme principal
	void StartRecording();

	//-------------------------------------------------------------------------
	// Stoppe un enregistrement
	// Attention, appel depuis un thread différent du programme principal
	void StopRecording();

	//-------------------------------------------------------------------------
	// Retourne true si l'hétérodyne est actif
	bool IsRunning() {return bRunning;};
	
#ifdef TESTHETERODYNE
	//-------------------------------------------------------------------------
	// Init du pointeur sur le wavefile à traiter
	void SetWaveFile( 
		CWaveFile *testwav	// Pointeur sur le fichier à lire
		);

	//-------------------------------------------------------------------------
	// Indique si fin du fichier wave
	bool IsEndFile() {return bEndFile;};
#endif
	
private:
	// Pointeur sur la mémorisation des paramètres
	CParameters *pParams;
	// Pointeur sur le canal Audio
    PaStream *stream;
	// Erreur éventuelle
    PaError err;
	// Indique que l'hétérodyne est en cours
	bool bRunning;
	// Mutex de controle d'accès aux infos de l'instance
	// entre le thread principal et la callback
	pthread_mutex_t mutexDatas;
	// Gestionnaire du fichiers wav en enregistrement éventuel
	CWaveFile wavefile;
	// Inidique qu'un enregistrement wav est en cours
	bool bRecording;
	// Indique que l'enregistrement wav de l'hétérodyne est demandé
	bool bWave;
	// Décalage en Hz de la fréquence hétérodyne
	int iDecH;
	// Fréquence FME
	int iFME;
	// Fréquence hétérodyne en Hz
	int iFreqH;
	// Mode de fonctionnement de l'hétérodyne
	int ModeHeterodyne;
	// Mode de lancement de l'hétérodyne
	int TrigHeterodyne;
	// Buffer des échantillons reçus
	short BufferEchA[CHUNK_LENGTHHET];
	short BufferEchB[CHUNK_LENGTHHET];
	// Nombre d'échantillons reçus
	int iNbEchA;
	int iNbEchB;
	// Nombre de trame reçues
	int iNbTrames;
	// Nombre de trames traitées
	int iNbAnalyses;
	// Gestionnaire d'état du Monitoring
	CMonitStatus *pGestMonitStatus;
	// Définition d'un complexe pour le calcul des échantillons de la fréquence hétérodyne
	struct complexe
	{
		double Re;
		double Im;
	};
	complexe ej2PiFoTe;
	complexe ej2PiFoNTe;
	complexe Signal;
	double   FrequencePorteuse;

#ifdef TESTHETERODYNE
	// Pointeur sur le fichier à lire
	CWaveFile *pTestwav;
	
	// Indicateur de fin de fichier
	bool bEndFile;
#endif

};

#endif	/* CHETERODYNE_H */

