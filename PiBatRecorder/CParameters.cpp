/* 
 * File:   CParameters.cpp
 * Author: Jean-Do
 * 
 * Created on 20 août 2015, 11:25
 */
//-------------------------------------------------------------------------
// Classe de gestion des paramètres du PiBatRecorder
// Gère les paramètres en mémoire et dans un fichier

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include "CParameters.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
CParameters::CParameters()
{
	//printf("CParameters::CParameters\n");
	// Par défaut, on est pas en mode auto
	bAuto = false;
	// Nom du fichier de sauvegarde des paramètres (/home/pi/PiBatRecorder/PiBatParams.txt)
	strcpy(sFilePathParams, "/home/pi/PiBatRecorder/PiBatParams.txt");
	// Nom du fichier de sauvegarde de la config (/home/pi/PiBatRecorder/PiBatConfig.txt)
	strcpy(sFilePathConfig, "/home/pi/PiBatRecorder/PiBatConfig.txt");
	// Temps min d'enregistrement d'un fichier wav (0 à 3s, 2s par défaut)
	fDRecMin = 2.0;
	// Temps max d'enregistrement d'un fichier wav (0 à 99s, 30s par défaut)
	fDRecMax = 30.0;
	// Prefixe des noms des fichiers wav (PiBat par défaut)
	strcpy(sWaveName, "PiBat");
	// Niveau du volume d'enregistrement (0 a 31, 20 par défaut)
	iNivRec = 20;
	// Type d'enregistrement (WAV ou ACT, WAV par défaut)
	eRecType = WAV;
	// Heure de debut d'enregistrement du mode AUTO (22:00 par défaut)
	strcpy(sHDeb, "22:00");
	// Heure de fin d'enregistrement du mode AUTO (06:00 par défaut)
	strcpy(sHFin, "06:00");
	// Seuil de détection en dB au-dessus du niveau moyen (40 par défaut)
	iThreshold = 40;
	// Frequence min d'interet en Hz (18000 par défaut)
	iFMin = 18000;
	// Frequence max d'interet en Hz (96000 par défaut)
	iFMax = 96000;
	// Mode d'enregistrement (AUTO ou MANU, MANU par défaut)
	eRecMode = MANU;
	// Langue d'affichage (Français par défaut)
	eLangue = FR;
	// Décalage hétérodyne en Hz (1kHz par défaut)
	iDecHeter = 1000;
	// Volume d'écoute (0 a 31, 20 par défaut)
	iVolPlay = 20;
	// Type de micro
	iMikeType = HEADSET;
	// Mode de fonctionnement de l'hétérodyne
	iModeHeter = HET_AUTO;
	// Mode de déclenchement de l'hétérodyne
	iTrigHeter = HET_TRIGGER;
	// Sortie Audio
	iOutput = OUTHEADSET;
	// Type de carte audio
	iAudioCard = WOLFSON;
	// Type de clavier
	iKeyboard = TTP229I2C16KEYS;
	// Type d'écran
	iScreen = OLEDSH1106I2C128x64;
	// Type d'horloge
	iRTC = RTCI2C;
	// Path des commandes sh
	strcpy(WolfsonShPath, "/home/pi/");
	// Répertoire racine de sauvegarde des fichiers (racine de la clé USB, /mnt/usbkey par défaut)
	strcpy(USBKeyFilePath, "/mnt/usbkey/");
	// Répertoire racine de sauvegarde locale des fichiers si le 1er est absent (/home/pi/save par défaut)
	strcpy(LocalFilepath, "/home/pi/pibat");
	// Répertoire de sauvegarde des fichiers wav (vide par défaut)
	strcpy(WavDir, "");
	// Répertoire de sauvegarde des fichiers log (vide par défaut)
	strcpy(LogDir, "");
	// Paramètres spécifiques de PiBatScheduler
	// Latitude du site (47° par défaut)
	fLatitude = 47.0;
	// Longitude du site (0° par défaut)
	fLongitude = 0.0;
	// Mode de démarrage de l'enregistrement
	ScheduleType = SUN;
	// Décalage de début et de fin d'enregistrement par rapport au lever/coucher du soleil
	strcpy(sHDec, "+01:30");
	// Type de shutdown
	eShutDown = SD_REBOOT;
	// GMT
	iGMT = 1;
	// Avec gestion heure d'été
	bSummerTime = true;
	// Ligne de commande du lancement de l'enregistreur
	strcpy( sRecorderCmd, "/home/pi/PiBatRecorder/pibatrecorder -a &");
	
	//printf("CParameters::ReadParams\n");		
	// Lecture des paramètres
	ReadParams();
	//printf("CParameters::ReadParams OK\n");		
}

CParameters::~CParameters()
{
	// En mode auto, on n'écrit pas les paramètres, le fichier est géré par un logiciel externe
	if (!bAuto)
		// Ecriture des paramètres
		WriteParams();
}

//-------------------------------------------------------------------------
// Décodage d'une ligne d'un paramètre
void CParameters::DecodeParams(
	char *sLine	// Ligne à décoder
	)
{
// Si ligne de commentaire
	if (sLine[0] == '/' and sLine[1] == '/')
	{
		// On ne décode pas les commentaires
		//printf ("Commentaire=%s", sLine);
		return;
	}
	// Recherche des paramètres
	char temp[255];
	char *pStr;
// Threshold=40					Seuil de détection en dB au-dessus du niveau moyen de bruit (10 à 99, 40 par défaut)
	pStr = strstr(sLine, "Threshold=");
	if (pStr != NULL)
	{
		sscanf (sLine, "Threshold=%d\n", &iThreshold);
		LogFile::AddLog(LINFO, "Threshold=%d\n",  iThreshold);
		return;
	}
// MinFreqOfInterest=18000		Frequence min d'interet en Hz (10000 à 96000, 18000 par défaut)
	pStr = strstr(sLine, "MinFreqOfInterest=");
	if (pStr != NULL)
	{
		sscanf (sLine, "MinFreqOfInterest=%d\n", &iFMin);
		LogFile::AddLog(LINFO, "MinFreqOfInterest=%d\n",  iFMin);
		return;
	}
// MaxFreqOfInterest=96000		Frequence max d'interet en Hz (10000 à 96000, 18000 par défaut)
	pStr = strstr(sLine, "MaxFreqOfInterest=");
	if (pStr != NULL)
	{
		sscanf (sLine, "MaxFreqOfInterest=%d\n", &iFMax);
		LogFile::AddLog(LINFO, "MaxFreqOfInterest=%d\n",  iFMax);
		return;
	}
// RecordGain=20					Gain de l'enregistreur (0 à 31 dB, 20 par défaut)
	pStr = strstr(sLine, "RecordGain=");
	if (pStr != NULL)
	{
		sscanf (sLine, "RecordGain=%d\n", &iNivRec);
		LogFile::AddLog(LINFO, "RecordGain=%d\n",  iNivRec);
		return;
	}
// MinRecordTime=2				Temps min d'enregistrement d'un fichier wav (0 à 3s, 2s par défaut)
	pStr = strstr(sLine, "MinRecordTime=");
	if (pStr != NULL)
	{
		sscanf (sLine, "MinRecordTime=%f\n", &fDRecMin);
		LogFile::AddLog(LINFO, "MinRecordTime=%f\n",  fDRecMin);
		return;
	}
// MaxRecordTime=30				Temps max d'enregistrement d'un fichier wav (0 à 99s, 30s par défaut)
	pStr = strstr(sLine, "MaxRecordTime=");
	if (pStr != NULL)
	{
		sscanf (sLine, "MaxRecordTime=%f\n", &fDRecMax);
		LogFile::AddLog(LINFO, "MaxRecordTime=%f\n",  fDRecMax);
		return;
	}
// WavPrefix=PiBat				Prefixe des noms des fichiers wav ("PiBat" par défaut)
	pStr = strstr(sLine, "WavPrefix=");
	if (pStr != NULL)
	{
		sscanf (sLine, "WavPrefix=%s\n", &sWaveName);
		LogFile::AddLog(LINFO, "WavPrefix=%s\n",  sWaveName);
		return;
	}
// RecordType=WAV					Type d'enregistrement (WAV, LOG ou WAV+LOG, WAV par défaut)
	pStr = strstr(sLine, "RecordType=");
	if (pStr != NULL)
	{
		sscanf (sLine, "RecordType=%s\n",  temp);
		if (strstr(temp, "WAV+LOG") != NULL)
			eRecType = WAVLOG;
		else if (strstr(temp, "WAV") != NULL)
			eRecType = WAV;
		else
			eRecType = WAV;
		switch (eRecType)
		{
		case WAVLOG:
			LogFile::AddLog(LINFO, "RecordType=WAVLOG\n");
			break;
		default:
			LogFile::AddLog(LINFO, "RecordType=WAV\n");
		}
		return;
	}
// RecordMode=MANUAL				Mode d'enregistrement (MANUAL ou AUTO, MANUAL par défaut)
	pStr = strstr(sLine, "RecordMode=");
	if (pStr != NULL)
	{
		sscanf (sLine, "RecordMode=%s\n",  temp);
		if (strstr(temp, "AUTO") != NULL)
			eRecMode = AUTO;
		else
			eRecMode = MANU;
		switch (eRecMode)
		{
		case AUTO:
			LogFile::AddLog(LINFO, "RecordMode=AUTO\n");
			break;
		default:
			LogFile::AddLog(LINFO, "RecordMode=MANU\n");
		}
		return;
	}
// BeginRecordTime=22:00			Heure de debut d'enregistrement du mode AUTO (22:00 par défaut)
	pStr = strstr(sLine, "BeginRecordTime=");
	if (pStr != NULL)
	{
		sscanf (sLine, "BeginRecordTime=%s\n",  sHDeb);
		LogFile::AddLog(LINFO, "BeginRecordTime=%s\n",  sHDeb);
		return;
	}
// EndRecordTime=06:00			Heure de fin d'enregistrement du mode AUTO (06:00 par défaut)
	pStr = strstr(sLine, "EndRecordTime=");
	if (pStr != NULL)
	{
		sscanf (sLine, "EndRecordTime=%s\n",  sHFin);
		LogFile::AddLog(LINFO, "EndRecordTime=%s\n",  sHFin);
		return;
	}
// Output=HEADSET					Sortie du lecteur (HEADSET, LINE ou LOUDSPEAKER, HEADSET par défaut)
	pStr = strstr(sLine, "Output=");
	if (pStr != NULL)
	{
		sscanf (sLine, "Output=%s\n",  temp);
		if (strstr(temp, "LOUDSPEAKER") != NULL)
			iOutput = OUTLS;
		if (strstr(temp, "LINE") != NULL)
			iOutput = OUTLINE;
		else
			iOutput = OUTHEADSET;
		switch (iOutput)
		{
		case OUTLS:
			LogFile::AddLog(LINFO, "Output=LOUDSPEAKER\n");
			break;
		case OUTLINE:
			LogFile::AddLog(LINFO, "Output=LINE\n");
			break;
		default:
			LogFile::AddLog(LINFO, "Output=HEADSET\n");
		}
		return;
	}
// Volume=20						Volume de sortie (0 à 31 dB, 20 par défaut)
	pStr = strstr(sLine, "Volume=");
	if (pStr != NULL)
	{
		sscanf (sLine, "Volume=%d\n", &iVolPlay);
		LogFile::AddLog(LINFO, "Volume=%d\n",  iVolPlay);
		return;
	}
// HeterodyneShift=1000			Décalage hétérodyne en Hz (10 à 2000, 1000 par défaut)
	pStr = strstr(sLine, "HeterodyneShift=");
	if (pStr != NULL)
	{
		sscanf (sLine, "HeterodyneShift=%d\n", &iDecHeter);
		LogFile::AddLog(LINFO, "HeterodyneShift=%d\n",  iDecHeter);
		return;
	}
// HeterodyneMode=AUTO			Mode fonctionnement de l'hétérodyne (AUTO, MANUAL, NO, AUTO par défaut)
	pStr = strstr(sLine, "HeterodyneMode=");
	if (pStr != NULL)
	{
		sscanf (sLine, "HeterodyneMode=%s\n",  temp);
		if (strstr(temp, "NO") != NULL)
			iModeHeter = HET_SANS;
		else if (strstr(temp, "MANUAL") != NULL)
			iModeHeter = HET_MANU;
		else
			iModeHeter = HET_AUTO;
		switch (iModeHeter)
		{
		case HET_SANS:
			LogFile::AddLog(LINFO, "HeterodyneMode=NO\n");
			break;
		case HET_MANU:
			LogFile::AddLog(LINFO, "HeterodyneMode=MANUAL\n");
			break;
		default:
			LogFile::AddLog(LINFO, "HeterodyneMode=AUTO\n");
		}
		return;
	}
// HeterodyneTrig=TRIG			Mode de déclenchement de l'hétérodyne (TRIG, CONTINUE, TRIG par défaut)
	pStr = strstr(sLine, "HeterodyneTrig=");
	if (pStr != NULL)
	{
		sscanf (sLine, "HeterodyneTrig=%s\n",  temp);
		if (strstr(temp, "CONTINUE") != NULL)
			iTrigHeter = HET_CONTINUE;
		else
			iTrigHeter = HET_TRIGGER;
		switch (iTrigHeter)
		{
		case HET_CONTINUE:
			LogFile::AddLog(LINFO, "HeterodyneTrig=CONTINUE\n");
			break;
		default:
			LogFile::AddLog(LINFO, "HeterodyneTrig=TRIGGER\n");
		}
		return;
	}
// Language=FR					Langage d'affichage (FR ou GB, FR par défaut)
	pStr = strstr(sLine, "Language=");
	if (pStr != NULL)
	{
		sscanf (sLine, "Language=%s\n",  temp);
		if (strstr(temp, "GB") != NULL)
			eLangue = EN;
		else
			eLangue = FR;
		switch (eLangue)
		{
		case EN:
			LogFile::AddLog(LINFO, "Language=EN\n");
			break;
		default:
			LogFile::AddLog(LINFO, "Language=FR\n");
		}
		return;
	}
// Audio=WOLFSON					Carte audio Cirrus Logic Wolfson
	pStr = strstr(sLine, "Audio=");
	if (pStr != NULL)
	{
		sscanf (sLine, "Audio=%s\n",  temp);
		if (strstr(temp, "WOLFSON") != NULL)
			iAudioCard = WOLFSON;
		else
			iAudioCard = WOLFSON;
		switch (iAudioCard)
		{
		case WOLFSON:
			LogFile::AddLog(LINFO, "Audio=WOLFSON\n");
			break;
		default:
			LogFile::AddLog(LINFO, "Audio=WOLFSON\n");
		}
		return;
	}
// Keyboard=TTP229I2C16KEYS		Type de clavier, sensitif 16 touches TTP229
//         =NOKEYBOARD			Pas de clavier
	pStr = strstr(sLine, "Keyboard=");
	if (pStr != NULL)
	{
		sscanf (sLine, "Keyboard=%s\n",  temp);
		if (strstr(temp, "TTP229I2C16KEYS") != NULL)
			iKeyboard = TTP229I2C16KEYS;
		else
			iKeyboard = NOKEYBOARD;
		switch (iKeyboard)
		{
		case TTP229I2C16KEYS:
			LogFile::AddLog(LINFO, "Keyboard=TTP229I2C16KEYS\n");
			break;
		default:
			LogFile::AddLog(LINFO, "Keyboard=NOKEYBOARD\n");
		}
		return;
	}
// Screen=OLEDSH1106I2C128x64	Ecran OLED 128x64 pixels I2C avec controleur SH1106
//       =OLEDSSD1306I2C128x64	Ecran OLED 128x64 pixels I2C avec controleur SSD1306
//       =NOSCREEN				Pas d'écran
	pStr = strstr(sLine, "Screen=");
	if (pStr != NULL)
	{
		sscanf (sLine, "Screen=%s\n",  temp);
		if (strstr(temp, "OLEDSH1106I2C128x64") != NULL)
			iScreen = OLEDSH1106I2C128x64;
		else if (strstr(temp, "OLEDSSD1306I2C128x64") != NULL)
			iScreen = OLEDSSD1306I2C128x64;
		else
			iScreen = NOSCREEN;
		switch (iScreen)
		{
		case OLEDSH1106I2C128x64:
			LogFile::AddLog(LINFO, "Screen=TTP229I2C16KEYS\n");
			break;
		case OLEDSSD1306I2C128x64:
			LogFile::AddLog(LINFO, "Screen=TTP229I2C16KEYS\n");
			break;
		default:
			LogFile::AddLog(LINFO, "Screen=NOSCREEN\n");
		}
		return;
	}
// MikeInput=HEADSET			Micro mono sur entrée casque
//          =LINEMONO			Micro mono sur entrée ligne
//          =LINESTEREO			Micros stéréo sur entrée ligne
	pStr = strstr(sLine, "MikeInput=");
	if (pStr != NULL)
	{
		sscanf (sLine, "MikeInput=%s\n",  temp);
		if (strstr(temp, "LINESTEREO") != NULL)
			iMikeType = LINE_STEREO;
		else if (strstr(temp, "LINEMONO") != NULL)
			iMikeType = LINE_MONO;
		else
			iMikeType = HEADSET;
		switch (iMikeType)
		{
		case LINE_STEREO:
			LogFile::AddLog(LINFO, "MikeInput=LINE_STEREO\n");
			break;
		case LINE_MONO:
			LogFile::AddLog(LINFO, "MikeInput=LINE_MONO\n");
			break;
		default:
			LogFile::AddLog(LINFO, "MikeInput=HEADSET\n");
		}
		return;
	}
// RTC=RTCI2C					Horloge Temps réel de type I2C
//    =WEB						Horloge via Internet
//    =NORTC					Pas d'horloge
	pStr = strstr(sLine, "RTC=");
	if (pStr != NULL)
	{
		sscanf (sLine, "RTC=%s\n",  temp);
		if (strstr(temp, "NORTC") != NULL)
			iRTC = NORTC;
		else if (strstr(temp, "WEB") != NULL)
			iRTC = HWEB;
		else
			iRTC = RTCI2C;
		switch (iRTC)
		{
		case NORTC:
			LogFile::AddLog(LINFO, "RTC=NORTC\n");
			break;
		case HWEB:
			LogFile::AddLog(LINFO, "RTC=HWEB\n");
			break;
		default:
			LogFile::AddLog(LINFO, "RTC=RTCI2C\n");
		}
		return;
	}
// WolfsonShPath=/home/pi			Répertoire des commandes sh de la carte Wolson (/home/pi par défaut)
	pStr = strstr(sLine, "WolfsonShPath=");
	if (pStr != NULL)
	{
		sscanf (sLine, "WolfsonShPath=%s\n",  WolfsonShPath);
		// On ajoute éventuellement / en fin du path
		if (strlen(WolfsonShPath) > 0 and WolfsonShPath[strlen(WolfsonShPath)-1] != '/')
			strcat( WolfsonShPath, "/");
		LogFile::AddLog(LINFO, "WolfsonShPath=%s\n",  WolfsonShPath);
		return;
	}
// USBKeyFilePath=/mnt/usbkey/	Répertoire racine de sauvegarde des fichiers sur clé USB (/mnt/usbkey par défaut)
	pStr = strstr(sLine, "USBKeyFilePath=");
	if (pStr != NULL)
	{
		sscanf (sLine, "USBKeyFilePath=%s\n",  USBKeyFilePath);
		// On ajoute éventuellement / en fin du path
		if (strlen(USBKeyFilePath) > 0 and USBKeyFilePath[strlen(USBKeyFilePath)-1] != '/')
			strcat( USBKeyFilePath, "/");
		LogFile::AddLog(LINFO, "USBKeyFilePath=%s\n",  USBKeyFilePath);
		return;
	}
// LocalFilepath=/home/pi/pibat/	Répertoire racine de sauvegarde des fichiers sur la carte SD (/home/pi/pibat par défaut)
	pStr = strstr(sLine, "LocalFilepath=");
	if (pStr != NULL)
	{
		sscanf (sLine, "LocalFilepath=%s\n",  LocalFilepath);
		// On ajoute éventuellement / en fin du path
		if (strlen(LocalFilepath) > 0 and LocalFilepath[strlen(LocalFilepath)-1] != '/')
			strcat( LocalFilepath, "/");
		LogFile::AddLog(LINFO, "LocalFilepath=%s\n",  LocalFilepath);
		return;
	}
// WavDir=wav/					Répertoire de sauvegarde des fichiers wav (vide par défaut)
	pStr = strstr(sLine, "WavDir=");
	if (pStr != NULL)
	{
		sscanf (sLine, "WavDir=%s\n",  WavDir);
		// On ajoute éventuellement / en fin du path
		if (strlen(WavDir) > 0 and WavDir[strlen(WavDir)-1] != '/')
			strcat( WavDir, "/");
		LogFile::AddLog(LINFO, "WavDir=%s\n",  WavDir);
		return;
	}
// LogDir=log/                    Répertoire de sauvegarde des fichiers log (vide par défaut)
	pStr = strstr(sLine, "LogDir=");
	if (pStr != NULL)
	{
		sscanf (sLine, "LogDir=%s\n",  LogDir);
		// On ajoute éventuellement / en fin du path
		if (strlen(LogDir) > 0 and LogDir[strlen(LogDir)-1] != '/')
			strcat( LogDir, "/");
		LogFile::AddLog(LINFO, "LogDir=%s\n",  LogDir);
		return;
	}
	// Paramètres spécifiques de PiBatScheduler
// Latitude=45.0					Latitude du site (47° par défaut)
	pStr = strstr(sLine, "Latitude=");
	if (pStr != NULL)
	{
		sscanf (sLine, "Latitude=%f\n", &fLatitude);
		LogFile::AddLog(LINFO, "Latitude=%f\n",  fLatitude);
		return;
	}
// Longitude=47.0					Longitude du site (0° par défaut)
	pStr = strstr(sLine, "Longitude=");
	if (pStr != NULL)
	{
		sscanf (sLine, "Longitude=%f\n", &fLongitude);
		LogFile::AddLog(LINFO, "Longitude=%f\n",  fLongitude);
		return;
	}
// ScheduleType=SUN				Mode de démarrage de l'enregistrement (HOUR ou SUN, SUN par défaut)
	pStr = strstr(sLine, "ScheduleType=");
	if (pStr != NULL)
	{
		sscanf (sLine, "ScheduleType=%s\n",  temp);
		if (strstr(temp, "HOUR") != NULL)
			ScheduleType = HOUR;
		else
			ScheduleType = SUN;
		switch (ScheduleType)
		{
		case HOUR:
			LogFile::AddLog(LINFO, "ScheduleType=HOUR\n");
			break;
		default:
			LogFile::AddLog(LINFO, "ScheduleType=SUN\n");
		}
		return;
	}
// HourShift=+00:00				Décalage de début et de fin d'enregistrement par rapport au lever/coucher du soleil (+00:00 par défaut)
	pStr = strstr(sLine, "HourShift=");
	if (pStr != NULL)
	{
		LogFile::AddLog(LINFO, "HourShift=%s\n",  sHDec);
		LogFile::AddLog(LINFO, "HourShift=%s\n",  sHDec);
		return;
	}
// ShutDown=NO					Gestion de l'extinction avec NO, pas de shutdown
//         =AUTO				Shutdown automatique effectué de façon externe
	pStr = strstr(sLine, "ShutDown=");
	if (pStr != NULL)
	{
		sscanf (sLine, "ShutDown=%s\n",  temp);
		if (strstr(temp, "AUTO") != NULL)
			eShutDown = SD_AUTO;
		else
			eShutDown = SD_REBOOT;
		switch (eShutDown)
		{
		case SD_AUTO:
			LogFile::AddLog(LINFO, "ShutDown=AUTO\n");
			break;
		default:
			LogFile::AddLog(LINFO, "ShutDown=NO\n");
		}
		return;
	}
// TimeZone=1						Fuseau horaire (GMT+1 par défaut)
	pStr = strstr(sLine, "TimeZone=");
	if (pStr != NULL)
	{
		sscanf (sLine, "TimeZone=%d\n", &iGMT);
		LogFile::AddLog(LINFO, "TimeZone=%d\n",  iGMT);
		return;
	}
// SummerTime=Yes					Avec gestion heure d'été (Yes/No, Yes par défaut)
	pStr = strstr(sLine, "SummerTime=");
	if (pStr != NULL)
	{
		sscanf (sLine, "SummerTime=%s\n",  temp);
		if (strstr(temp, "Yes") != NULL)
		{
			bSummerTime = true;
			LogFile::AddLog(LINFO, "SummerTime=Yes\n");
		}
		else
		{
			bSummerTime = false;
			LogFile::AddLog(LINFO, "SummerTime=No\n");
		}
		return;
	}
// Recorder=PiBatRecorder -a		Ligne de commande de lancement du logiciel d'enregistrement (le répertoire est forcément /home/pi/PiBatRecorder)
	pStr = strstr(sLine, "Recorder=");
	if (pStr != NULL)
	{
		char arg[80];
		arg[0] = 0;;
		sscanf (sLine, "Recorder=%s %s\n",  temp, arg);
		strcpy( sRecorderCmd, "/home/pi/PiBatRecorder/");
		strcat( sRecorderCmd, temp);
		if (strlen(arg) > 0)
		{
			strcat( sRecorderCmd, " ");
			strcat( sRecorderCmd, arg);
		}
		LogFile::AddLog(LINFO, "Recorder=%s\n", sRecorderCmd);
		return;
	}
// LogLevel=LOG					  Niveau du fichier log de debug (/home/pi/PiBatRecorder/PiBatLog.txt) Niveau minimal LOG par défaut (infos de démarrage et erreurs)
//         =INFO				  Niveau avec des informations suplémentaires (paramètres de fonctionnement)
//         =DEBUG                 Niveau pour debugger un problème (attention, ce nievau peut avoir des conséquence sur le fonctionnement temps réel)
//         =REALTIME              Niveau pour débugger un problème temps réel mais les traces peuvent changer le comportement du logiciel
	pStr = strstr(sLine, "LogLevel=");
	if (pStr != NULL)
	{
		sscanf (sLine, "LogLevel=%s\n",  temp);
		if (strstr(temp, "INFO") != NULL)
			LogFile::LogLevel = LINFO;
		else if (strstr(temp, "DEBUG") != NULL)
			LogFile::LogLevel = LDEBUG;
		else if (strstr(temp, "REALTIME") != NULL)
			LogFile::LogLevel = LREALTIME;
		else
			LogFile::LogLevel = LLOG;
		switch (LogFile::LogLevel)
		{
		case LINFO:
			LogFile::AddLog(LINFO, "LogLevel=INFO\n");
			break;
		case LDEBUG:
			LogFile::AddLog(LINFO, "LogLevel=DEBUG\n");
			break;
		case LREALTIME:
			LogFile::AddLog(LINFO, "LogLevel=REALTIME\n");
			break;
		default:
			LogFile::AddLog(LINFO, "LogLevel=LOG\n");
		}
		return;
	}
// LogTerminal=NO				Pour sortir les traces du Log aussi sur la console (NO par défaut)
//			  =YES				Les traces de log sont écrites dans le fichier PiBatLog.txt ET sur la console
	pStr = strstr(sLine, "LogTerminal=");
	if (pStr != NULL)
	{
		sscanf (sLine, "LogTerminal=%s\n",  temp);
		if (strstr(temp, "YES") != NULL)
			LogFile::bConsole = true;
		else
			LogFile::bConsole = false;
		switch (LogFile::bConsole)
		{
		case true:
			LogFile::AddLog(LINFO, "LogTerminal=YES\n");
			break;
		default:
			LogFile::AddLog(LINFO, "LogTerminal=NO\n");
		}
		return;
	}
}

//-------------------------------------------------------------------------
// Lecture des paramètres depuis le fichier de sauvegarde
void CParameters::ReadParams()
{
	// Ligne de lecture
	char sLine[255];
	// Ouverture du fichier de configuration
	FILE *fc = fopen (sFilePathConfig, "r");
	if (fc != NULL)
	{
		LogFile::AddLog(LINFO, "CParameters::ReadParams lecture fichier [%s]\n", sFilePathConfig);
		// Lecture et décodage des différentes lignes
		while (fgets(sLine, 255, fc) != NULL)
		{
			//printf("DecodeParams(%s)\n", sLine);
			DecodeParams(sLine);
		}
		// Fermeture du fichier
		fclose(fc);
		LogFile::AddLog(LINFO, "Fin de lecture du fichier de configuration");		
	}
	else
	{
		LogFile::AddLog(LINFO, "CParameters::ReadParams Echec ouverture [%s]\n", sFilePathConfig);
	}
	// Ouverture du fichier des paramètres
	FILE *fp = fopen (sFilePathParams, "r");
	if (fp != NULL)
	{
		LogFile::AddLog(LINFO, "CParameters::ReadParams lecture fichier [%s]\n", sFilePathParams);
		// Lecture et décodage des différentes lignes
		while (fgets(sLine, 255, fp) != NULL)
		{
			//printf("DecodeParams(%s)\n", sLine);
			DecodeParams(sLine);
		}
		// Fermeture du fichier
		fclose(fp);
		LogFile::AddLog(LINFO, "Fin de lecture du fichier des paramètres");
	}
	else
	{
		LogFile::AddLog(LINFO,  "CParameters::ReadParams Echec ouverture [%s]\n", sFilePathParams);
	}
}

//-------------------------------------------------------------------------
// Enregistrement des paramètres dans le fichier de sauvegarde
void CParameters::WriteParams()
{
	// Ouverture du fichier de sauvegarde des paramètres
	FILE *f = fopen (sFilePathParams, "w+");
	if (f != NULL)
	{
		// Ecriture des paramètres
		LogFile::AddLog(LINFO, "CParameters::WriteParams écriture fichier [%s]\n", sFilePathParams);
// Threshold=40					Seuil de détection en dB au-dessus du niveau moyen de bruit (10 à 99, 40 par défaut)
		fprintf (f, "Threshold=%d\n", iThreshold);
		LogFile::AddLog(LINFO, "Threshold=%d\n", iThreshold);
// MinFreqOfInterest=18000		Frequence min d'interet en Hz (10000 à 96000, 18000 par défaut)
		fprintf (f, "MinFreqOfInterest=%d\n", iFMin);
		LogFile::AddLog(LINFO, "MinFreqOfInterest=%d\n", iFMin);
// MaxFreqOfInterest=96000		Frequence max d'interet en Hz (10000 à 96000, 18000 par défaut)
		fprintf (f, "MaxFreqOfInterest=%d\n", iFMax);
		LogFile::AddLog(LINFO, "MaxFreqOfInterest=%d\n", iFMax);
// RecordGain=20					Gain de l'enregistreur (0 à 31 dB, 20 par défaut)
		fprintf (f, "RecordGain=%d\n", iNivRec);
		LogFile::AddLog(LINFO, "RecordGain=%d\n", iNivRec);
// MinRecordTime=2				Temps min d'enregistrement d'un fichier wav (0 à 3s, 2s par défaut)
		fprintf (f, "MinRecordTime=%f\n", fDRecMin);
		LogFile::AddLog(LINFO, "MinRecordTime=%f\n", fDRecMin);
// MaxRecordTime=30				Temps max d'enregistrement d'un fichier wav (0 à 99s, 30s par défaut)
		fprintf (f, "MaxRecordTime=%f\n", fDRecMax);
		LogFile::AddLog(LINFO, "MaxRecordTime=%f\n", fDRecMax);
// WavPrefix=PiBat				Prefixe des noms des fichiers wav ("PiBat" par défaut)
		fprintf (f, "WavPrefix=%s\n", sWaveName);
		LogFile::AddLog(LINFO, "WavPrefix=%s\n", sWaveName);
// RecordType=WAV					Type d'enregistrement (WAV, LOG ou WAV+LOG, WAV par défaut)
		switch (eRecType)
		{
		case WAV:
			fprintf (f, "RecordType=WAV\n");
			LogFile::AddLog(LINFO, "RecordType=WAV\n");
			break;
		case LOGC:
			fprintf (f, "RecordType=LOG\n");
			LogFile::AddLog(LINFO, "RecordType=LOG\n");
			break;
		case WAVLOG:
		default:
			fprintf (f, "RecordType=WAV+LOG\n");
			LogFile::AddLog(LINFO, "RecordType=WAV+LOG\n");
		}
// RecordMode=MANUAL				Mode d'enregistrement (MANUAL ou AUTO, MANUAL par défaut)
		switch (eRecMode)
		{
		case MANU:
			fprintf (f, "RecordMode=MANUAL\n");
			LogFile::AddLog(LINFO, "RecordMode=MANUAL\n");
			break;
		case AUTO:
		default:
			fprintf (f, "RecordMode=AUTO\n");
			LogFile::AddLog(LINFO, "RecordMode=AUTO\n");
		}
// BeginRecordTime=22:00			Heure de debut d'enregistrement du mode AUTO (22:00 par défaut)
		fprintf (f, "BeginRecordTime=%s\n", sHDeb);
		LogFile::AddLog(LINFO, "BeginRecordTime=%s\n", sHDeb);
// EndRecordTime=06:00			Heure de fin d'enregistrement du mode AUTO (06:00 par défaut)
		fprintf (f, "EndRecordTime=%s\n", sHFin);
		LogFile::AddLog(LINFO, "EndRecordTime=%s\n", sHFin);
// Output=HEADSET					Sortie du lecteur (HEADSET ou LINE, HEADSET par défaut)
		switch (iOutput)
		{
		case OUTHEADSET:
			fprintf (f, "Output=HEADSET\n");
			LogFile::AddLog(LINFO, "Output=HEADSET\n");
			break;
		case OUTLINE:
		default:
			fprintf (f, "Output=LINE\n");
			LogFile::AddLog(LINFO, "Output=LINE\n");
		}
// Volume=20						Volume de sortie (0 à 31 dB, 20 par défaut)
		fprintf (f, "Volume=%d\n", iVolPlay);
		LogFile::AddLog(LINFO, "Volume=%d\n", iVolPlay);
// HeterodyneShift=1000			Décalage hétérodyne en Hz (10 à 2000, 1000 par défaut)
		fprintf (f, "HeterodyneShift=%d\n", iDecHeter);
		LogFile::AddLog(LINFO, "HeterodyneShift=%d\n", iDecHeter);
// HeterodyneMode=AUTO			Mode fonctionnement de l'hétérodyne (AUTO, MANUAL, NO, AUTO par défaut)
		switch (iModeHeter)
		{
		case HET_SANS:
			fprintf (f, "HeterodyneMode=NO\n");
			LogFile::AddLog(LINFO, "HeterodyneMode=NO\n");
			break;
		case HET_MANU:
			fprintf (f, "HeterodyneMode=MANUAL\n");
			LogFile::AddLog(LINFO, "HeterodyneMode=MANUAL\n");
			break;
		case HET_AUTO:
		default:
			fprintf (f, "HeterodyneMode=AUTO\n");
			LogFile::AddLog(LINFO, "HeterodyneMode=AUTO\n");
		}
// HeterodyneTrig=TRIG			Mode de déclenchement de l'hétérodyne (TRIG, CONTINUE, TRIG par défaut)
		switch (iTrigHeter)
		{
		case HET_CONTINUE:
			fprintf (f, "HeterodyneTrig=CONTINUE\n");
			LogFile::AddLog(LINFO, "HeterodyneTrig=CONTINUE\n");
			break;
		case HET_TRIGGER:
		default:
			fprintf (f, "HeterodyneTrig=TRIG\n");
			LogFile::AddLog(LINFO, "HeterodyneTrig=TRIG\n");
		}
// Language=FR					Langage d'affichage (FR ou GB, FR par défaut)
		switch (eLangue)
		{
		case FR:
			fprintf (f, "Language=FR\n");
			LogFile::AddLog(LINFO, "Language=FR\n");
			break;
		case EN:
		default:
			fprintf (f, "Language=GB\n");
			LogFile::AddLog(LINFO, "Language=GB\n");
		}
	// Paramètres spécifiques de PiBatScheduler
// Latitude=45.0					Latitude du site (47° par défaut)
		fprintf (f, "Latitude=%f\n", fLatitude);
		LogFile::AddLog(LINFO, "Latitude=%f\n", fLatitude);
// Longitude=47.0					Longitude du site (0° par défaut)
		fprintf (f, "Longitude=%f\n", fLongitude);
		LogFile::AddLog(LINFO, "Longitude=%f\n", fLongitude);
// ScheduleType=SUN				Mode de démarrage de l'enregistrement (HOUR ou SUN, SUN par défaut)
		switch (ScheduleType)
		{
		case HOUR:
			fprintf (f, "ScheduleType=HOUR\n");
			LogFile::AddLog(LINFO, "ScheduleType=HOUR\n");
			break;
		case SUN:
		default:
			fprintf (f, "ScheduleType=SUN\n");
			LogFile::AddLog(LINFO, "ScheduleType=SUN\n");
		}
// HourShift=+00:00				Décalage de début et de fin d'enregistrement par rapport au lever/coucher du soleil (+00:00 par défaut)
		fprintf (f, "HourShift=%s\n", sHDec);
		LogFile::AddLog(LINFO, "HourShift=%s\n", sHDec);
// TimeZone=1						Fuseau horaire (GMT+1 par défaut)
		fprintf (f, "TimeZone=%d\n", iGMT);
		LogFile::AddLog(LINFO, "TimeZone=%d\n", iGMT);
// SummerTime=Yes					Avec gestion heure d'été (Yes/No, Yes par défaut)
		if (bSummerTime)
		{
			fprintf (f, "SummerTime=Yes\n");
			LogFile::AddLog(LINFO, "SummerTime=Yes\n");
		}		
		else
		{
			fprintf (f, "SummerTime=No\n");
			LogFile::AddLog(LINFO, "SummerTime=No\n");
		}		
		// Fermeture du fichier
		fclose(f);
	}
	else
		LogFile::AddLog(LINFO, "CParameters::WriteParams Echec ouverture [%s]\n", sFilePathParams);
}

//-------------------------------------------------------------------------
// Initialise les paths de sauvegarde en fonction de la présence ou non de la clé
void CParameters::SetSavePaths()
{
	if (isUSBKey())
	{
		// Clé USB présente, on initialise les chemins en conséquence
		strcpy( WavPath, USBKeyFilePath);
		strcat( WavPath, WavDir);
		strcpy( LogPath, USBKeyFilePath);
		strcat( LogPath, LogDir);
	}
	else
	{
		// Clé USB absente, on initialise les chemins en conséquence
		// Vérification de la présence du répertoire local
		DIR * rep = opendir(LocalFilepath); 
		if (rep == NULL)
		{
			// Création du répertoire
			char sCmd[255];
			sprintf(sCmd, "mkdir %s", LocalFilepath);
			system(sCmd);
		}
		strcpy( WavPath, LocalFilepath);
		strcat( WavPath, WavDir);
		strcpy( LogPath, LocalFilepath);
		strcat( LogPath, LogDir);
	}
	LogFile::AddLog(LINFO, "Répertoire de sauvegarde des wav [%s]\n", WavPath);
	LogFile::AddLog(LINFO, "Répertoire de sauvegarde des log [%s]\n", LogPath);
}

//-------------------------------------------------------------------------
// Test de présence de la clé USB
// Retourne True si la clé USB est montée
bool CParameters::isUSBKey()
{
	//printf("isUSBKey [%s] = ", GetUSBKeyFilePath());
	bool bOK = false;
	// Commande df pour savoir ou est monté /mnt/usbkey
	char sCmd[255];
	sprintf(sCmd, "sudo df %s > /home/pi/testusbkey.txt", GetUSBKeyFilePath());
	system(sCmd);
	// Ligne de lecture
	char sLine[255];
	// Ouverture du fichier de résultat
	FILE *f = fopen ("/home/pi/testusbkey.txt", "r");
	if (f != NULL)
	{
		// Vérification présence /dev/sda1
		while (fgets(sLine, 255, f) != NULL)
		{
			if (strstr(sLine, "/dev/sda1") != NULL)
			{
				//printf("true\n");
				bOK = true;
				break;
			}
		}
		// Fermeture du fichier
		fclose(f);
		system("sudo rm /home/pi/testusbkey.txt");
		/*if (!bOK)
			printf("false\n");*/
	}
	else
	{
		// Fichier testusbkey.txt introuvable
		printf("fichier testusbkey.txt introuvable !\n");
	}
	return bOK;
}

//-------------------------------------------------------------------------
// Monte la clé USB
// Retourne True si la clé USB est montée
bool CParameters::MountUSBKey()
{
	bool bOK = true;
	// Si pas déjà montée
	if (not isUSBKey())
	{
		//printf("MountUSBKey [%s]\n", USBKeyFilePath);
		char temp[255];
		sprintf( temp, "sudo mount -o uid=pi,gid=pi /dev/sda1 %s", USBKeyFilePath);
		system(temp);
		if (!isUSBKey())
		{
			LogFile::AddLog(LINFO, "CParameters::MountUSBKey clé [%s] absente !\n", USBKeyFilePath),
			bOK = false;
		}
		/*else
			printf("CParameters::MountUSBKey clé [%s] montée\n", USBKeyFilePath);*/
	}
	return bOK;
}

//-------------------------------------------------------------------------
// Démonte la clé USB
// Retourne True si la clé USB est démontée
bool CParameters::UMountUSBKey()
{
	bool bOK = true;
	// Si clé USB présente
	if (isUSBKey())
	{
		//printf("UMountUSBKey [%s]\n", USBKeyFilePath);
		char temp[255];
		sprintf( temp, "sudo umount %s", USBKeyFilePath);
		system(temp);
		if (isUSBKey())
		{
			LogFile::AddLog(LINFO, "CParameters::MountUSBKey clé [%s] présente !\n", USBKeyFilePath),
			bOK = false;
		}
		/*else
			printf("CParameters::MountUSBKey clé [%s] démontée\n", USBKeyFilePath);*/
	}
	return bOK;
}

