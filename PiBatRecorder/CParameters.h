/* 
 * File:   CParameters.h
 * Author: Jean-Do
 *
 * Created on 20 août 2015, 11:25
 */
#include <string.h>
#include "LogFile.h"

#ifndef CPARAMETERS_H
#define	CPARAMETERS_H

// Liste de sparamètres gérés :
/*
 -- Paramètres du fichier PiBatParams.txt modifiable par l'opérateur via les menus de PiBatRecorder --
								++ Trigger ++
 Threshold=40					Seuil de détection en dB au-dessus du niveau moyen de bruit (10 à 99, 40 par défaut)
 MinFreqOfInterest=18000		Frequence min d'interet en Hz (10000 à 96000, 18000 par défaut)
 MaxFreqOfInterest=96000		Frequence max d'interet en Hz (10000 à 96000, 18000 par défaut)
 
								++ Record ++
 RecordGain=20					Gain de l'enregistreur (0 à 31 dB, 20 par défaut)
 MinRecordTime=2				Temps min d'enregistrement d'un fichier wav (0 à 3s, 2s par défaut)
 MaxRecordTime=30				Temps max d'enregistrement d'un fichier wav (0 à 99s, 30s par défaut)
 WavPrefix=PiBat				Prefixe des noms des fichiers wav ("PiBat" par défaut)
 RecordType=WAV					Type d'enregistrement (WAV, LOG ou WAV+LOG, WAV par défaut)
 RecordMode=MANUAL				Mode d'enregistrement (MANUAL ou AUTO, MANUAL par défaut)
 BeginRecordTime=22:00			Heure de debut d'enregistrement du mode AUTO (22:00 par défaut)
 EndRecordTime=06:00			Heure de fin d'enregistrement du mode AUTO (06:00 par défaut)
 
								++ Lecture/Hétérodyne ++
 Output=HEADSET					Sortie du lecteur (HEADSET, LINE ou LOUDSPEAKER, HEADSET par défaut)
 Volume=20						Volume de sortie (0 à 31 dB, 20 par défaut)
 HeterodyneShift=1000			Décalage hétérodyne en Hz (10 à 2000, 1000 par défaut)
 HeterodyneMode=AUTO			Mode fonctionnement de l'hétérodyne (AUTO, MANUAL, NO, AUTO par défaut)
 HeterodyneTrig=TRIG			Mode de déclenchement de l'hétérodyne (TRIG, CONTINUE, TRIG par défaut)
 	
								++ Paramètres spécifiques de PiBatScheduler ++
 Latitude=45.0					Latitude du site (47° par défaut)
 Longitude=47.0					Longitude du site (0° par défaut)
 ScheduleType=SUN				Mode de démarrage de l'enregistrement (HOUR ou SUN, SUN par défaut)
 HourShift=+00:00				Décalage de début et de fin d'enregistrement par rapport au lever/coucher du soleil (+00:00 par défaut)
 TimeZone=1						Fuseau horaire (GMT+1 par défaut)
 SummerTime=Yes					Avec gestion heure d'été (Yes/No, Yes par défaut)

								++ Affichage ++
 Language=FR					Langage d'affichage (FR ou GB, FR par défaut)
 
 -- Paramètres du fichier PiBatConfig.txt de la configuration matérielle à modifier lors de la création d'une configuration matérielle --
 LogLevel=LOG					Niveau du fichier log de debug (/home/pi/PiBatRecorder/PiBatLog.txt) Niveau minimal LOG par défaut (infos de démarrage et erreurs)
         =INFO					Niveau avec des informations suplémentaires (paramètres de fonctionnement)
         =DEBUG                 Niveau pour debugger un problème (attention, ce nievau peut avoir des conséquence sur le fonctionnement temps réel)
         =REALTIME              Niveau pour débugger un problème temps réel mais les traces peuvent changer le comportement du logiciel
 LogTerminal=NO					Pour sortir les traces du Log aussi sur la console (NO par défaut)
			=YES				Les traces de log sont écrites dans le fichier PiBatLog.txt ET sur la console
 Audio=WOLFSON					Carte audio Cirrus Logic Wolfson
 Keyboard=TTP229I2C16KEYS		Type de clavier, sensitif 16 touches TTP229
         =NOKEYBOARD			Pas de clavier
 Screen=OLEDSH1106I2C128x64		Ecran OLED 128x64 pixels I2C avec controleur SH1106
       =OLEDSSD1306I2C128x64	Ecran OLED 128x64 pixels I2C avec controleur SSD1306
       =NOSCREEN				Pas d'écran
 MikeInput=HEADSET				Micro mono sur entrée casque
          =LINEMONO				Micro mono sur entrée ligne
          =LINESTEREO			Micros stéréo sur entrée ligne
 RTC=RTCI2C						Horloge Temps réel de type I2C
    =WEB						Horloge via Internet
    =NORTC						Pas d'horloge
 WolfsonShPath=/home/pi/		Répertoire des commandes sh de la carte Wolson (/home/pi/ par défaut)
 USBKeyFilePath=/mnt/usbkey/	Répertoire racine de sauvegarde des fichiers sur clé USB (/mnt/usbkey par défaut)
 LocalFilepath=/home/pi/pibat/	Répertoire racine de sauvegarde des fichiers sur la carte SD (/home/pi/pibat par défaut)
 WavDir=wav/					Répertoire de sauvegarde des fichiers wav (vide par défaut)
 LogDir=log/                    Répertoire de sauvegarde des fichiers log (vide par défaut)
     ******** Paramètres spécifiques PiBatScheduler
 ShutDown=REBOOT				Gestion de l'extinction avec REBOOT, reboot journalier effectué en fin de période d'enregistrement
         =AUTO					Shutdown et réveil automatique effectué de façon externe, halt effectué en fin de période d'enregistrement
 Recorder=pibatrecorder	-a &	Ligne de commande de lancement du logiciel d'enregistrement (le répertoire est forcément /home/pi/PiBatRecorder)
*/
//-------------------------------------------------------------------------
// Type d'enregistrement 
enum TYPEREC{
	WAV		= 1,	// Enregistrement de fichiers wav
	LOGC	= 2,	// Enregistrement de fichiers CSV avec le log des cris
	ACTV	= 4,	// Enregistrement des activités des espèces détectées
	WAVLOG  = 3,	// Enregistrement Wav et Log
	ACTLOG	= 6,	// Enregistrement Activités et Log
	WAVACT	= 5,	// Enregistrement Wav et Activités
	ALL		= 7,	// Enregistrement des 3 types d'information
};


//-------------------------------------------------------------------------
// Mode d'enregistrement 
enum MODEREC{
	MANU = 1,	// Enregistrement manuel
	AUTO = 2	// Enregistrement Auto
};

//-------------------------------------------------------------------------
// Langue d'affichage
enum LANGUE{
	FR = 1,		// Français
	EN = 2		// Anglais
};

//-------------------------------------------------------------------------
// Sortie audio
enum AUDIOOUTPUT{
	OUTHEADSET = 1,	// Sortie casque
	OUTLINE = 2,	// Sortie ligne
	OUTLS = 3		// Sortie HP
};

//-------------------------------------------------------------------------
// Type d'entrée du micro
enum MIKEINPUT{
	HEADSET = 1,	// Entrée casque mono
	LINE_MONO = 2,	// Entrée ligne mono
	LINE_STEREO = 3	// Entrée ligne stéréo
};

//-------------------------------------------------------------------------
// Modes de fonctionnement de l'hétérodyne
enum MODE_HETERODYNE {
	HET_SANS,		// Pas d'hétérodyne
	HET_MANU,		// Hétérodyne manuel
	HET_AUTO		// Hétérodyne auto
};

//-------------------------------------------------------------------------
// Mode de déclenchement de l'hétérodyne
enum TRIG_HETERODYNE {
	HET_CONTINUE,		// Hétérodyne toujours actif
	HET_TRIGGER			// Hétérodyne fonctionne sur trigger uniquement
};

//-------------------------------------------------------------------------
// Type de carte audio
enum AUDIOCARD {
	WOLFSON		// Carte audio Cirrus Logic Wolfson
};

//-------------------------------------------------------------------------
// Type de clavier
enum KEYBOARDTYPE {
	TTP229I2C16KEYS,	// Clavier sensitif 16 touches TTP229
	NOKEYBOARD			// Pas de clavier
};

//-------------------------------------------------------------------------
// Type d'écran
enum SCREENTYPE {
	OLEDSH1106I2C128x64,	// Ecran OLED 128x64 pixels I2C avec controleur SH1106
	OLEDSSD1306I2C128x64,	// Ecran OLED 128x64 pixels I2C avec controleur SSD1306
	NOSCREEN				// Pas d'écran
};

//-------------------------------------------------------------------------
// Type d'horloge
enum RTCTYPE {
	RTCI2C,	// Horloge I2C
	HWEB,	// Horloge via Internet
	NORTC	// Pas d'horloge
};

//-------------------------------------------------------------------------
// Type de démarrage de l'enregistrement par PiBatScheduler
enum SCHEDULETYPE {
	HOUR,	// Utilisation de l'heure de début et de fin
	SUN		// Utilisation du coucher et lever du soleil
};

//-------------------------------------------------------------------------
// Type de Gestion de l'extinction
enum SHUTDOWNTYPE {
	SD_REBOOT,	// Reboot journalier effectué en fin de période d'enregistrement
    SD_AUTO		// Shutdown et réveil automatique effectué de façon externe, halt effectué en fin de période d'enregistrement
};

//-------------------------------------------------------------------------
// Classe de gestion des paramètres du PiBatRecorder
// Gère les paramètres en mémoire et dans un fichier
class CParameters
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	CParameters();

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~CParameters();

	//-------------------------------------------------------------------------
	// Retourne le Nom du fichier de sauvegarde des paramètres
	char *GetFilePathParams() { return sFilePathParams;};
	
	//-------------------------------------------------------------------------
	// Initialise le Nom du fichier de sauvegarde des paramètres
	void SetFilePathParams(char *pathName) { strcpy(sFilePathParams, pathName);};
	
	//-------------------------------------------------------------------------
	// Retourne le Nom du fichier de sauvegarde de la configuration
	char *GetFilePathConfig() { return sFilePathConfig;};
	
	//-------------------------------------------------------------------------
	// Initialise le Nom du fichier de sauvegarde de la configuration
	void SetFilePathConfig(char *pathName) { strcpy(sFilePathConfig, pathName);};
	
	//-------------------------------------------------------------------------
	// Retourne le temps min d'enregistrement d'un fichier wav
	float GetDRecMin() { return fDRecMin;};
	
	//-------------------------------------------------------------------------
	// Initialise le temps min d'enregistrement d'un fichier wav
	void SetDRecMin(float dRecMin) { fDRecMin=dRecMin;};
	
	//-------------------------------------------------------------------------
	// Retourne le temps max d'enregistrement d'un fichier wav
	float GetDRecMax() {return fDRecMax;};
	
	//-------------------------------------------------------------------------
	// Initialise le temps max d'enregistrement d'un fichier wav
	void SetDRecMax(float dRecMax) {fDRecMax=dRecMax;};
	
	//-------------------------------------------------------------------------
	// Retourne le prefixe des noms des fichiers wav (PIBAT par défaut)
	char *GetWaveName() {return sWaveName;};
	
	//-------------------------------------------------------------------------
	// Initialise le prefixe des noms des fichiers wav (PIBAT par défaut)
	void SetWaveName( char *WaveName) { strcpy(sWaveName, WaveName);};
	
	//-------------------------------------------------------------------------
	// Retourne le niveau du volume d'enregistrement
	int GetNivRec() { return iNivRec;};
	
	//-------------------------------------------------------------------------
	// Initialise le niveau du volume d'enregistrement
	void SetNivRec( int NivRec) {iNivRec=NivRec;};
	
	//-------------------------------------------------------------------------
	// Retourne le type d'enregistrement
	TYPEREC GetRecType() { return eRecType;};
	
	//-------------------------------------------------------------------------
	// Initialise le type d'enregistrement
	void SetRecType(TYPEREC RecType) { eRecType=RecType;};
	
	//-------------------------------------------------------------------------
	// Retourne l'heure de début d'enregistrement du mode AUTO
	char *GetHDeb() { return sHDeb;};
	
	//-------------------------------------------------------------------------
	// Initialise l'heure de début d'enregistrement du mode AUTO
	void SetHDeb(char *HDeb) { strcpy(sHDeb, HDeb);};
	
	//-------------------------------------------------------------------------
	// Retourne l'heure de fin d'enregistrement du mode AUTO
	char *GetHFin() { return sHFin;};
	
	//-------------------------------------------------------------------------
	// Initialise l'heure de fin d'enregistrement du mode AUTO
	void SetHFin(char *HFin) { strcpy(sHFin, HFin);};
	
	//-------------------------------------------------------------------------
	// Retourne le seuil de détection en dB au-dessus du niveau moyen
	int GetThreshold() { return iThreshold;};
	
	//-------------------------------------------------------------------------
	// Initialise le seuil de détection en dB au-dessus du niveau moyen
	void SetThreshold(int Threshold) { iThreshold=Threshold;};
	
	//-------------------------------------------------------------------------
	// Retourne la frequence min d'interet en Hz
	int GetFMin() { return iFMin;};
	
	//-------------------------------------------------------------------------
	// Initialise la frequence min d'interet en Hz
	void SetFMin( int FMin) { iFMin=FMin;};
	
	//-------------------------------------------------------------------------
	// Retourne la frequence max d'interet en Hz
	int GetFMax() { return iFMax;};
	
	//-------------------------------------------------------------------------
	// Initialise la frequence max d'interet en Hz
	void SetFMax( int FMax) { iFMax=FMax;};
	
	//-------------------------------------------------------------------------
	// Retourne le mode d'enregistrement
	MODEREC GetRecMode() { return eRecMode;};

	//-------------------------------------------------------------------------
	// Initialise le mode d'enregistrement
	void SetRecMode(MODEREC RecMode) { eRecMode=RecMode;};

	//-------------------------------------------------------------------------
	// Retourne la langue d'affichage
	LANGUE GetLangue() { return eLangue;};

	//-------------------------------------------------------------------------
	// Initialise la langue d'affichage
	void SetLangue(LANGUE Lg) { eLangue=Lg;};

	//-------------------------------------------------------------------------
	// Retourne le volume de replay
	int GetVolPlay() { return iVolPlay;};

	//-------------------------------------------------------------------------
	// Initialise le volume de replay
	void SetVolPlay(int vol) { iVolPlay=vol;};

	//-------------------------------------------------------------------------
	// Retourne le décalage hétérodyne
	int GetDecHeter() { return iDecHeter;};

	//-------------------------------------------------------------------------
	// Initialise le décalage hétérodyne
	void SetDecHeter(int Dec) { iDecHeter=Dec;};

	//-------------------------------------------------------------------------
	// Retourne le mode de fonctionnement de l'hétérodyne
	MODE_HETERODYNE GetModeHeter() { return iModeHeter;};

	//-------------------------------------------------------------------------
	// Initialise mode de fonctionnement de l'hétérodyne
	void SetModeHeter(MODE_HETERODYNE Mode) { iModeHeter=Mode;};

	//-------------------------------------------------------------------------
	// Retourne le mode de déclenchement de l'hétérodyne
	TRIG_HETERODYNE GetTrigHeter() { return iTrigHeter;};

	//-------------------------------------------------------------------------
	// Initialise mode de déclenchement de l'hétérodyne
	void SetTrigHeter(TRIG_HETERODYNE Mode) { iTrigHeter=Mode;};

	//-------------------------------------------------------------------------
	// Retourne l'entrée micro
	MIKEINPUT GetMikeInput() { return iMikeType;};

	//-------------------------------------------------------------------------
	// Initialise l'entrée micro
	void SetMikeInput(MIKEINPUT iMike) { iMikeType=iMike;};

	//-------------------------------------------------------------------------
	// Retourne le type de sortie audio
	AUDIOOUTPUT GetOutput() { return iOutput;};

	//-------------------------------------------------------------------------
	// Initialise le type de sortie audio
	void SetOutput(AUDIOOUTPUT iOut) { iOutput=iOut;};

	//-------------------------------------------------------------------------
	// Retourne le type de carte audio
	AUDIOCARD GetAudioCard() { return iAudioCard;};

	//-------------------------------------------------------------------------
	// Initialise le type de carte audio
	void SetAudioCard(AUDIOCARD iCard) { iAudioCard=iCard;};

	//-------------------------------------------------------------------------
	// Retourne le type de clavier
	KEYBOARDTYPE GetKeyboard() { return iKeyboard;};

	//-------------------------------------------------------------------------
	// Initialise le type de clavier
	void SetKeyboard(KEYBOARDTYPE iKB) { iKeyboard=iKB;};

	//-------------------------------------------------------------------------
	// Retourne le type d'écran
	SCREENTYPE GetScreen() { return iScreen;};

	//-------------------------------------------------------------------------
	// Initialise le type d'écran
	void SetScreen(SCREENTYPE iSC) { iScreen=iSC;};

	//-------------------------------------------------------------------------
	// Retourne le type d'horloge
	RTCTYPE GetRTC() { return iRTC;};

	//-------------------------------------------------------------------------
	// Initialise le type d'horloge
	void SetRTC(RTCTYPE iH) { iRTC=iH;};

	//-------------------------------------------------------------------------
	// Retourne le path des commandes de la carte audio
	char *GetWolfsonShPath() { return WolfsonShPath;};
	
	//-------------------------------------------------------------------------
	// Initialise le path des commandes de la carte audio
	void SetWolfsonShPath(char *sPath) {strcpy(WolfsonShPath, sPath);};
	
	//-------------------------------------------------------------------------
	// Retourne le répertoire racine de sauvegarde des fichiers sur clé USB (/mnt/usbkey par défaut)
	char *GetUSBKeyFilePath() { return USBKeyFilePath;};
	
	//-------------------------------------------------------------------------
	// Initialise le répertoire Répertoire racine de sauvegarde des fichiers sur clé USB (/mnt/usbkey par défaut)
	void SetUSBKeyFilePath( char *pPath) { strcpy(USBKeyFilePath, pPath);};
	
	//-------------------------------------------------------------------------
	// Retourne le répertoire racine de sauvegarde des fichiers sur la carte SD (/home/pi/pibat par défaut)
	char *GetLocalFilepath() { return LocalFilepath;};

	//-------------------------------------------------------------------------
	// Initialise le répertoire rracine de sauvegarde des fichiers sur la carte SD (/home/pi/pibat par défaut)
	void SetLocalFilepath( char *pPath) { strcpy(LocalFilepath, pPath);};

	//-------------------------------------------------------------------------
	// Retourne le répertoire de sauvegarde des fichiers wav (vide par défaut)
	char *GetWavDir() { return WavDir;};
	
	//-------------------------------------------------------------------------
	// Initialise le répertoire de sauvegarde des fichiers wav (vide par défaut)
	void SetWavDir( char *pPath) { strcpy(WavDir, pPath);};
	
	//-------------------------------------------------------------------------
	// Retourne le répertoire de sauvegarde des fichiers log (vide par défaut)
	char *GetLogDir() { return LogDir;};
	
	//-------------------------------------------------------------------------
	// Initialise le répertoire de sauvegarde des fichiers log (vide par défaut)
	void SetLogDir( char *pPath) { strcpy(LogDir, pPath);};
	
	//-------------------------------------------------------------------------
	// Initialise les paths de sauvegarde en fonction de la présence ou non de la clé
	void SetSavePaths();

	//-------------------------------------------------------------------------
	// Retourne le path complet de sauvegarde des fichiers wav
	char *GetWavPath() { return WavPath;};
	
	//-------------------------------------------------------------------------
	// Retourne le path complet de sauvegarde des fichiers log
	char *GetLogPath() { return LogPath;};
	
	//-------------------------------------------------------------------------
	// Retourne true si le logciel est lancé en mode auto
	bool GetModeAuto() { return bAuto;};
	
	//-------------------------------------------------------------------------
	// Initialise le mode auto
	void SetModeAuto( bool bMode) { bAuto = bMode;};
	
	//-------------------------------------------------------------------------
	// Décodage d'une ligne d'un paramètre
	void DecodeParams(
		char *sLine	// Ligne à décoder
		);

	//-------------------------------------------------------------------------
	// Lecture des paramètres depuis le fichier de sauvegarde
	void ReadParams();

	//-------------------------------------------------------------------------
	// Enregistrement des paramètres dans le fichier de sauvegarde
	void WriteParams();

	//-------------------------------------------------------------------------
	// Test de présence de la clé USB
	// Retourne True si la clé USB est montée
	bool isUSBKey();
	
	//-------------------------------------------------------------------------
	// Monte la clé USB
	// Retourne True si la clé USB est montée
	bool MountUSBKey();
	
	//-------------------------------------------------------------------------
	// Démonte la clé USB
	// Retourne True si la clé USB est démontée
	bool UMountUSBKey();
	
	//-------------------------------------------------------------------------
	// Retourne la Latitude du site (47° par défaut)
	float GetLatitude() {return fLatitude;};
	
	//-------------------------------------------------------------------------
	// Init la Latitude du site (47° par défaut)
	void SetLatitude(float fLat) { fLatitude = fLat;};
	
	//-------------------------------------------------------------------------
	// Retourne la Longitude du site (0° par défaut)
	float GetLongitude() {return fLongitude;};
	
	//-------------------------------------------------------------------------
	// Init la Longitude du site (0° par défaut)
	void SetLongitude(float fLon) { fLongitude = fLon;};
	
	//-------------------------------------------------------------------------
	// Retourne le Mode de démarrage de l'enregistrement
	SCHEDULETYPE GetScheduleType() {return ScheduleType;};
	
	//-------------------------------------------------------------------------
	// Init le Mode de démarrage de l'enregistrement
	void SetScheduleType(SCHEDULETYPE type) { ScheduleType = type;};
	
	//-------------------------------------------------------------------------
	// Retourne le Décalage de début et de fin d'enregistrement par rapport au lever/coucher du soleil
	char *GetHDec() { return sHDec;};
	
	//-------------------------------------------------------------------------
	// Initialise le Décalage de début et de fin d'enregistrement par rapport au lever/coucher du soleil
	void SetHDec(char *HDec) { strcpy(sHDec, HDec);};
	
	//-------------------------------------------------------------------------
	// Retourne le Type de shutdown
	SHUTDOWNTYPE GetShutdownType() {return eShutDown;};
	
	//-------------------------------------------------------------------------
	// Init le Type de shutdown
	void SetShutdownType(SHUTDOWNTYPE type) { eShutDown = type;};
	
	//-------------------------------------------------------------------------
	// Retourne le fuseau horaire
	int GetGMT() {return iGMT;};
	
	//-------------------------------------------------------------------------
	// Init du fuseau horaire
	void SetGMT(int gmt) { iGMT = gmt;};
	
	//-------------------------------------------------------------------------
	// Retourne true si l'heure d'été est gérée
	bool GetSummmerTime() {return bSummerTime;};
	
	//-------------------------------------------------------------------------
	// Init de la gestion de l'heure d'été
	void SetSummerTime(bool sumtime) { bSummerTime = sumtime;};

	//-------------------------------------------------------------------------
	// Retourne la ligne de commande de lancement du logiciel d'enregistrement
	char *GetRecorderCmd() { return sRecorderCmd;};
	
private:
	// Nom du fichier de sauvegarde des paramètres (/home/pi/PiBatRecorder/PiBatParams.txt)
	char sFilePathParams[80];
	// Nom du fichier de sauvegarde de la configuration (/home/pi/PiBatRecorder/PiBatConfig.txt)
	char sFilePathConfig[80];
	// Temps min d'enregistrement d'un fichier wav (0 à 3s, 2s par défaut)
	float fDRecMin;
	// Temps max d'enregistrement d'un fichier wav (0 à 99s, 30s par défaut)
	float fDRecMax;
	// Prefixe des noms des fichiers wav (PiBat par défaut)
	char sWaveName[11];
	// Niveau du volume d'enregistrement (1 a 31, 20 par défaut)
	int iNivRec;
	// Type d'enregistrement (WAV ou ACT, WAV par défaut)
	TYPEREC eRecType;
	// Heure de debut d'enregistrement du mode AUTO (22:00 par défaut)
	char sHDeb[6];
	// Heure de fin d'enregistrement du mode AUTO (06:00 par défaut)
	char sHFin[6];
	// Seuil de détection en dB au-dessus du niveau moyen (15 par défaut)
	int iThreshold;
	// Frequence min d'interet en Hz (15000 par défaut)
	int iFMin;
	// Frequence max d'interet en Hz (96000 par défaut)
	int iFMax;
	// Mode d'enregistrement (AUTO ou MANU, MANU par défaut)
	MODEREC eRecMode;
	// Langue d'affichage (Français par défaut)
	LANGUE eLangue;
	// Décalage hétérodyne en Hz (1kHz par défaut, de 100 à 2000Hz par pas de 100Hz)
	int iDecHeter;
	// Mode de fonctionnement de l'hétérodyne
	MODE_HETERODYNE iModeHeter;
	// Mode de déclenchement de l'hétérodyne
	TRIG_HETERODYNE iTrigHeter;
	// Volume d'écoute (0 a 31, 20 par défaut)
	int iVolPlay;
	// Type de micro
	MIKEINPUT iMikeType;
	// Sortie Audio
	AUDIOOUTPUT iOutput;
	// Type de carte audio
	AUDIOCARD iAudioCard;
	// Type de clavier
	KEYBOARDTYPE iKeyboard;
	// Type d'écran
	SCREENTYPE iScreen;
	// Type d'horloge
	RTCTYPE iRTC;
	// Path des fichiers de commandes sh de la carte audio
	char WolfsonShPath[255];
	// Répertoire racine de sauvegarde des fichiers sur clé USB (/mnt/usbkey par défaut)
	char USBKeyFilePath[255];
	// Répertoire racine de sauvegarde des fichiers sur la carte SD (/home/pi/pibat par défaut)
	char LocalFilepath[255];
	// Répertoire de sauvegarde des fichiers wav (vide par défaut)
	char WavDir[255];
	// Répertoire de sauvegarde des fichiers log (vide par défaut)
	char LogDir[255];
	// Path complet de sauvegarde des fichiers wav
	char WavPath[255];
	// Path complet de sauvegarde des fichiers log
	char LogPath[255];
	// Indique si le logiciel est lancé en mode auto
	bool bAuto;
	// Paramètres spécifiques de PiBatScheduler
	// Latitude du site (47° par défaut)
	float fLatitude;
	// Longitude du site (0° par défaut)
	float fLongitude;
	// Mode de démarrage de l'enregistrement
	SCHEDULETYPE ScheduleType;
	// Décalage de début et de fin d'enregistrement par rapport au lever/coucher du soleil
	char sHDec[7];
	// Type de shutdown
	SHUTDOWNTYPE eShutDown;
	// Fuseau horaire (GMT+1 par défaut)
	int iGMT;
	// Avec gestion heure d'été (Yes/No, Yes par défaut)
	bool bSummerTime;
	// Ligne de commande du lancement du logiciel d'enregistrement
	char sRecorderCmd[80];
	// Gestionnaire du fichier log
	LogFile log;
};

#endif	/* CPARAMETERS_H */

