/* 
 * File:   CPlay.cpp
 * Author: Jean-Do
 * 
 * Created on 19 septembre 2015, 16:24
 */

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include "portaudio.h"
#include "CPlay.h"

//-------------------------------------------------------------------------
// Constructeur
CPlay::CPlay(
	CParameters *pPar		// Pointeur sur les paramètres
	)
{
	// Mémorisation des paramètres
	pParams = pPar;
	// Init des variables
	bRunning   = false;
	// Par défaut, pas de données disponibles
	stream = NULL;
	fCurentPlay = 0.0;
	fLengthPlay = 0.0;
	pPathWaveFile[0] = 0;
	bExpTime = false;
}

//-------------------------------------------------------------------------
// Destructeur
CPlay::~CPlay()
{
}

//-------------------------------------------------------------------------
// Initialise le path et nom du fichier en lecture
// Retourne la durée du fichier en diziemme de secondes
unsigned int CPlay::SetFile(
	char* psWaveFile,	// Path et nom du fichier à lire
	bool bExpT			// True pour une expension de temps de 10, false pour 1
	)
{
	bExpTime = bExpT;
	if (psWaveFile != NULL and strlen(psWaveFile) > 0)
	{
		// Mémo du path et nom du fichier en lecture
		strcpy(pPathWaveFile, psWaveFile);
		// On ouvre le fichier wave
		waveFile.OpenWaveFileForRead(pPathWaveFile, bExpTime);
		// Récupération de la durée totale
		fLengthPlay = waveFile.GetPlayDuration();
		LogFile::AddLog(LDEBUG, "CPlay::SetFile %04.1fs %s\n", fLengthPlay, pPathWaveFile);
	}
	else
	{
		// Fermeture du fichier wave précédent si présent
		waveFile.CloseWavfile();
		fLengthPlay = 0.0;
	}
	// RAZ de la durée de lecture courante
	//printf("Fichier [%s] %fs\n", pPathWaveFile, fLengthPlay);
	fCurentPlay = 0.0;
	lNbEch = 0;
}
	
//-------------------------------------------------------------------------
// Lance la lecture en ouvrant le canal audio sur le dernier fichier
void CPlay::Start()
{
	LogFile::AddLog(LDEBUG, "CPlay::Start\n");
	if (bRunning)
	{
		LogFile::AddLog(LDEBUG, "Start alors qu'on est encore en running !\n");
		Stop();
	}
	// On initialise le niveau de lecture
	SetVolume();
	
	if (fLengthPlay > 0.01)
	{
		rate = waveFile.GetSampleRate();
		waveFile.SetDecimation(1);
		if (bExpTime and rate == 192000)
			rate = 19200;
		else if (!bExpTime and rate == 19200)
			rate = 192000;
		else if ( bExpTime and rate == 384000)
			rate = 38400;
		else if (!bExpTime and rate == 38400)
		{
			// On fait de la décimation par deux
			rate = 192000;
			waveFile.SetDecimation(2);
		}
		else if (!bExpTime and rate == 384000)
		{
			// On fait de la décimation par deux
			rate = 192000;
			waveFile.SetDecimation(2);
		}
		// Ouverture du canal audio
		err = Pa_OpenDefaultStream( &stream,
									0,				// Pas d'enregistrement
									1,				// Lecture en mono
									paInt16,		// 16 bits
									rate,			// 19.2 ou 192kHz
									CHUNK_LENGTHPLAY,// Nombre d'échantillons par callback
									Callback,		// Pointeur sur la callback
									this);			// Pointeur sur l'instance de la classe
		if (err == paNoError)
			// Démarre le canal
			err = Pa_StartStream( stream );
		else
		{
			LogFile::AddLog(LLOG, "CPlay::Start Pa_OpenDefaultStream erreur [%s]\n", Pa_GetErrorText(err));
		}
		if (err != paNoError)
		{
			LogFile::AddLog(LLOG, "CPlay::Start Pa_StartStream erreur [%s]\n", Pa_GetErrorText(err));
		}
		else
			bRunning = true;
	}
	else
		LogFile::AddLog(LLOG, "CPlay::Start Pas de fichier wav !\n");
}

//-------------------------------------------------------------------------
// Stoppe la lecture en fermant le canal audio
void CPlay::Stop()
{
	if (stream != NULL)
	{
		// On prend la main sur les données
		pthread_mutex_lock (&mutexDatas);
		bRunning = false;
		// On relache les données
		pthread_mutex_unlock(&mutexDatas);
		// Arrêt du canal
		err = Pa_StopStream( stream );
		if (err == paNoError)
		{
			// Fermeture du canal
			err = Pa_CloseStream( stream );
		}
		else
		{
			LogFile::AddLog(LLOG, "CPlay::Stop Pa_StopStream erreur [%s]\n", Pa_GetErrorText(err));
		}
		if (err != paNoError)
		{
			LogFile::AddLog(LLOG, "CPlay::Stop Pa_CloseStream erreur [%s]\n", Pa_GetErrorText(err));
		}
		stream = NULL;
	}
	// On ferme le fichier wav
	waveFile.CloseWavfile();
	LogFile::AddLog(LDEBUG, "CPlay::Stop\n");		
}

//-------------------------------------------------------------------------
// Avance ou recule la lecture d'un fragment de la durée du ficher en cours
void CPlay::NextTime(
	int iNext,		// -1 recule de 1/Coef
					// +1 avance de 1/Coef
	int iCoef		// Coefficient d'avancement
	)
{
	bool bStop = false;
	// On prend la main sur les données
	pthread_mutex_lock (&mutexDatas);
	// On repositionne le fichier de +/- 1/10
	// Test si on est sur les extrémités
	long lMaxL  = waveFile.GetDataLength() / sizeof(short);
	long lLimit = lMaxL / iCoef;
	if (lNbEch > (lMaxL - lLimit))
	{
		// Fin du fichier, on stoppe la lecture
		LogFile::AddLog(LDEBUG, "CPlay::NextTime A lNbEch %d, lLimit %d, lMaxL %d\n", lNbEch, lLimit, lMaxL);
		bStop = true;
	}
	else
	{
		// Prise en compte de l'avance ou du recul du fichier
		if (waveFile.ReadNext(iNext) == false)
		{
			// Fin du fichier, on stoppe la lecture
			LogFile::AddLog(LDEBUG, "CPlay::NextTime B\n");
			bStop = true;
		}
		else if (iNext > 0)
		{
			fCurentPlay += fLengthPlay / iCoef;
			lNbEch += (waveFile.GetDataLength()/sizeof(short)) / iCoef;
		}
		else
		{
			fCurentPlay -= fLengthPlay / iCoef;
			lNbEch -= (waveFile.GetDataLength()/sizeof(short)) / iCoef;
		}
	}
	// On relache les données
	pthread_mutex_unlock(&mutexDatas);
	if (bStop)
		Stop();
}
	
//-------------------------------------------------------------------------
// Initialise le volume de sortie
void CPlay::SetVolume()
{
	// Calcul du volume (paramètre 0-31, mixer 0-191)
	int iVol = pParams->GetVolPlay() * 191 / 31;
	if (iVol > 191)
		iVol = 191;
	// Set path gain to -6dB for safety. ie max 0.5Vrms output level. (0-191)
	// amixer -q -Dhw:sndrpiwsp cset name='HPOUT1 Digital Volume' 116
	char sCmd[160];
	// Sélection de la sortie utilisée
	switch (pParams->GetOutput())
	{
	default:
	case OUTHEADSET:
		// Volume sur la prise casque
		sprintf(sCmd, "amixer -q -Dhw:sndrpiwsp cset name='HPOUT1 Digital Volume' %d", iVol);
		LogFile::AddLog(LDEBUG, "CPlay::SetVolume Headset %s\n", sCmd);
		system(sCmd);
		break;
	case OUTLINE:
		// Volume sur la sortie ligne
		sprintf(sCmd, "amixer -q -Dhw:sndrpiwsp cset name='HPOUT2 Digital Volume' %d", iVol);
		LogFile::AddLog(LDEBUG, "CPlay::SetVolume Line %s\n", sCmd);
		system(sCmd);
		break;
	case OUTLS:
		// Volume sur la sortie HP
		sprintf(sCmd, "amixer -q -Dhw:sndrpiwsp cset name='Speaker Digital Volume' %d", iVol);
		LogFile::AddLog(LDEBUG, "CPlay::SetVolume Headset %s\n", sCmd);
		system(sCmd);
	}
}

//-------------------------------------------------------------------------
// Retourne les informations sur le fichier en lecture
void CPlay::GetInfo(
	float *pfCurPlay,		// Durée de la lecture courante en secondes
	float *pfLengthPlay		// Durée max du fichier en lecture en secondes
	)
{
	// On prend la main sur les données
	pthread_mutex_lock (&mutexDatas);
	// Init des infos
	*pfCurPlay    = fCurentPlay;
	*pfLengthPlay = fLengthPlay;
	//LogFile::AddLog(LDEBUG, "CPlay::GetInfo %04.1f/%04.1fs %s\n", fCurentPlay, fLengthPlay, pPathWaveFile);
	// On relache les données
	pthread_mutex_unlock(&mutexDatas);
}
	
//-------------------------------------------------------------------------
// Positionne le fichier en lecture à une durée précise
// Retourne false si la durée du fichier est dépassée ou inférieure à 0
bool CPlay::SetPosRead(
	float fPos	// Durée en secondes de positionnement du curseur
	)
{
	bool bOK = false;
	// On prend la main sur les données
	pthread_mutex_lock (&mutexDatas);
	if (!bRunning)
		lNbEch = waveFile.SetPosRead( fPos);
	if (lNbEch > 0)
		bOK = true;
	else
		lNbEch = 0;
	//printf("CPlay::GetInfo %04.1f/%04.1fs %s\n", fCurentPlay, fLengthPlay, sFileName);
	// On relache les données
	pthread_mutex_unlock(&mutexDatas);
	return bOK;
}
	
//-------------------------------------------------------------------------
// Traitement des échantillons
// Attention, appel depuis un thread différent du programme principal
int CPlay::TraiteEch(
	const void *inputBuffer,					// Buffer d'enregistrement
	void *outputBuffer,							// Buffer de lecture
	unsigned long framesPerBuffer,				// Taille des buffers
	const PaStreamCallbackTimeInfo* timeInfo,	// Info temporelles
	PaStreamCallbackFlags statusFlags			// Informations
	)
{
	//printf("CPlay::TraiteEch framesPerBuffer %d\n", framesPerBuffer);
	// On prend la main sur les données
	pthread_mutex_lock (&mutexDatas);
	//printf("CPlay::TraiteEch\n");
	int iReturn = paContinue;
	if (!bRunning)
	{
		//printf("CPlay::TraiteEch paComplete\n");
		iReturn = paComplete;
	}
	// Lecture des échantillons
	long lLength = waveFile.WavfileRead((short *)outputBuffer, framesPerBuffer);
	//printf("CPlay::TraiteEch lLength %d\n", lLength);
	if (lLength < CHUNK_LENGTHPLAY)
	{
		// Fin du fichier, on stoppe la lecture
		iReturn = paComplete;
		bRunning = false;
	}
	// On passe les échantillons en lecture
	//outputBuffer = Buffer;
	// Mise à jour de la durée courante
	fCurentPlay = (float)lNbEch / (float)rate;
	lNbEch += lLength;
	//if (lLength < CHUNK_LENGTHPLAY)
	//	printf("CPlay::TraiteEch OK\n");
	// On relache les données
	pthread_mutex_unlock(&mutexDatas);
	return iReturn;
}

//-------------------------------------------------------------------------
// Callback de traitement des échantillons
// Attention, appel depuis un thread différent du programme principal
int CPlay::Callback(
	const void *inputBuffer,					// Buffer d'enregistrement
	void *outputBuffer,							// Buffer de lecture
	unsigned long framesPerBuffer,				// Taille des buffers
	const PaStreamCallbackTimeInfo* timeInfo,	// Info temporelles
	PaStreamCallbackFlags statusFlags,			// Informations
	void *userData								// Pointeur sur l'instance
	)
{
	// Appel de la méthode de traitement de l'instance
	return ((CPlay *)userData)->TraiteEch( inputBuffer, outputBuffer,
			framesPerBuffer, timeInfo, statusFlags);
}
