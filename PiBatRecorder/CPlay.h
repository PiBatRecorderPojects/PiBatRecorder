/* 
 * File:   CPlay.h
 * Author: Jean-Do
 *
 * Created on 19 septembre 2015, 16:24
 */
#include <pthread.h>
#include <dirent.h>
#include "portaudio.h"
#include "CParameters.h"
#include "CWaveFile.h"

#ifndef CPLAY_H
#define	CPLAY_H

// Taille des buffers
#define CHUNK_LENGTHPLAY 8192

//-------------------------------------------------------------------------
// Classe de gestion du mode lecture
// Lance la lecture du dernier fichier
class CPlay
{
public:
	//-------------------------------------------------------------------------
	// Constructeur
	CPlay(
		CParameters *pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~CPlay();
	
	//-------------------------------------------------------------------------
	// Initialise le path et nom du fichier en lecture
	// Retourne la durée du fichier en diziemme de secondes
	unsigned int SetFile(
		char* psWaveFile,	// Path et nom du fichier à lire
		bool bExpT			// True pour une expension de temps de 10, false pour 1
		);
	
	//-------------------------------------------------------------------------
	// Lance la lecture en ouvrant le canal audio sur le fichier actif
	void Start();
	
	//-------------------------------------------------------------------------
	// Stoppe la lecture en fermant le canal audio
	void Stop();
	
	//-------------------------------------------------------------------------
	// Positionne le fichier en lecture à une durée précise
	// Retourne false si la durée du fichier est dépassée ou inférieure à 0
	bool SetPosRead(
		float fPos	// Durée en secondes de positionnement du curseur
		);
	
	//-------------------------------------------------------------------------
	// Avance ou recule la lecture d'un fragment de la durée du ficher en cours
	void NextTime(
		int iNext,		// -1 recule de 1/Coef
						// +1 avance de 1/Coef
		int iCoef=10	// Coefficient d'avancement
		);
	
	//-------------------------------------------------------------------------
	// Traitement des échantillons
	// Attention, appel depuis un thread différent du programme principal
	int TraiteEch(
		const void *inputBuffer,					// Buffer d'enregistrement
		void *outputBuffer,							// Buffer de lecture
		unsigned long framesPerBuffer,				// Taille des buffers
		const PaStreamCallbackTimeInfo* timeInfo,	// Info temporelles
		PaStreamCallbackFlags statusFlags			// Informations
		);

	//-------------------------------------------------------------------------
	// Callback de traitement des échantillons
	// Attention, appel depuis un thread différent du programme principal
	static int Callback(
		const void *inputBuffer,					// Buffer d'enregistrement
		void *outputBuffer,							// Buffer de lecture
		unsigned long framesPerBuffer,				// Taille des buffers
		const PaStreamCallbackTimeInfo* timeInfo,	// Info temporelles
		PaStreamCallbackFlags statusFlags,			// Informations
		void *userData								// Pointeur sur l'instance
		);
	
	//-------------------------------------------------------------------------
	// Initialise le volume de sortie en fonction de la valeur du paramètre volume
	void SetVolume();
	
	//-------------------------------------------------------------------------
	// Retourne les informations sur le fichier en lecture
	void GetInfo(
		float *pfCurPlay,	// Durée de la lecture courante en secondes
		float *pfLengthPlay	// Durée max du fichier en lecture en secondes
		);
	
	//-------------------------------------------------------------------------
	// Retourne true si on est en lecture
	bool bIsRunning() {return bRunning;};
	
private:
	// Pointeur sur la mémorisation des paramètres
	CParameters *pParams;
	// Pointeur sur le canal Audio
    PaStream *stream;
	// Erreur éventuelle
    PaError err;
	// Indique que la lecture est en cours
	bool bRunning;
	// Mutex de controle d'accès aux infos de l'instance
	// entre le thread principal et la callback
	pthread_mutex_t mutexDatas;
	// Buffer des échantillons en lecture
	short Buffer[CHUNK_LENGTHPLAY];
	// Fichier en lecture
	CWaveFile waveFile;
	// Path et nom du fichier en lecture
	char pPathWaveFile[256];
	// Durée totale du fichier en lecture en secondes
	float fLengthPlay;
	// Durée courante de la lecture en secondes
	float fCurentPlay;
	// Nombre d'échantillons lus
	long lNbEch;
	// True pour une expension de temps de 10, false pour 1
	bool bExpTime;
	// Fréquence d'échantillonnage du fichier en lecture
	int rate;
};

#endif	/* CPLAY_H */

