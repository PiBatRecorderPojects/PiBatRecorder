/* 
 * File:   CRecorder.cpp
 * Author: Jean-Do
 * 
 * Created on 20 août 2015, 11:22
 */
//-------------------------------------------------------------------------
// Classe de gestion du monitoring et de l'enregistrement
// Ouvre le canal d'enregistrement, traite les échantillons
// et, si le seuil est dépassé, passe en enregistrement

#include <cstdlib>
#include <stdio.h>
#include <math.h>
#include "CRecorder.h"

#define PI 3.14159265

// Nombre de buffer pour reconsidérer la fréquence hétérodyne
#define NBCHUNKH	 40

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
CRecorder::CRecorder(
			CParameters *pPar,			// Pointeur sur les paramètres
			CAnalyser *pAnalyse,		// Pointeur sur l'analyseur de trames
			CMonitStatus *pMonitStatus	// Pointeur sur gestionnaire d'état du Monitoring
			)
			: heterodyne(pPar, pMonitStatus)
			, log(pPar)
{
	//printf("CRecorder::CRecorder\n");		
	// Mémorisation des paramètres
	pParams = pPar;
	pAnalyser = pAnalyse;
	pGestMonitStatus = pMonitStatus;
	// Préfixe des fichier wav
	strcpy( sPrefWav, pPar->GetWaveName());
	// Durées min et max d'enregistrement
	dRecMin = pPar->GetDRecMin();
	dRecMax = pPar->GetDRecMax();
	// Calcul de la durée en µs d'un buffer d'échantillon
	dDurBuffer = (double)CHUNK_LENGTH * 1.0 / 192000.0;
	LogFile::AddLog(LDEBUG, "CRecorder::CRecorder durée d'une trame %lfms\n", dDurBuffer*1000.0);
	// Par défaut, pas de données disponibles
	stream = NULL;
	fRecordDuration = 0.0;
	iFME = 0;
	//printf("avant Pa_Initialize\n");		
	// Initialisation de PortAudio
	err = Pa_Initialize();
	if (err != paNoError)
	{
		LogFile::AddLog(LLOG, "CRecorder::CRecorder Pa_Initialize erreur [%s]", Pa_GetErrorText(err));
	}
	//printf("apres Pa_Initialize\n");		
}

//-------------------------------------------------------------------------
// Destructeur
CRecorder::~CRecorder()
{
	// Termine Port Audio
	err = Pa_Terminate();
	if (err != paNoError)
	{
		LogFile::AddLog(LLOG, "CRecorder::~CRecorder Pa_Terminate erreur [%s]", Pa_GetErrorText(err));
	}
}

//-------------------------------------------------------------------------
// Lance le monitoring en ouvrant le canal audio
// Appel uniquement depuis le thread principal
void CRecorder::Start()
{
	LogFile::AddLog(LDEBUG, "CRecorder::Start\n");		
	// Mémo des paramètres éventuellements modifiés par menu
	// Préfixe des fichier wav
	strcpy( sPrefWav, pParams->GetWaveName());
	// Durées min et max d'enregistrement
	dRecMin = pParams->GetDRecMin();
	dRecMax = pParams->GetDRecMax();
	// On initialise le niveau d'enregistrement
	SetRecordGain( pParams->GetNivRec());
	// Init de l'état
	pGestMonitStatus->InitStatus();
	// RAZ des infos hétérodyne
	iFME = 0;
	iNbBuffH = 0;
	// On initialise le temps de départ
	dTimeEch = 0.0;
	// Test si hétérodyne possible
	// Si en manuel, on ouvre aussi en lecture
	bHeterodyne = false;
	if (pParams->GetRecMode() == MANU)
	{
		// Le signal hétérodyne est autorisé
		bHeterodyne = true;
		bModeAuto = false;
	}
	else
		bModeAuto = true;
	// Init des indicateurs des fichiers à produire
	bWave = false;
	bLog  = false;
	bAct  = false;
	switch (pParams->GetRecType())
	{
	case WAV	:
		bWave = true;
		break;
	case LOGC	:
		bLog = true;
		break;
	case ACTV	:
		bAct = true;
		break;
	case WAVLOG	:
		bWave = true;
		bLog = true;
		break;
	case ACTLOG	:
		bAct = true;
		bLog = true;
		break;
	case WAVACT	:
		bWave = true;
		bAct = true;
		break;
	case ALL	:
		bWave = true;
		bLog = true;
		bAct = true;
		break;
	default		:
		bWave = true;
		break;
	}
    // Ouverture du canal audio
    err = Pa_OpenDefaultStream( &stream,
                                1,				// Enregistrement Mono
                                0,				// Pas de Lecture
                                paInt16,		// 16 bits
                                192000,			// 192kHz
                                CHUNK_LENGTH,   // Nombre d'échantillons par callback
                                Callback,		// Pointeur sur la callback
								this);			// Pointeur sur l'instance de la classe
	if (err == paNoError)
	{
		// Démarre le canal
		pGestMonitStatus->SetMonitoring(true);
		err = Pa_StartStream( stream );
	}
	else
	{
		LogFile::AddLog(LLOG, "CRecorder::Start Pa_OpenDefaultStream erreur [%s]", Pa_GetErrorText(err));
	}
	if (err != paNoError)
	{
		LogFile::AddLog(LLOG, "CRecorder::Start Pa_StartStream erreur [%s]", Pa_GetErrorText(err));
	}
	if (bHeterodyne)
		// On démarre le canal de lecture
		heterodyne.Start();
	LogFile::AddLog(LDEBUG, "CRecorder::Start OK\n");		
}

//-------------------------------------------------------------------------
// Stoppe le monitoring en fermant le canal audio
// Appel uniquement depuis le thread principal
void CRecorder::Stop()
{
	// Etat courant du monitoring
	MonitoringStatus MonitStatus;
	// Récupération de l'état
	pGestMonitStatus->GetStatus( &MonitStatus);
	// Arrêt éventuel de l'enregistrement
	if (MonitStatus.bRecording)
		StopRecording();
	// Arret éventuel de l'hétérodyne
	if (bHeterodyne)
		heterodyne.Stop();
	// Init de l'état
	pGestMonitStatus->SetMonitoring(false);
	// Arrêt du canal
	err = Pa_StopStream( stream );
	if (err == paNoError)
		// Fermeture du canal
		err = Pa_CloseStream( stream );
	else
	{
		LogFile::AddLog(LLOG, "CRecorder::Stop Pa_StopStream erreur [%s]", Pa_GetErrorText(err));
	}
	if (err != paNoError)
	{
		LogFile::AddLog(LLOG, "CRecorder::Stop Pa_CloseStream erreur [%s]", Pa_GetErrorText(err));
	}
	stream = NULL;
	LogFile::AddLog(LDEBUG, "CRecorder::Stop\n");		
}

//-------------------------------------------------------------------------
// Démarre un enregistrement
void CRecorder::StartRecord()
{
	// Mutex ???
	MonitoringStatus Status;
	pGestMonitStatus->GetStatus(&Status);
	StartRecording( &Status);
}

//-------------------------------------------------------------------------
// Stoppe un enregistrement
void CRecorder::StopRecord()
{
	// Mutex ???
	StopRecording();
}
	
//-------------------------------------------------------------------------
// Démarre un enregistrement
// Attention, appel depuis un thread différent du programme principal
void CRecorder::StartRecording(
	MonitoringStatus *pMonitStatus	// Etat courant
	)
{
	LogFile::AddLog(LDEBUG, "CRecorder::StartRecording\n");
	char fileName[80];
	char filePath[160];
	char timeStr[80];
	struct tm * timeinfo;
	time_t curtime;
	time(&curtime);
	timeinfo = localtime (&curtime);
	strftime(timeStr, 80, "%y%m%d_%H%M%S", timeinfo);
	// On passe en mode Record
	pGestMonitStatus->SetRecording(true);
	if (bWave)
	{
		// Préparation du nom du fichier wav
		sprintf(filePath, "%s%s_%s.wav", pParams->GetWavPath(), sPrefWav, timeStr);
		sprintf(fileName, "%s_%s.wav", sPrefWav, timeStr);
		// Initialisation du fichier wav
		wavefile.OpenWaveFileForWrite(filePath);
	}
	if (bLog)
	{
		// Création du log
		if (bWave)
			log.StartWavFile(pMonitStatus, fileName);
		else
			log.StartWavFile(pMonitStatus, timeStr);
	}
}

//-------------------------------------------------------------------------
// Stoppe un enregistrement
// Attention, appel depuis un thread différent du programme principal
void CRecorder::StopRecording()
{
	LogFile::AddLog(LDEBUG, "CRecorder::StopRecording\n");
	if (bWave)
		// Fermeture du fichier
		wavefile.CloseWavfile();
	pGestMonitStatus->SetRecording(false);
	// Arrêt du log
	if (bLog)
		log.StopWavFile(fRecordDuration);
	fRecordDuration = 0.0;
}

//-------------------------------------------------------------------------
// Préparation de l'hétérodyne
void CRecorder::PrepareHeterodyne(
	int iFME	// Fréquence de la dernière FME
	)
{
	// Init de la fréquence hérétodyne
	heterodyne.PrepareHeterodyne( iFME);
}

//-------------------------------------------------------------------------
// Traitement des échantillons
// Attention, appel depuis un thread différent du programme principal
int CRecorder::TraiteEch(
	const void *inputBuffer,					// Buffer d'enregistrement
	void *outputBuffer,							// Buffer de lecture
	unsigned long framesPerBuffer,				// Taille des buffers
	const PaStreamCallbackTimeInfo* timeInfo,	// Info temporelles
	PaStreamCallbackFlags statusFlags			// Informations
	)
{
	//printf("TraiteEch framesPerBuffer %d\n", framesPerBuffer);
	// Etat courant du monitoring
	MonitoringStatus MonitStatus;
	// Récupération du dernier état
	pGestMonitStatus->GetStatus( &MonitStatus);
	// Si on n'est pas en enregistrement et que le seuil est dépassé
//MonitStatus.bThreshold = true;	// Pour un enregistrement continue
	if (not MonitStatus.bRecording and MonitStatus.bThreshold)
	{
		// On passe en enregistrement
		StartRecording( &MonitStatus);
		MonitStatus.bRecording = true;
	}
	int iReturn = paContinue;
	if (not MonitStatus.bMonitoring)
	{
		// Arrêt du monitoring
		//printf("CRecorder::TraiteEch MonitStatus.bMonitoring=false\n");
		iReturn = paComplete;
	}
	// Mémorisation du buffer pour l'analyse
	if (framesPerBuffer <= CHUNK_LENGTH)
	{
		pAnalyser->SetData((short int *)inputBuffer, framesPerBuffer, dTimeEch);
		//memcpy(Buffer, inputBuffer, framesPerBuffer*sizeof(short));
		dTimeEch += dDurBuffer;
	}
	else
		LogFile::AddLog(LDEBUG, "TraiteEch buffer trop gros (%d)\n", framesPerBuffer);		
	// Si on est en enregistrement
	if (MonitStatus.bRecording)
	{
		// On mémorise les données dans le fichier
		if (bWave)
			fRecordDuration = wavefile.WavfileWrite( (short *)inputBuffer, framesPerBuffer);
		else
			fRecordDuration = (float)framesPerBuffer / 192000.0;
		//printf("TraiteEch fRecordDuration (%2.2f)\n", fRecordDuration);
		pGestMonitStatus->SetRecordTime(fRecordDuration);
		if (bLog)
			// Memo des infos dans le log
			log.AddCry( &MonitStatus);
		// Test si on doit stopper l'enregistrement
		if (not MonitStatus.bThreshold and fRecordDuration > dRecMin)
			// Plus de détection et durée min atteinte
			StopRecording();
		else if (fRecordDuration > dRecMax)
			// Durée max atteinte
			StopRecording();
	}
	// Si hétérodyne autorisé
	if (bHeterodyne)
		// Mémo des échantillons pour l'hétérodyne
		heterodyne.SetEchantillons((short *)inputBuffer, framesPerBuffer);

	//printf("TraiteEch OK\n");		
	
	return iReturn;
}

//-------------------------------------------------------------------------
// Callback de traitement des échantillons
// Attention, appel depuis un thread différent du programme principal
int CRecorder::Callback(
	const void *inputBuffer,					// Buffer d'enregistrement
	void *outputBuffer,							// Buffer de lecture
	unsigned long framesPerBuffer,				// Taille des buffers
	const PaStreamCallbackTimeInfo* timeInfo,	// Infos temporelles
	PaStreamCallbackFlags statusFlags,			// Informations
	void *userData								// Pointeur sur l'instance
	)
{
	// Appel de la méthode de traitement de l'instance
	return ((CRecorder *)userData)->TraiteEch( inputBuffer, outputBuffer,
			framesPerBuffer, timeInfo, statusFlags);
}

//-------------------------------------------------------------------------
// Initialise le niveau d'enregistrement
void CRecorder::SetRecordGain(
	int iGain	// Gain d'enregistrement
	)
{
	// Commande pour fixer le gain de 0 à 31
	// amixer -q -Dhw:sndrpiwsp cset name='IN1R Volume' 20
	// Commade pour lire le gain
	// amixer -q -Dhw:sndrpiwsp cget name='IN1R Volume'
	char sCmd[160];
	switch(pParams->GetMikeInput())
	{
	case HEADSET:
		// Init du gain sur l'entrée micro casque
		sprintf(sCmd, "amixer -q -Dhw:sndrpiwsp cset name='IN1R Volume' %d", iGain);
		LogFile::AddLog(LDEBUG, "CRecorder::SetRecordGain headset : gain %d %s\n", iGain, sCmd);
		system(sCmd);
		break;
	case LINE_MONO:
	case LINE_STEREO:
		// Init du gain pour les deux entées ligne
		sprintf(sCmd, "amixer -q -Dhw:sndrpiwsp cset name='IN3R Volume' %d", iGain);
		system(sCmd);
		sprintf(sCmd, "amixer -q -Dhw:sndrpiwsp cset name='IN3L Volume' %d", iGain);
		system(sCmd);
		LogFile::AddLog(LDEBUG, "CRecorder::SetRecordGain line : gain %d %s\n", iGain, sCmd);
	}
}

//-------------------------------------------------------------------------
// Initialise le seuil de détection
void CRecorder::SetThreshold(
	int iTh	// Seuil de détection
	)
{
	pAnalyser->SetThreshold( iTh);
}
	
