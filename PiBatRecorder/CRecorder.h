/* 
 * File:   CRecorder.h
 * Author: Jean-Do
 *
 * Created on 20 août 2015, 11:22
 */
#include <pthread.h>
#include "portaudio.h"
#include "CParameters.h"
#include "CStatus.h"
#include "CWaveFile.h"
#include "CAnalyser.h"
#include "LogWaveFile.h"
#include "CHeterodyne.h"

#ifndef CRECORDER_H
#define	CRECORDER_H

// Taille des buffers
#define CHUNK_LENGTH 8192

//-------------------------------------------------------------------------
// Classe de gestion du monitoring et de l'enregistrement
// Ouvre le canal d'enregistrement, traite les échantillons
// et, si le seuil est dépassé, passe en enregistrement
class CRecorder
{
public:
	//-------------------------------------------------------------------------
	// Constructeur
	CRecorder(
			CParameters *pPar,			// Pointeur sur les paramètres
			CAnalyser *pAnalyse,		// Pointeur sur l'analyseur de trames
			CMonitStatus *pMonitStatus	// Pointeur sur gestionnaire d'état du Monitoring
			);
	
	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~CRecorder();
	
	//-------------------------------------------------------------------------
	// Lance le monitoring en ouvrant le canal audio
	void Start();
	
	//-------------------------------------------------------------------------
	// Stoppe le monitoring en fermant le canal audio
	void Stop();
	
	//-------------------------------------------------------------------------
	// Traitement des échantillons
	// Attention, appel depuis un thread différent du programme principal
	int TraiteEch(
		const void *inputBuffer,					// Buffer d'enregistrement
		void *outputBuffer,							// Buffer de lecture
		unsigned long framesPerBuffer,				// Taille des buffers
		const PaStreamCallbackTimeInfo* timeInfo,	// Info temporelles
		PaStreamCallbackFlags statusFlags			// Informations
		);

	//-------------------------------------------------------------------------
	// Callback de traitement des échantillons
	// Attention, appel depuis un thread différent du programme principal
	static int Callback(
		const void *inputBuffer,					// Buffer d'enregistrement
		void *outputBuffer,							// Buffer de lecture
		unsigned long framesPerBuffer,				// Taille des buffers
		const PaStreamCallbackTimeInfo* timeInfo,	// Info temporelles
		PaStreamCallbackFlags statusFlags,			// Informations
		void *userData								// Pointeur sur l'instance
		);

	//-------------------------------------------------------------------------
	// Retourne le pointeur sur le gestionnaire hétérodyne
	CHeterodyne *GetHeterodyne() {return &heterodyne;};
	
	//-------------------------------------------------------------------------
	// Préparation de l'hétérodyne
	void PrepareHeterodyne(
		int iFME	// Fréquence de la dernière FME
		);
	
	//-------------------------------------------------------------------------
	// Initialise le gain d'enregistrement
	void SetRecordGain(
		int iGain	// Gain d'enregistrement
		);
	
	//-------------------------------------------------------------------------
	// Initialise le seuil de détection
	void SetThreshold(
		int iTh	// Seuil de détection
		);

	//-------------------------------------------------------------------------
	// Démarre un enregistrement
	void StartRecord();
	
	//-------------------------------------------------------------------------
	// Stoppe un enregistrement
	void StopRecord();
	
protected:
	//-------------------------------------------------------------------------
	// Démarre un enregistrement
	// Attention, appel depuis un thread différent du programme principal
	void StartRecording(
		MonitoringStatus *pMonitStatus	// Etat courant
		);
	
	//-------------------------------------------------------------------------
	// Stoppe un enregistrement
	// Attention, appel depuis un thread différent du programme principal
	void StopRecording();
	
	// Pointeur sur la mémorisation des paramètres
	CParameters *pParams;
	// Analyseur des cris
	CAnalyser *pAnalyser;
	// Générateur du log
	LogWaveFile log;
	// Gestionnaire hétérodyne
	CHeterodyne heterodyne;
	// Pointeur sur le canal Audio
    PaStream *stream;
	// Erreur éventuelle
    PaError err;
	// Fréquence FME pour l'hétérodyne
	int iFME;
	// Compteur de buffer pour une nouvelle FME
	int iNbBuffH;
	// Gestionnaire du fichiers wav
	CWaveFile wavefile;
	// Indique si l'hétérodyne est autorisé
	bool bHeterodyne;
	// Indique si le mode auto est demandé
	bool bModeAuto;
	// Indiqe si les fichiers wav sont à produire
	bool bWave;
	// Indique si le Log est à produire
	bool bLog;
	// Indique si le fichier d'activité est à produire
	bool bAct;
	// Gestionnaire d'état du Monitoring
	CMonitStatus *pGestMonitStatus;
	// Mutex de controle d'accès aux échantillons
	pthread_mutex_t *pMutexEchs;
	// Préfixe des fichier wav
	char sPrefWav[15];
	// Durée min d'enregistrement
	float dRecMin;
	// Durée max d'enregistrement
	float dRecMax;
	// Durée d'enregistrement
	float fRecordDuration;
	// Temps courant des échantillons en s (0.0 au démarrage du Monitoring)
	double dTimeEch;
	// Durée en s d'un buffer d'échantillons
	double dDurBuffer;
};

#endif	/* CRECORDER_H */

