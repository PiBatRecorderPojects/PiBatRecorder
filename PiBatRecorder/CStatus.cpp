/* 
 * File:   CStatus.cpp
 * Author: Jean-Do
 * 
 * Created on 10 novembre 2015, 14:40
 */
//-------------------------------------------------------------------------
// Classe de gestion de l'état de Monitoring/Recording de PiBatRecorder,
// Stocke les différents états et permet la gestion des priorités d'accès
// Gère la priorité d'accès entre les threads

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "CStatus.h"

//-------------------------------------------------------------------------
// Constructeur avec initialisation des états aux valeurs par défaut
CMonitStatus::CMonitStatus()
{
}

//-------------------------------------------------------------------------
// Destructeur
CMonitStatus::~CMonitStatus()
{
	InitStatus();
}

//-------------------------------------------------------------------------
// Init aux valeurs par défaut (hors Monitoring)
void CMonitStatus::InitStatus()
{
	// On prend la main sur les données
	pthread_mutex_lock( &mutexDatas);
	// Init des données
	MonitStatus.bDetection		= false;
	MonitStatus.bHeterodyne		= false;
	MonitStatus.bMonitoring		= false;
	MonitStatus.bRecording		= false;
	MonitStatus.bThreshold		= false;
	MonitStatus.iNivMoy			= -200;
	MonitStatus.fRecordDuration = 0.0;
	MonitStatus.iPertAnalyse	= 0;
	MonitStatus.iPertHeterodyne	= 0;
	MonitStatus.lastCry.Dur		= 0;
	MonitStatus.lastCry.FI		= 0;
	MonitStatus.lastCry.FME		= 0;
	MonitStatus.lastCry.FT		= 0;
	MonitStatus.lastCry.NivMax	= -200;
	MonitStatus.lastCry.TDeb	= 0.0;
	MonitStatus.lastCry.TFin	= 0.0;
	strcpy(MonitStatus.lastCry.Struct, "      ");
	// On relache les données
	pthread_mutex_unlock( &mutexDatas);
}

//-------------------------------------------------------------------------
// Copie l'état global dans la structure passée en paramètre
// La copie est protégée par un mutex
void CMonitStatus::GetStatus(
	MonitoringStatus *pStatus	// Structure à initialiser
	)
{
	// On prend la main sur les données
	pthread_mutex_lock( &mutexDatas);
	// Copie de la structure
	memcpy(pStatus, &MonitStatus, sizeof(MonitoringStatus));
	// On relache les données
	pthread_mutex_unlock( &mutexDatas);
}

//-------------------------------------------------------------------------
// Initialisation du Monitoring
// L'init est protégée par un mutex
void CMonitStatus::SetMonitoring(
	bool bMonitoring	// Nouvel état du monitoring
	)
{
	// On prend la main sur les données
	pthread_mutex_lock( &mutexDatas);
	// Initialisation
	//printf("CMonitStatus::SetMonitoring(%d)\n", bMonitoring);
	MonitStatus.bMonitoring = bMonitoring;
	// On relache les données
	pthread_mutex_unlock( &mutexDatas);
}

//-------------------------------------------------------------------------
// Initialisation de l'enregistrement
// L'init est protégée par un mutex
void CMonitStatus::SetRecording(
	bool bRecording	// Nouvel état de l'enregistrement
	)
{
	// On prend la main sur les données
	pthread_mutex_lock( &mutexDatas);
	// Initialisation
	MonitStatus.bRecording = bRecording;
	// RAZ durée d'enregistrement
	MonitStatus.fRecordDuration = 0.0;
	// On relache les données
	pthread_mutex_unlock( &mutexDatas);
}

//-------------------------------------------------------------------------
// Initialisation de l'indication du seuil dépassé
// L'init est protégée par un mutex
void CMonitStatus::SetThreshold(
	bool bThreshold	// Nouvel état du seuil
	)
{
	// On prend la main sur les données
	pthread_mutex_lock( &mutexDatas);
	// Initialisation
	MonitStatus.bThreshold = bThreshold;
	// On relache les données
	pthread_mutex_unlock( &mutexDatas);
}

//-------------------------------------------------------------------------
// Initialisation de l'indication de détection d'un cri
// L'init est protégée par un mutex
void CMonitStatus::SetDetection(
	bool bDetection	// Nouvel état de détection
	)
{
	// On prend la main sur les données
	pthread_mutex_lock( &mutexDatas);
	// Initialisation
	MonitStatus.bDetection = bDetection;
	MonitStatus.bThreshold = bDetection;
	// On relache les données
	pthread_mutex_unlock( &mutexDatas);
}

//-------------------------------------------------------------------------
// Initialisation du niveau de bruit moyen
// L'init est protégée par un mutex
void CMonitStatus::SetNivMoy(
	int iNivMoy	// Nouveau niveau de bruit moyen
	)
{
	// On prend la main sur les données
	pthread_mutex_lock( &mutexDatas);
	// Initialisation
	MonitStatus.iNivMoy = iNivMoy;
	// On relache les données
	pthread_mutex_unlock( &mutexDatas);
}

//-------------------------------------------------------------------------
// Initialisation du nombre de pertes de trames en analyse
// L'init est protégée par un mutex
void CMonitStatus::SetPertAnalyse(
	int iPertAnalyse	// Nouveau nombre de pertes
	)
{
	// On prend la main sur les données
	pthread_mutex_lock( &mutexDatas);
	// Initialisation
	MonitStatus.iPertAnalyse = iPertAnalyse;
	// On relache les données
	pthread_mutex_unlock( &mutexDatas);
}

//-------------------------------------------------------------------------
// Initialisation du nombre de pertes de trames en hétérodyne
// L'init est protégée par un mutex
void CMonitStatus::SetPertHeterodyne(
	int iPertHeterodyne	// Nouveau nombre de pertes
	)
{
	// On prend la main sur les données
	pthread_mutex_lock( &mutexDatas);
	// Initialisation
	MonitStatus.iPertHeterodyne = iPertHeterodyne;
	// On relache les données
	pthread_mutex_unlock( &mutexDatas);
}

//-------------------------------------------------------------------------
// Initialisation du nombre de cris de l'enregistrement courant
// L'init est protégée par un mutex
void CMonitStatus::SetNbCry(
	int iNbCry	// Nouveau nombre
	)
{
	// On prend la main sur les données
	pthread_mutex_lock( &mutexDatas);
	// Initialisation
	MonitStatus.iNbCrys = iNbCry;
	// On relache les données
	pthread_mutex_unlock( &mutexDatas);
}

//-------------------------------------------------------------------------
// Initialisation du dernier cri détecté
// L'init est protégée par un mutex
void CMonitStatus::SetLastCry(
	CryParams *plastCry	// Nouveau cri
	)
{
	// On prend la main sur les données
	pthread_mutex_lock( &mutexDatas);
	// Initialisation
	memcpy( &(MonitStatus.lastCry), plastCry, sizeof(CryParams));
	// On relache les données
	pthread_mutex_unlock( &mutexDatas);
}

//-------------------------------------------------------------------------
// RAZ du dernier cri
// L'init est protégée par un mutex
void CMonitStatus::RAZLastCry()
{
	// On prend la main sur les données
	pthread_mutex_lock( &mutexDatas);
	// Init des données
	MonitStatus.lastCry.Dur		= 0;
	MonitStatus.lastCry.FI		= 0;
	MonitStatus.lastCry.FME		= 0;
	MonitStatus.lastCry.FT		= 0;
	MonitStatus.lastCry.NivMax	= -200;
	MonitStatus.lastCry.TDeb	= 0.0;
	MonitStatus.lastCry.TFin	= 0.0;
	strcpy(MonitStatus.lastCry.Struct, "      ");
	// On relache les données
	pthread_mutex_unlock( &mutexDatas);
}

//-------------------------------------------------------------------------
// Initialisation de la durée d'enregistrement
// L'init est protégée par un mutex
void CMonitStatus::SetRecordTime(
	float fRecordTime	// Durée d'enregistrement
	)
{
	// On prend la main sur les données
	pthread_mutex_lock( &mutexDatas);
	// Initialisation
	MonitStatus.fRecordDuration = fRecordTime;
	// On relache les données
	pthread_mutex_unlock( &mutexDatas);
}

