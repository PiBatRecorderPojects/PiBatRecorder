/* 
 * File:   CStatus.h
 * Author: Jean-Do
 *
 * Created on 10 novembre 2015, 14:40
 */

#include <pthread.h>

#ifndef CSTATUS_H
#define	CSTATUS_H

//-------------------------------------------------------------------------
// Structure de mémorisation des résultats d'analyse
struct CryParams {
	int 	FI;			// Fréquence initiale en kHz
	int		FME;		// Fréquence de maximum énergie en kHz
	int		FT;			// Fréquence terminale en kHz
	int		NivMax;		// Niveau max en dB
	double	TDeb;		// Heure de début du cri en secondes
	double	TFin;		// Heure de fin du cri en secondes
	float	Dur;		// Durée du cri en ms
	char	Struct[10];	// Structure ou nom de l'espèce
};

//-------------------------------------------------------------------------
// Structure des états
struct MonitoringStatus
{
	bool  bMonitoring;		// Indique si on est en Monitoring
	bool  bRecording;		// Indique si on est en enregistrement
	bool  bThreshold;		// Indique si le seuil est dépassé
	bool  bHeterodyne;		// Indique si l'hétérodyne est actif
	bool  bDetection;		// Indique si un cri a été détecté
	int   iNivMoy;			// Niveau moyen du bruit en dB
	float fRecordDuration;	// Durée d'enregistrement
	int   iPertAnalyse;		// Nombre de trames perdues en analyse
	int   iPertHeterodyne;	// Nombre de trames perdues en hétérodyne
	int   iNbCrys;			// Nombre de cris de l'enregistrement courant
	CryParams lastCry;		// Infos sur le dernier cris détecté
};


//-------------------------------------------------------------------------
// Classe de gestion de l'état de Monitoring/Recording de PiBatRecorder,
// Stocke les différents états et permet la gestion des priorités d'accès
// Gère la priorité d'accès entre les threads
class CMonitStatus
{
public:
	//-------------------------------------------------------------------------
	// Constructeur avec initialisation des états aux valeurs par défaut
	CMonitStatus();
	
	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~CMonitStatus();

	//-------------------------------------------------------------------------
	// Init aux valeurs par défaut (hors Monitoring)
	void InitStatus();

	//-------------------------------------------------------------------------
	// Copie l'état global dans la structure passée en paramètre
	// La copie est protégée par un mutex
	void GetStatus(
		MonitoringStatus *pStatus	// Structure à initialiser
		);

	//-------------------------------------------------------------------------
	// Initialisation du Monitoring
	// L'init est protégée par un mutex
	void SetMonitoring(
		bool bMonitoring	// Nouvel état du monitoring
		);

	//-------------------------------------------------------------------------
	// Initialisation de l'enregistrement
	// L'init est protégée par un mutex
	void SetRecording(
		bool bRecording	// Nouvel état de l'enregistrement
		);

	//-------------------------------------------------------------------------
	// Initialisation de l'indication du seuil dépassé
	// L'init est protégée par un mutex
	void SetThreshold(
		bool bThreshold	// Nouvel état du seuil
		);

	//-------------------------------------------------------------------------
	// Initialisation de l'indication de détection d'un cri
	// L'init est protégée par un mutex
	void SetDetection(
		bool bDetection	// Nouvel état de détection
		);

	//-------------------------------------------------------------------------
	// Initialisation du niveau de bruit moyen
	// L'init est protégée par un mutex
	void SetNivMoy(
		int iNivMoy	// Nouveau niveau de bruit moyen
		);

	//-------------------------------------------------------------------------
	// Initialisation du nombre de pertes de trames en analyse
	// L'init est protégée par un mutex
	void SetPertAnalyse(
		int iPertAnalyse	// Nouveau nombre de pertes
		);

	//-------------------------------------------------------------------------
	// Initialisation du nombre de pertes de trames en hétérodyne
	// L'init est protégée par un mutex
	void SetPertHeterodyne(
		int iPertHeterodyne	// Nouveau nombre de pertes
		);

	//-------------------------------------------------------------------------
	// Initialisation du nombre de cris de l'enregistrement courant
	// L'init est protégée par un mutex
	void SetNbCry(
		int iNbCry	// Nouveau nombre
		);

	//-------------------------------------------------------------------------
	// RAZ du dernier cri
	// L'init est protégée par un mutex
	void RAZLastCry();

	//-------------------------------------------------------------------------
	// Initialisation du dernier cri détecté
	// L'init est protégée par un mutex
	void SetLastCry(
		CryParams *plastCry	// Nouveau cri
		);

	//-------------------------------------------------------------------------
	// Initialisation de la durée d'enregistrement
	// L'init est protégée par un mutex
	void SetRecordTime(
		float fRecordTime	// Durée d'enregistrement
		);
	
private:
	// Mémo des états
	MonitoringStatus MonitStatus;
	// Mutex de controle d'accès aux infos
	pthread_mutex_t mutexDatas;
};

#endif	/* CSTATUS_H */

