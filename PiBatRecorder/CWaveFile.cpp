/* 
 * File:   CWaveFile.cpp
 * Author: Jean-Do
 * 
 * Created on 20 août 2015, 10:28
 */
//-------------------------------------------------------------------------
// Classe pour la création d'un fichier wave
// Par défaut de type Mono, 16 bits et 19.200kHz (expention de temps x10)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "LogFile.h"
#include "CWaveFile.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation de la classe)
CWaveFile::CWaveFile()
{
	wavfile = NULL;
	nbEch = 0;
	tEch = 1.0 / 192000.0;
	bWrite = false;
	iDecimation = 1;
}

//-------------------------------------------------------------------------
// Destructeur (fermeture du fichier)
CWaveFile::~CWaveFile()
{
	if (wavfile != NULL)
		// Fermeture du fichier
		CloseWavfile();
}

//-------------------------------------------------------------------------
// Ouverture du fichier en écriture
// Returne true si OK et false en cas d'erreur
bool CWaveFile::OpenWaveFileForWrite(
		const char *filepath,	// Path et nom du fichier à créer
		bool bExp10				// Indique que la Fe est divisée par 10
		)
{
	bool bOK = false;
	if (wavfile != NULL)
		// Fermeture du fichier
		CloseWavfile();
	
	// Préparation de la structure
	int samples_per_second = 19200;
	if (not bExp10)
		samples_per_second = 192000;
	int bits_per_sample = 16;

	strncpy(header.riff_tag,"RIFF",4);
	strncpy(header.wave_tag,"WAVE",4);
	strncpy(header.fmt_tag, "fmt ",4);
	strncpy(header.data_tag,"data",4);

	header.riff_length = 0;
	header.fmt_length = 16;
	header.audio_format = 1;
	header.num_channels = 1;
	header.sample_rate = samples_per_second;
	header.byte_rate = samples_per_second*(bits_per_sample/8);
	header.block_align = bits_per_sample/8;
	header.bits_per_sample = bits_per_sample;
	header.data_length = 0;

	// Ouverture du fichier
	wavfile = fopen( filepath, "w+");
	if (wavfile)
	{
		// Ecriture de l'entête
		fwrite( &header, sizeof(header), 1, wavfile);
		fflush( wavfile);
		bOK = true;
	}
	bWrite = true;
	
	return bOK;
}

//-------------------------------------------------------------------------
// Ouverture du fichier en lecture
// Returne true si OK et false en cas d'erreur
bool CWaveFile::OpenWaveFileForRead(
		const char *filepath,	// Path et nom du fichier à créer
		bool bExpT				// True pour une expension de temps de 10, false pour 1
		)
{
	bool bOK = false;
	if (wavfile != NULL)
		// Fermeture du fichier
		CloseWavfile();
	
	// Ouverture du fichier
	wavfile = fopen( filepath, "r");
	if (wavfile)
	{
		// Lecture de l'entête
		memset(&header, 0, sizeof(wavfile_header));
		int iNbRead = fread( &header, sizeof(wavfile_header), 1, wavfile);
		if (iNbRead == 1)
		{
			bOK = true;
			LogFile::AddLog(LDEBUG, "CWaveFile::OpenWaveFileForRead(%s) Taille ech %d, length %d (%X), rate %dHz, durée %fs\n", filepath,
					header.bits_per_sample, header.data_length/sizeof(short), header.data_length, header.sample_rate,
					(float)((float)(header.data_length/sizeof(short)) / (float)header.sample_rate));
		}
		else
			LogFile::AddLog(LLOG, "CWaveFile::OpenWaveFileForRead(%s) erreur lecture header (%d)\n", filepath, iNbRead);
	}
	else
	{
		LogFile::AddLog(LLOG, "CWaveFile::OpenWaveFileForRead(%s) erreur lecture fichier !\n", filepath);
	}
	bWrite = false;
	bExpTime = bExpT;
	
	return bOK;
}

//-------------------------------------------------------------------------
// Fermeture du fichier
void CWaveFile::CloseWavfile()
{
	if (wavfile)
	{
		if (bWrite)
		{
			// Calcul de la taille
			int file_length = ftell(wavfile);
			// Mise à jour de l'entête
			int data_length = file_length - sizeof(struct wavfile_header);
			fseek( wavfile, sizeof(struct wavfile_header) - sizeof(int), SEEK_SET);
			fwrite(&data_length, sizeof(data_length), 1, wavfile);
			int riff_length = file_length - 8;
			fseek( wavfile, 4, SEEK_SET);
			fwrite( &riff_length, sizeof(riff_length), 1, wavfile);
		}
		// Fermeture du fichier
		fclose(wavfile);
		wavfile = NULL;
		nbEch = 0;
	}
}
	
//-------------------------------------------------------------------------
// Ecriture des échantillons
float CWaveFile::WavfileWrite(short data[], int length )
{
	float dur = 0.0;
	if (wavfile)
	{
		// Ecriture des échantillons dans le fichier
		fwrite( data, sizeof(short), length, wavfile);
		nbEch += (long)length;
		dur = GetRecordDuration();
		//printf("WavfileWrite nbEch %d, tEch %f\n", nbEch, tEch);		
	}
	else
		LogFile::AddLog(LLOG, "CWaveFile::WavfileWrite fichier non initialisé !\n");		
	return dur;
}

//-------------------------------------------------------------------------
// Lecture des échantillons
// Retourne la longueur effectivement lue
long CWaveFile::WavfileRead(
	short *pData,	// Pointeur sur les données à lire
	long length		// Longueur à lire
	)
{
	//printf("CWaveFile::WavfileRead (decim %d, length %d)\n", iDecimation, length);
	long lRead = 0;
	if (wavfile != NULL)
	{
		// Lecture des données
		if (iDecimation == 1)
			lRead = fread( pData, sizeof(short), length, wavfile);
		else if (iDecimation > 1)
		{
			// Lecture de n fois plus de données dans un buffer partiel
			short tmpBuff[length*iDecimation];
			lRead = fread( tmpBuff, sizeof(short), length*iDecimation, wavfile);
			//printf("fread = \n", lRead);
			if (lRead > 0)
			{
				// Init du buffer de sortie
				int j, i;
				for (j=0, i=0; j<lRead; i++, j+=iDecimation)
					pData[i] = tmpBuff[j];
				lRead = i;
			}
		}
	}
	else
		LogFile::AddLog(LLOG, "CWaveFile::WavfileRead fichier non initialisé !\n");		
	//printf("CWaveFile::WavfileRead=%d\n", lRead);
		
	return lRead;
}

//-------------------------------------------------------------------------
// Retourne la durée d'enregistrement en cours en secondes
float CWaveFile::GetRecordDuration()
{
	// Calcul de la durée à partir du nombre d'échantillons et de la durée d'un échantillon
	// Prend en compte la durée réelle sans l'expansion de temps
	// Donc avec 192kHz de fréquence d'échantillonnage
	return (float)nbEch / 192000.0;
}

//-------------------------------------------------------------------------
// Retourne la durée de lecture totale en secondes
float CWaveFile::GetPlayDuration()
{
	// Calcul de la durée à partir du nombre d'échantillons et de la durée d'un échantillon
	// Attention, prend en compte la durée avec ou sans l'expansion de temps
	// Donc avec 19.2kHz ou 192kHz de fréquence d'échantillonnage
	int iNbEch = header.data_length/sizeof(short);
	/*printf("CWaveFile::GetPlayDuration length %d (%X), rate %dHz (%X), durée %fs\n",
			iNbEch, header.data_length, header.sample_rate, header.sample_rate, (float)((float)iNbEch / (float)header.sample_rate));*/
	float fDur = 0.0;
	int rate = header.sample_rate;
	if (bExpTime and rate == 192000)
		rate = 19200;
	else if (!bExpTime and rate == 19200)
		rate = 192000;
	else if ( bExpTime and rate == 384000)
		rate = 38400;
	else if (!bExpTime and rate == 38400)
		rate = 384000;
	fDur = (float)iNbEch / (float)rate;
	//printf("CWaveFile::GetPlayDuration bExpTime %d, rate %dHz, Duration %f\n", bExpTime, rate, fDur);
	return fDur;
}

//-------------------------------------------------------------------------
// Positionne le fichier en lecture à +/- Coef de sa durée totale
// Retourne false si la durée du fichier est dépassée ou inférieure à 0
bool CWaveFile::ReadNext(
	int iNext,	// +1 pour avancer, -1 pour reculer
	int iCoef	// Coefficient d'avancement
	)
{
	long lMaxL = header.data_length / sizeof(short);
	long lNext = lMaxL / iCoef;
	long lNb   = lNext * iNext * sizeof(short) * iDecimation;
	if (wavfile != NULL and fseek( wavfile, lNb, SEEK_CUR) == 0)
		return true;
	return false;
}

//-------------------------------------------------------------------------
// Positionne le fichier en lecture à une durée précise
// Retourne -1 si la durée du fichier est dépassée ou inférieure à 0
// et le nombre d'échantillons lus sinon
long CWaveFile::SetPosRead(
	float fPos	// Durée en secondes de positionnement du curseur
	)
{
	double fDur = GetPlayDuration();
	long lMaxL = header.data_length / sizeof(short);
	double fNext = (double)lMaxL * (double)fPos / fDur;
	long lNb   = (long)fNext * sizeof(short) * iDecimation;
	//printf("CWaveFile::SetPosRead(%f) lNb %d/ lMaxL %d, fDur %f, fNext %d\n", fPos, lNb, lMaxL, fDur, fNext);
	if (wavfile != NULL and fseek( wavfile, lNb, SEEK_CUR) == 0)
		return (long)fNext;
	return -1;
}
	
//-------------------------------------------------------------------------
// Retourne la taille des données du fichier
long CWaveFile::GetDataLength()
{
	return header.data_length;
}

//-------------------------------------------------------------------------
// Initialise le taux de décimation en lecture
void CWaveFile::SetDecimation(
	int iDecim	// Valeurs possibles : 1, 2 et 3
	)
{
	if (iDecim >= 1 and iDecim <= 3)
		iDecimation = iDecim;
}
		

