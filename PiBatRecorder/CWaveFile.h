/* 
 * File:   CWaveFile.h
 * Author: Jean-Do
 *
 * Created on 20 août 2015, 10:28
 */
#include <stdio.h>
#include <inttypes.h>

#ifndef CWAVEFILE_H
#define	CWAVEFILE_H

//-------------------------------------------------------------------------
// Classe pour la création d'un fichier wave
// Par défaut de type Mono, 16 bits et 19.200kHz (expention de temps x10)
class CWaveFile
{
	
	// Entête d'un fichier wav
	struct wavfile_header {
		char	riff_tag[4];
		int		riff_length;
		char	wave_tag[4];
		char	fmt_tag[4];
		int		fmt_length;
		short	audio_format;
		short	num_channels;
		int		sample_rate;
		int		byte_rate;
		short	block_align;
		short	bits_per_sample;
		char	data_tag[4];
		int		data_length;
	};

public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation de la classe)
	CWaveFile();

	//-------------------------------------------------------------------------
	// Destructeur (fermeture du fichier)
	virtual ~CWaveFile();

	//-------------------------------------------------------------------------
	// Ouverture du fichier en écriture
	// Returne true si OK et false en cas d'erreur
	bool OpenWaveFileForWrite(
			const char *filepath,	// Path et nom du fichier à créer
			bool bExp10 = true		// Indique que la Fe est divisée par 10
			);
	
	//-------------------------------------------------------------------------
	// Ouverture du fichier en lecture
	// Returne true si OK et false en cas d'erreur
	bool OpenWaveFileForRead(
			const char *filepath,	// Path et nom du fichier à créer
			bool bExpT				// True pour une expension de temps de 10, false pour 1
			);
	
	//-------------------------------------------------------------------------
	// Fermeture du fichier
	void CloseWavfile();
	
	//-------------------------------------------------------------------------
	// Retourne la fréquence d'échantillonnage en Hz
	int GetSampleRate() {return header.sample_rate;};
	
	//-------------------------------------------------------------------------
	// Ecriture des échantillons
	// Retourne la durée d'enregistrement en secondes
	float WavfileWrite(short data[], int length );
		
	//-------------------------------------------------------------------------
	// Lecture des échantillons
	// Retourne la longueur effectivement lue
	long WavfileRead(
		short *pData,	// Pointeur sur les donnée à lire
		long length		// Longueur à lire
		);

	//-------------------------------------------------------------------------
	// Positionne le fichier en lecture à +/- Coef de sa durée totale
	// Retourne false si la durée du fichier est dépassée ou inférieure à 0
	bool ReadNext(
		int iNext,	// +1 pour avancer, -1 pour reculer
		int iCoef=10	// Coefficient d'avancement
		);
	
	//-------------------------------------------------------------------------
	// Positionne le fichier en lecture à une durée précise
	// Retourne -1 si la durée du fichier est dépassée ou inférieure à 0
	// et le nombre d'échantillons lus sinon
	long SetPosRead(
		float fPos	// Durée en secondes de positionnement du curseur
		);
	
	//-------------------------------------------------------------------------
	// Retourne la durée d'enregistrement en cours en secondes
	float GetRecordDuration();
		
	//-------------------------------------------------------------------------
	// Retourne la durée de lecture totale en secondes
	float GetPlayDuration();
		
	//-------------------------------------------------------------------------
	// Retourne la taille des données du fichier
	long GetDataLength();
		
	//-------------------------------------------------------------------------
	// Initialise le taux de décimation en lecture
	void SetDecimation(
		int iDecim	// Valeurs possibles : 1, 2 et 3
		);
		
private:
	//-------------------------------------------------------------------------
	// Pointeur sur le fichier
	FILE *wavfile;
	
	//-------------------------------------------------------------------------
	// Structure d'entête
	struct wavfile_header header;
	
	//-------------------------------------------------------------------------
	// Nombre d'échantillons du fichier en écriture
	long nbEch;
	
	//-------------------------------------------------------------------------
	// Temps d'un échantillon
	float tEch;
	
	//-------------------------------------------------------------------------
	// Indique une ouverture en écriture
	bool bWrite;

	//-------------------------------------------------------------------------
	// True pour une expension de temps de 10, false pour 1
	bool bExpTime;
	
	//-------------------------------------------------------------------------
	// Indique le taut de décimation pour respecter la fréquence d'échantillonnage de sortie
	// Valeurs possible 1, 2 et 3
	int iDecimation;
};

#endif	/* CWAVEFILE_H */

