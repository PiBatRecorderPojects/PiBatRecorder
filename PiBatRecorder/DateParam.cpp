/* 
 * File:   DateParam.cpp
 * Author: Jean-Do
 * 
 * Created on 4 septembre 2015, 09:51
 */
//-------------------------------------------------------------------------
// Classe de modification de la date système

#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include "CParameters.h"
#include "DateParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
DateParam::DateParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   012345678901234567890
	//                    Date 01/01/2015
	strcpy( indicModif, "000000x10x10xxx100000");
}

//-------------------------------------------------------------------------
// Destructeur
DateParam::~DateParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void DateParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		// Init des min et max du digit et du pointeur sur la valeur à modifier
		int iMin, iMax;
		int *pValue;
		switch (iIndiceModif)
		{
		case 0:
			iMin = 1;
			iMax = GetMaxMonth(iNewM, iNewA);
			pValue = &iNewJ;
			break;
		case 1:
			iMin = 1;
			iMax = 12;
			pValue = &iNewM;
			break;
		case 2:
		default:
			iMin = 2015;
			iMax = 2100;
			pValue = &iNewA;
			break;
		}
		switch (iKey)
		{
		case K_PLUS:
			// On incrémente le digit
			(*pValue)++;
			if (*pValue > iMax)
				*pValue = iMin;
			break;
		case K_MINUS:
			// On décrémente le digit
			(*pValue)--;
			if (*pValue < iMin)
				*pValue = iMax;
			break;
		case K_R:
			// On incrémente de 1/10 le digit
			(*pValue)+= (iMax-iMin+1)/10;
			if (*pValue > iMax)
				*pValue = iMin;
			break;
		case K_L:
			// On décrémente de 1/10 le digit
			(*pValue)-= (iMax-iMin+1)/10;
			if (*pValue < iMin)
				*pValue = iMax;
			break;
		case K_SQUARE:
			// Init de la valeur min
			*pValue = iMin;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			*pValue = iMax;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void DateParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	// Préparation de la ligne
	//                   012345678901234567890
	//                    Date 01/01/2015
	if (!bModif)
	{
		// Affichage de la date courante
		time_t curtime;
		struct tm * timeinfo;
		time(&curtime);
		timeinfo = localtime (&curtime);
		//                             123456789012345678901
		strftime(lineParam, MAX_LINEPARAM, " Date %d/%m/%Y", timeinfo);
		sscanf(lineParam, " Date %d/%d/%d", &iNewJ, &iNewM, &iNewA);
	}
	else
	{
		// Affichage de la date en cours de modif avec en inverse la zone du curseur
		// 012345678901234567890
		// Date 01/01/2015
		sprintf(lineParam, " Date %02d/%02d/%04d", iNewJ, iNewM, iNewA);
	}
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void DateParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	char temp[MAX_LINEPARAM];
	time_t curtime;
	struct tm * timeinfo;
	time(&curtime);
	timeinfo = localtime (&curtime);
	strftime(temp, MAX_LINEPARAM, "%d/%m/%Y", timeinfo);
	sscanf(temp, "%d/%d/%d", &iNewJ, &iNewM, &iNewA);
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void DateParam::SetParam()
{
	if (bModif)
	{
		// Vérification du paramètre
		bool bOK = true;
		if (iNewJ < 1 or iNewJ > GetMaxMonth(iNewM, iNewA))
			bOK = false;
		else if (iNewM < 1 or iNewM > 12)
			bOK = false;
		else if (iNewA < 2015 or iNewA > 2100)
			bOK = false;
		if (bOK)
		{
			// Mise à la date du système
			// ATTENTION, il faut mettre aussi l'heure sinon l'heure est mise à 00:00:00
			time_t curtime;
			struct tm * timeinfo;
			time(&curtime);
			timeinfo = localtime (&curtime);
			char sHeure[80];
			strftime(sHeure, 80, "%H:%M:%S", timeinfo);
			// sudo date --set="MM/JJ/AAAA HH:MM:SS"
			char sCmd[160];
			sprintf(sCmd, "sudo date --set=\"%02d/%02d/%04d %s\"", iNewM, iNewJ, iNewA, sHeure);
			system(sCmd);
			// Mise à jour de l'horloge
			system("sudo hwclock -w");
		}
		bModif = false;
	}
	// Appel de la méthode parente
	GenericParam::SetParam();
}

//-------------------------------------------------------------------------
// Donne le max d'un mois
int DateParam::GetMaxMonth( int iMois, int Annee)
{
	int iMax = 31;
	if (iMois == 2)
	{
		if (IsBisextile(Annee))
			iMax = 29;
		else
			iMax = 28;
	}
	else if (iMois == 4 or iMois == 6 or iMois == 9 or iMois == 11)
		iMax = 30;
	else
		iMax = 31;
	return iMax;
}

//-------------------------------------------------------------------------
// Indique si une année est bisextile
bool DateParam::IsBisextile( int Annee)
{
    // 1 - Si l'année est divisible par 4, passez à l'étape 2, sinon passez à l'étape 5.
    // 2 - Si l'année est divisible par 100, passez à l'étape 3. Sinon, passez à l'étape 4.
    // 3 - Si l'année est divisible par 400, passez à l'étape 4. Sinon, passez à l'étape 5.
    // 4 - L'année est une année bissextile (elle a 366 jours).
    // 5 - L'année n'est pas une année bissextile (elle a 365 jours).
	bool bisextile = false;
	if ((Annee % 4) == 0)
	{
		if ((Annee % 100) == 0)
		{
			if ((Annee % 400) == 0)
				bisextile = true;
			else
				bisextile = false;
		}
		else
			bisextile = true;
	}
	else
		bisextile = false;
	return bisextile;
}

