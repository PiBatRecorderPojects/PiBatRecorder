/* 
 * File:   DateParam.h
 * Author: Jean-Do
 *
 * Created on 4 septembre 2015, 09:51
 */
#include "GenericParam.h"

#ifndef DATEPARAM_H
#define	DATEPARAM_H

//-------------------------------------------------------------------------
// Classe de modification de la date système
class DateParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	DateParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~DateParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	virtual void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

protected:
	//-------------------------------------------------------------------------
	// Indique si une année est bisextile
	bool IsBisextile( int Annee);
	
	//-------------------------------------------------------------------------
	// Donne le max d'un mois
	int GetMaxMonth( int iMois, int Annee);
	
private:
	// Mémo de la valeur courante
	int iNewJ, iNewM, iNewA;
};

#endif	/* DATEPARAM_H */

