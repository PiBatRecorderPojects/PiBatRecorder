/* 
 * File:   DecHeterParam.cpp
 * Author: Jean-Do
 * 
 * Created on 3 septembre 2015, 19:17
 */
//-------------------------------------------------------------------------
// Classe de modification du décalage hétérodyne (100 à 2000Hz par pas de 100Hz)

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "DecHeterParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
DecHeterParam::DecHeterParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   012345678901234567890
	//                    Decalage Het. 2000Hz
	//                    Hetero. shift 2000Hz
	strcpy( indicModif, "000000000000000xxx100");
	// Mémo de la valeur courante
	iOldDec = pParams->GetDecHeter();
	iNewDec = iOldDec;
}

//-------------------------------------------------------------------------
// Destructeur
DecHeterParam::~DecHeterParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void DecHeterParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_PLUS:
			// On incrémente la valeur de 10Hz
			iNewDec += 10;
			if (iNewDec > 2000)
				iNewDec = 10;
			break;
		case K_MINUS:
			// On décrémente la valeur de 10Hz
			iNewDec -= 10;
			if (iNewDec < 10)
				iNewDec = 2000;
			break;
		case K_R:
			// On incrémente la valeur de 200Hz
			iNewDec += 200;
			if (iNewDec > 2000)
				iNewDec = 200;
			break;
		case K_L:
			// On décrémente la valeur de 200Hz
			iNewDec -= 200;
			if (iNewDec < 10)
				iNewDec = 2000;
			break;
		case K_SQUARE:
			// Init de la valeur min
			iNewDec = 10;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			iNewDec = 2000;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			iNewDec = iOldDec;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void DecHeterParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
		iNewDec = pParams->GetDecHeter();
	//                      012345678901234567890
	//                       Décalage Het. 2000Hz
	//			             Hetero. shift 2000Hz
	if (pParams->GetLangue() == FR)
		sprintf(lineParam, " D%ccalage Het. %04dHz", 0x82, iNewDec);
	else
		sprintf(lineParam, " Hetero. shift %04dHz", iNewDec);
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void DecHeterParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	iOldDec = pParams->GetDecHeter();
	iNewDec = iOldDec;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void DecHeterParam::SetParam()
{
	// Vérification du paramètre
	if (iNewDec < 10 or iNewDec > 2000)
		// Valeur précédente
		iNewDec = iOldDec;
	pParams->SetDecHeter(iNewDec);
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}

