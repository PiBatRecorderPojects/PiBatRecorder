/* 
 * File:   DecHeterParam.h
 * Author: Jean-Do
 *
 * Created on 3 septembre 2015, 19:17
 */
#include "GenericParam.h"

#ifndef DECHETERPARAM_H
#define	DECHETERPARAM_H

//-------------------------------------------------------------------------
// Classe de modification du décalage hétérodyne (100 à 2000Hz par pas de 100Hz)
class DecHeterParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	DecHeterParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~DecHeterParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Mémo de la valeur avant modif
	int iOldDec;
	// Mémo de la valeur courante
	int iNewDec;
};

#endif	/* DECHETERPARAM_H */

