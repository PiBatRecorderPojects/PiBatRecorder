// **** Fichier de configuration de PiBatRecorder
// Niveau du fichier log de debug (/home/pi/PiBatRecorder/PiBatLog.txt)
//LogLevel=Log					Niveau minimal LOG par défaut (infos de démarrage et erreurs)
//LogLevel=INFO					Niveau avec des informations suplémentaires (paramètres de fonctionnement)
//LogLevel=DEBUG				Niveau pour debugger un problème (attention, ce nievau peut avoir des conséquence sur le fonctionnement temps réel)
//LogLevel=REALTIME				Niveau pour débugger un problème temps réel mais les traces peuvent changer le comportement du logiciel
LogLevel=DEBUG
// **** Pour sortir les traces du Log aussi sur la console (NO par défaut, YES pour des traces)
LogTerminal=YES
// **** Carte audio Cirrus Logic Wolfson
Audio=WOLFSON
// **** Type de clavier
//Keyboard=TTP229I2C16KEYS		Sensitif 16 touches TTP229
//        =NOKEYBOARD			Pas de clavier
Keyboard=TTP229I2C16KEYS
// **** Type d'écran
//Screen=OLEDSH1106I2C128x64	Ecran OLED 128x64 pixels I2C avec controleur SH1106
//       =OLEDSSD1306I2C128x64	Ecran OLED 128x64 pixels I2C avec controleur SSD1306
//       =NOSCREEN				Pas d'écran
Screen=OLEDSH1106I2C128x64
// **** Type de micro
//MikeInput=HEADSET				Micro mono sur entrée casque
//         =LINEMONO			Micro mono sur entrée ligne
//         =LINESTEREO			Micros stéréo sur entrée ligne
MikeInput=HEADSET
// **** Type d'horloge
//RTC=RTCI2C					Horloge Temps réel de type I2C
//   =WEB						Horloge via Internet
//   =NORTC						Pas d'horloge
RTC=RTCI2C
// **** Répertoire des commandes sh de la carte Wolson
WolfsonShPath=/home/pi/
// **** Répertoire racine de sauvegarde des fichiers sur clé USB
USBKeyFilePath=/mnt/usbkey/
// **** Répertoire racine de sauvegarde des fichiers sur la carte SD
LocalFilepath=/home/pi/pibat/
// **** Répertoire de sauvegarde des fichiers wav (vide)
WavDir=
// **** Répertoire de sauvegarde des fichiers log (vide)
LogDir=
//      ******** Paramètres spécifiques PiBatScheduler
// **** Gestion de l'extinction
//ShutDown=REBOOT				Gestion de l'extinction avec REBOOT, reboot journalier effectué en fin de période d'enregistrement
//        =AUTO					Shutdown et réveil automatique effectué de façon externe, halt effectué en fin de période d'enregistrement
ShutDown=REBOOT
// **** Ligne de commande de lancement du logiciel d'enregistrement (le répertoire est forcément /home/pi/PiBatRecorder)
Recorder=pibatrecorder -a
