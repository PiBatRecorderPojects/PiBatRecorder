/* 
 * File:   GenericMode.cpp
 * Author: Jean-Do
 * 
 * Created on 10 décembre 2015, 11:41
 */

#include "GenericMode.h"
#include <ctime>

/*-----------------------------------------------------------------------------
 Classe générique de gestion d'un mode de fonctionnement
 Définie l'interface minimale de gestion d'un mode de fonctionnement
-----------------------------------------------------------------------------*/

// Mémorise la dernière touche frappées
// (commune à l'ensemble des modes de fonctionnement)
int GenericMode::iLastKey = K_NO;

//-------------------------------------------------------------------------
// Constructeur
GenericMode::GenericMode(
	CParameters *pPar,					// Pointeur sur les paramètres
	KeyOledManager *pKeyScreenManager,	// Pointeur sur le gestionnaire du clavier et de l'écran
	bool bAutoMode						// True si le logiciel fonctionne entièrement en mode automatique
										// Si oui, le clavier et l'écran ne sont pas gérés
	)
{
	pParams = pPar;
	pKeyOledManager = pKeyScreenManager;
	mode = NOMODE;
	bModeAuto = bAutoMode;
}

//-------------------------------------------------------------------------
// Destructeur
GenericMode::~GenericMode()
{
}

//-------------------------------------------------------------------------
// Début du mode
void GenericMode::BeginMode()
{
	// Effacement de l'écran
	pKeyOledManager->ClearScreen();
}

//-------------------------------------------------------------------------
// Fin du mode
void GenericMode::EndMode()
{
}
	
//-------------------------------------------------------------------------
// Affichage du mode sur l'écran
// Cette méthode est appelée régulièrement par la classe PiBatManager
// à charge des différents modes d'afficher les informations nécessaires
void GenericMode::PrintMode()
{
	if (!bModeAuto)
	{
		// De base, affichage régulier de la date et de l'heure sur la dernière ligne
		time_t curtime;
		struct tm * timeinfo;
		char sTime[22];
		time(&curtime);
		timeinfo = localtime (&curtime);
		strftime(sTime, 22, "%d/%m/%Y - %H:%M:%S", timeinfo);
		pKeyOledManager->DrawString(sTime, 0, L8T1, true, 1);
		pKeyOledManager->DisplayScreen();
	}
}

//-------------------------------------------------------------------------
// Traitement des ordres claviers
// Retourne éventuellement un mode demandé (NOMODE sinon)
// Cette méthode est appelée régulièrement par la classe PiBatManager
// à charge des différents modes de traiter les actions opérateurs
int GenericMode::ManageKey(
	unsigned short key	// Touche sollicitée par l'opérateur
	)
{
	MODEFONC newMode = NOMODE;
	// Pas de répétition de touche pour le changement de mode
	if (key != iLastKey)
	{
		switch (key)
		{
		case K_ON:
			printf("GenericMode::ManageKey got to QUIT\n");
			newMode = QUIT; 
			break;
		case K_RASP:
			printf("GenericMode::ManageKey got to MONITORING\n");
			newMode = MONITORING; 
			break;
		case K_WAVE:
			printf("GenericMode::ManageKey got to PLAY\n");
			newMode = PLAY; 
			break;
		case K_HOUSE:
			printf("GenericMode::ManageKey got to MENU\n");
			newMode = MENU; 
			break;
		}
	}
	iLastKey = key;
	return newMode;
}

//-------------------------------------------------------------------------
// Complète la ligne avec des blancs pour faire 21 caractères
void GenericMode::FinalLine(
	char *pLine	// Ligne à complèter
	)
{
	while (strlen(pLine) < 21)
		strcat(pLine, " ");
}

