/* 
 * File:   GenericMode.h
 * Author: Jean-Do
 *
 * Created on 10 décembre 2015, 11:40
 */
#include "KeyOledManager.h"
#include "CParameters.h"

#ifndef GENERICMODE_H
#define	GENERICMODE_H

// Modes de fonctionnement
enum MODEFONC {
	NOMODE,		// Mode indéterminé
	INIT,		// Mode d'initialisation (au démarrage du logiciel)
	MENU,		// Mode d'affichage du menu (accessible via la touche Maison)
	MONITORING,	// Mode d'écoute et éventuellement enregistrement (accessible via la touche Framboise)
	PLAY,		// Mode d'écoute d'un fichier enregistré (accessible via a touche Wawe)
	QUIT,       // Mode de demande de sortie (accessible via la touche ON/OFF)
	END,		// Mode de sortie du logiciel (suite au mode Quit)
	ERROR,		// Mode erreur (sur détection d'une erreur)
	EXIT		// Indique la sortie du logiciel
};

/*-----------------------------------------------------------------------------
 Classe générique de gestion d'un mode de fonctionnement
 Définie l'interface minimale de gestion d'un mode de fonctionnement
-----------------------------------------------------------------------------*/
class GenericMode
{
public:
	//-------------------------------------------------------------------------
	// Constructeur
	GenericMode(
		CParameters *pPar,					// Pointeur sur les paramètres
		KeyOledManager *pKeyScreenManager,	// Pointeur sur le gestionnaire du clavier et de l'écran
		bool bAutoMode=false				// True si le logiciel fonctionne entièrement en mode automatique
			                                // Si oui, le clavier et l'écran ne sont pas gérés
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~GenericMode();
	
	//-------------------------------------------------------------------------
	// Début du mode
	virtual void BeginMode();
	
	//-------------------------------------------------------------------------
	// Fin du mode
	virtual void EndMode();
	
	//-------------------------------------------------------------------------
	// Affichage du mode sur l'écran
	// Cette méthode est appelée régulièrement par la classe PiBatManager
	// à charge des différents modes d'afficher les informations nécessaires
	virtual void PrintMode();
	
	//-------------------------------------------------------------------------
	// Traitement des ordres claviers
	// Si la touche est une touche de changement de mode, retourne le mode demandé
	// Cette méthode est appelée régulièrement par la classe PiBatManager
	// à charge des différents modes de traiter les actions opérateurs
	virtual int ManageKey(
		unsigned short key	// Touche sollicitée par l'opérateur
		);
	
	//-------------------------------------------------------------------------
	// Complète la ligne avec des blancs pour faire 21 caractères
	void FinalLine(
		char *pLine	// Ligne à complèter
		);

	//-------------------------------------------------------------------------
	// Retourne true en mode auto
	bool GetModeAuto() { return bModeAuto;};
	
	//-------------------------------------------------------------------------
	// Initialise le mode auto
	bool SetModeAuto(
		bool bAutoMode	// Nouvelle valeur pour le mode auto
		) { bModeAuto = bAutoMode;};
	
protected:
	// Pointeur sur la mémorisation des paramètres
	CParameters *pParams;

	// Pointeur sur le gestionnaire du clavier et de l'écran
	KeyOledManager *pKeyOledManager;
	
	// Mode courant
	MODEFONC mode;
	
	// Indique si le fonctionnement est en mode automatique
	bool bModeAuto;
	
	// Mémorise la dernière touche frappées
	// (commune à l'ensemble des modes de fonctionnement)
	static int iLastKey;
};

#endif	/* GENERICMODE_H */

