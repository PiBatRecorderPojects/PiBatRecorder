/* 
 * File:   GenericParam.cpp
 * Author: Jean-Do
 * 
 * Created on 1 septembre 2015, 18:21
 */
//-------------------------------------------------------------------------
// Classe générique de gestion des modification des paramètres

#include "GenericParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
GenericParam::GenericParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
{
	pKeyOled = pKOled;
	pParams  = pPar;
	bModif   = false;
	iCurpos  = 0;
	iIndiceModif = 0;
	bValid = true;
	strcpy( lineParam, "");
	// Chaine de description des zones modifiables
	// 0 indique une zone non modifiable
	// x indique une zone modifiable
	// 1 indique le position du curseur pour une zone modifiable
	// 1 est réservé à la partie droite d'un chiffre à modifier ou pour chaque digit à modifier
	//                   123456789012345678901
	strcpy( indicModif, "000000000000000000000");
}

//-------------------------------------------------------------------------
// Destructeur
GenericParam::~GenericParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void GenericParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	bool bTrouve = false;
	switch (iKey)
	{
	case K_RIGHT:
		printf("GenericParam::ReceiveKey K_RIGHT\n");
		// On passe à la case modifiable suivante
		if (ilstDeb[iIndiceModif+1] != -1)
		{
			iIndiceModif++;
			iCurpos = ilstDeb[iIndiceModif];
		}
		break;
	case K_LEFT:
		printf("GenericParam::ReceiveKey K_LEFT\n");
		// On passe à la case modifiable précédente
		if (iIndiceModif > 0)
		{
			iIndiceModif--;
			iCurpos = ilstDeb[iIndiceModif];
		}
		break;
	case K_TRIANGLE:
		printf("GenericParam::ReceiveKey K_TRIANGLE\n");
		// Valide le paramètre
		SetParam();
		break;
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void GenericParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	// Finalise la ligne avec des blancs
	FinalLine();
	// Si ligne active, on met une flêche en début de ligne
	if (bActive)
		lineParam[0] = 0x10;
	if (!bModif or iIndiceModif < 0 or ilstDeb[iIndiceModif] == -1)
		// Affichage de la ligne sans partie inversée
		pKeyOled->DrawString( lineParam, 0, iLine);
	else
	{
		// Affichage avec la partie en modif inversée
		// Init des portions
		char tmp[MAX_LINEPARAM+1];
		int iD, iF, y;
		iD = ilstDeb[iIndiceModif];
		iF = ilstFin[iIndiceModif];
		// Affichage du début en normal
		strncpy(tmp, lineParam, iD);
		tmp[iD] = 0;
		pKeyOled->DrawString( tmp, 0, iLine);
		//printf("P1 [%s]", tmp);
		y = strlen(tmp) * 6;
		// Affichage de la partie en modification en inversé
		strncpy(tmp, &lineParam[iD], iF-iD+1);
		tmp[iF-iD+1] = 0;
		pKeyOled->DrawString( tmp, y, iLine, false);
		//printf(", P2 [%s]", tmp);
		y = y + (strlen(tmp) * 6);
		// Affichage de la fin en normal
		if (strlen(lineParam)-iF-1 > 0)
		{
			strncpy(tmp, &lineParam[iF+1], strlen(lineParam)-iF-1);
			tmp[strlen(lineParam)-iF-1] = 0;
			pKeyOled->DrawString( tmp, y, iLine);
			//printf(", P3 [%s]", tmp);
		}
		//printf("\n");
	}
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void GenericParam::StartModif()
{
	bModif = true;
}

//-------------------------------------------------------------------------
// Stoppe la modification du paramètre
void GenericParam::StopModif()
{
	bModif = false;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void GenericParam::SetParam()
{
	// Réinit sans modif
	StopModif();
	// Mémo des paramètres
	pParams->WriteParams();
}

//-------------------------------------------------------------------------
// Complète la igne avec de sblanc pour faire 21 caractères
void GenericParam::FinalLine()
{
	while (strlen(lineParam) < MAX_LINEPARAM)
		strcat(lineParam, " ");
}

//-------------------------------------------------------------------------
// Calcul des débuts et fins de szonesd à modifier
void GenericParam::FindBeginEndModif()
{
	for (int i=0; i<MAX_MODIF; i++)
	{
		ilstDeb[i] = -1;
		ilstFin[i] = -1;
	}
	bool bZone = false;
	bool bUn   = false;
	// "0000000000000001xxxxx"
	// "000000000011111000000"
	// "00000000000000000x100"
	// "0000000000000000x10x1"
	for (int i=0, j=-1; i<strlen(indicModif); i++)
	{
		if (indicModif[i] != '0')
		{
			if (!bZone)
			{
				// Nouvelle zone de modif
				j++;
				ilstDeb[j] = i;
				ilstFin[j] = i;
				bZone = true;
			}
			if (indicModif[i] == '1')
			{
				if (!bUn)
					// Un trouvé
					bUn = true;
				else
				{
					// Nouvelle zone de modif
					j++;
					ilstDeb[j] = i;
					ilstFin[j] = i;
					bZone = true;
				}
			}
			ilstFin[j] = i;
		}
		else
		{
			// Fin éventuelle d'une zone de modif
			bZone = false;
			bUn   = false;
		}
	}
}

