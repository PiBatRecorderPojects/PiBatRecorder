/* 
 * File:   GenericParam.h
 * Author: Jean-Do
 *
 * Created on 1 septembre 2015, 18:21
 */
#include "KeyOledManager.h"
#include "CParameters.h"

#ifndef GENERICPARAM_H
#define	GENERICPARAM_H

// Nombre max de caractère d'une ligne
#define MAX_LINEPARAM 21
// Nombre max de zones modifiable pour une ligne
#define MAX_MODIF	  15

//-------------------------------------------------------------------------
// Classe générique de gestion des modification des paramètres
class GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	GenericParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~GenericParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	virtual void StartModif();

	//-------------------------------------------------------------------------
	// Stoppe la modification du paramètre
	virtual void StopModif();

	//-------------------------------------------------------------------------
	// Retourne True si la modif est en cours
	bool IsModif() {return bModif;};
	
	//-------------------------------------------------------------------------
	// Complète la ligne avec de sblanc pour faire 21 caractères
	void FinalLine();

	//-------------------------------------------------------------------------
	// Calcul des débuts et fins de szonesd à modifier
	void FindBeginEndModif();
	
	//-------------------------------------------------------------------------
	// Retourne true si le paramètre est valide
	bool GetbValid() {return bValid;};

	//-------------------------------------------------------------------------
	// Initialise la validité du paramètre
	void SetbValid( bool bVal) {bValid = bVal;};

protected:
	// Pointeur sur la mémorisation des paramètres
	CParameters *pParams;

	// Gestionnaire clavier/LCD
	KeyOledManager *pKeyOled;

	// Indique si le menu est en cours de modification
	bool bModif;
	
	// Position du curseur de modification (-1 pas de curseur)
	int iCurpos;
	
	// Ligne d'affichage du paramètre
	char lineParam[MAX_LINEPARAM+1];
	
	// Indicateur des caratères modifiables sur la ligne
	// 0 = non modifiable, x ou 1 = modifiable 1 indiquant le curseur
	char indicModif[MAX_LINEPARAM+1];
	
	// Indice de début des zones modifiables
	int ilstDeb[MAX_MODIF];
	
	// Indices de fin des zones modifiables
	int ilstFin[MAX_MODIF];
	
	// Indice de la zone à modifier
	int iIndiceModif;
	
	// Indique si le paramètres est valide ou non
	bool bValid;
};

#endif	/* GENERICPARAM_H */

