/* 
 * File:   HourParam.cpp
 * Author: Jean-Do
 * 
 * Created on 4 septembre 2015, 10:39
 */
//-------------------------------------------------------------------------
// Classe de modification de l'heure système

#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include "CParameters.h"
#include "HourParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
HourParam::HourParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Heure 12:28:36
	//                    Hour  12:38:36
	strcpy( indicModif, "0000000x10x10x1000000");
}

//-------------------------------------------------------------------------
// Destructeur
HourParam::~HourParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void HourParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		// Init des min et max du digit et du pointeur sur la valeur à modifier
		int iMin, iMax;
		int *pValue;
		switch (iIndiceModif)
		{
		case 0:
			iMin = 0;
			iMax = 23;
			pValue = &iNewH;
			break;
		case 1:
			iMin = 0;
			iMax = 59;
			pValue = &iNewM;
			break;
		case 2:
		default:
			iMin = 0;
			iMax = 59;
			pValue = &iNewS;
			break;
		}
		switch (iKey)
		{
		case K_PLUS:
			// On incrémente le digit
			(*pValue)++;
			if (*pValue > iMax)
				*pValue = iMin;
			break;
		case K_MINUS:
			// On décrémente le digit
			(*pValue)--;
			if (*pValue < iMin)
				*pValue = iMax;
			break;
		case K_R:
			// On incrémente de 1/10 le digit
			(*pValue)+= (iMax-iMin+1)/10;
			if (*pValue > iMax)
				*pValue = iMin;
			break;
		case K_L:
			// On décrémente de 1/10 le digit
			(*pValue)-= (iMax-iMin+1)/10;
			if (*pValue < iMin)
				*pValue = iMax;
			break;
		case K_SQUARE:
			// Init de la valeur min
			*pValue = iMin;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			*pValue = iMax;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void HourParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	// Affichage du menu
	//                   123456789012345678901
	//                   Heure 12:28:36
	//                   Hour  12:38:36
	if (!bModif)
	{
		// Affichage de la date courante
		time_t curtime;
		struct tm * timeinfo;
		time(&curtime);
		timeinfo = localtime (&curtime);
		if (pParams->GetLangue() == FR)
			strftime(lineParam, MAX_LINEPARAM, " Heure %H:%M:%S", timeinfo);
		else
			strftime(lineParam, MAX_LINEPARAM, " Hour  %H:%M:%S", timeinfo);
		sscanf(lineParam, "Heure %d:%d:%d", &iNewH, &iNewM, &iNewS);
	}
	else
	{
		// Affichage de la date en cours de modif avec en inverse la zone du curseur
		char tmp[MAX_LINEPARAM+1];
		int iD, iF, y;
		if (pParams->GetLangue() == FR)
			sprintf(lineParam, " Heure %02d:%02d:%02d", iNewH, iNewM, iNewS);
		else
			sprintf(lineParam, " Hour  %02d:%02d:%02d", iNewH, iNewM, iNewS);
	}
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void HourParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	char temp[22];
	time_t curtime;
	struct tm * timeinfo;
	time(&curtime);
	timeinfo = localtime (&curtime);
	strftime(temp, MAX_LINEPARAM, "%H:%M:%S", timeinfo);
	sscanf(temp, "%d:%d:%d", &iNewH, &iNewM, &iNewS);
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void HourParam::SetParam()
{
	// Vérification du paramètre
	bool bOK = true;
	if (iNewH < 0 or iNewH > 23)
		bOK = false;
	else if (iNewM < 0 or iNewM > 59)
		bOK = false;
	else if (iNewS < 0 or iNewS > 59)
		bOK = false;
	if (bOK)
	{
		// Mise à la date du système
		// sudo date -s HH:MM:SS
		char sCmd[160];
		sprintf(sCmd, "sudo date -s %02d:%02d:%02d", iNewH, iNewM, iNewS);
		system(sCmd);
		// Mise à jour de l'horloge
		system("sudo hwclock -w");
	}
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}

