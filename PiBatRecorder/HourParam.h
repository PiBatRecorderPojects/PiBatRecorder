/* 
 * File:   HourParam.h
 * Author: Jean-Do
 *
 * Created on 4 septembre 2015, 10:39
 */
#include "GenericParam.h"

#ifndef HOURPARAM_H
#define	HOURPARAM_H

//-------------------------------------------------------------------------
// Classe de modification de l'heure système
class HourParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	HourParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~HourParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	virtual void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Mémo de la valeur courante
	int iNewH, iNewM, iNewS;
};

#endif	/* HOURPARAM_H */

