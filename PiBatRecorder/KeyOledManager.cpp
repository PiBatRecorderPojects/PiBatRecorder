/* 
 * File:   KeyOledManager.cpp
 * Author: Jean-Do
 * 
 * Created on 9 décembre 2015, 21:24
 */

#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include "KeyOledManager.h"
#include "CParameters.h"

/*-----------------------------------------------------------------------------
 Classe de gestion du clavier et de l'afficheur LCD
 Le clavier est de type sensitif 16 touches TTP229
	Achat sur http://fr.aliexpress.com/item/Raspberry-Pi-Model-B-B-TTP229-LSF-Detector-Controller-Capacitive-Touch-Keypad-Supports-Up-To-16/32244765970.html?ws_ab_test=searchweb201556_3_79_78_77_91_80,searchweb201644_5,searchweb201560_9
	Constructeur sur http://www.waveshare.com/product/RPi-Touch-Keypad.htm
 L'écran est de type OLED 128x64 SSD1106 ou SSD1306 (0,96" ou 1,26")
	Achat sur http://fr.aliexpress.com/item/1-3-Inch-white-I2C-IIC-OLED-LCD-Module-Serial-128X64-LED-Display-Modules-for-Arduino/32445305003.html
-----------------------------------------------------------------------------*/

//-------------------------------------------------------------------------
// Constructeur
KeyOledManager::KeyOledManager()
{
	// Par défaut, pas d'écran ni clavier
	bKeyboardOK = false;
	bScreenOK = false;
}

//-------------------------------------------------------------------------
// Destructeur
KeyOledManager::~KeyOledManager()
{
	if (bKeyboardOK)
		// Fermeture du fichier d'accès au clavier
		close(KeyboardFfile);
	
	if (bScreenOK)
		// Fermeture de l'écran
		screen.close();
}

//-------------------------------------------------------------------------
// Initialisation de l'écran et du clavier
void KeyOledManager::InitScreenAndKeyboard(
	int iScreenType,	// Type de l'écran (OLEDSH1106I2C128x64, OLEDSSD1306I2C128x64 ou NOSCREEN)
	int iKeyboardType	// Type du clavier (TTP229I2C16KEYS ou NOKEYBOARD)
	)
{
	// Initialisation de l'écran
	bScreenOK = true;
	switch(iScreenType)
	{
	case OLEDSH1106I2C128x64:
		// Ecran de type SH1106
		printf("Ecran de type SH1106\n");
		if ( !screen.init(OLED_I2C_RESET, OLED_SH1106_I2C_128x64) )
		{
			printf("KeyOledManager, impossible d'initialiser un écran de type OLEDSH1106_I2C128x64\n");
			bScreenOK = false;
		}
		break;
	case OLEDSSD1306I2C128x64:
		// Ecran de type SSD1306
		printf("Ecran de type SSD1306\n");
		if ( !screen.init(OLED_I2C_RESET, OLED_ADAFRUIT_I2C_128x64) )
		{
			printf("KeyOledManager, impossible d'initialiser un écran de type OLEDSH1106_I2C128x64\n");
			bScreenOK = false;
		}
		break;
	case NOSCREEN:
		// Pas d'écran
		printf("Pas d'écran\n");
		bScreenOK = false;
		break;
	default:
		printf("KeyOledManager, type d'écran non supporté !\n");
		bScreenOK = false;
	}
	if (bScreenOK)
		screen.begin();

	// Initialisation du clavier
	bKeyboardOK = true;
    const char *devName = "/dev/i2c-1";
	switch (iKeyboardType)
	{
	case TTP229I2C16KEYS:
		// Ouverture du fichier d'accès au clavier (bus I2C)
		printf("Clavier de type TTP229I2C16KEYS\n");
		KeyboardFfile = open(devName, O_RDWR);
		if (KeyboardFfile == -1)
		{
			printf("KeyOledManager, ouverture %s impossible !\n", devName);
			bKeyboardOK = false;
		}
		// Init de l'adresse d'accès au clavier
		if (bKeyboardOK and ioctl(KeyboardFfile, I2C_SLAVE, 0x57) < 0)
		{
			printf("KeyOledManager, impossible de lire l'adresse 0x57 sur le bus I2C\n");
			bKeyboardOK = false;
		}
		break;
	case NOKEYBOARD:
		printf("Pas de clavier\n");
		bKeyboardOK = false;
		break;
	default:
		printf("Type de clavier incompatible !\n");
		bKeyboardOK = false;
	}
}

//-------------------------------------------------------------------------
// Gestion de l'écran
// Toutes les fonctions d'écriture sur l'écran ne font que mettre à jour
// une zone mémoire sans effet direct sur l'écran.
// Seules les fonctions
// - ClearScreen (effacement de la zone mémoire et affichage à l'écran)
// - DisplayScreen (affichage de la zone mémoire sur l'écran)
// Affectent vraiment l'état de l'écran
// En appelant plusieurs fonctions d'écriture dans la zone mémoire
// il est possible de superposer plusieurs éléments sur les mêmes pixels

//-------------------------------------------------------------------------
// Effacement de la zone mémoire et affichage à l'écran
void KeyOledManager::ClearScreen()
{
	if (bScreenOK)
		screen.clearDisplay();
}

//-------------------------------------------------------------------------
// Affichage de la zone mémoire préparée par avance sur l'écran
void KeyOledManager::DisplayScreen()
{
	if (bScreenOK)
		screen.display();
}

//-------------------------------------------------------------------------
// Affichage d'une chaine à une position donnée.
// Cette fonction n'affecte que la zone mémoire dédiée à l'écran
void KeyOledManager::DrawString(
	char *str,			// Chaine à afficher
	uint16_t x,			// Position de départ de la chaîne (en pixel, 0 à partir de la gauche)
	uint16_t y,			// Position de départ de la chaîne (en pixel, 0 en haut à partir du haut)
	bool bWhite,		// true texte blanc sur noir, false texte noir sur blanc
	int iSize			// 1 = caractères de 7x5 pixels, 2 = caractères de 14x10, 3 = 21x15
	)
{
	if (bScreenOK)
	{
		if (bWhite)
			screen.setTextColor(WHITE, BLACK);
		else
			screen.setTextColor(BLACK, WHITE);
		screen.setTextSize(iSize);
		screen.setCursor( x, y);
		screen.print(str);
	}
}

//-------------------------------------------------------------------------
// Affichage d'une image à une position donnée
// Cette fonction n'affecte que la zone mémoire dédiée à l'écran
// La bitmap est déclarée de type
// const unsigned char Bat64x64 [] = {
// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
// }; Utiliser LCDAssistant.exe pour transformer un fichier BMP et définition de ce type
void KeyOledManager::DrawPicture(
	uint16_t x,			// Position de départ de l'image (en pixel, 0 à partir de la gauche)
	uint16_t y,			// Position de départ de l'image (en pixel, 0 en haut à partir du haut)
	uint8_t *bitmap,	// Pointeur sur les octets de la bitmap
	uint16_t w,			// Largeur en pixel de la bitmap
	uint16_t h			// Hauteur en pixel de la bitmap
	)
{
	if (bScreenOK)
	{
		screen.drawBitmap( x, y, bitmap, w, h, 1);
	}
}

//-------------------------------------------------------------------------
// Affichage d'une ligne
// Cette fonction n'affecte que la zone mémoire dédiée à l'écran
void KeyOledManager::DrawLine(
	uint16_t x0,		// Position de départ de la ligne (en pixel, 0 à partir de la gauche)
	uint16_t y0,		// Position de départ de la ligne (en pixel, 0 en haut à partir du haut)
	uint16_t x1,		// Position de fin de la ligne
	uint16_t y1, 		// Position de fin de la ligne
	bool bWhite			// true blanc sur noir, false noir sur blanc
	)
{
	if (bScreenOK)
	{
		uint16_t color = WHITE;
		if (!bWhite)
			color = BLACK;
		screen.drawLine( x0, y0, x1, y1, color);
	}
}

//-------------------------------------------------------------------------
// Affichage d'un rectangle
// Cette fonction n'affecte que la zone mémoire dédiée à l'écran
void KeyOledManager::DrawRectangle(
	uint16_t x,			// Position de départ du coin supérieur gauche (en pixel, 0 à partir de la gauche)
	uint16_t y,			// Position de départ du coin supérieur gauche (en pixel, 0 en haut à partir du haut)
	uint16_t w,			// Largeur en pixel
	uint16_t h,			// Hauteur en pixel
	bool bFill,			// True pour un rectangle plein et False pour un rectangle vide
	bool bWhite			// true blanc sur noir, false noir sur blanc
	)
{
	if (bScreenOK)
	{
		uint16_t color = WHITE;
		if (!bWhite)
			color = BLACK;
		if (bFill)
			screen.fillRect( x, y, w, h, color);
		else
			screen.drawRect( x, y, w, h, color);
	}
}

//-------------------------------------------------------------------------
// Affichage d'un triangle
// Cette fonction n'affecte que la zone mémoire dédiée à l'écran
void KeyOledManager::DrawTriangle(
	uint16_t x0,		// Position du premier sommet (en pixel, 0 à partir de la gauche)
	uint16_t y0,		// Position du premier sommet (en pixel, 0 en haut à partir du haut)
	uint16_t x1,		// Position du second sommet
	uint16_t y1, 		// Position du second sommet
	uint16_t x2,		// Position du troisième sommet
	uint16_t y2, 		// Position du troisième sommet
	bool bFill,			// True pour un triangle plein et False pour un triangle vide
	bool bWhite			// true blanc sur noir, false noir sur blanc
	)
{
	if (bScreenOK)
	{
		uint16_t color = WHITE;
		if (!bWhite)
			color = BLACK;
		if (bFill)
			screen.fillTriangle( x0, y0, x1, y1, x2, y2, color);
		else
			screen.drawTriangle( x0, y0, x1, y1, x2, y2, color);
	}
}

//-------------------------------------------------------------------------
// Affichage d'un cercle
// Cette fonction n'affecte que la zone mémoire dédiée à l'écran
void KeyOledManager::DrawCircle(
	uint16_t x0,		// Position du centre (en pixel, 0 à partir de la gauche)
	uint16_t y0,		// Position ducentre (en pixel, 0 en haut à partir du haut)
	uint16_t radius,	// Rayon en pixel
	bool bFill,			// True pour un cercle plein et False pour un cercle vide
	bool bWhite			// true blanc sur noir, false noir sur blanc
	)
{
	if (bScreenOK)
	{
		uint16_t color = WHITE;
		if (!bWhite)
			color = BLACK;
		if (bFill)
			screen.fillCircle( x0, y0, radius, color);
		else
			screen.drawCircle( x0, y0, radius, color);
	}
}

//-------------------------------------------------------------------------
// Lecture du clavier, retourne la touche pressée ou K_NO
unsigned short KeyOledManager::GetKey()
{
    unsigned short usKey = K_NO;
	if (bKeyboardOK)
	{
		// Lecture d'un mot de 16 bits sur le clavier
		if (read(KeyboardFfile,&usKey,2) != 2)
			printf("KeyOledManager, lecture bus I2C du clavier impossible\n");
	}
	return usKey;
}

