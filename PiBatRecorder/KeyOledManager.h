/* 
 * File:   KeyOledManager.h
 * Author: Jean-Do
 *
 * Created on 9 décembre 2015, 21:24
 */

#ifndef KEYOLEDMANAGER_H
#define	KEYOLEDMANAGER_H

#include "./ArduiPi_OLED-master/ArduiPi_OLED_lib.h"
#include "./ArduiPi_OLED-master/Adafruit_GFX.h"
#include "./ArduiPi_OLED-master/ArduiPi_OLED.h"

// Définition des touches du clavier
#define K_NO		0
#define K_DOWN		1
#define K_RIGHT		2
#define K_UP		4
#define K_LEFT		8
#define K_ON		16
#define K_RASP		32
#define K_WAVE		64
#define K_TRIANGLE	128
#define K_CIRCLE	256
#define K_X			512
#define K_SQUARE	1024
#define K_R			2048
#define K_HOUSE		4096
#define K_PLUS		8192
#define K_MINUS		16384
#define K_L			32768

// Définition des constantes de l'écran OLED
// Avec taille de 2 pour la police
#define MAXCHART2	10
#define MAXLINET2	4
#define L1T2		0
#define L2T2		16
#define L3T2		32
#define L4T2		48
// Avec taille de 1 pour la police
#define MAXCHART1	21
#define MAXLINET1	8
#define L1T1		0
#define L2T1		8
#define L3T1		16
#define L4T1		24
#define L5T1		32
#define L6T1		40
#define L7T1		48
#define L8T1		56

/*-----------------------------------------------------------------------------
 Classe de gestion du clavier et de l'afficheur LCD
 Le clavier est de type sensitif 16 touches TTP229
	Achat sur http://fr.aliexpress.com/item/Raspberry-Pi-Model-B-B-TTP229-LSF-Detector-Controller-Capacitive-Touch-Keypad-Supports-Up-To-16/32244765970.html?ws_ab_test=searchweb201556_3_79_78_77_91_80,searchweb201644_5,searchweb201560_9
	Constructeur sur http://www.waveshare.com/product/RPi-Touch-Keypad.htm
 L'écran est de type OLED 128x64 SSD1106 ou SSD1306
	Achat sur http://fr.aliexpress.com/item/1-3-Inch-white-I2C-IIC-OLED-LCD-Module-Serial-128X64-LED-Display-Modules-for-Arduino/32445305003.html
-----------------------------------------------------------------------------*/
class KeyOledManager
{
public:
	//-------------------------------------------------------------------------
	// Constructeur
	KeyOledManager();
	
	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~KeyOledManager();

	//-------------------------------------------------------------------------
	// Initialisation de l'écran et du clavier
	void InitScreenAndKeyboard(
		int iScreenType,	// Type de l'écran (OLEDSH1106I2C128x64, OLEDSSD1306I2C128x64 ou NOSCREEN)
		int iKeyboardType	// Type du clavier (TTP229I2C16KEYS ou NOKEYBOARD)
		);

	//-------------------------------------------------------------------------
	// Gestion de l'écran
	// Toutes les fonctions d'écriture sur l'écran ne font que mettre à jour
	// une zone mémoire sans effet direct sur l'écran.
	// Seules les fonctions
	// - ClearScreen (effacement de la zone mémoire et affichage à l'écran)
	// - DisplayScreen (affichage de la zone mémoire sur l'écran)
	// Affectent vraiment l'état de l'écran
	// En appelant plusieurs fonctions d'écriture dans la zone mémoire
	// il est possible de superposer plusieurs éléments sur les mêmes pixels
	
	//-------------------------------------------------------------------------
	// Effacement de la zone mémoire et affichage à l'écran
	void ClearScreen();

	//-------------------------------------------------------------------------
	// Affichage de la zone mémoire préparée par avance sur l'écran
	void DisplayScreen();

	//-------------------------------------------------------------------------
	// Affichage d'une chaine à une position donnée.
	// Cette fonction n'affecte que la zone mémoire dédiée à l'écran
	void DrawString(
			char *str,			// Chaine à afficher
			uint16_t x=0,		// Position de départ de la chaîne (en pixel, 0 à partir de la gauche)
			uint16_t y=0,		// Position de départ de la chaîne (en pixel, 0 en haut à partir du haut)
			bool bWhite=true,	// true texte blanc sur noir, false texte noir sur blanc
			int iSize=1			// 1 = caractères de 7x5 pixels, 2 = caractères de 14x10, 3 = 21x15
			);

	//-------------------------------------------------------------------------
	// Affichage d'une image à une position donnée
	// Cette fonction n'affecte que la zone mémoire dédiée à l'écran
	// La bitmap est déclarée de type
	// const unsigned char Bat64x64 [] = {
	// 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	// }; Utiliser LCDAssistant.exe pour transformer un fichier BMP et définition de ce type
	void DrawPicture(
			uint16_t x,			// Position de départ de l'image (en pixel, 0 à partir de la gauche)
			uint16_t y,			// Position de départ de l'image (en pixel, 0 en haut à partir du haut)
			uint8_t *bitmap,	// Pointeur sur les octets de la bitmap
			uint16_t w,			// Largeur en pixel de la bitmap
			uint16_t h			// Hauteur en pixel de la bitmap
			);

	//-------------------------------------------------------------------------
	// Affichage d'une ligne
	// Cette fonction n'affecte que la zone mémoire dédiée à l'écran
	void DrawLine(
			uint16_t x0,		// Position de départ de la ligne (en pixel, 0 à partir de la gauche)
			uint16_t y0,		// Position de départ de la ligne (en pixel, 0 en haut à partir du haut)
			uint16_t x1,		// Position de fin de la ligne
			uint16_t y1, 		// Position de fin de la ligne
			bool bWhite=true	// true blanc sur noir, false noir sur blanc
			);

	//-------------------------------------------------------------------------
	// Affichage d'un rectangle
	// Cette fonction n'affecte que la zone mémoire dédiée à l'écran
	void DrawRectangle(
			uint16_t x,			// Position de départ du coin supérieur gauche (en pixel, 0 à partir de la gauche)
			uint16_t y,			// Position de départ du coin supérieur gauche (en pixel, 0 en haut à partir du haut)
			uint16_t w,			// Largeur en pixel
			uint16_t h,			// Hauteur en pixel
			bool bFill=false,	// True pour un rectangle plein et False pour un rectangle vide
			bool bWhite=true	// true blanc sur noir, false noir sur blanc
			);

	//-------------------------------------------------------------------------
	// Affichage d'un triangle
	// Cette fonction n'affecte que la zone mémoire dédiée à l'écran
	void DrawTriangle(
			uint16_t x0,		// Position du premier sommet (en pixel, 0 à partir de la gauche)
			uint16_t y0,		// Position du premier sommet (en pixel, 0 en haut à partir du haut)
			uint16_t x1,		// Position du second sommet
			uint16_t y1, 		// Position du second sommet
			uint16_t x2,		// Position du troisième sommet
			uint16_t y2, 		// Position du troisième sommet
			bool bFill=false,	// True pour un triangle plein et False pour un triangle vide
			bool bWhite=true	// true blanc sur noir, false noir sur blanc
			);

	//-------------------------------------------------------------------------
	// Affichage d'un cercle
	// Cette fonction n'affecte que la zone mémoire dédiée à l'écran
	void DrawCircle(
			uint16_t x0,		// Position du centre (en pixel, 0 à partir de la gauche)
			uint16_t y0,		// Position ducentre (en pixel, 0 en haut à partir du haut)
			uint16_t radius,	// Rayon en pixel
			bool bFill=false,	// True pour un cercle plein et False pour un cercle vide
			bool bWhite=true	// true blanc sur noir, false noir sur blanc
			);

	//-------------------------------------------------------------------------
	// Lecture du clavier, retourne la touche pressée ou K_NO
	unsigned short GetKey();
	
private:
	// Fichier d'accès au clavier
	int KeyboardFfile;
	
	// Indique si le clavier est OK
	bool bKeyboardOK;
	
	// Gestionnaire Ecran via la bibliothèque AdaFruit
	ArduiPi_OLED screen;

	// Indique si l'écran est OK
	bool bScreenOK;
};

#endif	/* KEYOLEDMANAGER_H */

