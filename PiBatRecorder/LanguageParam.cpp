/* 
 * File:   LanguageParam.cpp
 * Author: Jean-Do
 * 
 * Created on 4 septembre 2015, 10:56
 */
//-------------------------------------------------------------------------
// Classe de modification de la langue d'affichage

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "LanguageParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
LanguageParam::LanguageParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Langue   Français
	//                    Language English
	strcpy( indicModif, "000000000010000000000");
	// Mémo de la valeur courante
	iOldMode = pParams->GetLangue();
	iNewMode = iOldMode;
}

//-------------------------------------------------------------------------
// Destructeur
LanguageParam::~LanguageParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void LanguageParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_PLUS:
		case K_R:
			// On incrémente le digit
			iNewMode++;
			if (iNewMode > EN)
				iNewMode = FR;
			break;
		case K_MINUS:
		case K_L:
			// On décrémente le digit
			iNewMode--;
			if (iNewMode < FR)
				iNewMode = EN;
			break;
		case K_SQUARE:
			// Init de la valeur min
			iNewMode = FR;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			iNewMode = EN;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			iNewMode = iOldMode;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void LanguageParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
		iNewMode = pParams->GetLangue();
	//                  012345678901234567890
	//                   Langue   Français
	//                   Language English
	switch (iNewMode)
	{
	case FR:
		sprintf(lineParam, " Langue   Fran%cais", 0x87);
		break;
	case EN:
	default:
		sprintf(lineParam, " Language English");
	}
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void LanguageParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	iOldMode = pParams->GetLangue();
	iNewMode = iOldMode;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void LanguageParam::SetParam()
{
	// Vérification du paramètre
	if (iNewMode < FR or iNewMode > EN)
		// Valeur précédente
		iNewMode = iOldMode;
	pParams->SetLangue((LANGUE)iNewMode);
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}
