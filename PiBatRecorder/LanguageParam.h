/* 
 * File:   LanguageParam.h
 * Author: Jean-Do
 *
 * Created on 4 septembre 2015, 10:56
 */
#include "GenericParam.h"

#ifndef LANGUAGEPARAM_H
#define	LANGUAGEPARAM_H

//-------------------------------------------------------------------------
// Classe de modification de la langue d'affichage
class LanguageParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	LanguageParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~LanguageParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Mémo de la valeur avant modif
	int iOldMode;
	// Mémo de la valeur courante
	int iNewMode;
};

#endif	/* LANGUAGEPARAM_H */

