/* 
 * File:   LogFile.cpp
 * Author: Jean-Do
 * 
 * Created on 18 janvier 2016, 14:55
 */
//-------------------------------------------------------------------------
// Classe de gestion du fichier log de déroulement des logiciels

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctime>
#include "LogFile.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
LogFile::LogFile()
{
	// Si 1ère instance
	if (LogFile::pInstance == NULL)
	{
		LogFile::pInstance = this;
		// Initialisation du path+nom du fichier
		strcpy( sFilePath, "/home/pi/PiBatRecorder/PiBatLog.txt");
		// Vérification du nombre de ligne du fichier
		int iNbLines = 0;
		char sLine[255];
		FILE *f = fopen (sFilePath, "r");
		if (f != NULL)
		{
			// Lecture des différentes lignes
			while (fgets(sLine, 255, f) != NULL)
			{
				iNbLines++;
			}
			// Fermeture du fichier
			fclose(f);
		}
		// Si plus de 512 lignes
		if (iNbLines > 512)
		{
			// On renomme le fichier en .old
			system("mv /home/pi/PiBatRecorder/PiBatLog.txt /home/pi/PiBatRecorder/PiBatLog.old");
			// On recopie les 256 dernières lignes dans le fichier .txt
			sprintf(sLine, "tail -n+%d /home/pi/PiBatRecorder/PiBatLog.old > /home/pi/PiBatRecorder/PiBatLog.txt", iNbLines - 255);
			system(sLine);
			// On efface le fichier .old
			system("rm /home/pi/PiBatRecorder/PiBatLog.old");
		}
	}
}

//-------------------------------------------------------------------------
// Pointeur sur l'instance de la classe
LogFile *LogFile::pInstance = NULL;

//-------------------------------------------------------------------------
// Info statique du niveau actif du log
LEVEL_LOG LogFile::LogLevel = LINFO;

//-------------------------------------------------------------------------
// Info statique du nom du logiciel affiché dans chaque trace
char LogFile::softName[32];

//-------------------------------------------------------------------------
// Info statique permettant de doubler le Log avec des sorties consoles
bool LogFile::bConsole = false;

//-------------------------------------------------------------------------
// Destructeur
LogFile::~LogFile()
{
}

//-------------------------------------------------------------------------
// Ajout d'une ligne dans le fichier log
void LogFile::AddLog(
	LEVEL_LOG Level,		// Niveau de la ligne de log, si le niveau est supérieur au niveau actif, le log n'est pas mémorisé
	const char *pLogString,	// Pointeur sur la chaîne (\n à la fin inutile)
	...						// Liste des paramètres de conposition de la chaîne style sprintf
	)
{
	// Si le niveau du log est compatible
	if (Level <= LogFile::LogLevel and LogFile::pInstance != NULL)
	{
		// Préparation de la chaine
		char sLine[256];
		va_list args;
		va_start (args, pLogString);
		vsnprintf (sLine, 255, pLogString, args);
		va_end (args);
		if (sLine[strlen(sLine)-1] != '\n')
			strcat(sLine, "\n");
		// Récupération de l'heure courante
		time_t curtime;
		struct tm * timeinfo;
		time(&curtime);
		timeinfo = localtime(&curtime);
		// Création de la date et de l'heure
		char sTime[80];
		strftime(sTime, 80, "%d/%m/%Y - %H:%M:%S", timeinfo);
		// Ouverture du fichier
		FILE *f;
		f = fopen (LogFile::pInstance->sFilePath, "a+");
		if (f != NULL)
		{
			// Ecriture du fichier avec en début de ligne la date et l'heure
			fprintf (f, "%s %s %s", sTime, LogFile::softName, sLine);
			fflush  (f);
			// Fermeture du fichier
			fclose(f);
		}
		if (LogFile::bConsole)
			printf  (   "%s %s %s", sTime, LogFile::softName, sLine);
	}
}

