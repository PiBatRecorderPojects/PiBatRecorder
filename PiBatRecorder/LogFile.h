/* 
 * File:   LogFile.h
 * Author: Jean-Do
 *
 * Created on 18 janvier 2016, 14:55
 */

#ifndef LOGFILE_H
#define	LOGFILE_H

//-------------------------------------------------------------------------
// Niveaux des entrées du log
enum LEVEL_LOG {
	LLOG,		// Niveau de base avec les date d'entrée et sortie d'un logiciel et de ses principales fonctions
	LINFO,		// Niveau avec des informations complémentaires
	LDEBUG,		// Niveau avec des infos de debug pouvant affecter le fonctionnement temps réel
	LREALTIME	// Niveau maximum avec des conséquences sur le temps réel
};

//-------------------------------------------------------------------------
// Classe de gestion du fichier log de déroulement des logiciels
class LogFile
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	LogFile();

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~LogFile();
	
	//-------------------------------------------------------------------------
	// Fonction statique d'ajout d'une ligne dans le fichier log
	static void AddLog(
		LEVEL_LOG Level,		// Niveau de la ligne de log, si le niveau est supérieur au niveau actif, le log n'est pas mémorisé
		const char *pLogString,	// Pointeur sur la chaîne (\n à la fin inutile)
		...						// Liste des paramètres de conposition de la chaîne style sprintf
		);
	
	//-------------------------------------------------------------------------
	// Info statique du niveau actif du log
	static LEVEL_LOG LogLevel;
	
	//-------------------------------------------------------------------------
	// Info statique permettant de doubler le Log avec des sorties consoles
	static bool bConsole;
	
	
	//-------------------------------------------------------------------------
	// Info statique du nom du logiciel affiché dans chaque trace
	static char softName[32];

protected:
	// Pointeur sur l'instance de la classe
	static LogFile *pInstance;
	
private:
	// Nom du fichier de sauvegarde (/mnt/usbkey/PiBatLog.txt)
	char sFilePath[80];
};

#endif	/* LOGFILE_H */

