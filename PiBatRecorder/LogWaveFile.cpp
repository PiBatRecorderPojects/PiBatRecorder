/* 
 * File:   LogWaveFile.cpp
 * Author: Jean-Do
 * 
 * Created on 3 septembre 2015, 14:07
 */
//-------------------------------------------------------------------------
// Classe de gestion du fichier log des cris au format CSV

#include <stdio.h>
#include <stdlib.h>
#include "LogWaveFile.h"
#include "CAnalyser.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
LogWaveFile::LogWaveFile(
	CParameters		*pPar		// Pointeur sur les paramètres
	)
{
	// RAZ des infos
	bWritting = false;
	memset(sFilePath  , 0, 80);
	memset(sFileWav   , 0, 80);
	memset(sStructures, 0, 256);
	iNbCry  = 0;
	iFIMin  = 0;
	iFIMax  = 0;
	iFMEMin = 0;
	iFMEMax = 0;
	iFTMin  = 0;
	iFTMax  = 0;
	fDurMin = 0;
	fDurMax = 0;
	iNivMin = 0;
	iNivMax = 0;
	pParams = pPar;
}

//-------------------------------------------------------------------------
// Destructeur
LogWaveFile::~LogWaveFile()
{
}

//-------------------------------------------------------------------------
// Démarre le log d'un fichier wav
void LogWaveFile::StartWavFile(
	MonitoringStatus *pMonitStatus, // Pointeur sur l'état courant
	char *pathWav	// Path du fichier wave
	)
{
	// Mémo des infos
	CryParams *pCry = &(pMonitStatus->lastCry);
	int iNbC = pMonitStatus->iNbCrys;
	bWritting = true;
	iNbCry  = iNbC;
	if (pCry->FME != 0)
	{
		strcpy(sFileWav   , pathWav);
		strcpy(sStructures, " ");
		strcat(sStructures, pCry->Struct);
		strcat(sStructures, ",");
		iFIMin  = pCry->FI;
		iFIMax  = pCry->FI;
		iFMEMin = pCry->FME;
		iFMEMax = pCry->FME;
		iFTMin  = pCry->FT;
		iFTMax  = pCry->FT;
		fDurMin = pCry->Dur;
		fDurMax = pCry->Dur;
		iNivMin = pCry->NivMax;
		iNivMax = pCry->NivMax;
	}
}

//-------------------------------------------------------------------------
// Ajout des caractéristiques d'un cri
void LogWaveFile::AddCry(
	MonitoringStatus *pMonitStatus // Pointeur sur l'état courant
	)
{
	if (bWritting)
	{
		// Mémo des infos
		CryParams *pCry = &(pMonitStatus->lastCry);
		int iNbC = pMonitStatus->iNbCrys;
		iNbCry  += iNbC;
		if (pCry->FME != 0)
		{
			if (pCry->FI < iFIMin or iFIMin == 0)
				iFIMin  = pCry->FI;
			if (pCry->FI > iFIMax or iFIMax == 0)
				iFIMax  = pCry->FI;
			if (pCry->FME < iFMEMin or iFMEMin == 0)
				iFMEMin = pCry->FME;
			if (pCry->FME > iFMEMax or iFMEMax == 0)
				iFMEMax = pCry->FME;
			if (pCry->FT < iFTMin or iFTMin == 0)
				iFTMin  = pCry->FT;
			if (pCry->FT > iFTMax or iFTMax == 0)
				iFTMax  = pCry->FT;
			if (pCry->Dur < fDurMin or fDurMin < 0.0001)
				fDurMin = pCry->Dur;
			if (pCry->Dur > fDurMax or fDurMax < 0.0001)
				fDurMax = pCry->Dur;
			if (pCry->NivMax < iNivMin or iNivMin == 0)
				iNivMin = pCry->NivMax;
			if (pCry->NivMax < iNivMax or iNivMax == 0)
				iNivMax = pCry->NivMax;
			char sTruct[80];
			strcpy(sTruct, " ");
			strcat(sTruct, pCry->Struct);
			strcat(sTruct, ",");
			if (strstr(sStructures, sTruct) == NULL)
				strcat(sStructures, sTruct);
		}
	}
}

//-------------------------------------------------------------------------
// Stoppe le log d'un fichier wav
void LogWaveFile::StopWavFile(
	int iDuree	// Durée du fichier
	)
{
	bWritting = false;
	FILE *f;
	f = NULL;
	if (strlen(sFilePath) < 1)
	{
		// Création du nom du fichier de sauvegarde (/mnt/usbkey/Log_AAMMJJ_HHMMSS.csv)
		char timeStr[80];
		struct tm * timeinfo;
		time_t curtime;
		time(&curtime);
		timeinfo = localtime (&curtime);
		strftime(timeStr, 80, "%y%m%d_%H%M%S", timeinfo);
		sprintf(sFilePath , "%sLog_%s.csv", pParams->GetWavPath(), timeStr);
		// Création du fichier
		f = fopen (sFilePath, "a+");
		if (f != NULL)
		{
			// Création de l'entête
			fprintf (f, "Wave file\tDur\tFIMin\tFIMax\tFMEMin\tFMEMax\tFTMin\tFTMax\tDurMin\tDurMax\tNivMin\tNivMax\tNb\tStructures(s)\n");
			fprintf (f, " \ts\tkHz\tkHz\tkHz\tkHz\tkHz\tkHz\tms\tms\tdB\tdB\t \t \n");
			fflush  (f);
		}
		else
			printf("LogWaveFile::StopWavFile Echec création [%s]\n", sFilePath);
	}
	// Mémo des informations dans le log
	if (f == NULL)
		f = fopen (sFilePath, "a+");
	if (f != NULL)
	{
		// Ecriture de la ligne concernant le fichier wav
		fprintf (f, "%s\t%02d\t%02d\t%02d\t%02d\t%02d\t%02d\t%02d\t%05.1f\t%05.1f\t%02d\t%02d\t%d\t%s\n"
				,sFileWav,iDuree,iFIMin/1000,iFIMax/1000,iFMEMin/1000,iFMEMax/1000,iFTMin/1000,iFTMax/1000
				,fDurMin,fDurMax,iNivMin,iNivMax,iNbCry,sStructures);
		fflush  (f);
		// Fermeture du fichier
		fclose(f);
	}
	else
		printf("LogWaveFile::StopWavFile Echec ouverture [%s]\n", sFilePath);
}
