/* 
 * File:   LogWaveFile.h
 * Author: Jean-Do
 *
 * Created on 3 septembre 2015, 14:07
 */

#include "CStatus.h"
#include "CParameters.h"
#include "CAnalyser.h"

#ifndef LOGWAVEFILE_H
#define	LOGWAVEFILE_H

//-------------------------------------------------------------------------
// Classe de gestion du fichier log des cris au format CSV
class LogWaveFile
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	LogWaveFile(
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~LogWaveFile();

	//-------------------------------------------------------------------------
	// Retourne le Nom du fichier de sauvegarde
	char *GetFilePath() { return sFilePath;};
	
	//-------------------------------------------------------------------------
	// Initialise le Nom du fichier de sauvegarde
	void SetFilePath(char *pathName) { strcpy(sFilePath, pathName);};

	//-------------------------------------------------------------------------
	// Démarre le log d'un fichier wav
	void StartWavFile(
		MonitoringStatus *pMonitStatus, // Pointeur sur l'état courant
		char *pathWav	// Path du fichier wave
		);
	
	//-------------------------------------------------------------------------
	// Ajout des caractéristiques d'un cri
	void AddCry(
		MonitoringStatus *pMonitStatus // Pointeur sur l'état courant
		);
	
	//-------------------------------------------------------------------------
	// Stoppe le log d'un fichier wav
	void StopWavFile(
		int iDuree	// Durée du fichier
		);

private:
	// Indique si on est en écriture log
	bool bWritting;
	
	// Nom du fichier de sauvegarde (/mnt/usbkey/Log_AAMMJJ_HHMMSS.csv)
	char sFilePath[80];

	// Paramètres d'un fichier wav
	char sFileWav[80];
	
	// Nombre de cris
	int iNbCry;
	
	// FI Min et Max
	int iFIMin, iFIMax;
	
	// FME Min et Max
	int iFMEMin, iFMEMax;
	
	// FT Min et Max
	int iFTMin, iFTMax;
	
	// Durée Min et Max
	float fDurMin, fDurMax;
	
	// Niveau Min et Max
	int iNivMin, iNivMax;
	
	// Liste des structures détectées
	char sStructures[256];
	
	// Pointeur sur les paramètres
	CParameters *pParams;
};

#endif	/* LOGWAVEFILE_H */

