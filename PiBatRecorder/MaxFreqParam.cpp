/* 
 * File:   MaxFreqParam.cpp
 * Author: Jean-Do
 * 
 * Created on 3 septembre 2015, 18:39
 */
//-------------------------------------------------------------------------
// Classe de modification de la fréquence max d'intéret (16 <-> 96)

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "MaxFreqParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
MaxFreqParam::MaxFreqParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Frequence max. 96kHz
	//                    Max. Frequency 96kHz
	strcpy( indicModif, "0000000000000000x1000");
	// Mémo de la valeur courante
	iOldMax = pParams->GetFMax()/1000;
	iNewMax = iOldMax;
}

//-------------------------------------------------------------------------
// Destructeur
MaxFreqParam::~MaxFreqParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void MaxFreqParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_R:
			// On incrémente la valeur de 10kHz
			iNewMax += 10;
			if (iNewMax > 96)
				iNewMax = 10;
			break;
		case K_L:
			// On décrémente la valeur de 10kHz
			iNewMax -= 10;
			if (iNewMax < 10)
				iNewMax = 96;
			break;
		case K_PLUS:
			// On incrémente la valeur de 1kHz
			iNewMax += 1;
			if (iNewMax > 96)
				iNewMax = 10;
			break;
		case K_MINUS:
			// On décrémente la valeur de 1kHz
			iNewMax -= 1;
			if (iNewMax < 10)
				iNewMax = 96;
			break;
		case K_SQUARE:
			// Init de la valeur min
			iNewMax = 10;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			iNewMax = 96;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			iNewMax = iOldMax;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void MaxFreqParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
		iNewMax = pParams->GetFMax()/1000;
	//                   123456789012345678901
	//                    Frequence max. 96kHz
	//                    Max. Frequency 96kHz
	if (pParams->GetLangue() == FR)
		sprintf(lineParam, " Fr%cquence max. %02dkHz", 0x82, iNewMax);
	else
		sprintf(lineParam, " Max. Frequency %02dkHz", iNewMax);
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void MaxFreqParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	iOldMax = pParams->GetFMax()/1000;
	iNewMax = iOldMax;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void MaxFreqParam::SetParam()
{
	// Vérification du paramètre
	if (iNewMax < 10 or iNewMax > 95)
		// Valeur précédente
		iNewMax = iOldMax;
	pParams->SetFMax(iNewMax*1000);
	/*if (iNewMax < (pParams->GetFMin()/1000))
		pParams->SetFMin((iNewMax*1000) - 1);*/
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}
