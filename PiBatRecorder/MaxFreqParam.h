/* 
 * File:   MaxFreqParam.h
 * Author: Jean-Do
 *
 * Created on 3 septembre 2015, 18:39
 */
#include "GenericParam.h"

#ifndef MAXFREQPARAM_H
#define	MAXFREQPARAM_H

//-------------------------------------------------------------------------
// Classe de modification de la fréquence max d'intéret (16 <-> 96)
class MaxFreqParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	MaxFreqParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~MaxFreqParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Mémo de la valeur avant modif
	int iOldMax;
	
	// Mémo de la valeur courante
	int iNewMax;
};

#endif	/* MAXFREQPARAM_H */

