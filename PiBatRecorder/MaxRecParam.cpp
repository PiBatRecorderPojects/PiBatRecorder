/* 
 * File:   MaxRecParam.cpp
 * Author: Jean-Do
 * 
 * Created on 2 septembre 2015, 21:20
 */
//-------------------------------------------------------------------------
// Classe de modification de la durée max d'enregistrement (10 <-> 99)

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "MaxRecParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
MaxRecParam::MaxRecParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Enregistrem. max 10s
	//                    Max. record time 10s
	strcpy( indicModif, "000000000000000000x10");
	// Mémo de la valeur courante
	iOldMax = pParams->GetDRecMax();
	iNewMax = iOldMax;
}

//-------------------------------------------------------------------------
// Destructeur
MaxRecParam::~MaxRecParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void MaxRecParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_R:
			// On incrémente la valeur de 10
			iNewMax += 10;
			if (iNewMax > 99)
				iNewMax = 10;
			break;
		case K_L:
			// On décrémente la valeur de 10
			iNewMax -= 10;
			if (iNewMax < 10)
				iNewMax = 99;
			break;
		case K_PLUS:
			// On incrémente la valeur de 1
			iNewMax += 1;
			if (iNewMax > 99)
				iNewMax = 10;
			break;
		case K_MINUS:
			// On décrémente la valeur de 1
			iNewMax -= 1;
			if (iNewMax < 10)
				iNewMax = 99;
			break;
		case K_SQUARE:
			// Init de la valeur min
			iNewMax = 10;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			iNewMax = 99;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			iNewMax = iOldMax;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void MaxRecParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
		iNewMax = pParams->GetDRecMax();
	//                   123456789012345678901
	//                   Enregistreme. max 10s
	//                   Max. record time  10s
	if (pParams->GetLangue() == FR)
		sprintf(lineParam, " Enregistrem. max %02ds", iNewMax);
	else
		sprintf(lineParam, " Max. record time %02ds", iNewMax);
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void MaxRecParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	iOldMax = pParams->GetDRecMax();
	iNewMax = iOldMax;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void MaxRecParam::SetParam()
{
	// Vérification du paramètre
	if (iNewMax < 10 or iNewMax > 99)
		// Valeur précédente
		iNewMax = iOldMax;
	pParams->SetDRecMax(iNewMax);
	/*if (iNewMax < (pParams->GetDRecMin()))
		pParams->SetDRecMax((iNewMax) - 1);*/
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}
