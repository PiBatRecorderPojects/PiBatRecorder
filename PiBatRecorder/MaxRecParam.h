/* 
 * File:   MaxRecParam.h
 * Author: Jean-Do
 *
 * Created on 2 septembre 2015, 21:20
 */
#include "GenericParam.h"

#ifndef MAXRECPARAM_H
#define	MAXRECPARAM_H

//-------------------------------------------------------------------------
// Classe de modification de la durée max d'enregistrement (10 <-> 99)
class MaxRecParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	MaxRecParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~MaxRecParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Mémo de la valeur avant modif
	int iOldMax;
	
	// Mémo de la valeur courante
	int iNewMax;
};

#endif	/* MAXRECPARAM_H */

