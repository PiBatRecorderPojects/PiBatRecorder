/* 
 * File:   MinFreqParam.cpp
 * Author: Jean-Do
 * 
 * Created on 2 septembre 2015, 22:05
 */
//-------------------------------------------------------------------------
// Classe de modification de la fréquence min d'intéret (15 <-> 95)

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "MinFreqParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
MinFreqParam::MinFreqParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Frequence min. 96kHz
	//                    Min. Frequency 96kHz
	strcpy( indicModif, "0000000000000000x1000");
	// Mémo de la valeur courante
	iOldMin = pParams->GetFMin()/1000;
	iNewMin = iOldMin;
}

//-------------------------------------------------------------------------
// Destructeur
MinFreqParam::~MinFreqParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void MinFreqParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_R:
			// On incrémente la valeur de 10kHz
			iNewMin += 10;
			if (iNewMin > 96)
				iNewMin = 10;
			break;
		case K_L:
			// On décrémente la valeur de 10kHz
			iNewMin -= 10;
			if (iNewMin < 10)
				iNewMin = 96;
			break;
		case K_PLUS:
			// On incrémente la valeur de 1kHz
			iNewMin += 1;
			if (iNewMin > 96)
				iNewMin = 10;
			break;
		case K_MINUS:
			// On décrémente la valeur de 1kHz
			iNewMin -= 1;
			if (iNewMin < 10)
				iNewMin = 96;
			break;
		case K_SQUARE:
			// Init de la valeur min
			iNewMin = 10;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			iNewMin = 96;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			iNewMin = iOldMin;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void MinFreqParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
		iNewMin = pParams->GetFMin()/1000;
	//                   123456789012345678901
	//                   Frequence min. 96 kHz
	//                   Min. Frequency 96 kHz
	if (pParams->GetLangue() == FR)
		sprintf(lineParam, " Fr%cquence min. %02dkHz", 0x82, iNewMin);
	else
		sprintf(lineParam, " Min. Frequency %02dkHz", iNewMin);
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void MinFreqParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	iOldMin = pParams->GetFMin()/1000;
	iNewMin = iOldMin;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void MinFreqParam::SetParam()
{
	if (iNewMin < 10 or iNewMin > 95)
		// Valeur précédente
		iNewMin = iOldMin;
	pParams->SetFMin(iNewMin*1000);
	/*if (iNewMin > (pParams->GetFMax()))
		pParams->SetFMax((iNewMin) + 1);*/
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}


