/* 
 * File:   MinFreqParam.h
 * Author: Jean-Do
 *
 * Created on 2 septembre 2015, 22:05
 */
#include "GenericParam.h"

#ifndef MINFREQPARAM_H
#define	MINFREQPARAM_H

//-------------------------------------------------------------------------
// Classe de modification de la fréquence min d'intéret (15 <-> 95)
class MinFreqParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	MinFreqParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~MinFreqParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Mémo de la valeur avant modif
	int iOldMin;
	
	// Mémo de la valeur courante
	int iNewMin;
};

#endif	/* MINFREQPARAM_H */

