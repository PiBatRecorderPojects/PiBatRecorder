/* 
 * File:   MinRecParam.cpp
 * Author: Jean-Do
 * 
 * Created on 2 septembre 2015, 21:06
 */
//-------------------------------------------------------------------------
// Classe de modification de la durée min d'enregistrement (0 <-> 9)

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "MinRecParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
MinRecParam::MinRecParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Enregistrem. min 10s
	//                    Min. record time 10s
	strcpy( indicModif, "000000000000000000x10");
	// Mémo de la valeur courante
	iOldMin = pParams->GetDRecMin();
	iNewMin = iOldMin;
}

//-------------------------------------------------------------------------
// Destructeur
MinRecParam::~MinRecParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void MinRecParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_R:
			// On incrémente la valeur de 10
			iNewMin += 10;
			if (iNewMin > 30)
				iNewMin = 1;
			break;
		case K_L:
			// On décrémente la valeur de 10
			iNewMin -= 10;
			if (iNewMin < 1)
				iNewMin = 30;
			break;
		case K_PLUS:
			// On incrémente la valeur de 1
			iNewMin += 1;
			if (iNewMin > 30)
				iNewMin = 1;
			break;
		case K_MINUS:
			// On décrémente la valeur de 1
			iNewMin -= 1;
			if (iNewMin < 1)
				iNewMin = 30;
			break;
		case K_SQUARE:
			// Init de la valeur min
			iNewMin = 1;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			iNewMin = 30;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			iNewMin = iOldMin;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void MinRecParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
		iNewMin = pParams->GetDRecMin();
	//                   123456789012345678901
	//                    Enregistrem. min 10s
	//                    Min. record time 10s
	if (pParams->GetLangue() == FR)
		sprintf(lineParam, " Enregistrem. min %02ds", iNewMin);
	else
		sprintf(lineParam, " Min. record time %02ds", iNewMin);
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void MinRecParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	iOldMin = pParams->GetDRecMin();
	iNewMin = iOldMin;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void MinRecParam::SetParam()
{
	// Récupération de la valeur du paramètre
	if (iNewMin < 1 or iNewMin > 30)
		// Valeur précédente
		iNewMin = iOldMin;
	pParams->SetDRecMin(iNewMin);
	/*if (iNewMin > (pParams->GetDRecMax()))
		pParams->SetDRecMin(iNewMin + 1);*/
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}

