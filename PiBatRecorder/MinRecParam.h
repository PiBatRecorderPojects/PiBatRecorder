/* 
 * File:   MinRecParam.h
 * Author: Jean-Do
 *
 * Created on 2 septembre 2015, 21:06
 */
#include "GenericParam.h"

#ifndef MINRECPARAM_H
#define	MINRECPARAM_H

//-------------------------------------------------------------------------
// Classe de modification de la durée min d'enregistrement (0 <-> 9)
class MinRecParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	MinRecParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~MinRecParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Mémo de la valeur avant modif
	int iOldMin;
	
	// Mémo de la valeur courante
	int iNewMin;
};

#endif	/* MINRECPARAM_H */

