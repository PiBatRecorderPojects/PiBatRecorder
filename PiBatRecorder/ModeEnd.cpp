/* 
 * File:   ModeEnd.cpp
 * Author: Jean-Do
 * 
 * Created on 11 décembre 2015, 21:54
 */

#include "ModeEnd.h"
#include "logo_oled_nb_triste.h"
#include "Version.h"

/*-----------------------------------------------------------------------------
 Classe de geston du mode Fin
 Se contente d'afficher le logo du logiciel et quitte le logiciel
-----------------------------------------------------------------------------*/


//-------------------------------------------------------------------------
// Constructeur
ModeEnd::ModeEnd(
	CParameters *pPar,					// Pointeur sur les paramètres
	KeyOledManager *pKeyScreenManager,	// Pointeur sur le gestionnaire du clavier et de l'écran
	bool bAutoMode						// True si le logiciel fonctionne entièrement en mode automatique
										// Si oui, le clavier et l'écran ne sont pas gérés
	):GenericMode(pPar, pKeyScreenManager, bAutoMode)
{
}

//-------------------------------------------------------------------------
// Destructeur
ModeEnd::~ModeEnd()
{
}

//-------------------------------------------------------------------------
// Début du mode
void ModeEnd::BeginMode()
{
	printf("ModeEnd::BeginMode");
	if (!bModeAuto)
	{
		// Effacement de l'écran
		pKeyOledManager->ClearScreen();
		// Affichage du logo
		pKeyOledManager->DrawPicture(0, 0, (uint8_t *)logo_oled_nb_triste, 128, 64);
		pKeyOledManager->DrawString((char *)VERSION, 90, L1T1);
		pKeyOledManager->DisplayScreen();
	}
	// Mémorisation du temps de maintenant pour le timeout
	timeBegin = millis();
}

//-------------------------------------------------------------------------
// Fin du mode
void ModeEnd::EndMode()
{
	printf("ModeEnd::EndMode");
	if (!bModeAuto)
	{
		// Effacement de l'écran
		pKeyOledManager->ClearScreen();
		pKeyOledManager->DisplayScreen();
	}
}
	
//-------------------------------------------------------------------------
// Affichage du mode sur l'écran
// Cette méthodes est appelée régulièrement par la classe PiBatManager
// à charge des différents modes d'afficher les informations nécessaires
void ModeEnd::PrintMode()
{
	// Ne fait rien
}

//-------------------------------------------------------------------------
// Traitement des ordres claviers
// Retourne éventuellement un mode demandé (NOMODE sinon)
// Cette méthodes est appelée régulièrement par la classe PiBatManager
// à charge des différents modes de traiter les actions opérateurs
int ModeEnd::ManageKey(
	unsigned short key	// Touche sollicitée par l'opérateur
	)
{
	// Mémorise le temps de maintenant
	unsigned int timeNow = millis();
	MODEFONC newMode = NOMODE;
	
	// Test si le timeout est passé (3s)
	// En mode auto, on sort immédiatement
	if (bModeAuto or (timeNow - timeBegin) > 3000)
		// On quitte le logiciel
		newMode = EXIT;

	return newMode;
}

