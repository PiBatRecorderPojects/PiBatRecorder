/* 
 * File:   ModeEnd.h
 * Author: Jean-Do
 *
 * Created on 11 décembre 2015, 21:54
 */
#include "GenericMode.h"

#ifndef MODEEND_H
#define	MODEEND_H

/*-----------------------------------------------------------------------------
 Classe de geston du mode Fin
 Se contente d'afficher le logo du logiciel et quitte le logiciel
-----------------------------------------------------------------------------*/
class ModeEnd : public GenericMode
{
public:
	//-------------------------------------------------------------------------
	// Constructeur
	ModeEnd(
		CParameters *pPar,					// Pointeur sur les paramètres
		KeyOledManager *pKeyScreenManager,	// Pointeur sur le gestionnaire du clavier et de l'écran
		bool bAutoMode=false				// True si le logiciel fonctionne entièrement en mode automatique
			                                // Si oui, le clavier et l'écran ne sont pas gérés
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~ModeEnd();
	
	//-------------------------------------------------------------------------
	// Début du mode
	virtual void BeginMode();
	
	//-------------------------------------------------------------------------
	// Fin du mode
	virtual void EndMode();
	
	//-------------------------------------------------------------------------
	// Affichage du mode sur l'écran
	// Cette méthode est appelée régulièrement par la classe PiBatManager
	// à charge des différents modes d'afficher les informations nécessaires
	virtual void PrintMode();
	
	//-------------------------------------------------------------------------
	// Traitement des ordres claviers
	// Si la touche est une touche de changement de mode, retourne le mode demandé
	// Cette méthode est appelée régulièrement par la classe PiBatManager
	// à charge des différents modes de traiter les actions opérateurs
	virtual int ManageKey(
		unsigned short key	// Touche sollicitée par l'opérateur
		);
	
private:
	// Mémorisation du temps en début de mode pour le timeout
	unsigned int timeBegin;
};

#endif	/* MODEEND_H */

