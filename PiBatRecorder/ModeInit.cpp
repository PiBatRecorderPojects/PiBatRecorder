/* 
 * File:   ModeInit.cpp
 * Author: Jean-Do
 * 
 * Created on 10 décembre 2015, 14:39
 */

#include "ModeInit.h"
#include "logo_oled_nb_heureuse.h"
#include "Version.h"

//-------------------------------------------------------------------------
// Constructeur
ModeInit::ModeInit(
	CParameters *pPar,					// Pointeur sur les paramètres
	KeyOledManager *pKeyScreenManager,	// Pointeur sur le gestionnaire du clavier et de l'écran
	bool bAutoMode						// True si le logiciel fonctionne entièrement en mode automatique
										// Si oui, le clavier et l'écran ne sont pas gérés
	):GenericMode(pPar, pKeyScreenManager, bAutoMode)
{
}

//-------------------------------------------------------------------------
// Destructeur
ModeInit::~ModeInit()
{
}

//-------------------------------------------------------------------------
// Début du mode
void ModeInit::BeginMode()
{
	if (!bModeAuto)
	{
		// Effacement de l'écran
		pKeyOledManager->ClearScreen();
		// Affichage du logo
		pKeyOledManager->DrawPicture(0, 0, (uint8_t *)logo_oled_nb_heureuse, 128, 64);
		pKeyOledManager->DrawString((char *)VERSION, 90, L1T1);
		pKeyOledManager->DisplayScreen();
	}
	// Mémorisation du temps de maintenant pour le timeout
	timeBegin = millis();
}

//-------------------------------------------------------------------------
// Affichage du mode sur l'écran
// Cette méthodes est appelée régulièrement par la classe PiBatManager
// à charge des différents modes d'afficher les informations nécessaires
void ModeInit::PrintMode()
{
	// Ne fait rien
}

//-------------------------------------------------------------------------
// Traitement des ordres claviers
// Retourne éventuellement un mode demandé (NOMODE sinon)
// Cette méthodes est appelée régulièrement par la classe PiBatManager
// à charge des différents modes de traiter les actions opérateurs
int ModeInit::ManageKey(
	unsigned short key	// Touche sollicitée par l'opérateur
	)
{
	// Pas de traitement des touches
	// Mémorise le temps de maintenant
	unsigned int timeNow = millis();
	MODEFONC newMode = NOMODE;
	
	// Test si le timeout est passé (2s)
	// En mode auto, on sort immédiatement
	if (bModeAuto or (timeNow - timeBegin) > 2000)
		// On revient dans le mode précédent
		newMode = MONITORING;

	return newMode;
}

