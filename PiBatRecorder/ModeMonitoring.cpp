/* 
 * File:   ModeMonitoring.cpp
 * Author: Jean-Do
 * 
 * Created on 10 décembre 2015, 15:06
 */

#include <wiringPi.h>
#include "ModeMonitoring.h"

/*-----------------------------------------------------------------------------
 Classe de gestion du mode Monitoring
 Affiche les paramètres du mode et gère les modifications des paramètres en temps réel
-----------------------------------------------------------------------------*/

//-------------------------------------------------------------------------
// Constructeur
 ModeMonitoring:: ModeMonitoring(
	CParameters *pPar,					// Pointeur sur les paramètres
	KeyOledManager *pKeyScreenManager,	// Pointeur sur le gestionnaire du clavier et de l'écran
	bool bAutoMode						// True si le logiciel fonctionne entièrement en mode automatique
										// Si oui, le clavier et l'écran ne sont pas gérés
	):GenericMode(pPar, pKeyScreenManager, bAutoMode)
	, cAnalyser(pPar, &GestMonitStatus)
	, cRecorder(pPar, &cAnalyser, &GestMonitStatus)
{
	 bDemandeAff = false;
	 bAttenteAuto = false;
	 bInitHetManu = false;
	 iFreqHet = 47000;
}

//-------------------------------------------------------------------------
// Destructeur
 ModeMonitoring::~ ModeMonitoring()
{
}

//-------------------------------------------------------------------------
// Début du mode
void  ModeMonitoring::BeginMode()
{
	LogFile::AddLog(LDEBUG, "ModeMonitoring::BeginMode\n");
	timeHeter = millis();
	// Sélection de l'entrée utilisée
	char temp[255];
	strcpy( temp, pParams->GetWolfsonShPath());
	switch (pParams->GetMikeInput())
	{
	default:
	case HEADSET:
		// Sélection de l'enregistrement via la prise casque
		strcat( temp, "Record_from_Headset.sh -q");
		break;
	case LINE_MONO:
	case LINE_STEREO:
		// Sélection de l'enregistrement via l'entrée ligne
		strcat( temp, "Record_from_lineIn.sh -q");
	}
	LogFile::AddLog(LDEBUG, "ModeMonitoring::BeginMode sélection entrée: %s\n", temp);
	system( temp);
	if (!bModeAuto)
	{
		// Sélection de la sortie utilisée
		strcpy( temp, pParams->GetWolfsonShPath());
		//printf("GetWolfsonShPath [%s]\n", temp);
		switch (pParams->GetOutput())
		{
		default:
		case OUTHEADSET:
			// Sélection de la lecture via la prise casque
			strcat( temp, "Playback_to_Headset.sh -q");
			break;
		case OUTLINE:
			// Sélection de la lecture via la sortie ligne
			strcat( temp, "Playback_to_Lineout.sh -q");
			break;
		case OUTLS:
			// Sélection de la lecture via la sortie HP
			strcat( temp, "Playback_to_Speakers.sh -q");
		}
		LogFile::AddLog(LDEBUG, "ModeMonitoring::BeginMode sélection sortie: %s\n", temp);
		system( temp);
		// Effacement de l'écran
		pKeyOledManager->ClearScreen();
		pKeyOledManager->DisplayScreen();
	}
	else
		LogFile::AddLog(LDEBUG, "ModeMonitoring::BeginMode pas de sortie en mode auto\n");
	bInitHetManu = false;
}

//-------------------------------------------------------------------------
// Fin du mode
void ModeMonitoring::EndMode()
{	
	LogFile::AddLog(LDEBUG, "ModeMonitoring::EndMode\n");
	// Si le monotoring est actif
	if (MonitStatus.bMonitoring)
	{
		// On stoppe le monitoring
		// On stoppe l'analyse
		cAnalyser.Stop();
		// On stoppe le canal audio
		cRecorder.Stop();
	}
}

//-------------------------------------------------------------------------
// Affichage du mode sur l'écran
// Cette méthode est appelée régulièrement par la classe PiBatManager
// à charge des différents modes d'afficher les informations nécessaires
void  ModeMonitoring::PrintMode()
{
	// Test si on est en phase active
	bool bActivePhase = IsActivePhase();
	bool bActivCry = false;
	// Test s'il faut lancer ou stopper le monitoring
	GestMonitStatus.GetStatus( &MonitStatus);
	if (bActivePhase)
	{
		// Si le monitoring est non actif
		if (not MonitStatus.bMonitoring)
		{
			// On lance le monitoring
			// On démarre le canal audio
			cRecorder.Start();
			// On démarre l'analyse
			cAnalyser.Start();
		}
	}
	else
	{
		// Si le monotoring est actif
		if (MonitStatus.bMonitoring)
		{
			// On stoppe le monitoring
			// On stoppe l'analyse
			cAnalyser.Stop();
			// On stoppe le canal audio
			cRecorder.Stop();
		}
	}
	
	// En mode auto, on n'affiche rien... sauf pendant 3s suite à l'appui sur carré
	bool bAff = true;
	if (!bModeAuto or bDemandeAff)
	{
		char temp[22];
		if (bActivePhase)
		{
			// Affichage du mode actif
			// Affichage du monitoring
			if (MonitStatus.lastCry.FME > 0)
				bActivCry = true;
			// 1ère ligne en taille 2 Logo Monitoring ou Record et, si Record, temps d'enregistrement en s
			//   1234567890
			//  "||ATTENTE "
			//  "O 99.9s"
			if (MonitStatus.bRecording)
			{
				sprintf(temp, "   %04.1fs   ", MonitStatus.fRecordDuration);
				pKeyOledManager->DrawString(temp, 0, 0, true, 2);
				pKeyOledManager->DrawCircle( 7, 7, 6, true);
			}
			else
			{
				if (pParams->GetLangue() == FR)
					sprintf(temp, "   Attente");
				else
					sprintf(temp, "   Waitting");
				pKeyOledManager->DrawString(temp, 0, 0, true, 2);
				pKeyOledManager->DrawRectangle( 0, 0, 5, 14, true);
				pKeyOledManager->DrawRectangle( 8, 0, 5, 14, true);
			}
			//                       123456789012345678901"
			// 3ème ligne taille 1, "FIxx FMExx FTxx kHz  "
			if (bActivCry)
				sprintf(temp, "FI%02d FME%02d FT%02d kHz", MonitStatus.lastCry.FI/1000, MonitStatus.lastCry.FME/1000, MonitStatus.lastCry.FT/1000);
			else
				sprintf(temp, "FI-- FME-- FT-- kHz");
			pKeyOledManager->DrawString(temp, 0, L3T1);
			// 4ème ligne taille 1, "Duree xxms Struct    "
			if (bActivCry)
				sprintf(temp, "Dur%ce %02dms %s", 0x82, (int)MonitStatus.lastCry.Dur, MonitStatus.lastCry.Struct);
			else
				sprintf(temp, "Dur%ce --ms ----", 0x82);
			FinalLine(temp);
			pKeyOledManager->DrawString(temp, 0, L4T1);
			// 5ème ligne taille 1, "NivMax-84 Bruit-100dB"
			// 5ème ligne taille 1, "LevMax-84 Noise-100dB"
			if (pParams->GetLangue() == FR)
			{
				if (bActivCry)
					sprintf(temp, "NivMax%03d Bruit% 4ddB", MonitStatus.lastCry.NivMax, MonitStatus.iNivMoy);
				else
					sprintf(temp, "NivMax -- Bruit% 4ddB", MonitStatus.iNivMoy);
			}
			else
			{
				if (bActivCry)
					sprintf(temp, "LevMax%03d Noise%03d dB", MonitStatus.lastCry.NivMax, MonitStatus.iNivMoy);
				else
					sprintf(temp, "LevMax -- Noise%03d dB", MonitStatus.iNivMoy);
			}
			pKeyOledManager->DrawString(temp, 0, L5T1);
			// 6ème ligne taille 1, "V25 G25 Se40dB000 000"
			// 6ème ligne taille 1, "V25 G25 Th40dB000 000"
			int iPertA = MonitStatus.iPertAnalyse;
			int iPertH = MonitStatus.iPertHeterodyne;
			char sPert[20];
			if (iPertA > 2 or iPertH > 2)
				sprintf( sPert, "% 3d % 3d", iPertA, iPertH);
			else
				sprintf( sPert, "       ");
			if (pParams->GetLangue() == FR)
				sprintf(temp, "V%02d G%02d Se%02ddB%s", pParams->GetVolPlay(), pParams->GetNivRec(),
						pParams->GetThreshold(), sPert);
			else
				sprintf(temp, "V%02d G%02d Th%02ddB%s", pParams->GetVolPlay(), pParams->GetNivRec(),
						pParams->GetThreshold(), sPert);
			pKeyOledManager->DrawString(temp, 0, L6T1);
			// 8ème ligne taille 1, "Hétérodyne sans      "
			// 8ème ligne taille 1, "Hétérodyne FME+2220Hz"
			// 8ème ligne taille 1, "Hétérodyne 47.50 kHz "
			switch (pParams->GetModeHeter())
			{
			case HET_SANS:
				if (pParams->GetLangue() == FR)
					sprintf(temp, "H%ct%crodyne sans      ", 0x82, 0x82);
				else
					sprintf(temp, "Heterodyne no      ");
				break;
			case HET_AUTO:
				if (pParams->GetLangue() == FR)
					sprintf(temp, "H%ct%crodyne FME+%04dHz", 0x82, 0x82, pParams->GetDecHeter());
				else
					sprintf(temp, "Heterodyne FME+%04dHz", pParams->GetDecHeter());
				// En cas de détection, init de la fréquence hétérodyne
				if (bActivCry and (millis() - timeHeter) > 5000)
				{
					timeHeter = millis();
					cRecorder.GetHeterodyne()->PrepareHeterodyne(iFreqHet, pParams->GetModeHeter(), pParams->GetTrigHeter());
				}
				else if (not bActivCry and (millis() - timeHeter) > 5000)
				{
					timeHeter = 0;
					cRecorder.GetHeterodyne()->PrepareHeterodyne(0, pParams->GetModeHeter(), pParams->GetTrigHeter());
				}
				break;
			case HET_MANU:
				if (pParams->GetLangue() == FR)
					sprintf(temp, "H%ct%crodyne % 5.2f kHz ", 0x82, 0x82, ((float)iFreqHet)/1000.0);
				else
					sprintf(temp, "Heterodyne % 5.2f kHz ", ((float)iFreqHet)/1000.0);
				if (not bInitHetManu)
				{
					cRecorder.GetHeterodyne()->PrepareHeterodyne(iFreqHet, pParams->GetModeHeter(), pParams->GetTrigHeter());
					bInitHetManu = true;
				}
			}
			pKeyOledManager->DrawString(temp, 0, L7T1);
			// 8ème ligne taille 1, "10/12/2014 - 15:38:42" fait par la classe générique ci-dessous
		}
		else
		{
			// Affichage du mode attente
			// 3 lignes en taille 2 Logo Monitoring ou Record et, si Record, temps d'enregistrement en s
			//   1234567890
			//  " ATTENTE  "
			//  " H. Début "
			//  "  12:28   "
			if (pParams->GetLangue() == FR)
			{
				pKeyOledManager->DrawString((char *)" ATTENTE  ", 0, L1T2, true, 2);
				sprintf( temp, " H. d%cbut ", 0x82);
				pKeyOledManager->DrawString( temp, 0, L2T2, true, 2);
			}
			else
			{
				pKeyOledManager->DrawString((char *)" Waitting  ", 0, L1T2, true, 2);
				pKeyOledManager->DrawString((char *)" begin H.", 0, L2T2, true, 2);
			}
			sprintf( temp, "  %s   ", pParams->GetHDeb());
			pKeyOledManager->DrawString( temp, 0, L3T2, true, 2);
		}
		bool bMem = bModeAuto;
		if (!bModeAuto or bDemandeAff)
			bModeAuto = false;
		// Appel de la méthode de base pour l'affichage régulier de la date
		GenericMode::PrintMode();
		bModeAuto = bMem;
		// Test de fin de l'affichage momentané
		if (bDemandeAff)
		{
			unsigned int uiNow = millis();
			if (uiNow - timeAff > 3000)
			{
				LogFile::AddLog(LDEBUG, "ModeMonitoring::PrintMode Fin de l'affichage momentané\n");
				bDemandeAff = false;
				// Effacement de l'écran
				pKeyOledManager->ClearScreen();
				pKeyOledManager->DisplayScreen();
			}
		}
	}
}

//-------------------------------------------------------------------------
// Traitement des ordres claviers
// Retourne éventuellement un mode demandé (NOMODE sinon)
// Cette méthode est appelée régulièrement par la classe PiBatManager
// à charge des différents modes de traiter les actions opérateurs
int  ModeMonitoring::ManageKey(
	unsigned short key	// Touche sollicitée par l'opérateur
	)
{
	int newMode = NOMODE;
	if (!bModeAuto and !bAttenteAuto and key != iLastKey)
	{
		switch (key)
		{
		case K_UP:
			// Monte le seuil d'un dB
			if (pParams->GetThreshold() < 99)
			{
				pParams->SetThreshold(pParams->GetThreshold()+1);
				cRecorder.SetThreshold(pParams->GetThreshold());
			}
			break;
		case K_DOWN:
			// Baisse le seuil d'un dB
			if (pParams->GetThreshold() > 10)
			{
				pParams->SetThreshold(pParams->GetThreshold()-1);
				cRecorder.SetThreshold(pParams->GetThreshold());
			}
			break;
		case K_TRIANGLE:
			// Hétérodyne passe successivement en mode Auto / Manuel / Sans
			switch (pParams->GetModeHeter())
			{
			case HET_SANS:
				pParams->SetModeHeter(HET_MANU);
				break;
			case HET_MANU:
				pParams->SetModeHeter(HET_AUTO);
				break;
			case HET_AUTO:
				pParams->SetModeHeter(HET_SANS);
			}
			// Init du mode hétérodyne
			cRecorder.GetHeterodyne()->PrepareHeterodyne(iFreqHet, pParams->GetModeHeter(), pParams->GetTrigHeter());
			break;
		case K_SQUARE:
			// Force la fréquence de l'hétérodyne à la dernière FME
			if (iLastFME != 0)
				// Init du mode hétérodyne
				cRecorder.GetHeterodyne()->PrepareHeterodyne(iLastFME, pParams->GetModeHeter(), pParams->GetTrigHeter());
			break;
		case K_RIGHT:
			// Hétérodyne +100Hz/1kHz
			if (pParams->GetModeHeter() == HET_AUTO)
			{
				if (pParams->GetDecHeter() < 1900)
					pParams->SetDecHeter(pParams->GetDecHeter()+100);
				else if (pParams->GetDecHeter() < 2000)
					pParams->SetDecHeter(2000);
			}
			else if (pParams->GetModeHeter() == HET_MANU)
			{
				if (iFreqHet <= 95000)
					iFreqHet += 1000;
				else if (iFreqHet > 95000)
					iFreqHet = 96000;
			}
			// Init du mode hétérodyne
			cRecorder.GetHeterodyne()->PrepareHeterodyne(iFreqHet, pParams->GetModeHeter(), pParams->GetTrigHeter());
			break;
		case K_LEFT:
			// Hétérodyne -100Hz/-1kHz
			if (pParams->GetModeHeter() == HET_AUTO)
			{
				if (pParams->GetDecHeter() > 100)
					pParams->SetDecHeter(pParams->GetDecHeter()-100);
				else if (pParams->GetDecHeter() < 100)
					pParams->SetDecHeter(10);
			}
			else if (pParams->GetModeHeter() == HET_MANU)
			{
				if (iFreqHet >= 11000)
					iFreqHet -= 1000;
				else if (iFreqHet < 11000)
					iFreqHet = 10000;
			}
			// Init du mode hétérodyne
			cRecorder.GetHeterodyne()->PrepareHeterodyne(iFreqHet, pParams->GetModeHeter(), pParams->GetTrigHeter());
			break;
		case K_R:
			// Hétérodyne +10Hz/+100Hz
			if (pParams->GetModeHeter() == HET_AUTO)
			{
				if (pParams->GetDecHeter() < 1990)
					pParams->SetDecHeter(pParams->GetDecHeter()+10);
				else if (pParams->GetDecHeter() < 2000)
					pParams->SetDecHeter(2000);
			}
			else if (pParams->GetModeHeter() == HET_MANU)
			{
				if (iFreqHet <= 95900)
					iFreqHet += 100;
				else if (iFreqHet > 95900)
					iFreqHet = 96000;
			}
			// Init du mode hétérodyne
			cRecorder.GetHeterodyne()->PrepareHeterodyne(iFreqHet, pParams->GetModeHeter(), pParams->GetTrigHeter());
			break;
		case K_L:
			// Hétérodyne -10Hz/-100Hz
			if (pParams->GetModeHeter() == HET_AUTO)
			{
				if (pParams->GetDecHeter() > 20)
					pParams->SetDecHeter(pParams->GetDecHeter()-10);
				else if (pParams->GetDecHeter() > 10)
					pParams->SetDecHeter(10);
			}
			else if (pParams->GetModeHeter() == HET_MANU)
			{
				if (iFreqHet >= 10100)
					iFreqHet -= 100;
				else if (iFreqHet < 10100)
					iFreqHet = 10000;
			}
			// Init du mode hétérodyne
			cRecorder.GetHeterodyne()->PrepareHeterodyne(iFreqHet, pParams->GetModeHeter(), pParams->GetTrigHeter());
			break;
		case K_PLUS:
			// Volume +1
			if (pParams->GetVolPlay() < 31)
			{
				pParams->SetVolPlay(pParams->GetVolPlay()+1);
				cRecorder.GetHeterodyne()->SetVolume();
			}
			break;
		case K_MINUS:
			// Volume -1
			if (pParams->GetVolPlay() > 0)
			{
				pParams->SetVolPlay(pParams->GetVolPlay()-1);
				cRecorder.GetHeterodyne()->SetVolume();
			}
			break;
		case K_CIRCLE:
			// Force l'enregistrement
			cRecorder.StartRecord();
			break;
		case K_X:
			// Stoppe l'enregistrement
			cRecorder.StopRecord();
			break;
		default:
			// Traite les changements de mode
			newMode = GenericMode::ManageKey(key);
		}
	}
	else if (key != iLastKey)
	{
		switch (key)
		{
		case K_SQUARE:
			// Demande d'affichage momentané
			LogFile::AddLog(LDEBUG, "ModeMonitoring::ManageKey Demande d'affichage momentané\n");
			bDemandeAff = true;
			timeAff = millis();
			break;
		default:
			// Traite les changements de mode
			newMode = GenericMode::ManageKey(key);
		}
	}
	iLastKey = key;
	return newMode;
}

//-------------------------------------------------------------------------
// Test si on est dans une phase active ou en attente de l'heure de début
bool ModeMonitoring::IsActivePhase()
{
	bool bActivePhase = false;
	if (bModeAuto or pParams->GetRecMode() == AUTO)
	{
		// Calcul de l'heure de départ en secondes depuis 00:00:00
		int iHe, iMn;
		sscanf(pParams->GetHDeb(), "%d:%d", &iHe, &iMn);
		int iHDeb = iHe * 3600 + iMn * 60;
		// Calcul de l'heure de fin en secondes depuis 00:00:00
		sscanf(pParams->GetHFin(), "%d:%d", &iHe, &iMn);
		int iHFin = iHe * 3600 + iMn * 60;
		// Récupération de l'heure courante en secondes depuis 00:00:00
		time_t curtime;
		struct tm * timeinfo;
		time(&curtime);
		timeinfo = localtime (&curtime);
		int iHCur = timeinfo->tm_hour * 3600 + timeinfo->tm_min * 60 + timeinfo->tm_sec;
		
		// Attention, le temps du monitoring est sur la nuit
		// 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23
		// ++++++++++++++++++HF----------------------------------------------HD+++
		// ---HD+++++++++++++HF-------------------------------------------HD++++HF
		// Si on est dans la zone d'activité
		if ( (iHFin < iHDeb and (iHCur <= iHFin or iHCur >= iHDeb))
			or
			 (iHFin > iHDeb and iHCur >= iHDeb and iHCur <= iHFin)	)
		{
			bActivePhase = true;
		}
		// Sinon, on est dans la zone inactive
		else
		{
			bActivePhase = false;
		}
	}
	else
		bActivePhase = true;
	
	return bActivePhase;
}
