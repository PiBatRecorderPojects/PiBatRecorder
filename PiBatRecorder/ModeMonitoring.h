/* 
 * File:   ModeMonitoring.h
 * Author: Jean-Do
 *
 * Created on 10 décembre 2015, 15:06
 */
#include "CRecorder.h"
#include "GenericMode.h"

#ifndef MODEMONITORING_H
#define	MODEMONITORING_H

/*-----------------------------------------------------------------------------
 Classe de gestion du mode Monitoring
 Affiche les paramètres du mode et gère les modifications des paramètres en temps réel
-----------------------------------------------------------------------------*/
class  ModeMonitoring : public GenericMode
{
public:
	//-------------------------------------------------------------------------
	// Constructeur
	 ModeMonitoring(
		CParameters *pPar,					// Pointeur sur les paramètres
		KeyOledManager *pKeyScreenManager,	// Pointeur sur le gestionnaire du clavier et de l'écran
		bool bAutoMode=false				// True si le logiciel fonctionne entièrement en mode automatique
			                                // Si oui, le clavier et l'écran ne sont pas gérés
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~ModeMonitoring();
	
	//-------------------------------------------------------------------------
	// Début du mode
	virtual void BeginMode();
	
	//-------------------------------------------------------------------------
	// Fin du mode
	virtual void EndMode();
	
	//-------------------------------------------------------------------------
	// Affichage du mode sur l'écran
	// Cette méthode est appelée régulièrement par la classe PiBatManager
	// à charge des différents modes d'afficher les informations nécessaires
	virtual void PrintMode();
	
	//-------------------------------------------------------------------------
	// Traitement des ordres claviers
	// Si la touche est une touche de changement de mode, retourne le mode demandé
	// Cette méthode est appelée régulièrement par la classe PiBatManager
	// à charge des différents modes de traiter les actions opérateurs
	virtual int ManageKey(
		unsigned short key	// Touche sollicitée par l'opérateur
		);

	//-------------------------------------------------------------------------
	// Test si on est dans une phase active ou en attente de l'heure de début
	bool IsActivePhase();
	
private:
	// Gestion du monitoring et de l'enregistrement
	CRecorder cRecorder;
	
	// Analyseur des cris
	CAnalyser cAnalyser;
	
	// Gestion du statut du Monitoring
	CMonitStatus GestMonitStatus;
	
	// Etat courant du monitoring
	MonitoringStatus MonitStatus;

	// Indique qu'on est en attente de l'heure de début en auto
	bool bAttenteAuto;
	
	// Indique une demande d'affichage momentanée en mode auto
	bool bDemandeAff;
	
	// Heure de début d'un affichage momentané
	unsigned int timeAff;
	
	// Fréquence de l'hétérodyne en mode manuel (Hz)
	int iFreqHet;
	
	// Indique si l'init de la fréquence hétérodyne a été fait en mode manuel
	bool bInitHetManu;
	
	// Dernière FME
	int iLastFME;
	
	// Heure du dernier changement de la fréquence hétérodyne
	unsigned int timeHeter;
};

#endif	/* MODEMONITORING_H */

