/* 
 * File:   ModeParametres.cpp
 * Author: Jean-Do
 * 
 * Created on 12 décembre 2015, 09:34
 */

#include "RAZParam.h"
#include "ModeParametres.h"
#include "RecVolumeParam.h"
#include "MinRecParam.h"
#include "MaxRecParam.h"
#include "ThresholdParam.h"
#include "MinFreqParam.h"
#include "MaxFreqParam.h"
#include "PrefixParam.h"
#include "VolumeParam.h"
#include "DecHeterParam.h"
#include "RecordTypeParam.h"
#include "RecordModeParam.h"
#include "StartHourParam.h"
#include "StopHourParam.h"
#include "DateParam.h"
#include "HourParam.h"
#include "LanguageParam.h"
#include "AboutParam.h"
#include "OutputParam.h"
#include "TypeHeterodyneParam.h"
#include "TrigHeterodyneParam.h"
#include "MountUSBKey.h"

//-------------------------------------------------------------------------
// Constructeur
ModeParametres::ModeParametres(
	CParameters *pPar,					// Pointeur sur les paramètres
	KeyOledManager *pKeyScreenManager,	// Pointeur sur le gestionnaire du clavier et de l'écran
	bool bAutoMode						// True si le logiciel fonctionne entièrement en mode automatique
										// Si oui, le clavier et l'écran ne sont pas gérés
	):GenericMode(pPar, pKeyScreenManager, bAutoMode)
{
	// Création des modificateurs de paramètres
	LstModifParams.push_back(new ThresholdParam     (pKeyScreenManager, pPar));
	LstModifParams.push_back(new MinFreqParam       (pKeyScreenManager, pPar));
	LstModifParams.push_back(new MaxFreqParam       (pKeyScreenManager, pPar));
	LstModifParams.push_back(new RecVolumeParam     (pKeyScreenManager, pPar));
	LstModifParams.push_back(new MinRecParam        (pKeyScreenManager, pPar));
	LstModifParams.push_back(new MaxRecParam        (pKeyScreenManager, pPar));
	LstModifParams.push_back(new RecordTypeParam    (pKeyScreenManager, pPar));
	LstModifParams.push_back(new RecordModeParam    (pKeyScreenManager, pPar));
	LstModifParams.push_back(new StartHourParam     (pKeyScreenManager, pPar));
	LstModifParams.push_back(new StopHourParam      (pKeyScreenManager, pPar));
	LstModifParams.push_back(new PrefixParam        (pKeyScreenManager, pPar));
	LstModifParams.push_back(new TypeHeterodyneParam(pKeyScreenManager, pPar));
	LstModifParams.push_back(new TrigHeterodyneParam(pKeyScreenManager, pPar));
	LstModifParams.push_back(new DecHeterParam      (pKeyScreenManager, pPar));
	LstModifParams.push_back(new VolumeParam        (pKeyScreenManager, pPar));
	LstModifParams.push_back(new OutputParam        (pKeyScreenManager, pPar));
	LstModifParams.push_back(new DateParam          (pKeyScreenManager, pPar));
	LstModifParams.push_back(new HourParam          (pKeyScreenManager, pPar));
	LstModifParams.push_back(new LanguageParam      (pKeyScreenManager, pPar));
	pRAZParam =				 new RAZParam           (pKeyScreenManager, pPar);
	LstModifParams.push_back(pRAZParam);
	LstModifParams.push_back(new MountUSBKey        (pKeyScreenManager, pPar));
	LstModifParams.push_back(new AboutParam         (pKeyScreenManager, pPar));
	// Récupération des indices des zones à modifier
	for (int i=0; i<LstModifParams.size(); i++)
		LstModifParams[i]->FindBeginEndModif();
}

//-------------------------------------------------------------------------
// Destructeur
ModeParametres::~ModeParametres()
{
	// Destruction des modificateurs de paramètres
	for (int i=0; i<LstModifParams.size(); i++)
		if (LstModifParams[i] != NULL)
			delete LstModifParams[i];
}

//-------------------------------------------------------------------------
// Début du mode
void ModeParametres::BeginMode()
{
	//printf("ModeParametres::BeginMode\n");
	if (!bModeAuto)
	{
		// Init des indices de départ
		iMenu     = 0;
		iMenuUp   = 0;
		iMenuDown = 7;
		bModifOnNextAff = false;
		// Init de la liste des fichiers à effacer
		((RAZParam *)pRAZParam)->FindFiles();
		// Passe le 1er modificateur en modif
		LstModifParams[iMenu]->StartModif();
		// Effacement de l'écran
		pKeyOledManager->ClearScreen();
		pKeyOledManager->DisplayScreen();
	}
}

//-------------------------------------------------------------------------
// Affichage du mode sur l'écran
// Cette méthodes est appelée régulièrement par la classe PiBatManager
// à charge des différents modes d'afficher les informations nécessaires
void ModeParametres::PrintMode()
{
	//printf("PrintMode Up %d, Down %d, iMenu %d, max %d\n", iMenuUp, iMenuDown, iMenu, LstModifParams.size());
	// Positionne le curseur en début d'écran
	int iCursor = 0;
	// Pour l'ensemble des paramètres à afficher
	for (int i=iMenuUp; i<=iMenuDown; i++)
	{
		bool bSelectedMenu = false;
		if (i == iMenu)
			bSelectedMenu = true;
		//printf("PrintMode i %d\n", i);
		if (bModifOnNextAff and i == iMenu and LstModifParams[i]->IsModif() == false and (millis()-timeAff) > 500)
		{
			bModifOnNextAff = false;
			LstModifParams[i]->StartModif();
		}
		// Affichage de la ligne du paramètre
		LstModifParams[i]->AffParam(iCursor, bSelectedMenu);
		// Suite à l'éventuelle validation d'un paramètre par l'opérateur, il n'est plus en modification
		// On positionne les variables pour le rendre de nouveau modifiable après quelques ms
		// Ce mécanisme fait clignotter la zone à modifier pour montrer à l'opérateur la prise en compte de la modif
		if (not bModifOnNextAff and i == iMenu and LstModifParams[i]->IsModif() == false)
		{
			bModifOnNextAff = true;
			timeAff = millis();
		}
		// On avance le curseur d'un ligne
		iCursor += 8;
		//printf("PrintMode i %d OK\n", i);
	}
	pKeyOledManager->DisplayScreen();
	//printf("PrintMode OK\n");
}

//-------------------------------------------------------------------------
// Traitement des ordres claviers
// Retourne éventuellement un mode demandé (NOMODE sinon)
// Cette méthodes est appelée régulièrement par la classe PiBatManager
// à charge des différents modes de traiter les actions opérateurs
int ModeParametres::ManageKey(
	unsigned short key	// Touche sollicitée par l'opérateur
	)
{
	int newMode = NOMODE;
	if (key != iLastKey)
	{
		switch (key)
		{
		case K_UP:
		case K_DOWN:
			// On stoppe la modif du paramètre précédent
			if (iMenu < LstModifParams.size() and LstModifParams[iMenu]->IsModif())
				LstModifParams[iMenu]->StopModif();
			// Paramètre suivant
			if (key == K_UP)
			{
				// Passe au paramètre précédent
				iMenu--;
				if (iMenu < 0)
					iMenu = LstModifParams.size()-1;
			}
			else
			{
				// Passe au paramètre suivant
				iMenu++;
				if (iMenu > LstModifParams.size())
					iMenu = 0;
			}
			// Init des indices des menus sup et inf
			if (iMenu < 3)
			{
				//printf("Cas A\n");
				iMenuUp = 0;
				iMenuDown = iMenuUp + 7;
			}
			else if (iMenu > (LstModifParams.size()-5))
			{
				//printf("Cas B\n");
				iMenuDown = LstModifParams.size()-1;
				iMenuUp = iMenuDown - 7;
			}
			else
			{
				//printf("Cas C\n");
				iMenuUp = iMenu - 3;
				iMenuDown = iMenuUp + 7;
			}
			// On passe en modif le nouveau paramètre
			if (iMenu < LstModifParams.size())
				LstModifParams[iMenu]->StartModif();
			//printf("Up %d, Down %d, iMenu %d, max %d\n", iMenuUp, iMenuDown, iMenu, LstModifParams.size());
			break;
		case K_RIGHT:
		case K_LEFT:
		case K_TRIANGLE:
		case K_CIRCLE:
		case K_X:
		case K_SQUARE:
		case K_R:
		case K_PLUS:
		case K_MINUS:
		case K_L:
			// Traitement par le paramètre sélectionné
			if (iMenu < LstModifParams.size())
				LstModifParams[iMenu]->ReceiveKey(key);
			break;
		default:
			// Traitement générique
			newMode = GenericMode::ManageKey(key);
		}
	}
	iLastKey = key;
	return newMode;
}

