/* 
 * File:   ModePlay.cpp
 * Author: Jean-Do
 * 
 * Created on 10 décembre 2015, 16:50
 */

#include "ModePlay.h"

//-------------------------------------------------------------------------
// Constructeur
 ModePlay:: ModePlay(
	CParameters *pPar,					// Pointeur sur les paramètres
	KeyOledManager *pKeyScreenManager,	// Pointeur sur le gestionnaire du clavier et de l'écran
	bool bAutoMode						// True si le logiciel fonctionne entièrement en mode automatique
										// Si oui, le clavier et l'écran ne sont pas gérés
	):GenericMode(pPar, pKeyScreenManager, bAutoMode)
	, cPlayer(pPar)
{
	iFile = 0;
	bPlaying = false;
	bX10 = true;
	bPause = false;
	bRAZFile = false;
	bConfirmRAZ = false;
}

//-------------------------------------------------------------------------
// Destructeur
 ModePlay::~ ModePlay()
{
}

//-------------------------------------------------------------------------
// Fonction C de comparaison de nom de fichiers
int compare (const void * a, const void * b)
{
  if ( *(std::string*)a < *(std::string*)b )
	  // A est plus petit que B, il faut donc le placer après
	  return 1;
  return -1;
}

//-------------------------------------------------------------------------
// Début du mode
void  ModePlay::BeginMode()
{
	printf("ModePlay::BeginMode\n");
	bPlaying = false;
	bX10 = true;
	bPause = false;
	bRAZFile = false;
	bConfirmRAZ = false;
	if (!bModeAuto)
	{
		char temp[255];
		strcpy( temp, pParams->GetWolfsonShPath());
		//printf("GetWolfsonShPath [%s]\n", temp);
		// Sélection de la sortie utilisée
		switch (pParams->GetOutput())
		{
		default:
		case OUTHEADSET:
			// Sélection de la lecture via la prise casque
			strcat( temp, "Playback_to_Headset.sh -q");
			break;
		case OUTLINE:
			// Sélection de la lecture via la sortie ligne
			strcat( temp, "Playback_to_Lineout.sh -q");
			break;
		case OUTLS:
			// Sélection de la lecture via la sortie HP
			strcat( temp, "Playback_to_Speakers.sh -q");
		}
		//printf("cmd [%s]\n", temp);
		system( temp);
		// Effacement de l'écran
		pKeyOledManager->ClearScreen();
		// On prépare la liste des fichiers présents
		lstWaveFiles.clear();
		DIR *rep = opendir(pParams->GetWavPath());   
		if (rep != NULL) 
		{ 
			struct dirent *ent; 
			while ((ent = readdir(rep)) != NULL) 
			{
				//0123456789
				//toto.wav
				//printf("Ajout %s\n", ent->d_name);
				std::string wavePath(pParams->GetWavPath());
				wavePath += ent->d_name;
				if (wavePath.substr(wavePath.size()-4, 4) == ".wav")
				{
					printf("Ajout du fichier %s\n", wavePath.data());
					lstWaveFiles.push_back(wavePath);
				}
			} 
			closedir(rep); 
		}
		if (lstWaveFiles.size() > 0)
		{
			// Trie de la liste par ordre alphabétique, les noms des fichiers étant taguer
			// avec la date ét l'heure ils seront dans l'orde du plus récent au plus ancien
			qsort (lstWaveFiles.data(), lstWaveFiles.size(), sizeof(std::string), compare);	
			// On positionne le fichier actif au 1er (le plus récent)
			iFile = 0;
			cPlayer.SetFile( (char *)lstWaveFiles[iFile].data(), bX10);
			fCurPlay = 0.0;
		}
		else
			iFile = -1;
	}
	bPause = false;
}

//-------------------------------------------------------------------------
// Fin du mode
void ModePlay::EndMode()
{	
	printf("ModePlay::EndMode\n");
	// Si une lecture est en cours
	if (bPlaying or cPlayer.bIsRunning())
		// Arrêt lecture
		cPlayer.Stop();
	else
		// Fermeture fichier wav sélectionné
		cPlayer.SetFile( NULL, bX10);
}

//-------------------------------------------------------------------------
// Affichage du mode sur l'écran
// Cette méthode est appelée régulièrement par la classe PiBatManager
// à charge des différents modes d'afficher les informations nécessaires
void  ModePlay::PrintMode()
{
	// Si fin de lecture on se met en arrêt lecture
	if (!cPlayer.bIsRunning() and bPlaying)
	{
		bPlaying = false;
		cPlayer.Stop();
	}
	if (!bModeAuto)
	{
		char sFileName[22];
		char sTemp[22];
		char sOut[22];
		char sTotalWaveName[256];
		int y;
		// Affichage du monitoring
		if (iFile >= 0)
		{
			// Récupération de la durée du fichier et de la position du curseur
			cPlayer.GetInfo( &fCurPlay, &fLengthPlay);
			// 1ère ligne en taille 2 Logo Play ou Pause et curseur de position de la lecture
			//   1234567890
			//  "|| ____--"
			//  "-- ____--"
			pKeyOledManager->DrawString((char *)"  ", 0, 0, true, 2);
			if (cPlayer.bIsRunning())
			{
				pKeyOledManager->DrawTriangle( 0, 0, 14, 7, 0, 14, true);
			}
			else
			{
				pKeyOledManager->DrawRectangle( 0, 0, 5, 14, true);
				pKeyOledManager->DrawRectangle( 8, 0, 5, 14, true);
			}
			// Affichage de la position du curseur dans le fichier
			pKeyOledManager->DrawRectangle( 18, 0, 110, 14, true, false);
			pKeyOledManager->DrawRectangle( 18, 0, 110, 14, false);
			if (bPlaying or bPause)
			{
				float x = fCurPlay / fLengthPlay * 112;
				//printf("x %f\n", x);
				pKeyOledManager->DrawRectangle( 18+(int)x, 0, 110-(int)x, 14, true);
			}
			if (bRAZFile)
			{
				//                       123456789012345678901"
				// 3ème ligne taille 1, "Effacem. fichier? Oui"
				// 3ème ligne taille 1, "Effacem. fichier? Non"
				// 3ème ligne taille 1, "      Del file ?  Yes"
				// 3ème ligne taille 1, "      Del file ?  No "
				y = 19 * 5;
				if (pParams->GetLangue() == FR)
					pKeyOledManager->DrawString((char *)"Effacem. fichier? ", 0, L3T1, true);
				else
					pKeyOledManager->DrawString((char *)"      Del file ?  ", 0, L3T1, true);
				if (bConfirmRAZ)
				{
					if (pParams->GetLangue() == FR)
						pKeyOledManager->DrawString((char *)"Oui", y, L3T1, false);
					else
						pKeyOledManager->DrawString((char *)"Yes", y, L3T1, false);
				}
				else
				{
					if (pParams->GetLangue() == FR)
						pKeyOledManager->DrawString((char *)"Non", y, L3T1, false);
					else
						pKeyOledManager->DrawString((char *)"No ", y, L3T1, false);
				}
			}
			else
			{
				//                       123456789012345678901"
				// 3ème ligne taille 1, "X10xxx/xxxs Casqu Vxx"
				// 3ème ligne taille 1, "X10xxx/xxxs Ligne Vxx"
				// 3ème ligne taille 1, "X10xxx/xxxs HP    Vxx"
				// 3ème ligne taille 1, "X10xxx/xxxs Heads Vxx"
				// 3ème ligne taille 1, "X10xxx/xxxs Line  Vxx"
				// 3ème ligne taille 1, "X10xxx/xxxs Louds Vxx"
				switch (pParams->GetOutput())
				{
				default:
				case OUTHEADSET:
					if (pParams->GetLangue() == FR)
						sprintf(sOut, "Casqu");
					else
						sprintf(sOut, "Heads");
					break;
				case OUTLINE:
					if (pParams->GetLangue() == FR)
						sprintf(sOut, "Ligne");
					else
						sprintf(sOut, "Line ");
					break;
				case OUTLS:
					if (pParams->GetLangue() == FR)
						sprintf(sOut, "HP   ");
					else
						sprintf(sOut, "Louds");
					break;
				}
				if (bX10)
					sprintf( sTemp, "X10% 3d/% 3ds %s V%02d", (int)fCurPlay, (int)fLengthPlay, sOut, pParams->GetVolPlay());
				else
					sprintf( sTemp, "X1 % 3d/% 3ds %s V%02d", (int)fCurPlay, (int)fLengthPlay, sOut, pParams->GetVolPlay());
				pKeyOledManager->DrawString(sTemp, 0, L3T1, true);
			}
			//                       123456789012345678901"
			// 4ème à 7ème lignes taille 1, "NOMFILExxxxxxxxxx xxs" si fichier actif en noir sur blanc
			int iDeb, iFin, iNb, iMax;
			iNb = lstWaveFiles.size();
			iMax = 4;
			if (iFile == 0)
			{
				// Premier fichier sélectionné
				// 0             iFile
				// 0 1 2 3 4 5 6 Liste Wav
				// 0 1 2 3       Position écran
				iDeb = 0;
				if (iNb > iMax)
					iFin = iDeb + iMax - 1;
				else
					iFin = iDeb + iNb - 1;
			}
			else if (iFile > iNb-iMax)
			{
				// Dernier fichier = dernier de la liste
				//         4     iFile
				// 0 1 2 3 4 5 6 Liste Wav
				//       0 1 2 3 Position écran
				iFin = iNb - 1;
				if (iNb > iMax)
					iDeb = iFin - iMax + 1;
				else
					iDeb = iFin - iNb + 1;
			}
			else
			{
				// Premier fichier = le précédent de celui sélectionné
				//       3       iFile 
				// 0 1 2 3 4 5 6 Liste Wav
				//     0 1 2 3   Position écran
				iDeb = iFile - 1;
				if (iNb > iMax)
					iFin = iDeb + iMax - 1;
				else
					iFin = iDeb + iNb - 1;
			}
			//printf("iFile %d, iNb %d, iDeb %d, iFin %d, iMax %d\n", iFile, iNb, iDeb, iFin, iMax);
			y = L4T1;
			for (int i=iDeb; i<=iFin; i++)
			{
				strcpy( sTotalWaveName, lstWaveFiles[i].data());
				//printf("%d, sTotalWaveName=[%s]\n", i, sTotalWaveName);
				char *pChar;
				// Init du nom, on prend les 21 dernières lettres sans compter .wav
				//           [0123456789012345678901]
				// 0000000000111111111122222222223333333 37 - 4 - 22
				// 0123456789012345678901234567890123456
				// /mnt/usbkey/PiBat_20150919_231502.wav
				if (strlen(sTotalWaveName) > 24)
				{
					strncpy(sFileName, &(sTotalWaveName[strlen(sTotalWaveName)-25]), 21);
					sFileName[22] = 0;
				}
				else if (strlen(sTotalWaveName) <= 21)
					strcpy(sFileName, sTotalWaveName);
				else
				{
					strncpy(sFileName, sTotalWaveName, 21);
					sFileName[22] = 0;
				}
				// Affichage de la ligne
				if (i == iFile)
					pKeyOledManager->DrawString(sFileName, 0, y, false);
				else
					pKeyOledManager->DrawString(sFileName, 0, y, true);
				//printf("%d, sFileName=[%s]\n", i, sFileName);
				// Ligne suivante
				y += 8;
			}
		}
		else
		{
			// Lecture impossible, pas de fichier !
			int y = L3T1;
			if (pParams->GetLangue() == FR)
				pKeyOledManager->DrawString((char *)"0 fichier!", 0, 0, true, 2);
			else
				pKeyOledManager->DrawString((char *)"0 file !  ", 0, 0, true, 2);
			for (int i=0; i<5; i++)
			{
				pKeyOledManager->DrawString((char *)"                     ", 0, y, true, 1);
				// Ligne suivante
				y += 8;
			}
		}
		// 8ème ligne taille 1, "10/12/2014 - 15:38:42" fait par la classe générique ci-dessous
	}
	// Appel de la méthode de base pour l'affichage régulier de la date
	GenericMode::PrintMode();
}

//-------------------------------------------------------------------------
// Traitement des ordres claviers
// Retourne éventuellement un mode demandé (NOMODE sinon)
// Cette méthode est appelée régulièrement par la classe PiBatManager
// à charge des différents modes de traiter les actions opérateurs
int ModePlay::ManageKey(
	unsigned short key	// Touche sollicitée par l'opérateur
	)
{
	int newMode = NOMODE;
	if (key != iLastKey)
	{
		switch (key)
		{
		case K_UP:
			if (bRAZFile)
				TraiteRAZFile(key);
			// Fichier précédent
			else if (!bPlaying and iFile > 0)
			{
				// On sélectionne le fichier précédent
				iFile--;
				cPlayer.SetFile( (char *)lstWaveFiles[iFile].data(), bX10);
				fCurPlay = 0.0;
			}
			break;
		case K_DOWN:
			if (bRAZFile)
				TraiteRAZFile(key);
			// Fichier suivant
			else if (!bPlaying and iFile < lstWaveFiles.size()-1)
			{
				// On sélectionne le fichier suivant
				iFile++;
				cPlayer.SetFile( (char *)lstWaveFiles[iFile].data(), bX10);
				fCurPlay = 0.0;
			}
			break;
		case K_TRIANGLE:
			// Lance la lecture X10
			if (!bPlaying and !bRAZFile)
			{
				bX10 = true;
				cPlayer.SetFile( (char *)lstWaveFiles[iFile].data(), bX10);
				if (bPause)
				{
					bool bOK = cPlayer.SetPosRead(fCurPause);
					printf("Lecture suite pause %f = %d\n", fCurPause, bOK);
				}
				cPlayer.Start();
				bPlaying = true;
				bPause = false;
			}
			else if (bRAZFile)
				TraiteRAZFile(key);
			break;
		case K_CIRCLE:
			// Lance la lecture X1
			if (!bPlaying and !bRAZFile)
			{
				bX10 = false;
				cPlayer.SetFile( (char *)lstWaveFiles[iFile].data(), bX10);
				if (bPause)
				{
					bool bOK = cPlayer.SetPosRead(fCurPause);
					printf("Lecture suite pause %f = %d\n", fCurPause, bOK);
				}
				cPlayer.Start();
				bPlaying = true;
				bPause = false;
			}
			else if (bRAZFile)
				TraiteRAZFile(key);
			break;
		case K_SQUARE:
			// Pause lecture
			if (bPlaying and !bRAZFile)
			{
				fCurPause = fCurPlay;
				cPlayer.Stop();
				bPause = true;
			}
			else if (bRAZFile)
				TraiteRAZFile(key);
			break;
		case K_X:
			// Arrêt lecture
			if (bPlaying or bPause)
			{
				fCurPause = 0.0;
				cPlayer.Stop();
				bPlaying = false;
				bPause = false;
			}
			else
				TraiteRAZFile(key);
			break;
		case K_LEFT:
			if (bRAZFile)
				TraiteRAZFile(key);
			else
				// On recule de 1/5 dans le fichier
				cPlayer.NextTime(-1, 5);
			break;
		case K_RIGHT:
			if (bRAZFile)
				TraiteRAZFile(key);
			else
				// On avance de 1/5 dans le fichier
				cPlayer.NextTime(1, 5);
			break;
		case K_L:
			if (bRAZFile)
				TraiteRAZFile(key);
			else
				// Recule de 1/10 dans le fichier
				cPlayer.NextTime(-1, 10);
			break;
		case K_R:
			if (bRAZFile)
				TraiteRAZFile(key);
			else
				// Avance de 1/10 dans le fichier
				cPlayer.NextTime(1, 10);
			break;
		case K_PLUS:
			if (bRAZFile)
				TraiteRAZFile(key);
			else
				// Volume +1
				if (pParams->GetVolPlay() < 31)
				{
					pParams->SetVolPlay(pParams->GetVolPlay()+1);
					cPlayer.SetVolume();
				}
			break;
		case K_MINUS:
			if (bRAZFile)
				TraiteRAZFile(key);
			else
				// Volume -1
				if (pParams->GetVolPlay() > 0)
				{
					pParams->SetVolPlay(pParams->GetVolPlay()-1);
					cPlayer.SetVolume();
				}
			break;
		default:
			// Traite les changements de mode
			newMode = GenericMode::ManageKey(key);
		}
	}
	iLastKey = key;

	return newMode;
}

//-------------------------------------------------------------------------
// Traitement des ordres en mode RAZ du fichier sélectionné
void ModePlay::TraiteRAZFile(
	unsigned short key	// Touche sollicitée par l'opérateur
	)
{
	switch (key)
	{
	case K_UP:
	case K_DOWN:
	case K_CIRCLE:
	case K_SQUARE:
	case K_LEFT:
	case K_RIGHT:
	case K_L:
	case K_R:
	case K_PLUS:
	case K_MINUS:
		// En mode RAZ du fichier sélectionné, on inverse la confirmation
		if (bRAZFile)
			bConfirmRAZ = !bConfirmRAZ;
		break;
	case K_TRIANGLE:
		// Test si confirmation effacement
		if (bRAZFile and bConfirmRAZ)
		{
			// Oui, on efface le fichier et on sort du mode effacement
			char temp[255];
			// Effacment du fichier
			sprintf(temp, "sudo rm %s", lstWaveFiles[iFile].data());
			system(temp);
			// On enlève le fichier de la liste
			lstWaveFiles.erase(lstWaveFiles.begin()+iFile);
			if (iFile >= lstWaveFiles.size())
				iFile--;
			else if (iFile < 0)
				iFile = 0;
			if (lstWaveFiles.size() == 0)
				iFile = -1;
			// On sort du mode
			bRAZFile = false;
			bConfirmRAZ = false;
		}
		break;
	case K_X:
		// On passe en mode RAZ du fichier sélectionné ou on en sort sans rien faire
		if (bRAZFile)
		{
			bRAZFile = false;
			bConfirmRAZ = false;
		}
		else
		{
			bRAZFile = true;
			bConfirmRAZ = false;
		}
		break;
	}
}
