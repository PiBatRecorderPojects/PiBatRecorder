/* 
 * File:   ModePlay.h
 * Author: Jean-Do
 *
 * Created on 10 décembre 2015, 16:50
 */
#include <string>
#include <vector>
#include "CPlay.h"
#include "GenericMode.h"

#ifndef MODEPLAY_H
#define	MODEPLAY_H

/*-----------------------------------------------------------------------------
 Classe de gestion du mode Lecture
 Affiche les paramètres du mode et gère les modifications des paramètres en temps réel
-----------------------------------------------------------------------------*/
class ModePlay : public GenericMode
{
public:
	//-------------------------------------------------------------------------
	// Constructeur
	 ModePlay(
		CParameters *pPar,					// Pointeur sur les paramètres
		KeyOledManager *pKeyScreenManager,	// Pointeur sur le gestionnaire du clavier et de l'écran
		bool bAutoMode=false				// True si le logiciel fonctionne entièrement en mode automatique
			                                // Si oui, le clavier et l'écran ne sont pas gérés
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~ModePlay();
	
	//-------------------------------------------------------------------------
	// Début du mode
	virtual void BeginMode();
	
	//-------------------------------------------------------------------------
	// Fin du mode
	virtual void EndMode();
	
	//-------------------------------------------------------------------------
	// Affichage du mode sur l'écran
	// Cette méthode est appelée régulièrement par la classe PiBatManager
	// à charge des différents modes d'afficher les informations nécessaires
	virtual void PrintMode();
	
	//-------------------------------------------------------------------------
	// Traitement des ordres claviers
	// Si la touche est une touche de changement de mode, retourne le mode demandé
	// Cette méthode est appelée régulièrement par la classe PiBatManager
	// à charge des différents modes de traiter les actions opérateurs
	virtual int ManageKey(
		unsigned short key	// Touche sollicitée par l'opérateur
		);
	
	//-------------------------------------------------------------------------
	// Traitement des ordres en mode RAZ du fichier sélectionné
	void TraiteRAZFile(
		unsigned short key	// Touche sollicitée par l'opérateur
		);

private:
	// Player
	CPlay cPlayer;
	
	// Liste des fichiers wav présents
	std::vector<std::string> lstWaveFiles;
	
	// Indice dans la liste des fichiers
	int iFile;
	
	// Indique si une lecture est en cours
	bool bPlaying;
	
	// Inidque si on est en expansion de temps
	bool bX10;
	
	// Position du curseur de lecture
	float fCurPlay;
	
	// Durée du fichier sélectionné
	float fLengthPlay;
	
	// Mémorisation de la position du curseur sur pause
	float fCurPause;
	
	// Inique si on est en pause
	bool bPause;
	
	// Indique si on est en mode effacement du fichier sélectionné
	bool bRAZFile;
	
	// Indique si la confirmation d'effacmenet du fichier sélectionnée est affichée
	bool bConfirmRAZ;
};

#endif	/* MODEPLAY_H */

