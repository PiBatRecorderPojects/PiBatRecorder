/* 
 * File:   MountUSBKey.cpp
 * Author: Jean-Do
 * 
 * Created on 24 décembre 2015, 15:04
 */
//-------------------------------------------------------------------------
// Classe pour monter/démonter la clé USB

#include <stdio.h>
#include <dirent.h>
#include <list>
#include "MountUSBKey.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
MountUSBKey::MountUSBKey(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Monter clé USB ? Non
	//                    Mount USB key  ?  No
	//                    Démonter clé USB?Non
	//                    Umount USB key ?  No
	strcpy( indicModif, "0000000000000000001xx");
	bConfirm  = false;
	timeBegin = millis();
}

//-------------------------------------------------------------------------
// Destructeur
MountUSBKey::~MountUSBKey()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void MountUSBKey::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_PLUS:
		case K_R:
		case K_CIRCLE:
		case K_MINUS:
		case K_L:
		case K_SQUARE:
		case K_LEFT:
		case K_RIGHT:
			// On inverse la valeur
			bConfirm = not bConfirm;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void MountUSBKey::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (millis() - timeBegin > 1000)
	{
		// Test de la présence de la clé
		bUSBKey = pParams->isUSBKey();
		timeBegin = millis();
	}
	//                   123456789012345678901
	//                    Monter clé USB ? Non
	//                    Mount USB key  ?  No
	//                    Démonter clé USB?Non
	//                    Umount USB key ?  No
	if (bUSBKey)
	{
		// Clé USB montée
		if (pParams->GetLangue() == FR)
		{
			if (bConfirm)
				sprintf(lineParam, " D%cmonter cl%c USB?Oui", 0x82, 0x82);
			else
				sprintf(lineParam, " D%cmonter cl%c USB?Non", 0x82, 0x82);
		}
		else
		{
			if (bConfirm)
				sprintf(lineParam, " Umount USB key ? Yes");
			else
				sprintf(lineParam, " Umount USB key ? No ");
		}
	}
	else
	{
		if (pParams->GetLangue() == FR)
		{
			if (bConfirm)
				sprintf(lineParam, " Monter cl%c USB ? Oui", 0x82);
			else
				sprintf(lineParam, " Monter cl%c USB ? Non", 0x82);
		}
		else
		{
			if (bConfirm)
				sprintf(lineParam, " Mount USB key  ? Yes");
			else
				sprintf(lineParam, " Mount USB key  ? No ");
		}
	}
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void MountUSBKey::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void MountUSBKey::SetParam()
{
	// Vérification du paramètre
	bModif = false;
	if (bConfirm)
	{
		if (bUSBKey)
			// Clé présente et confirmation, on la démonte
			pParams->UMountUSBKey();
		else
			// Clé absente et confirmation, on la monte
			pParams->MountUSBKey();
		bConfirm = false;
		// Réinit des path de sauvegarde
		pParams->SetSavePaths();
		// Réinit d ela présence de la clé USB
		bUSBKey = pParams->isUSBKey();
	}
	// Appel de la méthode parente
	GenericParam::SetParam();
}
