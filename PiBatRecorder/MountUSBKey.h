/* 
 * File:   MountUSBKey.h
 * Author: Jean-Do
 *
 * Created on 24 décembre 2015, 15:04
 */
#include "GenericParam.h"

#ifndef MOUNTUSBKEY_H
#define	MOUNTUSBKEY_H

//-------------------------------------------------------------------------
// Classe pour monter/démonter la clé USB
class MountUSBKey : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	MountUSBKey(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~MountUSBKey();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Confirmation de la commande
	bool bConfirm;
	// Présence ou non de la clé USB
	bool bUSBKey;
	// Mémorisation du temps pour tester la présence de la clé toute les secondes
	unsigned int timeBegin;
};

#endif	/* MOUNTUSBKEY_H */

