/* 
 * File:   OutputParam.cpp
 * Author: Jean-Do
 * 
 * Created on 21 décembre 2015, 12:24
 */
//-------------------------------------------------------------------------
// Classe de modification de la sortie audio

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "OutputParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
OutputParam::OutputParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Sortie Casque
	//                    Sortie Ligne
	//                    Sortie Haut-parleur
	//                    Output Headset
	//                    Output Line
	//                    Output Loudspeaker
	strcpy( indicModif, "000000001xxxxxxxxxxx0");
	// Mémo de la valeur courante
	iOldType = pParams->GetOutput();
	iNewType = iOldType;
}

//-------------------------------------------------------------------------
// Destructeur
OutputParam::~OutputParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void OutputParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_PLUS:
		case K_R:
			// On incrémente la valeur de 1
			iNewType += 1;
			if (iNewType > OUTLS)
				iNewType = OUTHEADSET;
			break;
		case K_MINUS:
		case K_L:
			// On décrémente la valeur de 1
			iNewType -= 1;
			if (iNewType < OUTHEADSET)
				iNewType = OUTLS;
			break;
		case K_SQUARE:
			// Init de la valeur min
			iNewType = OUTHEADSET;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			iNewType = OUTLS;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			iNewType = iOldType;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void OutputParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
		iNewType = pParams->GetOutput();
	//                   123456789012345678901
	//                    Sortie Casque
	//                    Sortie Ligne
	//                    Sortie Haut-parleur
	//                    Output Headset
	//                    Output Line
	//                    Output Loudspeaker
	switch (iNewType)
	{
	default:
	case OUTHEADSET:
		if (pParams->GetLangue() == FR)
			sprintf(lineParam, " Sortie Casque");
		else
			sprintf(lineParam, " Output Headset");
		break;
	case OUTLINE:
		if (pParams->GetLangue() == FR)
			sprintf(lineParam, " Sortie Ligne");
		else
			sprintf(lineParam, " Output Line");
		break;
	case OUTLS:
		if (pParams->GetLangue() == FR)
			sprintf(lineParam, " Sortie Haut-parleur");
		else
			sprintf(lineParam, " Output Loudspeaker");
		break;
	}
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void OutputParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	iOldType = pParams->GetOutput();
	iNewType = iOldType;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void OutputParam::SetParam()
{
	// Vérification du paramètre
	if (iNewType < OUTHEADSET or iNewType > OUTLS)
		// Valeur précédente
		iNewType = iOldType;
	pParams->SetOutput((AUDIOOUTPUT)iNewType);
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}


