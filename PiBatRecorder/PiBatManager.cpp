/* 
 * File:   PiBatManager.cpp
 * Author: Jean-Do
 * 
 * Created on 17 août 2015, 21:13
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <wiringPi.h>
#include <dirent.h> 
#include "PiBatManager.h"

//-------------------------------------------------------------------------
// Constructeur
PiBatManager::PiBatManager(
	bool bAuto	// True si fonctionnement entièrement automatique
	)
	: GestInit(&cParams, &KScreen, bAuto)
	, GestModeParams(&cParams, &KScreen, bAuto)
	, GestMonitoring(&cParams, &KScreen, bAuto)
	, GestPlay(&cParams, &KScreen, bAuto)
	, GestQuit(&cParams, &KScreen, bAuto)
	, GestFin(&cParams, &KScreen, bAuto)
{
	//printf("PiBatRecorder::PiBatRecorder\n");		
	// Init des variables
	bQuit = false;
	bAutoMode = bAuto;
	iCurrentMode = NOMODE;
	// Si, pas de clavier ou écran, on passe automatiquement en mode auto
	if (cParams.GetScreen() == NOSCREEN or cParams.GetKeyboard() == NOKEYBOARD)
		bAutoMode = true;
	// Si mode auto, on force le fonctionnement en monitoring auto
	if (bAutoMode)
	{
		cParams.SetModeAuto( true);
		cParams.SetRecMode( AUTO);
		// Mémo du démarrage de PiBatRecorder dans le log
		LogFile::AddLog(LLOG, "PiBatRecorder start (mode auto)");
	}
	else
	{
		// Initialisation de l'écran et du clavier
		KScreen.InitScreenAndKeyboard( cParams.GetScreen(), cParams.GetKeyboard());
		// Mémo du démarrage de PiBatRecorder dans le log
		LogFile::AddLog(LLOG, "PiBatRecorder start (mode normal)");
	}
}

//-------------------------------------------------------------------------
// Destructeur
PiBatManager::~PiBatManager()
{
}

//-------------------------------------------------------------------------
// Séquence d'initialisation du programme
void PiBatManager::Init()
{
	// Reset de la carte audio
	char temp[255];
	strcpy( temp, cParams.GetWolfsonShPath());
	strcat( temp, "Reset_paths.sh -q");
	system(temp);

	// On essai de monter la clé USB
	cParams.MountUSBKey();
	// Init des répertoires de sauvegarde sur la clé ou en local
	cParams.SetSavePaths();
}

//-------------------------------------------------------------------------
// Boucle principale du programme
// Lecture du clavier et gestion des modes de fonctionnement
void PiBatManager::Run()
{
	// Par défaut, le gestionnaire courant est l'init
	pCurrentGest = &GestInit;
	pCurrentGest->BeginMode();
	// Mémorisation de lheure du dernier test de sortie en auto
	unsigned int timeTestExit;
	timeTestExit = millis();
	// Mémorisation de l'heure du dernier affichage
	// pour cadencer l'affichage sur 200ms
	unsigned int timeAff;
	timeAff = millis();
	// Mémorisation de l'heure de la derniere modif hétrodyne
	// pour cadencer le changement de fréquence hétérodyne
	unsigned int timeHet;
	timeHet = millis();
	// Mémorisation de l'heure du dernier clic clavier
	// pour autoriser la même touche après 500ms
	unsigned int timeKey;
	timeKey = millis();
	// Boucle principale de lecture du clavier, affichage, changement de mode...
	while (!bQuit)
	{
		//printf("timeNow %d (%dms)\n", millis(), millis() - timeNow);
		// Mémo de l'heure courante
		timeNow = millis();
		// On force l'affichage toutes les 200ms
		if ((timeNow - timeAff) > 200)
		{
			// On mémorise l'heure courante
			timeAff = timeNow;
			// On affiche le mode courant
			pCurrentGest->PrintMode();
		}

		// On teste le clavier toutes les 50ms
		if ((timeNow - timeKey) > 50)
		{
			// Lecture du clavier et appel systématique de la fonction ManageKey
			// car des timers sont éventuellement gérés dans cette méthode
			int iNewMode = pCurrentGest->ManageKey(KScreen.GetKey());
			if (iNewMode != NOMODE)
				// Changement de mode de fonctionnement
				bQuit = SetMode(iNewMode);
		}
		
		// On teste la sortie en mode auto toutes les secondes
		if (bAutoMode and (timeNow - timeTestExit) > 1000)
		{
			// Test de présence du fichier PiBatStop.txt
			FILE *fc = fopen ("/home/pi/PiBatRecorder/PiBatStop.txt", "r");
			if (fc != NULL)
			{
				// Fichier présent, il faut sortir de PiBatRecorder
				LogFile::AddLog(LLOG, "PiBatRecorder end by PiBatStop.txt");
				fclose(fc);
				// Fin du mode précédent
				pCurrentGest->EndMode();
				delay(200);
				// Demande de sortie du logiciel
				bQuit = true;
			}
			else
				timeTestExit = timeNow;
		}
	}
	// Si shutdown demandé
	if (GestQuit.IsWithHalt())
	{	
		LogFile::AddLog(LLOG, "PiBatRecorder go to halt system");
		system("sudo halt");
	}
	else
		LogFile::AddLog(LLOG, "PiBatRecorder go to end whitout halt\n");		
}

//-------------------------------------------------------------------------
// Change le mode courant
// Retourne true si on doit quitter le programme
bool PiBatManager::SetMode( int newMode)
{
	if (newMode != iCurrentMode)
	{
		// Traitement d'un changement de mode
		bQuit = false;
		// Fin du mode précédent
		pCurrentGest->EndMode();
		// Init du pointeur sur le nouveau mode
		switch (newMode)
		{
		case INIT:
			LogFile::AddLog(LLOG, "PiBatRecorder go to INIT");
			pCurrentGest = &GestInit;
			break;
		case MENU:
			LogFile::AddLog(LLOG, "PiBatRecorder go to MENU");
			pCurrentGest = &GestModeParams;
			break;
		case MONITORING:
			LogFile::AddLog(LLOG, "PiBatRecorder go to MONITORING");
			pCurrentGest = &GestMonitoring;
			break;
		case PLAY:
			LogFile::AddLog(LLOG, "PiBatRecorder go to PLAY");
			pCurrentGest = &GestPlay;
			break;
		case QUIT:
			LogFile::AddLog(LLOG, "PiBatRecorder go to QUIT");
			pCurrentGest = &GestQuit;
			// Mémo du mode précédent
			GestQuit.SetOldMode((MODEFONC)iCurrentMode);
			break;
		case END:
			LogFile::AddLog(LLOG, "PiBatRecorder go to END");
			pCurrentGest = &GestFin;
			break;
		case ERROR:
			LogFile::AddLog(LLOG, "PiBatRecorder go to ERROR");
			pCurrentGest = &GestModeParams;
			break;
		case EXIT:
		default:
			LogFile::AddLog(LLOG, "PiBatRecorder go to EXIT");
			bQuit = true;
		}
		if (!bQuit)
			// Début du nouveau mode
			pCurrentGest->BeginMode();
		// Mémo du mode courant
		iCurrentMode = newMode;
	}
	
	return bQuit;
}

