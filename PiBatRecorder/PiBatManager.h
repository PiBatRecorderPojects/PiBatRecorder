/* 
 * File:   PiBatManager.h
 * Author: Jean-Do
 *
 * Created on 17 août 2015, 21:13
 */

#include "ModePlay.h"
#include "ModeMonitoring.h"
#include "KeyOledManager.h"
#include "CParameters.h"
#include "ModeInit.h"
#include "ModeParametres.h"
#include "QuitMode.h"
#include "ModeEnd.h"

#ifndef PIBATMANAGER_H
#define	PIBATMANAGER_H

/*-----------------------------------------------------------------------------
 Classe principale du PiBatRecorder
 Gestion du clavier et de l'afficheur LCD
-----------------------------------------------------------------------------*/
class PiBatManager
{
public:
	//-------------------------------------------------------------------------
	// Constructeur
	PiBatManager(
			bool bAuto=false // True si fonctionnement entièrement automatique
		);
	
	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~PiBatManager();

	//-------------------------------------------------------------------------
	// Change le mode courant
	// Retourne true si on doit quitter le programme
	bool SetMode( int newMode);

	//-------------------------------------------------------------------------
	// Séquence d'initialisation du programme
	void Init();

	//-------------------------------------------------------------------------
	// Boucle principale du programme
	// Lecture du clavier et gestion des modes de fonctionnement
	void Run();
	
	//-------------------------------------------------------------------------
	// Monte la clé USB
	// Retourne True si la clé USB est montée
	bool MountUSBKey();
	
	//-------------------------------------------------------------------------
	// Traitement du lancement et de l'arrêt du monitoring auto
	void TraiteMonitoringAuto();
	
private:
	// Gestionnaire clavier/LCD
	KeyOledManager KScreen;
	// Indique si un mode entièrement auto est demandé
	bool bAutoMode;
	// Indique si une sortie est en cours
	bool bQuit;
	// Heure courante
	unsigned int timeNow;
	// Heure de départ du mode auto en nombre de secondes depuis 00:00:00
	int iHDeb;
	// Heure de fin du mode auto en nombre de secondes depuis 00:00:00
	int iHFin;
	// Mode courant
	int iCurrentMode;
	// Mémorisation des paramètres
	CParameters cParams;
	// Gestionnaire d'init
	ModeInit GestInit;
	// Gestionnaire de menus
	ModeParametres GestModeParams;
	// Gestionnaire de monitoring
	ModeMonitoring GestMonitoring;
	// Gestionnaire de lecture
	ModePlay GestPlay;
	// Gestionnaire de sortie
	QuitMode GestQuit;
	// Gestionnaire de fin
	ModeEnd GestFin;
	// Pointeur sur le gestionnaire de l'état courant
	GenericMode *pCurrentGest;
};

#endif	/* PIBATMANAGER_H */

