/* 
 * File:   PrefixParam.cpp
 * Author: Jean-Do
 * 
 * Created on 3 septembre 2015, 18:45
 */
//-------------------------------------------------------------------------
// Classe de modification du préfixe des fichiers wav

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "PrefixParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
PrefixParam::PrefixParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Préfixe [xxxxx]
	//                    Prefix  [xxxxx]
	strcpy( indicModif, "000000000011111000000");
	// Mémo de la valeur courante
	strcpy(sOldPref, pParams->GetWaveName());
	strcpy(sNewPref, pParams->GetWaveName());
}

//-------------------------------------------------------------------------
// Destructeur
PrefixParam::~PrefixParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void PrefixParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		// Init des min et max du digit (A <-> z)
		char cMin = 'A';
		char cMax = 'z';
		switch (iKey)
		{
		case K_R:
			// On incrémente le digit de 10
			sNewPref[iCurpos]+=10;
			if (sNewPref[iCurpos] > cMax)
				sNewPref[iCurpos] = cMin;
			else if (sNewPref[iCurpos] == 91)
				sNewPref[iCurpos] = 'a';
			break;
		case K_L:
			// On décrémente le digit de 10
			sNewPref[iCurpos]-=10;
			if (sNewPref[iCurpos] < cMin)
				sNewPref[iCurpos] = cMax;
			else if (sNewPref[iCurpos] == 96)
				sNewPref[iCurpos] = 'Z';
			break;
		case K_PLUS:
			// On incrémente le digit de 1
			sNewPref[iCurpos]+=1;
			if (sNewPref[iCurpos] > cMax)
				sNewPref[iCurpos] = cMin;
			else if (sNewPref[iCurpos] == 91)
				sNewPref[iCurpos] = 'a';
			break;
		case K_MINUS:
			// On décrémente le digit de 1
			sNewPref[iCurpos]-=1;
			if (sNewPref[iCurpos] < cMin)
				sNewPref[iCurpos] = cMax;
			else if (sNewPref[iCurpos] == 96)
				sNewPref[iCurpos] = 'Z';
			break;
		case K_SQUARE:
			// Init de la valeur min
			sNewPref[iCurpos] = 'a';
			break;
		case K_CIRCLE:
			// Init de la valeur max
			sNewPref[iCurpos] = 'Z';;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			strcpy(sNewPref, sOldPref);
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void PrefixParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
		strcpy(sNewPref, pParams->GetWaveName());
	//                   123456789012345678901
	//                   Préfixe [xxxxx]
	//                   Prefix  [xxxxx]
	if (pParams->GetLangue() == FR)
		sprintf(lineParam, " Pr%cfixe [%s]", 0x82, sNewPref);
	else
		sprintf(lineParam, " Prefix  [%s]", sNewPref);
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void PrefixParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	strcpy(sOldPref, pParams->GetWaveName());
	strcpy(sNewPref, pParams->GetWaveName());
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void PrefixParam::SetParam()
{
	pParams->SetWaveName(sNewPref);
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}


