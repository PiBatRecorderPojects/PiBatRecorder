/* 
 * File:   PrefixParam.h
 * Author: Jean-Do
 *
 * Created on 3 septembre 2015, 18:45
 */
#include "GenericParam.h"

#ifndef PREFIXPARAM_H
#define	PREFIXPARAM_H

//-------------------------------------------------------------------------
// Classe de modification du préfixe des fichiers wav
class PrefixParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	PrefixParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~PrefixParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Mémo de la valeur avant modif
	char sOldPref[6];
	
	// Mémo de la valeur courante
	char sNewPref[6];
};

#endif	/* PREFIXPARAM_H */

