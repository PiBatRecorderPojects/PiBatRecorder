/* 
 * File:   QuitMode.cpp
 * Author: Jean-Do
 * 
 * Created on 10 décembre 2015, 12:02
 */

#include "QuitMode.h"

/*-----------------------------------------------------------------------------
 Classe de gestion du mode Quit
 Se contente d'afficher un message opérateur et d'attendre
 Si l'opérateur ne confirme pas la sortie avant la fin d'un time-out,
 on retourne automatiquement dans le mode précédent
 Sinon, on passe en mode END
-----------------------------------------------------------------------------*/

//-------------------------------------------------------------------------
// Constructeur
QuitMode::QuitMode(
	CParameters *pPar,					// Pointeur sur les paramètres
	KeyOledManager *pKeyScreenManager,	// Pointeur sur le gestionnaire du clavier et de l'écran
	bool bAutoMode						// True si le logiciel fonctionne entièrement en mode automatique
										// Si oui, le clavier et l'écran ne sont pas gérés
	):GenericMode(pPar, pKeyScreenManager, bAutoMode)
{
	bNoKey = false;
	iOldMode = MENU;
	bHalt = false;
}

//-------------------------------------------------------------------------
// Destructeur
QuitMode::~QuitMode()
{
}

//-------------------------------------------------------------------------
// Début du mode
void QuitMode::BeginMode()
{
	printf("QuitMode::BeginMode\n");
	if (!bModeAuto)
	{
		// Effacement de l'écran
		pKeyOledManager->ClearScreen();
		// Affichage du message pour l'opérateur
		//                                       0123456789012345678901	
		if (pParams->GetLangue() == FR)
		{
			pKeyOledManager->DrawString((char *)"Quitter ?", 0, 0, true, 2);
			pKeyOledManager->DrawString((char *)"Merci de confirmer", 0, L3T1);
			pKeyOledManager->DrawString((char *)"avec un nouvel appui", 0, L4T1);
			pKeyOledManager->DrawString((char *)"sur OFF", 0, L5T1);
		}
		else
		{
			pKeyOledManager->DrawString((char *)"Quit ?", 0, 0, true, 2);
			pKeyOledManager->DrawString((char *)"Please confirm", 0, L3T1);
			pKeyOledManager->DrawString((char *)"with a new touch", 0, L4T1);
			pKeyOledManager->DrawString((char *)"on OFF", 0, L5T1);
		}
		pKeyOledManager->DisplayScreen();
	}
	// Mémorisation du temps de maintenant pour le timeout
	timeBegin = millis();
}

//-------------------------------------------------------------------------
// Fin du mode
void QuitMode::EndMode()
{
	printf("QuitMode::EndMode\n");
	if (!bModeAuto)
	{
		// Effacement de l'écran
		pKeyOledManager->ClearScreen();
		pKeyOledManager->DisplayScreen();
	}
}
	
//-------------------------------------------------------------------------
// Affichage du mode sur l'écran
// Cette méthode est appelée régulièrement par la classe PiBatManager
// à charge des différents modes d'afficher les informations nécessaires
void QuitMode::PrintMode()
{
	// Appel de la méthode de base pour l'affichage régulier de la date
	GenericMode::PrintMode();
}

//-------------------------------------------------------------------------
// Traitement des ordres claviers
// Retourne éventuellement un mode demandé (NOMODE sinon)
// Cette méthode est appelée régulièrement par la classe PiBatManager
// à charge des différents modes de traiter les actions opérateurs
int QuitMode::ManageKey(
	unsigned short key	// Touche sollicitée par l'opérateur
	)
{
	// Mémorise le temps de maintenant
	unsigned int timeNow = millis();
	int newMode = NOMODE;
	
	// Pas de répétition de touche pour le changement de mode
	if (key != iLastKey)
	{
		switch (key)
		{
		case K_NO:
			// l'Opérateur à levé le doigt, on autorise une nouvelle touche dans 200ms
			bNoKey = true;
			// Et on mémorise le temps
			timeUp = timeNow;
			break;
		case K_ON:
			if (bNoKey and (timeNow - timeUp) > 200)
			{
				// La sortie est confirmée avec halt du système
				newMode = END;
				bHalt = true;
			}
			break;
		case K_RIGHT:
			// Permet de sortir sans halt
			if (bNoKey and (timeNow - timeUp) > 200)
			{
				// La sortie est confirmée sans halt
				newMode = END;
				bHalt = false;
			}
			break;
		default:
			newMode = GenericMode::ManageKey( key); 
			break;
		}
	}
	iLastKey = key;
	// Test si le timeout est passé (5s)
	if (newMode != END and (timeNow - timeBegin) > 5000)
		// On revient dans le mode précédent
		newMode = iOldMode;
	// En mode auto on quitte sans halt
	if (newMode == END and bModeAuto)
		// La sortie est confirmée sans halt
		bHalt = false;

	return newMode;
}

//-------------------------------------------------------------------------
// Init du mode précédent pour un abandon
void QuitMode::SetOldMode(
	MODEFONC oldMode	// Ancien mode de fonctionnement
	)
{
	iOldMode = oldMode;
}

//-------------------------------------------------------------------------
// Retourne True si un halt est demandé
bool QuitMode::IsWithHalt()
{
	return bHalt;
}
