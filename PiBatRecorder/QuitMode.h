/* 
 * File:   QuitMode.h
 * Author: Jean-Do
 *
 * Created on 10 décembre 2015, 12:02
 */
#include "GenericMode.h"

#ifndef QUITMODE_H
#define	QUITMODE_H

/*-----------------------------------------------------------------------------
 Classe de gestion du mode Quit
 Se contente d'afficher un message opérateur et d'attendre
 Si l'opérateur ne confirme pas la sortie avant la fin d'un time-out,
 on retourne automatiquement dans le mode précédent
 Sinon, on passe en mode END
-----------------------------------------------------------------------------*/
class QuitMode : public GenericMode
{
public:
	//-------------------------------------------------------------------------
	// Constructeur
	QuitMode(
		CParameters *pPar,					// Pointeur sur les paramètres
		KeyOledManager *pKeyScreenManager,	// Pointeur sur le gestionnaire du clavier et de l'écran
		bool bAutoMode=false				// True si le logiciel fonctionne entièrement en mode automatique
			                                // Si oui, le clavier et l'écran ne sont pas gérés
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~QuitMode();
	
	//-------------------------------------------------------------------------
	// Début du mode
	virtual void BeginMode();
	
	//-------------------------------------------------------------------------
	// Fin du mode
	virtual void EndMode();
	
	//-------------------------------------------------------------------------
	// Affichage du mode sur l'écran
	// Cette méthode est appelée régulièrement par la classe PiBatManager
	// à charge des différents modes d'afficher les informations nécessaires
	virtual void PrintMode();
	
	//-------------------------------------------------------------------------
	// Traitement des ordres claviers
	// Si la touche est une touche de changement de mode, retourne le mode demandé
	// Cette méthode est appelée régulièrement par la classe PiBatManager
	// à charge des différents modes de traiter les actions opérateurs
	virtual int ManageKey(
		unsigned short key	// Touche sollicitée par l'opérateur
		);
	
	//-------------------------------------------------------------------------
	// Init du mode précédent pour un abandon
	void SetOldMode(
		MODEFONC oldMode	// Ancien mode de fonctionnement
		);
	
	//-------------------------------------------------------------------------
	// Retourne True si un halt est demandé
	bool IsWithHalt();
	
private:
	// Mémorisation de l'ancien mode de fonctionnement pour un retour auto après le timeout
	MODEFONC iOldMode;
	
	// Mémorisation du temps en début de mode pour le timeout
	unsigned int timeBegin;
	
	// Mémorisation du moment ou l'opérateur lève le doigt de la touche
	unsigned int timeUp;
	
	// Indique que l'attente de levé du doigt est effective
	bool bNoKey;
	
	// Inique une sortie avec halt
	bool bHalt;
};

#endif	/* QUITMODE_H */

