/* 
 * File:   RAZParam.cpp
 * Author: Jean-Do
 * 
 * Created on 20 septembre 2015, 13:06
 */
//-------------------------------------------------------------------------
// Classe de RAZ des fichiers wav, txt, csv de la clé USB

#include <stdio.h>
#include <dirent.h>
#include <list>
#include <string>
#include "RAZParam.h"

// Liste des fichiers wav, txt et csv présents
std::list<std::string> lstWaveFiles;

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
RAZParam::RAZParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    RAZ xxxxx fich.? Non
	//                    Del xxxxx files?  No
	strcpy( indicModif, "0000000000000000001xx");
	bRecherche = false;
	bConfirm   = false;
	iNbTot = 0;
	iNbWav = 0;
	iNbTxt = 0;
	iNbCsv = 0;
}

//-------------------------------------------------------------------------
// Destructeur
RAZParam::~RAZParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void RAZParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_PLUS:
		case K_R:
		case K_CIRCLE:
		case K_MINUS:
		case K_L:
		case K_SQUARE:
			// On inverse la valeur
			bConfirm = not bConfirm;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Recherche des fichiers à effacer
void RAZParam::FindFiles()
{
	iNbTot = 0;
	iNbWav = 0;
	iNbTxt = 0;
	iNbCsv = 0;
	// Recherche des fichiers sur la clé USB
	DIR *rep = opendir(pParams->GetWavPath());   
	if (rep != NULL) 
	{ 
		struct dirent *ent; 

		while ((ent = readdir(rep)) != NULL) 
		{
			//0123456789
			//toto.wav
			//printf("Ajout %s\n", ent->d_name);
			std::string wavePath(pParams->GetWavPath());
			wavePath += ent->d_name;
			std::string ext = wavePath.substr(wavePath.size()-4, 4);
			if (ext == ".wav")
				iNbWav++;
			if (ext == ".csv")
				iNbCsv++;
			if (ext == ".wav" or ext == ".csv")
			{
				//cout << "Ajout du fichier " << wavePath << endl;
				lstWaveFiles.push_front(wavePath);
				iNbTot++;
			}
		} 
		closedir(rep); 
	}
	bRecherche = true;
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void RAZParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	//                   123456789012345678901 iNbTot
	//                    RAZ xxxxx fich.? Non
	//                    Del xxxxx files?  No
	if (pParams->GetLangue() == FR)
	{
		if (bConfirm)
			sprintf(lineParam, " RAZ % 5d fich.? Oui", iNbTot);
		else
			sprintf(lineParam, " RAZ % 5d fich.? Non", iNbTot);
	}
	else
	{
		if (bConfirm)
			sprintf(lineParam, " Del % 5d files? Yes", iNbTot);
		else
			sprintf(lineParam, " Del % 5d files? No ", iNbTot);
	}
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void RAZParam::StartModif()
{
	if (!bRecherche)
		FindFiles();
	// Appel de la méthode parente
	GenericParam::StartModif();
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void RAZParam::SetParam()
{
	// Vérification du paramètre
	bModif = false;
	bRecherche = false;
	if (bConfirm)
	{
		// Effacement des fichiers
		for (std::list<std::string>::iterator it=lstWaveFiles.begin(); it != lstWaveFiles.end(); ++it)
		{
			remove(it->data());
		}
		bConfirm = false;
	}
	// Appel de la méthode parente
	GenericParam::SetParam();
}
