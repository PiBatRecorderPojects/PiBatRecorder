/* 
 * File:   RAZParam.h
 * Author: Jean-Do
 *
 * Created on 20 septembre 2015, 13:06
 */
#include "GenericParam.h"

#ifndef RAZPARAM_H
#define	RAZPARAM_H

//-------------------------------------------------------------------------
// Classe de RAZ des fichiers wav, txt, csv de la clé USB
class RAZParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	RAZParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~RAZParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	void StartModif();

	//-------------------------------------------------------------------------
	// Recherche des fichiers à effacer
	void FindFiles();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Mémo de la recherche des fichiers
	bool bRecherche;
	// Nombre de fichiers total et pour chaque type
	int iNbTot, iNbWav, iNbTxt, iNbCsv;
	// Confirmation du RAZ
	bool bConfirm;
};

#endif	/* RAZPARAM_H */

