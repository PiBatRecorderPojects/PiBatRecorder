/* 
 * File:   RecVolumeParam.cpp
 * Author: Jean-Do
 * 
 * Created on 1 septembre 2015, 18:27
 */
//-------------------------------------------------------------------------
// Classe de modification du volume d'enregistrement (01 <-> 31)

#include <stdio.h>
#include <stdlib.h>
#include "RecVolumeParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
RecVolumeParam::RecVolumeParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Gain enregistr. 31dB
	//                    Record Gain     31dB
	strcpy( indicModif, "00000000000000000x100");
	// Mémo de la valeur courante
	iOldVol = pParams->GetNivRec();
	iNewVol = iOldVol;
}

//-------------------------------------------------------------------------
// Destructeur
RecVolumeParam::~RecVolumeParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void RecVolumeParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_R:
			// On incrémente la valeur de 10
			iNewVol += 10;
			if (iNewVol > 31)
				iNewVol = 1;
			break;
		case K_L:
			// On décrémente la valeur de 10
			iNewVol -= 10;
			if (iNewVol < 1)
				iNewVol = 31;
			break;
		case K_PLUS:
			// On incrémente la valeur de 1
			iNewVol += 1;
			if (iNewVol > 31)
				iNewVol = 1;
			break;
		case K_MINUS:
			// On décrémente la valeur de 1
			iNewVol -= 1;
			if (iNewVol < 1)
				iNewVol = 31;
			break;
		case K_SQUARE:
			// Init de la valeur min
			iNewVol = 1;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			iNewVol = 31;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			iNewVol = iOldVol;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void RecVolumeParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
		iNewVol = pParams->GetNivRec();
	//                   123456789012345678901
	//                    Gain enregistr. 31dB
	//                    Record Gain     31dB
	if (pParams->GetLangue() == FR)
		sprintf(lineParam, " Gain Enregistr. %02ddB", iNewVol);
	else
		sprintf(lineParam, " Record gain     %02ddB", iNewVol);
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void RecVolumeParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	iOldVol = pParams->GetNivRec();
	iNewVol = iOldVol;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void RecVolumeParam::SetParam()
{
	// Vérification du paramètre
	if (iNewVol < 1 or iNewVol > 31)
		// Valeur précédente
		iNewVol = iOldVol;
	pParams->SetNivRec(iNewVol);
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}


