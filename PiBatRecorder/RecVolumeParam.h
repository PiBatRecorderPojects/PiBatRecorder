/* 
 * File:   RecVolumeParam.h
 * Author: Jean-Do
 *
 * Created on 1 septembre 2015, 18:27
 */
#include "GenericParam.h"

#ifndef RECVOLUMEPARAM_H
#define	RECVOLUMEPARAM_H

//-------------------------------------------------------------------------
// Classe de modification du volume d'enregistrement (01 <-> 31)
class RecVolumeParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	RecVolumeParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~RecVolumeParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Mémo de la valeur avant modif
	int iOldVol;
	// Mémo de la valeur courante
	int iNewVol;
};

#endif	/* RECVOLUMEPARAM_H */

