/* 
 * File:   RecordModeParam.cpp
 * Author: Jean-Do
 * 
 * Created on 3 septembre 2015, 21:04
 */
//-------------------------------------------------------------------------
// Classe de modification du mode d'enregistrement

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "RecordModeParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
RecordModeParam::RecordModeParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Mode Enregis. Manuel
	//                    Record Mode   Manual
	strcpy( indicModif, "0000000000000001xxxxx");
	// Mémo de la valeur courante
	iOldMode = pParams->GetRecMode();
	iNewMode = iOldMode;
}

//-------------------------------------------------------------------------
// Destructeur
RecordModeParam::~RecordModeParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void RecordModeParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_PLUS:
		case K_R:
		case K_CIRCLE:
			// On incrémente le digit
			iNewMode++;
			if (iNewMode > AUTO)
				iNewMode = MANU;
			break;
		case K_MINUS:
		case K_L:
		case K_SQUARE:
			// On décrémente le digit
			iNewMode--;
			if (iNewMode < MANU)
				iNewMode = AUTO;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			iNewMode = iOldMode;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void RecordModeParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
		iNewMode = pParams->GetRecMode();
	//                   123456789012345678901
	//                    Mode Enregis. Manuel
	//                    Record Mode   Manual
	if (pParams->GetLangue() == FR)
	{
		if (iNewMode == MANU)
			sprintf(lineParam, " Mode Enregis. Manuel");
		else
			sprintf(lineParam, " Mode Enregis. Auto. ");
	}
	else
	{
		if (iNewMode == MANU)
			sprintf(lineParam, " Record Mode   Manual");
		else
			sprintf(lineParam, " Record Mode   Auto. ");
	}
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void RecordModeParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	iOldMode = pParams->GetRecMode();
	iNewMode = iOldMode;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void RecordModeParam::SetParam()
{
	// Vérification du paramètre
	if (iNewMode < MANU or iNewMode > AUTO)
		// Valeur précédente
		iNewMode = iOldMode;
	pParams->SetRecMode((MODEREC)iNewMode);
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}
