/* 
 * File:   RecordTypeParam.cpp
 * Author: Jean-Do
 * 
 * Created on 3 septembre 2015, 20:39
 */
//-------------------------------------------------------------------------
// Classe de modification du type d'enregistrement

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "RecordTypeParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
RecordTypeParam::RecordTypeParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Enregis. Wav
	//                    Record   Wav+Log+Act
	strcpy( indicModif, "00000000001xxxxxxxxxx");
	// Mémo de la valeur courante
	iOldType = pParams->GetRecType();
	iNewType = iOldType;
}

//-------------------------------------------------------------------------
// Destructeur
RecordTypeParam::~RecordTypeParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void RecordTypeParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_PLUS:
		case K_R:
			// On incrémente la valeur de 1
			iNewType += 1;
			if (iNewType > ALL)
				iNewType = WAV;
			break;
		case K_MINUS:
		case K_L:
			// On décrémente la valeur de 1
			iNewType -= 1;
			if (iNewType < WAV)
				iNewType = ALL;
			break;
		case K_SQUARE:
			// Init de la valeur min
			iNewType = WAV;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			iNewType = ALL;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			iNewType = iOldType;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void RecordTypeParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
		iNewType = pParams->GetRecType();
	//                   123456789012345678901
	//                    Enregis. Wav
	//                    Record   Wav+Log+Act
	char sType[21];
	switch (iNewType)
	{
	default:
	case WAV:
		sprintf( sType, "WAV");
		break;
	case LOGC:
		sprintf( sType, "LOG");
		break;
	case ACTV:
		sprintf( sType, "ACT");
		break;
	case WAVLOG:
		sprintf( sType, "WAV+LOG");
		break;
	case ACTLOG:
		sprintf( sType, "ACT+LOG");
		break;
	case WAVACT:
		sprintf( sType, "WAV+ACT");
		break;
	case ALL:
		sprintf( sType, "WAV+LOG+ACT");
		break;
	}
	if (pParams->GetLangue() == FR)
		sprintf(lineParam, " Enregis. %s", sType);
	else
		sprintf(lineParam, " Record   %s", sType);
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void RecordTypeParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	iOldType = pParams->GetRecType();
	iNewType = iOldType;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void RecordTypeParam::SetParam()
{
	// Vérification du paramètre
	if (iNewType < WAV or iNewType > ALL)
		// Valeur précédente
		iNewType = iOldType;
	pParams->SetRecType((TYPEREC)iNewType);
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}


