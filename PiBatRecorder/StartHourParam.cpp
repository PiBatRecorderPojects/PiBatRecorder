/* 
 * File:   StartHourParam.cpp
 * Author: Jean-Do
 * 
 * Created on 3 septembre 2015, 21:23
 */
//-------------------------------------------------------------------------
// Classe de modification de l'heure de départ de l'enregistrement auto

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "StartHourParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
StartHourParam::StartHourParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Début Enregis. 21:52
	//                    Start Record   21:52
	strcpy( indicModif, "0000000000000000x10x1");
	// Mémo de la valeur courante
	char sOldH[12];
	strcpy(sOldH, pParams->GetHDeb());
	sscanf(sOldH, "%02d:%02d", &iOldH, &iOldM);
	iNewH = iOldH;
	iNewM = iOldM;
}

//-------------------------------------------------------------------------
// Destructeur
StartHourParam::~StartHourParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void StartHourParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		// Init des min et max du digit et du pointeur sur la valeur à modifier
		int iMin, iMax;
		int *pValue;
		switch (iIndiceModif)
		{
		case 0:
			iMin = 0;
			iMax = 23;
			pValue = &iNewH;
			break;
		case 1:
		default:
			iMin = 0;
			iMax = 59;
			pValue = &iNewM;
			break;
		}
		switch (iKey)
		{
		case K_PLUS:
			// On incrémente le digit
			(*pValue)++;
			if (*pValue > iMax)
				*pValue = iMin;
			break;
		case K_MINUS:
			// On décrémente le digit
			(*pValue)--;
			if (*pValue < iMin)
				*pValue = iMax;
			break;
		case K_R:
			// On incrémente de 1/10 le digit
			(*pValue)+= (iMax-iMin+1)/10;
			if (*pValue > iMax)
				*pValue = iMin;
			break;
		case K_L:
			// On décrémente de 1/10 le digit
			(*pValue)-= (iMax-iMin+1)/10;
			if (*pValue < iMin)
				*pValue = iMax;
			break;
		case K_SQUARE:
			// Init de la valeur min
			*pValue = iMin;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			*pValue = iMax;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			iNewH = iOldH;
			iNewM = iOldM;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void StartHourParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
	{
		char sTmp[21];
		strcpy(sTmp, pParams->GetHDeb());
		sscanf(sTmp, "%02d:%02d", &iNewH, &iNewM);
	}
	//                   123456789012345678901
	//                    Début Enregis. 21:52
	//                    Start Record   21:52
	if (pParams->GetLangue() == FR)
		sprintf(lineParam, " D%cbut Enregis. %02d:%02d", 0x82, iNewH, iNewM);
	else
		sprintf(lineParam, " Start Record   %02d:%02d", iNewH, iNewM);
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void StartHourParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	char sOldH[21];
	strcpy(sOldH, pParams->GetHDeb());
	sscanf(sOldH, "%02d:%02d", &iOldH, &iOldM);
	iNewH = iOldH;
	iNewM = iOldM;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void StartHourParam::SetParam()
{
	// Vérification du paramètre
	//printf("StartHourParam::SetParam iNewH %d, iNewM %d\n", iNewH, iNewM);
	if (iNewH < 0 or iNewH > 23)
	{
		// Valeur précédente
		iNewH = iOldH;
		iNewM = iOldM;
	}
	else if (iNewM < 0 or iNewM > 59)
	{
		// Valeur précédente
		iNewH = iOldH;
		iNewM = iOldM;
	}
	char sNewH[21];
	sprintf(sNewH, "%02d:%02d", iNewH, iNewM);
	//printf("StartHourParam::SetParam sNewH %s\n", sNewH);
	pParams->SetHDeb(sNewH);
	//printf("StartHourParam::SetParam GetHDeb %s\n", pParams->GetHDeb());
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
	//printf("StartHourParam::SetParamB GetHDeb %s\n", pParams->GetHDeb());
}
