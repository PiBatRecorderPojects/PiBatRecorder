/* 
 * File:   StartHourParam.h
 * Author: Jean-Do
 *
 * Created on 3 septembre 2015, 21:23
 */
#include "GenericParam.h"

#ifndef STARTHOURPARAM_H
#define	STARTHOURPARAM_H

//-------------------------------------------------------------------------
// Classe de modification de l'heure de départ de l'enregistrement auto
class StartHourParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	StartHourParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~StartHourParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Mémo de la valeur avant modif
	int iOldH, iOldM;
	// Mémo de la valeur courante
	int iNewH, iNewM;
};

#endif	/* STARTHOURPARAM_H */

