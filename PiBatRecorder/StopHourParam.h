/* 
 * File:   StopHourParam.h
 * Author: Jean-Do
 *
 * Created on 4 septembre 2015, 09:42
 */
#include "GenericParam.h"

#ifndef STOPHOURPARAM_H
#define	STOPHOURPARAM_H

//-------------------------------------------------------------------------
// Classe de modification de l'heure de fin de l'enregistrement auto
class StopHourParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	StopHourParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~StopHourParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Mémo de la valeur avant modif
	int iOldH, iOldM;
	// Mémo de la valeur courante
	int iNewH, iNewM;
};

#endif	/* STOPHOURPARAM_H */

