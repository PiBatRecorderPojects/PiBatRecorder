/* 
 * File:   ThresholdParam.cpp
 * Author: Jean-Do
 * 
 * Created on 2 septembre 2015, 21:36
 */
//-------------------------------------------------------------------------
// Classe de modification du seuil d'enregistrement (10 <-> 49)

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "ThresholdParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
ThresholdParam::ThresholdParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Seuil     10 dB
	//                    Threshold 50 dB
	strcpy( indicModif, "00000000000x100000000");
	// Mémo de la valeur courante
	iOldTh = pParams->GetThreshold();
	iNewTh = iOldTh;
}

//-------------------------------------------------------------------------
// Destructeur
ThresholdParam::~ThresholdParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void ThresholdParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_R:
			// On incrémente la valeur de 10
			iNewTh += 10;
			if (iNewTh > 99)
				iNewTh = 10;
			break;
		case K_L:
			// On décrémente la valeur de 10
			iNewTh -= 10;
			if (iNewTh < 10)
				iNewTh = 99;
			break;
		case K_PLUS:
			// On incrémente la valeur de 1
			iNewTh += 1;
			if (iNewTh > 99)
				iNewTh = 10;
			break;
		case K_MINUS:
			// On décrémente la valeur de 1
			iNewTh -= 1;
			if (iNewTh < 10)
				iNewTh = 99;
			break;
		case K_SQUARE:
			// Init de la valeur min
			iNewTh = 10;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			iNewTh = 99;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			iNewTh = iOldTh;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void ThresholdParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
		iNewTh = pParams->GetThreshold();
	//                   123456789012345678901
	//                    Seuil     10 dB
	//                    Threshold 50 dB
	if (pParams->GetLangue() == FR)
		sprintf(lineParam, " Seuil     %02d dB", iNewTh);
	else
		sprintf(lineParam, " Threshold %02d dB", iNewTh);
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void ThresholdParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	iOldTh = pParams->GetThreshold();
	iNewTh = iOldTh;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void ThresholdParam::SetParam()
{
	// Vérification du paramètre
	if (iNewTh < 10 or iNewTh > 49)
		// Valeur précédente
		iNewTh = iOldTh;
	pParams->SetThreshold(iNewTh);
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}
