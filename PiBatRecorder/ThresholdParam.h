/* 
 * File:   ThresholdParam.h
 * Author: Jean-Do
 *
 * Created on 2 septembre 2015, 21:36
 */
#include "GenericParam.h"

#ifndef THRESHOLDPARAM_H
#define	THRESHOLDPARAM_H

//-------------------------------------------------------------------------
// Classe de modification du seuil d'enregistrement (10 <-> 49)
class ThresholdParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	ThresholdParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~ThresholdParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Mémo de la valeur avant modif
	int iOldTh;
	
	// Mémo de la valeur courante
	int iNewTh;
};

#endif	/* THRESHOLDPARAM_H */

