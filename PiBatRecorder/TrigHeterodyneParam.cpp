/* 
 * File:   TrigHeterodyneParam.cpp
 * Author: Jean-Do
 * 
 * Created on 21 décembre 2015, 13:43
 */
//-------------------------------------------------------------------------
// Classe de modification du type de déclanchement de l'hétérodyne

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "TrigHeterodyneParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
TrigHeterodyneParam::TrigHeterodyneParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Hétérodyne Continue
	//                    Heterodyne Trigger
	strcpy( indicModif, "0000000000001xxxxxxx0");
	// Mémo de la valeur courante
	iOldType = pParams->GetTrigHeter();
	iNewType = iOldType;
}

//-------------------------------------------------------------------------
// Destructeur
TrigHeterodyneParam::~TrigHeterodyneParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void TrigHeterodyneParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_PLUS:
		case K_R:
			// On incrémente la valeur de 1
			iNewType += 1;
			if (iNewType > HET_TRIGGER)
				iNewType = HET_CONTINUE;
			break;
		case K_MINUS:
		case K_L:
			// On décrémente la valeur de 1
			iNewType -= 1;
			if (iNewType < HET_CONTINUE)
				iNewType = HET_TRIGGER;
			break;
		case K_SQUARE:
			// Init de la valeur min
			iNewType = HET_CONTINUE;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			iNewType = HET_TRIGGER;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			iNewType = iOldType;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void TrigHeterodyneParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
		iNewType = pParams->GetTrigHeter();
	//                   123456789012345678901
	//                    Hétérodyne Continue
	//                    Heterodyne Trigger
	switch (iNewType)
	{
	default:
	case HET_TRIGGER:
		if (pParams->GetLangue() == FR)
			sprintf(lineParam, " H%ct%crodyne Trigger", 0x82, 0x82);
		else
			sprintf(lineParam, " Heterodyne Trigger");
		break;
	case HET_CONTINUE:
		if (pParams->GetLangue() == FR)
			sprintf(lineParam, " H%ct%crodyne Continue", 0x82, 0x82);
		else
			sprintf(lineParam, " Heterodyne Continue");
		break;
	}
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void TrigHeterodyneParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	iOldType = pParams->GetTrigHeter();
	iNewType = iOldType;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void TrigHeterodyneParam::SetParam()
{
	// Vérification du paramètre
	if (iNewType < HET_CONTINUE or iNewType > HET_TRIGGER)
		// Valeur précédente
		iNewType = iOldType;
	pParams->SetTrigHeter((TRIG_HETERODYNE)iNewType);
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}

