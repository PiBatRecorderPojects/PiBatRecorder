/* 
 * File:   TrigHeterodyneParam.h
 * Author: Jean-Do
 *
 * Created on 21 décembre 2015, 13:43
 */

#include "GenericParam.h"

#ifndef TRIGHETERODYNEPARAM_H
#define	TRIGHETERODYNEPARAM_H

//-------------------------------------------------------------------------
// Classe de modification du type de déclanchement de l'hétérodyne
class TrigHeterodyneParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	TrigHeterodyneParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~TrigHeterodyneParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Mémo de la valeur avant modif
	int iOldType;
	// Mémo de la valeur courante
	int iNewType;
};


#endif	/* TRIGHETERODYNEPARAM_H */

