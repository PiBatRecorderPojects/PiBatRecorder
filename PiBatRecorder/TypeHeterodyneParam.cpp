/* 
 * File:   TypeHeterodyneParam.cpp
 * Author: Jean-Do
 * 
 * Created on 21 décembre 2015, 12:38
 */
//-------------------------------------------------------------------------
// Classe de modification du type d'hétérodyne

#include "TypeHeterodyneParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
TypeHeterodyneParam::TypeHeterodyneParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Hétérodyne Sans
	//                    Heterodyne Manual
	//                    Heterodyne Auto
	strcpy( indicModif, "0000000000001xxxxx000");
	// Mémo de la valeur courante
	iOldType = pParams->GetModeHeter();
	iNewType = iOldType;
}

//-------------------------------------------------------------------------
// Destructeur
TypeHeterodyneParam::~TypeHeterodyneParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void TypeHeterodyneParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_PLUS:
		case K_R:
			// On incrémente la valeur de 1
			iNewType += 1;
			if (iNewType > HET_AUTO)
				iNewType = HET_SANS;
			break;
		case K_MINUS:
		case K_L:
			// On décrémente la valeur de 1
			iNewType -= 1;
			if (iNewType < HET_SANS)
				iNewType = HET_AUTO;
			break;
		case K_SQUARE:
			// Init de la valeur min
			iNewType = HET_SANS;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			iNewType = HET_AUTO;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			iNewType = iOldType;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void TypeHeterodyneParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
		iNewType = pParams->GetModeHeter();
	//                   123456789012345678901
	//                    Hétérodyne Sans
	//                    Heterodyne Manual
	//                    Heterodyne Auto
	switch (iNewType)
	{
	default:
	case HET_AUTO:
		if (pParams->GetLangue() == FR)
			sprintf(lineParam, " H%ct%crodyne Auto", 0x82, 0x82);
		else
			sprintf(lineParam, " Heterodyne Auto");
		break;
	case HET_MANU:
		if (pParams->GetLangue() == FR)
			sprintf(lineParam, " H%ct%crodyne Manuel", 0x82, 0x82);
		else
			sprintf(lineParam, " Heterodyne Manual");
		break;
	case HET_SANS:
		if (pParams->GetLangue() == FR)
			sprintf(lineParam, " H%ct%crodyne Sans", 0x82, 0x82);
		else
			sprintf(lineParam, " Heterodyne No");
		break;
	}
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void TypeHeterodyneParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	iOldType = pParams->GetModeHeter();
	iNewType = iOldType;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void TypeHeterodyneParam::SetParam()
{
	// Vérification du paramètre
	if (iNewType < HET_SANS or iNewType > HET_AUTO)
		// Valeur précédente
		iNewType = iOldType;
	pParams->SetModeHeter((MODE_HETERODYNE)iNewType);
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}

