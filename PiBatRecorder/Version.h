/* 
 * File:   Version.h
 * Author: Jean-Do
 *
 * Created on 21 décembre 2015, 16:32
 */

#ifndef VERSION_H
#define	VERSION_H

// Version de départ du logiciel PiBatRecorder
//#define VERSION "V0.50"
// 2ème version du logiciel PiBatRecorder
#define VERSION "V0.60"

#endif	/* VERSION_H */

