/* 
 * File:   VolumeParam.cpp
 * Author: Jean-Do
 * 
 * Created on 3 septembre 2015, 19:04
 */
//-------------------------------------------------------------------------
// Classe de modification du volume d'écoute (00 <-> 31)

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "VolumeParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
VolumeParam::VolumeParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Volume 31 dB
	//                    Volume 31 dB
	strcpy( indicModif, "00000000x100000000000");
	// Mémo de la valeur courante
	iOldVol = pParams->GetVolPlay();
	iNewVol = iOldVol;
}

//-------------------------------------------------------------------------
// Destructeur
VolumeParam::~VolumeParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void VolumeParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_R:
			// On incrémente la valeur de 10
			iNewVol += 10;
			if (iNewVol > 31)
				iNewVol = 1;
			break;
		case K_L:
			// On décrémente la valeur de 10
			iNewVol -= 10;
			if (iNewVol < 1)
				iNewVol = 31;
			break;
		case K_PLUS:
			// On incrémente la valeur de 1
			iNewVol += 1;
			if (iNewVol > 31)
				iNewVol = 1;
			break;
		case K_MINUS:
			// On décrémente la valeur de 1
			iNewVol -= 1;
			if (iNewVol < 1)
				iNewVol = 31;
			break;
		case K_SQUARE:
			// Init de la valeur min
			iNewVol = 1;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			iNewVol = 31;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			iNewVol = iOldVol;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void VolumeParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
		iNewVol = pParams->GetVolPlay();
	//                   123456789012345678901
	//                    Volume 31 dB
	//                    Volume 31 dB
	if (pParams->GetLangue() == FR)
		sprintf(lineParam, " Volume %02d dB", iNewVol);
	else
		sprintf(lineParam, " Volume %02d dB", iNewVol);
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void VolumeParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	iOldVol = pParams->GetVolPlay();
	iNewVol = iOldVol;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void VolumeParam::SetParam()
{
	// Vérification du paramètre
	if (iNewVol < 0 or iNewVol > 31)
		// Valeur précédente
		iNewVol = iOldVol;
	pParams->SetVolPlay(iNewVol);
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}



