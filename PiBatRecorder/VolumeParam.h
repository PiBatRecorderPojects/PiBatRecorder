/* 
 * File:   VolumeParam.h
 * Author: Jean-Do
 *
 * Created on 3 septembre 2015, 19:04
 */
#include "GenericParam.h"

#ifndef VOLUMEPARAM_H
#define	VOLUMEPARAM_H

//-------------------------------------------------------------------------
// Classe de modification du volume d'écoute (00 <-> 31)
class VolumeParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	VolumeParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~VolumeParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Mémo de la valeur avant modif
	int iOldVol;
	
	// Mémo de la valeur courante
	int iNewVol;
};

#endif	/* VOLUMEPARAM_H */

