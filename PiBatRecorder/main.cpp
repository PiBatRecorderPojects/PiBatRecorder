/* 
 * File:   main.cpp
 * Author: Jean-Do
 *
 * Created on 17 août 2015, 20:36
 */

#include <stdio.h>
#include "Version.h"
#include "PiBatManager.h"
 
/*
 * 
 */
int main(int argc, char** argv)
{
	strcpy(LogFile::softName, "PiBatRecorder");
	//LogFile::bConsole = true;
	LogFile::LogLevel = LINFO;
	//LogFile::LogLevel = LDEBUG;
	bool bVersion = false;
	// Récupération du répertoire de lancement
	char pathExec[512];
	char path[512];
	char *p;
	strcpy(path, argv[0]);
	strcpy(pathExec, argv[0]);
	p = strrchr((char *)path, '/');
	if (p != NULL)
		p[1] = 0;
	//printf("Path d'exécution [%s]\n", path, pathExec);	
	// Vérification des paramètres de lancement
	bool bAuto = false;
	//cdprintf("argc %d\n", argc);
	if (argc > 1)
	{
		// Test de l'option
		if (strstr(argv[1], "-a") != NULL or strstr(argv[1], "-A") != NULL)
		{
			bAuto = true;
			//printf("Lancement avec -a\n", argv[1]);
		}
		else if (strstr(argv[1], "-v") != NULL or strstr(argv[1], "-V") != NULL)
		{
			// Appel juste pour indiquer la version
			bVersion = true;
			printf("PiBatRecorder Version %s\n", VERSION);
		}
		/*else
			printf("Paramètre [%s] invalide !\n", argv[1]);*/
	}
	/*else
		printf("Pas de paramètre au lancement\n");*/
	if (!bVersion)
	{
		if (bAuto)
			LogFile::AddLog(LLOG, "Start (mode auto)");
		else
			LogFile::AddLog(LLOG, "Start (mode normal)");
		// Création du manager
		PiBatManager manager(bAuto);
		manager.Init();
		// Lancement de la boucle principale
		manager.Run();
		// Mémo de la fin de PiBatRecorder dans le log
		LogFile::AddLog(LLOG, "Stop");
	}
	// Sortie du logiciel
	return 0;
}
