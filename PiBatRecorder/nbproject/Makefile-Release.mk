#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=None-Windows
CND_DLIB_EXT=dll
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/AboutParam.o \
	${OBJECTDIR}/CAnalyser.o \
	${OBJECTDIR}/CHeterodyne.o \
	${OBJECTDIR}/CParameters.o \
	${OBJECTDIR}/CPlay.o \
	${OBJECTDIR}/CRecorder.o \
	${OBJECTDIR}/CStatus.o \
	${OBJECTDIR}/CWaveFile.o \
	${OBJECTDIR}/DateParam.o \
	${OBJECTDIR}/DecHeterParam.o \
	${OBJECTDIR}/GenericMode.o \
	${OBJECTDIR}/GenericParam.o \
	${OBJECTDIR}/GpuFFT/gpu_fft.o \
	${OBJECTDIR}/GpuFFT/gpu_fft_base.o \
	${OBJECTDIR}/GpuFFT/gpu_fft_shaders.o \
	${OBJECTDIR}/GpuFFT/gpu_fft_trans.o \
	${OBJECTDIR}/GpuFFT/gpu_fft_twiddles.o \
	${OBJECTDIR}/GpuFFT/mailbox.o \
	${OBJECTDIR}/HourParam.o \
	${OBJECTDIR}/KeyOledManager.o \
	${OBJECTDIR}/LanguageParam.o \
	${OBJECTDIR}/LogFile.o \
	${OBJECTDIR}/LogWaveFile.o \
	${OBJECTDIR}/MaxFreqParam.o \
	${OBJECTDIR}/MaxRecParam.o \
	${OBJECTDIR}/MinFreqParam.o \
	${OBJECTDIR}/MinRecParam.o \
	${OBJECTDIR}/ModeEnd.o \
	${OBJECTDIR}/ModeInit.o \
	${OBJECTDIR}/ModeMonitoring.o \
	${OBJECTDIR}/ModeParametres.o \
	${OBJECTDIR}/ModePlay.o \
	${OBJECTDIR}/MountUSBKey.o \
	${OBJECTDIR}/OutputParam.o \
	${OBJECTDIR}/PiBatManager.o \
	${OBJECTDIR}/PrefixParam.o \
	${OBJECTDIR}/QuitMode.o \
	${OBJECTDIR}/RAZParam.o \
	${OBJECTDIR}/RecVolumeParam.o \
	${OBJECTDIR}/RecordModeParam.o \
	${OBJECTDIR}/RecordTypeParam.o \
	${OBJECTDIR}/StartHourParam.o \
	${OBJECTDIR}/StopHourParam.o \
	${OBJECTDIR}/ThresholdParam.o \
	${OBJECTDIR}/TrigHeterodyneParam.o \
	${OBJECTDIR}/TypeHeterodyneParam.o \
	${OBJECTDIR}/VolumeParam.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pibatrecorder.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pibatrecorder.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pibatrecorder ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/AboutParam.o: AboutParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/AboutParam.o AboutParam.cpp

${OBJECTDIR}/CAnalyser.o: CAnalyser.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CAnalyser.o CAnalyser.cpp

${OBJECTDIR}/CHeterodyne.o: CHeterodyne.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CHeterodyne.o CHeterodyne.cpp

${OBJECTDIR}/CParameters.o: CParameters.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CParameters.o CParameters.cpp

${OBJECTDIR}/CPlay.o: CPlay.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CPlay.o CPlay.cpp

${OBJECTDIR}/CRecorder.o: CRecorder.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CRecorder.o CRecorder.cpp

${OBJECTDIR}/CStatus.o: CStatus.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CStatus.o CStatus.cpp

${OBJECTDIR}/CWaveFile.o: CWaveFile.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CWaveFile.o CWaveFile.cpp

${OBJECTDIR}/DateParam.o: DateParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DateParam.o DateParam.cpp

${OBJECTDIR}/DecHeterParam.o: DecHeterParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DecHeterParam.o DecHeterParam.cpp

${OBJECTDIR}/GenericMode.o: GenericMode.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GenericMode.o GenericMode.cpp

${OBJECTDIR}/GenericParam.o: GenericParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GenericParam.o GenericParam.cpp

${OBJECTDIR}/GpuFFT/gpu_fft.o: GpuFFT/gpu_fft.c 
	${MKDIR} -p ${OBJECTDIR}/GpuFFT
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GpuFFT/gpu_fft.o GpuFFT/gpu_fft.c

${OBJECTDIR}/GpuFFT/gpu_fft_base.o: GpuFFT/gpu_fft_base.c 
	${MKDIR} -p ${OBJECTDIR}/GpuFFT
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GpuFFT/gpu_fft_base.o GpuFFT/gpu_fft_base.c

${OBJECTDIR}/GpuFFT/gpu_fft_shaders.o: GpuFFT/gpu_fft_shaders.c 
	${MKDIR} -p ${OBJECTDIR}/GpuFFT
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GpuFFT/gpu_fft_shaders.o GpuFFT/gpu_fft_shaders.c

${OBJECTDIR}/GpuFFT/gpu_fft_trans.o: GpuFFT/gpu_fft_trans.c 
	${MKDIR} -p ${OBJECTDIR}/GpuFFT
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GpuFFT/gpu_fft_trans.o GpuFFT/gpu_fft_trans.c

${OBJECTDIR}/GpuFFT/gpu_fft_twiddles.o: GpuFFT/gpu_fft_twiddles.c 
	${MKDIR} -p ${OBJECTDIR}/GpuFFT
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GpuFFT/gpu_fft_twiddles.o GpuFFT/gpu_fft_twiddles.c

${OBJECTDIR}/GpuFFT/mailbox.o: GpuFFT/mailbox.c 
	${MKDIR} -p ${OBJECTDIR}/GpuFFT
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/GpuFFT/mailbox.o GpuFFT/mailbox.c

${OBJECTDIR}/HourParam.o: HourParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/HourParam.o HourParam.cpp

${OBJECTDIR}/KeyOledManager.o: KeyOledManager.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/KeyOledManager.o KeyOledManager.cpp

${OBJECTDIR}/LanguageParam.o: LanguageParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/LanguageParam.o LanguageParam.cpp

${OBJECTDIR}/LogFile.o: LogFile.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/LogFile.o LogFile.cpp

${OBJECTDIR}/LogWaveFile.o: LogWaveFile.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/LogWaveFile.o LogWaveFile.cpp

${OBJECTDIR}/MaxFreqParam.o: MaxFreqParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MaxFreqParam.o MaxFreqParam.cpp

${OBJECTDIR}/MaxRecParam.o: MaxRecParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MaxRecParam.o MaxRecParam.cpp

${OBJECTDIR}/MinFreqParam.o: MinFreqParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MinFreqParam.o MinFreqParam.cpp

${OBJECTDIR}/MinRecParam.o: MinRecParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MinRecParam.o MinRecParam.cpp

${OBJECTDIR}/ModeEnd.o: ModeEnd.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ModeEnd.o ModeEnd.cpp

${OBJECTDIR}/ModeInit.o: ModeInit.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ModeInit.o ModeInit.cpp

${OBJECTDIR}/ModeMonitoring.o: ModeMonitoring.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ModeMonitoring.o ModeMonitoring.cpp

${OBJECTDIR}/ModeParametres.o: ModeParametres.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ModeParametres.o ModeParametres.cpp

${OBJECTDIR}/ModePlay.o: ModePlay.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ModePlay.o ModePlay.cpp

${OBJECTDIR}/MountUSBKey.o: MountUSBKey.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MountUSBKey.o MountUSBKey.cpp

${OBJECTDIR}/OutputParam.o: OutputParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/OutputParam.o OutputParam.cpp

${OBJECTDIR}/PiBatManager.o: PiBatManager.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/PiBatManager.o PiBatManager.cpp

${OBJECTDIR}/PrefixParam.o: PrefixParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/PrefixParam.o PrefixParam.cpp

${OBJECTDIR}/QuitMode.o: QuitMode.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/QuitMode.o QuitMode.cpp

${OBJECTDIR}/RAZParam.o: RAZParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/RAZParam.o RAZParam.cpp

${OBJECTDIR}/RecVolumeParam.o: RecVolumeParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/RecVolumeParam.o RecVolumeParam.cpp

${OBJECTDIR}/RecordModeParam.o: RecordModeParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/RecordModeParam.o RecordModeParam.cpp

${OBJECTDIR}/RecordTypeParam.o: RecordTypeParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/RecordTypeParam.o RecordTypeParam.cpp

${OBJECTDIR}/StartHourParam.o: StartHourParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/StartHourParam.o StartHourParam.cpp

${OBJECTDIR}/StopHourParam.o: StopHourParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/StopHourParam.o StopHourParam.cpp

${OBJECTDIR}/ThresholdParam.o: ThresholdParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ThresholdParam.o ThresholdParam.cpp

${OBJECTDIR}/TrigHeterodyneParam.o: TrigHeterodyneParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/TrigHeterodyneParam.o TrigHeterodyneParam.cpp

${OBJECTDIR}/TypeHeterodyneParam.o: TypeHeterodyneParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/TypeHeterodyneParam.o TypeHeterodyneParam.cpp

${OBJECTDIR}/VolumeParam.o: VolumeParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/VolumeParam.o VolumeParam.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pibatrecorder.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
