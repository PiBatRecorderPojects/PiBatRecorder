/* 
 * File:   HourShiftParam.cpp
 * Author: Jean-Do
 * 
 * Created on 5 janvier 2016, 22:22
 */
//-------------------------------------------------------------------------
// Classe de modification du décalage par rapport au coucher/lever du soleil

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "HourShiftParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
HourShiftParam::HourShiftParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Dec. coucher +21:52 
	//                    Sunset shift +21:52 
	strcpy( indicModif, "000000000000001xx01x0");
	// Mémo de la valeur courante
	char sOldH[12];
	strcpy(sOldH, pParams->GetHDec());
	sscanf(sOldH, "%+03d:%02d", &iOldH, &iOldM);
	iNewH = iOldH;
	iNewM = iOldM;
}

//-------------------------------------------------------------------------
// Destructeur
HourShiftParam::~HourShiftParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void HourShiftParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		// Init des min et max du digit et du pointeur sur la valeur à modifier
		int iMin, iMax;
		int *pValue;
		switch (iIndiceModif)
		{
		case 0:
			iMin = -4;
			iMax = 4;
			pValue = &iNewH;
			break;
		case 1:
		default:
			iMin = 0;
			iMax = 59;
			pValue = &iNewM;
			break;
		}
		switch (iKey)
		{
		case K_PLUS:
			// On incrémente le digit
			(*pValue)++;
			if (*pValue > iMax)
				*pValue = iMin;
			break;
		case K_MINUS:
			// On décrémente le digit
			(*pValue)--;
			if (*pValue < iMin)
				*pValue = iMax;
			break;
		case K_R:
			// On incrémente de 1/10 le digit
			(*pValue)+= (iMax-iMin+1)/10;
			if (*pValue > iMax)
				*pValue = iMin;
			break;
		case K_L:
			// On décrémente de 1/10 le digit
			(*pValue)-= (iMax-iMin+1)/10;
			if (*pValue < iMin)
				*pValue = iMax;
			break;
		case K_SQUARE:
			// Init de la valeur min
			*pValue = iMin;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			*pValue = iMax;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			iNewH = iOldH;
			iNewM = iOldM;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void HourShiftParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
	{
		char sTmp[21];
		strcpy(sTmp, pParams->GetHDec());
		sscanf(sTmp, "%+03d:%02d", &iNewH, &iNewM);
	}
	//                   123456789012345678901
	//                    Dec. coucher +21:52 
	//                    Sunset shift +21:52 
	if (pParams->GetLangue() == FR)
		sprintf(lineParam, " Dec. coucher %+03d:%02d", iNewH, iNewM);
	else
		sprintf(lineParam, " Sunset shift %+03d:%02d", iNewH, iNewM);
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void HourShiftParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	char sOldH[21];
	strcpy(sOldH, pParams->GetHDec());
	sscanf(sOldH, "%+03d:%02d", &iOldH, &iOldM);
	iNewH = iOldH;
	iNewM = iOldM;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void HourShiftParam::SetParam()
{
	// Vérification du paramètre
	//printf("HourShiftParam::SetParam iNewH %d, iNewM %d\n", iNewH, iNewM);
	if (iNewH < -4 or iNewH > 4)
	{
		// Valeur précédente
		iNewH = iOldH;
		iNewM = iOldM;
	}
	else if (iNewM < 0 or iNewM > 59)
	{
		// Valeur précédente
		iNewH = iOldH;
		iNewM = iOldM;
	}
	char sNewH[21];
	sprintf(sNewH, "%+03d:%02d", iNewH, iNewM);
	//printf("HourShiftParam::SetParam sNewH %s\n", sNewH);
	pParams->SetHDec(sNewH);
	//printf("HourShiftParam::SetParam GetHDec %s\n", pParams->GetHDec());
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
	//printf("HourShiftParam::SetParamB GetHDec %s\n", pParams->GetHDec());
}

