/* 
 * File:   HourShiftParam.h
 * Author: Jean-Do
 *
 * Created on 5 janvier 2016, 22:22
 */
#include "GenericParam.h"

#ifndef HOURSHIFTPARAM_H
#define	HOURSHIFTPARAM_H

//-------------------------------------------------------------------------
// Classe de modification du décalage par rapport au coucher/lever du soleil
class HourShiftParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	HourShiftParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~HourShiftParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Mémo de la valeur avant modif
	int iOldH, iOldM;
	
	// Mémo de la valeur courante
	int iNewH, iNewM;
};

#endif	/* HOURSHIFTPARAM_H */

