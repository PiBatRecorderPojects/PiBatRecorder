/* 
 * File:   LatitudeParam.cpp
 * Author: Jean-Do
 * 
 * Created on 5 janvier 2016, 22:19
 */
//-------------------------------------------------------------------------
// Classe de modification de la latitude d'un site

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "LatitudeParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
LatitudeParam::LatitudeParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Latitude  +90.5
	strcpy( indicModif, "00000000000xxxx100000");
	// Mémo de la valeur courante
	fOldLat = pParams->GetLatitude();
	fNewLat = fOldLat;
}

//-------------------------------------------------------------------------
// Destructeur
LatitudeParam::~LatitudeParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void LatitudeParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_R:
			// On incrémente la valeur de 1°
			fNewLat += 1.0;
			if (fNewLat > 90.0)
				fNewLat = -89.9;
			break;
		case K_L:
			// On décrémente la valeur de 1°
			fNewLat -= 1.0;
			if (fNewLat < -89.9)
				fNewLat = 90.0;
			break;
		case K_PLUS:
			// On incrémente la valeur de 0.1°
			fNewLat += 0.1;
			if (fNewLat > 90.0)
				fNewLat = -89.9;
			break;
		case K_MINUS:
			// On décrémente la valeur de 0.1°
			fNewLat -= 0.1;
			if (fNewLat < -89.9)
				fNewLat = 90.0;
			break;
		case K_SQUARE:
			// Init de la valeur min
			fNewLat = 42.0;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			fNewLat = 50.0;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			fNewLat = fOldLat;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void LatitudeParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
		fNewLat = pParams->GetLatitude();
	//                   123456789012345678901
	//                    Latitude  +90.5
	sprintf(lineParam, " Latitude  %+05.1f", fNewLat);
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void LatitudeParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	fOldLat = pParams->GetLatitude();
	fNewLat = fOldLat;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void LatitudeParam::SetParam()
{
	// Vérification du paramètre
	if (fNewLat < -89.9 or fNewLat > 90.0)
		// Valeur précédente
		fNewLat = fOldLat;
	pParams->SetLatitude(fNewLat);
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}

