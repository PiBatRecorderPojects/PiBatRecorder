/* 
 * File:   LatitudeParam.h
 * Author: Jean-Do
 *
 * Created on 5 janvier 2016, 22:19
 */
#include "GenericParam.h"

#ifndef LATITUDEPARAM_H
#define	LATITUDEPARAM_H

//-------------------------------------------------------------------------
// Classe de modification de la latitude d'un site
class LatitudeParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	LatitudeParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~LatitudeParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Mémo de la valeur avant modif
	float fOldLat;
	
	// Mémo de la valeur courante
	float fNewLat;
};

#endif	/* LATITUDEPARAM_H */

