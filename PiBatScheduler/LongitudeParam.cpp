/* 
 * File:   LongitudeParam.cpp
 * Author: Jean-Do
 * 
 * Created on 5 janvier 2016, 22:20
 */
//-------------------------------------------------------------------------
// Classe de modification de la longitude d'un site

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "LongitudeParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
LongitudeParam::LongitudeParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Longitude +180.5
	strcpy( indicModif, "00000000000xxxxx10000");
	// Mémo de la valeur courante
	fOldLon = pParams->GetLongitude();
	fNewLon = fOldLon;
}

//-------------------------------------------------------------------------
// Destructeur
LongitudeParam::~LongitudeParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void LongitudeParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_R:
			// On incrémente la valeur de 1°
			fNewLon += 1.0;
			if (fNewLon > 180.0)
				fNewLon = -179.9;
			break;
		case K_L:
			// On décrémente la valeur de 1°
			fNewLon -= 1.0;
			if (fNewLon < -179.9)
				fNewLon = 180.0;
			break;
		case K_PLUS:
			// On incrémente la valeur de 0.1°
			fNewLon += 0.1;
			if (fNewLon > 180.0)
				fNewLon = -179.9;
			break;
		case K_MINUS:
			// On décrémente la valeur de 0.1°
			fNewLon -= 0.1;
			if (fNewLon < -179.9)
				fNewLon = 180.0;
			break;
		case K_SQUARE:
			// Init de la valeur min
			fNewLon = -5.0;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			fNewLon = 9.0;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			fNewLon = fOldLon;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void LongitudeParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
		fNewLon = pParams->GetLongitude();
	//                   123456789012345678901
	//                    Longitude +180.5
	sprintf(lineParam, " Longitude %+06.1f", fNewLon);
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void LongitudeParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	fOldLon = pParams->GetLongitude();
	fNewLon = fOldLon;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void LongitudeParam::SetParam()
{
	// Vérification du paramètre
	if (fNewLon < -179.9 or fNewLon > 180.0)
		// Valeur précédente
		fNewLon = fOldLon;
	pParams->SetLongitude(fNewLon);
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}

