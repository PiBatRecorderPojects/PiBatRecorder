/* 
 * File:   LongitudeParam.h
 * Author: Jean-Do
 *
 * Created on 5 janvier 2016, 22:20
 */

#include "GenericParam.h"

#ifndef LONGITUDEPARAM_H
#define	LONGITUDEPARAM_H

//-------------------------------------------------------------------------
// Classe de modification de la longitude d'un site
class LongitudeParam : public GenericParam
{
public:
	//-------------------------------------------------------------------------
	// Constructeur (initialisation des paramètres aux valeurs par défaut)
	LongitudeParam(
		KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
		CParameters		*pPar		// Pointeur sur les paramètres
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~LongitudeParam();

	//-------------------------------------------------------------------------
	// Traitement d'une touche clavier
	virtual void ReceiveKey(
		int iKey	// Touche à traiter
		);
	
	//-------------------------------------------------------------------------
	// Affichage du paramètre
	virtual void AffParam(
		int iLine,		// Indice de la ligne ou afficher le paramètre
		bool bActive	// Inidque si la ligne est active
		);

	//-------------------------------------------------------------------------
	// Start la modification du paramètre
	void StartModif();

	//-------------------------------------------------------------------------
	// Mise en jour du paramètre
	virtual void SetParam();

private:
	// Mémo de la valeur avant modif
	float fOldLon;
	
	// Mémo de la valeur courante
	float fNewLon;
};

#endif	/* LONGITUDEPARAM_H */

