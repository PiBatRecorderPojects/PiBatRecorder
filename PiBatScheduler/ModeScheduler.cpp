/* 
 * File:   ModeScheduler.cpp
 * Author: Jean-Do
 * 
 * Created on 5 janvier 2016, 20:20
 */
//-------------------------------------------------------------------------
// Classe de gestion de la planification des enregistrement

#include <ctime>
#include <math.h>
#include <dirent.h>
#include <list>
#include <string>
#include "ModeScheduler.h"

#define PI 3.14159265

//-------------------------------------------------------------------------
// Constructeur
ModeScheduler::ModeScheduler(
	CParameters *pPar,					// Pointeur sur les paramètres
	KeyOledManager *pKeyScreenManager,	// Pointeur sur le gestionnaire du clavier et de l'écran
	bool bAutoMode						// True si le logiciel fonctionne entièrement en mode automatique
										// Si oui, le clavier et l'écran ne sont pas gérés
	):GenericMode(pPar, pKeyScreenManager, bAutoMode)
{
	// Par défaut, pas d'affichage
	bDemandeAff = false;
	// PiBatRecorder non lancé
	bRecording = false;
	bExit = false;
}

//-------------------------------------------------------------------------
// Destructeur
ModeScheduler::~ModeScheduler()
{
}

//-------------------------------------------------------------------------
// Début du mode
void ModeScheduler::BeginMode()
{
	LogFile::AddLog(LDEBUG, "ModeScheduler::BeginMode\n");
	time_t curtime;
	struct tm * timeinfo;
	bRecording = false;
	// Effacement de l'écran
	pKeyOledManager->ClearScreen();
	pKeyOledManager->DisplayScreen();
	
	// Calcul des heures de début et fin d'enregistrement
	// Si planification au soleil
	if (pParams->GetScheduleType() == SUN)
		CalculBeginEnd();
	// Sinon l'heure de début et de fin est directement dans le fichier des paramètres
	
	// Calcul de l'heure de départ de PiBatRecorder (HDeb - 5mn)
	char sHDeb[15];
	sscanf(pParams->GetHDeb(), "%d:%d", &iHeDeb, &iMnDeb);
	time(&curtime);
	timeinfo = localtime(&curtime);
	timeinfo->tm_hour = iHeDeb;
	timeinfo->tm_min  = iMnDeb;
	timeinfo->tm_sec  = 0;
	curtime = mktime(timeinfo);
	curtime -= (5 * 60);
	RecorderTime = curtime;
	timeinfo = localtime(&RecorderTime);
	char sTemp[160];
	strftime(sTemp, 160, "ModeScheduler::BeginMode Attente enregistrement %d/%m/%Y - %H:%M:%S\n", timeinfo);
	LogFile::AddLog(LLOG, sTemp);
	
	// Calcul de l'heure de reboot ou halt (HFin + 5mn)
	char sHFin[15];
	sscanf(pParams->GetHFin(), "%d:%d", &iHeFin, &iMnFin);
	time(&curtime);
	if (iHeFin < iHeDeb)
		// Le reboot est le lendemain
		curtime += (24 * 3600);
	timeinfo = localtime(&curtime);
	timeinfo->tm_hour = iHeFin;
	timeinfo->tm_min  = iMnFin;
	timeinfo->tm_sec  = 0;
	curtime = mktime(timeinfo);
	curtime += (5 * 60);
	RebootTime = curtime;
	timeinfo = localtime(&RebootTime);
	strftime(sTemp, 160, "ModeScheduler::BeginMode Attente reboot/halt %d/%m/%Y - %H:%M:%S\n", timeinfo);
	LogFile::AddLog(LLOG, sTemp);
	
	// Demande d'affichage momentané de l'heure de début et de fin
	bDemandeAff = true;
	timeAff = millis();
}

//-------------------------------------------------------------------------
// Fin du mode
void ModeScheduler::EndMode()
{
	LogFile::AddLog(LDEBUG, "ModeScheduler::EndMode\n");
	if (bRecording)
	{
		// Arrêt de PiBatRecorder en céant un fichier PiBatStop.txt
		LogFile::AddLog(LLOG, "ModeScheduler::EndMode stop PiBatRecorder via PiBatStop.txt\n");
		system("touch /home/pi/PiBatRecorder/PiBatStop.txt");
	}
}
	
//-------------------------------------------------------------------------
// Test de l'heure pour lancer les actions
void ModeScheduler::TestTimeForAction()
{
	// Récupération de l'heure courante
	time_t curtime;
	struct tm * timeinfo;
	time(&curtime);
	timeinfo = localtime(&curtime);
	
	// Test si l'enregistrement doit commencer
	if (!bRecording and curtime >= RecorderTime)
	{
		// On passe en enregistrement
		bRecording = true;
		// Effacement de l'écran
		pKeyOledManager->ClearScreen();
		pKeyOledManager->DisplayScreen();
		// On lance le logiciel d'enregistrement
		StartRecorder();
	}
	
	// Test si le reboot ou halt doit commencer
	if (curtime >= RebootTime)
	{
		char sTmp[160];
		// Il est l'heure de faire un reboot ou un halt
		// Test du type d'action à réaliser
		switch (pParams->GetShutdownType())
		{
		case SD_REBOOT:
			// Un reboot est lancé pour redémarrer le PI tout les jours après la période d'enregistrement
			LogFile::AddLog(LLOG, "ModeScheduler::TestTimeForAction start reboot\n");
			system("sudo reboot&");
			bExit = true;
			break;
		default:
			// Un halt est lancé pour que l'arrêt de l'alimentation soit sans conséquence
			LogFile::AddLog(LLOG, "ModeScheduler::TestTimeForAction start halt\n");
			system("sudo halt&");
			bExit = true;
		}
	}
	
	// Si enregistrement, test si PiBatRecorder est en exécution
	if (bRecording and !bExit)
	{
		// Préparation de la commande à chercher
		char sTmp[255];
		strcpy(sTmp, pParams->GetRecorderCmd());
		if (sTmp[strlen(sTmp)-1] == '&')
			sTmp[strlen(sTmp)-1] = 0;
		if (sTmp[strlen(sTmp)-1] == ' ')
			sTmp[strlen(sTmp)-1] = 0;
		if (sTmp[strlen(sTmp)-1] == 0x0d)
			sTmp[strlen(sTmp)-1] = 0;
		bool bPiBatRecorder = false;
		// Test si ./pibatrecorder est présent dans "ps -ef"
		system("ps -ef > /home/pi/test.txt");
		// Ligne de lecture
		char sLine[255];
		// Ouverture du fichier de configuration
		FILE *f = fopen ("/home/pi/test.txt", "r");
		if (f != NULL)
		{
			// Lecture et décodage des différentes lignes
			while (fgets(sLine, 255, f) != NULL)
			{
				if (strstr(sLine, sTmp) != NULL)
				{
					bPiBatRecorder = true;
					break;
				}
			}
			// Fermeture et effacement du fichier
			fclose(f);
			system("rm /home/pi/test.txt");
			if (!bPiBatRecorder)
			{
				// Mémo de l'absence de PiBatRecorder
				LogFile::AddLog(LLOG, "ModeScheduler::TestTimeForAction recorder [%s] is out !", sTmp);
				// PiBatRecorder a planté, on le relance
				StartRecorder();
			}
		}
	}
}

//-------------------------------------------------------------------------
// Lancement du programme d'enregistrement
void ModeScheduler::StartRecorder()
{
	// Effacment éventuel du fichier PiBatStop.txt
	system("rm /home/pi/PiBatRecorder/PiBatStop.txt");
	// On lance le logiciel d'enregistrement
	char sTmp[160];
	strcpy( sTmp, pParams->GetRecorderCmd());
	if (sTmp[strlen(sTmp)-1] != '&')
	{
		if (sTmp[strlen(sTmp)-1] != ' ')
			strcat(sTmp, " ");
		strcat(sTmp, "&");
	}
	system(sTmp);
	// Mémo de l'action dans le log
	LogFile::AddLog(LLOG, "ModeScheduler::StartRecorder start recorder : [%s]", sTmp);
}

//-------------------------------------------------------------------------
// Affichage du mode sur l'écran
// Cette méthodes est appelée régulièrement par la classe PiBatManager
// à charge des différents modes d'afficher les informations nécessaires
void ModeScheduler::PrintMode()
{
	if (bDemandeAff)
	{
		// Test de fin de l'affichage momentané
		unsigned int uiNow = millis();
		if (uiNow - timeAff > 5000)
		{
			//printf("ModeScheduler::PrintMode Fin de l'affichage momentané\n");
			bDemandeAff = false;
			// Effacement de l'écran
			pKeyOledManager->ClearScreen();
			pKeyOledManager->DisplayScreen();
		}
		else
		{
			// Sinon Si on est en attente du lancement de PiBatRecorder
			// On affiche les paramètres de lancement
			if (!bRecording)
			{
				char sLine[25];
				//       123456789012345678901
				// L1 T1 PiBatScheduler
				// L2 T1 Début enregistrement	Begin record
				// L3 T2 22:38
				// L5 T1 Fin   enregistrement	End record
				// L6 T2 06:42
				// L8 T1 08/01/2016 - 15:34:56
				sprintf(sLine, "PiBatScheduler");
				pKeyOledManager->DrawString(sLine, 0, L1T1, true, 1);
				if (pParams->GetLangue() == FR)
					sprintf(sLine, "D%cbut enregistrement", 0x82);
				else
					sprintf(sLine, "Begin record");
				pKeyOledManager->DrawString(sLine, 0, L2T1, true, 1);
				sprintf(sLine, "%02d:%02d", iHeDeb, iMnDeb);
				pKeyOledManager->DrawString(sLine, 0, L3T1, true, 2);
				if (pParams->GetLangue() == FR)
					sprintf(sLine, "Fin   enregistrement");
				else
					sprintf(sLine, "End   record");
				pKeyOledManager->DrawString(sLine, 0, L5T1, true, 1);
				sprintf(sLine, "%02d:%02d", iHeFin, iMnFin);
				pKeyOledManager->DrawString(sLine, 0, L6T1, true, 2);
			}
			// Sinon, on est en enregistrement, on affiche le nombre de fichiers
			else
			{
				char sLine[25];
				//       123456789012345678901
				// L1 T1 PiBatScheduler
				// L2 T1 Enregistrement... 	    Recording
				// L3 T2 xxxx fichiers          xxxx files
				// L5 T1 Fin   enregistrement	End record
				// L6 T2 06:42
				// L8 T1 08/01/2016 - 15:34:56
				sprintf(sLine, "PiBatScheduler");
				pKeyOledManager->DrawString(sLine, 0, L1T1, true, 1);
				if (pParams->GetLangue() == FR)
					sprintf(sLine, "Enregistrement...");
				else
					sprintf(sLine, "Recording...     ");
				pKeyOledManager->DrawString(sLine, 0, L2T1, true, 1);
				int iNbWaveFiles = GetNbWavFiles();
				if (iNbWaveFiles > 1)
				{
					if (pParams->GetLangue() == FR)
						sprintf(sLine, "%04d fichs", GetNbWavFiles());
					else
						sprintf(sLine, "%04d files", GetNbWavFiles());
				}
				else
				{
					if (pParams->GetLangue() == FR)
						sprintf(sLine, "%04d fich ", GetNbWavFiles());
					else
						sprintf(sLine, "%04d file ", GetNbWavFiles());
				}
				pKeyOledManager->DrawString(sLine, 0, L3T1, true, 2);
				if (pParams->GetLangue() == FR)
					sprintf(sLine, "Fin   enregistrement");
				else
					sprintf(sLine, "End   record");
				pKeyOledManager->DrawString(sLine, 0, L5T1, true, 1);
				sprintf(sLine, "%02d:%02d", iHeFin, iMnFin);
				pKeyOledManager->DrawString(sLine, 0, L6T1, true, 2);
			}
			// Appel de la méthode de base pour l'affichage de la date et de l'heure
			// On force hors mode auto pour l'affichage
			bool bAuto = bModeAuto;
			bModeAuto = false;
			GenericMode::PrintMode();
			bModeAuto = bAuto;
		}
	}
	// Test si une action doit être faite
	TestTimeForAction();
}

//-------------------------------------------------------------------------
// Traitement des ordres claviers
// Retourne éventuellement un mode demandé (NOMODE sinon)
// Cette méthodes est appelée régulièrement par la classe PiBatManager
// à charge des différents modes de traiter les actions opérateurs
int ModeScheduler::ManageKey(
	unsigned short key	// Touche sollicitée par l'opérateur
	)
{
	MODEFONC newMode = NOMODE;

	if (key != iLastKey)
	{
		switch (key)
		{
		case K_SQUARE:
			// Demande d'affichage momentané
			//printf("ModeScheduler::ManageKey Demande d'affichage momentané\n");
			bDemandeAff = true;
			timeAff = millis();
			break;
		default:
			// Traite les changements de mode
			newMode = (MODEFONC) GenericMode::ManageKey(key);
		}
		iLastKey = key;
	}
	if (bExit)
		// On sort immédiatement du logiciel
		newMode = EXIT;

	return newMode;
}

//-------------------------------------------------------------------------
// Calcul des heures de début et fin d'enregistrement en fonction du coucher et lever du soleil
void ModeScheduler::CalculBeginEnd()
{
	// Formules issues de http://jean-paul.cornec.pagesperso-orange.fr/heures_lc.htm
	tm currentday, nextday;
	int JCurrent, JNext;
	// Récupération de la date courante
	time_t curtime;
	struct tm * timeinfo;
	time(&curtime);
	timeinfo = localtime(&curtime);
	memcpy( &currentday, timeinfo, sizeof(tm));
	JCurrent = currentday.tm_yday;
	// Préparation des infos du jour suivant
	curtime += (24*3600);
	timeinfo = localtime(&curtime);
	memcpy( &nextday, timeinfo, sizeof(tm));
	JNext = nextday.tm_yday;
	
	// Récupération des valeurs des paramètres
	float Lat  = pParams->GetLatitude();
	float Long = pParams->GetLongitude();
	char sHDec[12];
	int iHH, iMM;
	strcpy(sHDec, pParams->GetHDec());
	sscanf(sHDec, "%d:%02d", &iHH, &iMM);
	float fDec = iHH + (iMM / 60);
	//printf("CalculBeginEnd fDec %f\n", fDec);
	int iGMTCurrent = pParams->GetGMT();
	int iGMTNext    = iGMTCurrent;
	// Gestion de l'heure d'été
	if (pParams->GetSummmerTime())
	{
		// Test si heure d'été pour le jour courant
		if (IsSummerTime(&currentday))
			iGMTCurrent +=1;
		// Test si heure d'été pour le jour suivant
		if (IsSummerTime(&nextday))
			iGMTNext +=1;
	}
	
	// Calcul de l'heure solaire du coucher de soleil pour le jour courant
	float HC = GetSunset( Lat, Long, JCurrent);
	// Calcul de l'heure légale en tenant compte de l'heure d'été
	HC += iGMTCurrent;
	int iHeC = (int)trunc(HC);
	int iMnC = (int)trunc((HC-(float)iHeC)*60);
	
	// Calcul de l'heure solaire du lever de soleil pour le jour suivant
	float HL = GetSunrise( Lat, Long, JNext);
	// Calcul de l'heure légale en tenant compte de l'heure d'été
	HL += iGMTNext;
	int iHeL = (int)trunc(HL);
	int iMnL = (int)trunc((HL-(float)iHeL)*60);
	
	// Prise en compte du décalage
	HL = HL - fDec;
	HC = HC + fDec;
	// Calcul et mémo de l'heure de départ d'enregistrement
	iHeDeb = (int)trunc(HC);
	iMnDeb = (int)trunc((HC-(float)iHeDeb)*60);
	char sHDeb[15];
	sprintf(sHDeb, "%02d:%02d", iHeDeb, iMnDeb);
	pParams->SetHDeb(sHDeb);
	// Calcul et mémo de l'heure de fin d'enregistrement
	iHeFin = (int)trunc(HL);
	iMnFin = (int)trunc((HL-(float)iHeFin)*60);
	char sHFin[15];
	sprintf(sHFin, "%02d:%02d", iHeFin, iMnFin);
	pParams->SetHFin(sHFin);
	// Mémo du fichier des paramètres pour PiBatRecorder
	pParams->WriteParams();
	LogFile::AddLog(LLOG, "ModeScheduler::CalculBeginEnd Lat %f, Long %f, Coucher %02d:%02d, Lever %02d:%02d, Départ %s, Fin %s\n"
			, Lat, Long, iHeC, iMnC, iHeL, iMnL, sHDeb, sHFin);
}

//-------------------------------------------------------------------------
// Calcul de l'Equation du Temps (ET) à 12h UTC pour le jour N
// et le sinus de la déclinaison solaire (SinDec et Dec)
void ModeScheduler::GetETAndSinDec(
	int   N,		// Rang du jour dans l'année (01/01 = 1)
	float *pET,		// Pointeur sur la valeur ET à retourner
	float *pSinDec,	// Pointeur sur la valeur SinDec à retourner
	float *pDec 	// Pointeur sur la valeur Dec à retourner
	)
{
	// Calcul de l'Equation du Temps à 12h UTC pour ce jour (ET)
	// 1ère étape : M = 356,8° + 0°,9856 x (j - 1)
	float M = 356.8 + 0.9856 * (N - 1);
	// 2ème étape : C = 1°,91378 x sin(M) + 0°,02 x sin(2M)
	float C = 1.91378 * sin(M*PI/180) + 0.02 * sin((2*M)*PI/180);
	// 3ème étape : L = 280° + C + 0°,9856 x j
	float L = 280 + C + 0.9856 * N;
	// 4ème étape : R = -2°,46522 x sin(2L) + 0°,05303x sin(4L)
	float R = -2.46522 * sin((2*L)*PI/180) + 0.05303 * sin((4*L)*PI/180);
	// Et enfin : Equation du Temps (minutes) = (C + R) x 4
	*pET = (C + R) * 4;
	// B - Pour la déclinaison du Soleil le jour "j" ce n'est pas plus compliqué :
	// Sin(déclinaison) = 0,39774 x sin(L)
	*pSinDec = 0.39774 * sin(L*PI/180);
	*pDec = asin(*pSinDec) * 180.0 / PI;
}

//-------------------------------------------------------------------------
// Retourne l'heure du coucher du soleil à partir des coordonnées géographiques
// et du rang du jour dans l'année
float ModeScheduler::GetSunset(
	float fLat,	// Latidude du lieu
	float fLon,	// Longitude du lieu
	int   N		// Rang du jour dans l'année (01/01 = 1)
	)
{
	// Calcul de l'Equation du Temps (ET) à 12h UTC pour le jour N
	// et le sinus de la déclinaison solaire (SinDec et Dec)
	float ET, SinDec, Dec;
	GetETAndSinDec( (float)N, &ET, &SinDec, &Dec);
	
	// Init Latitude et Longitude
	float LatRad = fLat * PI / 180;
	float LongH  = fLon / 15;
	// Calcul coucher
	// cos (Ho) = [ -0,01454 - sin ( Dec ) * sin ( Lat ) ] / [ cos ( Dec ) * cos ( Lat ) ]
	float CosHo = (-0.01454-SinDec*sin(LatRad)) / (cos(Dec*PI/180) * cos(LatRad));
	float Ho = acos(CosHo) * 180.0 / PI;
	Ho = Ho / 15;
	ET = ET / 60;
	// Heure solaire du coucher : HC = 12 + Ho + ET + Lon
	float HC = 12 + Ho + ET + LongH;

	return HC;
}

//-------------------------------------------------------------------------
// Retourne l'heure du lever du soleil à partir des coordonnées géographiques
// et du rang du jour dans l'année
float ModeScheduler::GetSunrise(
	float fLat,	// Latidude du lieu
	float fLon,	// Longitude du lieu
	int   N		// Rang du jour dans l'année (01/01 = 1)
	)
{
	// Calcul de l'Equation du Temps (ET) à 12h UTC pour le jour N
	// et le sinus de la déclinaison solaire (SinDec et Dec)
	float ET, SinDec, Dec;
	GetETAndSinDec( (float)N, &ET, &SinDec, &Dec);
	
	// Init Latitude et Longitude
	float LatRad = fLat * PI / 180;
	float LongH  = fLon / 15;
	// Calcul lever
	// cos (Ho) = [ -0,01454 - sin ( Dec ) * sin ( Lat ) ] / [ cos ( Dec ) * cos ( Lat ) ]
	float CosHo = (-0.01454-SinDec*sin(LatRad)) / (cos(Dec*PI/180) * cos(LatRad));
	float Ho = acos(CosHo) * 180.0 / PI;
	Ho = Ho / 15;
	ET = ET / 60;
	// Heure solaire du lever : HL = 12 - Ho + ET + Lon
	float HL = 12 - Ho + ET + LongH;

	return HL;
}

//-------------------------------------------------------------------------
// Retourne true si on est en heure d'été pour le jour passé en paramètre
bool ModeScheduler::IsSummerTime(
	tm *daytime	// Info du jour à tester
	)
{
	time_t tmptime;
	tm daytmp, dayinfo;
	tm *timeinfo;
	bool bSummerTime = false;
	// Mémo des valeurs de daytime (les appels à localtime détruisent les infos)
	memcpy( &dayinfo, daytime, sizeof(tm));
	// Test si on est en avril, mai, juin, juillet, aout et septembre
	// ATTENTION les mois commencent à 0 dans tm
	if (dayinfo.tm_mon > 2 and dayinfo.tm_mon < 9)
	{
		// On est en heure d'été
		bSummerTime = true;
		//printf("Mois d'été plein\n");
	}
	else if (dayinfo.tm_mon == 2 and dayinfo.tm_mday >= 24 and dayinfo.tm_mday <= 31)
	{
		// Recherche du dernier dimanche de mars
		// On part du 1er avril
		daytmp.tm_mday = 1;
		daytmp.tm_mon  = 3;
		daytmp.tm_year = dayinfo.tm_year;
		daytmp.tm_hour = 12;
		daytmp.tm_min  = 0;
		daytmp.tm_sec  = 0;
		tmptime = mktime(&daytmp);
		for (int i=0; i<7; i++)
		{
			tmptime -= (24*3600);
			timeinfo = localtime(&tmptime);
			if (timeinfo->tm_wday == 0)
			{
				if (dayinfo.tm_mday >= timeinfo->tm_mday)
					// On est en heure d'été
					bSummerTime = true;
				break;
			}
		}
	}
	else if (dayinfo.tm_mon == 9 and dayinfo.tm_mday >= 24 and dayinfo.tm_mday <= 31)
	{
		// Recherche du dernier dimanche d'octobre
		// On part du 1er novembre
		daytmp.tm_mday = 1;
		daytmp.tm_mon  = 10;
		daytmp.tm_year = dayinfo.tm_year;
		daytmp.tm_hour = 12;
		daytmp.tm_min  = 0;
		daytmp.tm_sec  = 0;
		tmptime = mktime(&daytmp);
		for (int i=0; i<7; i++)
		{
			tmptime -= (24*3600);
			timeinfo = localtime(&tmptime);
			if (timeinfo->tm_wday == 0)
			{
				if (dayinfo.tm_mday < timeinfo->tm_mday)
					// On est en heure d'été
					bSummerTime = true;
				break;
			}
		}
	}
	// Restitution des valeurs de daytime
	memcpy( daytime, &dayinfo, sizeof(tm));
	return bSummerTime;
}

//-------------------------------------------------------------------------
// Recherche du nombre de fichiers wav
// Retourne le nombre de fichiers wav sur l'espace de stockage
int ModeScheduler::GetNbWavFiles()
{
	int iNbWav = 0;
	// Recherche des fichiers sur l'espace de stockage
	DIR *rep = opendir(pParams->GetWavPath());   
	if (rep != NULL) 
	{ 
		struct dirent *ent; 

		while ((ent = readdir(rep)) != NULL) 
		{
			//0123456789
			//toto.wav
			//printf("Ajout %s\n", ent->d_name);
			std::string wavePath(pParams->GetWavPath());
			wavePath += ent->d_name;
			std::string ext = wavePath.substr(wavePath.size()-4, 4);
			if (ext == ".wav")
				iNbWav++;
		} 
		closedir(rep); 
	}
	return iNbWav;
}
