/* 
 * File:   ModeScheduler.h
 * Author: Jean-Do
 *
 * Created on 5 janvier 2016, 20:20
 */
#include "GenericMode.h"

#ifndef MODESCHEDULER_H
#define	MODESCHEDULER_H

//-------------------------------------------------------------------------
// Classe de gestion de la planification des enregistrement
class ModeScheduler : public GenericMode
{
public:
	//-------------------------------------------------------------------------
	// Constructeur
	ModeScheduler(
		CParameters *pPar,					// Pointeur sur les paramètres
		KeyOledManager *pKeyScreenManager,	// Pointeur sur le gestionnaire du clavier et de l'écran
		bool bAutoMode=false				// True si le logiciel fonctionne entièrement en mode automatique
			                                // Si oui, le clavier et l'écran ne sont pas gérés
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~ModeScheduler();
	
	//-------------------------------------------------------------------------
	// Début du mode
	virtual void BeginMode();
	
	//-------------------------------------------------------------------------
	// Fin du mode
	virtual void EndMode();
	
	//-------------------------------------------------------------------------
	// Affichage du mode sur l'écran
	// Cette méthode est appelée régulièrement par la classe PiBatManager
	// à charge des différents modes d'afficher les informations nécessaires
	virtual void PrintMode();
	
	//-------------------------------------------------------------------------
	// Calcul des heures de début et fin d'enregistrement
	void CalculBeginEnd();
	
	//-------------------------------------------------------------------------
	// Traitement des ordres claviers
	// Si la touche est une touche de changement de mode, retourne le mode demandé
	// Cette méthode est appelée régulièrement par la classe PiBatManager
	// à charge des différents modes de traiter les actions opérateurs
	virtual int ManageKey(
		unsigned short key	// Touche sollicitée par l'opérateur
		);
	
	//-------------------------------------------------------------------------
	// Retourne l'heure du coucher du soleil à partir des coordonnées géographiques
	// et du rang du jour dans l'année
	float GetSunset(
		float fLat,	// Latidude du lieu
		float fLon,	// Longitude du lieu
		int   N		// Rang du jour dans l'année (01/01 = 1)
		);

	//-------------------------------------------------------------------------
	// Retourne l'heure du lever du soleil à partir des coordonnées géographiques
	// et du rang du jour dans l'année
	float GetSunrise(
		float fLat,	// Latidude du lieu
		float fLon,	// Longitude du lieu
		int   N		// Rang du jour dans l'année (01/01 = 1)
		);

	//-------------------------------------------------------------------------
	// Calcul de l'Equation du Temps (ET) à 12h UTC pour le jour N
	// et le sinus de la déclinaison solaire (SinDec et Dec)
	void GetETAndSinDec(
		int   N,		// Rang du jour dans l'année (01/01 = 1)
		float *pET,		// Pointeur sur la valeur ET à retourner
		float *pSinDec,	// Pointeur sur la valeur SinDec à retourner
		float *pDec 	// Pointeur sur la valeur Dec à retourner
		);

	//-------------------------------------------------------------------------
	// Retourne true si on est en heure d'été pour le jour passé en paramètre
	bool IsSummerTime(
		struct tm * dayinfo	// Info du jour à teste
		);

	//-------------------------------------------------------------------------
	// Test de l'heure pour lancer les actions
	void TestTimeForAction();
	
	//-------------------------------------------------------------------------
	// Lancement du programme d'enregistrement
	void StartRecorder();
	
	//-------------------------------------------------------------------------
	// Recherche du nombre de fichiers wav
	// Retourne le nombre de fichiers wav sur l'espace de stockage
	int GetNbWavFiles();

private:
	// Indique si l'affichage est actif ou non
	bool bDemandeAff;
	
	// Mémorisation du temps pour le timeout de fin d'affichage
	unsigned int timeAff;

	// Indique que PiBatRecorder est lancé
	bool bRecording;
	
	// Iniidque une sortie du logiciel
	bool bExit;
	
	// Heure de départ du mode enregistrement
	int iHeDeb, iMnDeb;
	// Heure de fin du mode enregistrement
	int iHeFin, iMnFin;
	
	// Date et heure de lancement de PiBatRecorder
	time_t RecorderTime;
	// Date et heure du reboot
	time_t RebootTime;
};

#endif	/* MODESCHEDULER_H */

