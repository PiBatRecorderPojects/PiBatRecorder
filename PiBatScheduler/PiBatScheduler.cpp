/* 
 * File:   PiBatScheduler.cpp
 * Author: Jean-Do
 * 
 * Created on 17 août 2015, 21:13
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctime>
#include <wiringPi.h>
#include <dirent.h> 
#include "PiBatScheduler.h"

//-------------------------------------------------------------------------
// Constructeur
// Par défaut, tout est en mode auto
PiBatScheduler::PiBatScheduler()
	: GestInit(&cParams, &KScreen, false)
	, GestModeParams(&cParams, &KScreen, false)
	, GestPlay(&cParams, &KScreen, false)
	, GestScheduler(&cParams, &KScreen, false)
	, GestQuit(&cParams, &KScreen, false)
	, GestFin(&cParams, &KScreen, false)
{
	//printf("PiBatRecorder::PiBatRecorder\n");		
	// Init des variables
	bQuit = false;
	bAutoMode = true;
	iCurrentMode = NOMODE;
	// Initialisation de l'écran et du clavier
	KScreen.InitScreenAndKeyboard( cParams.GetScreen(), cParams.GetKeyboard());
	// Si, pas de clavier ou écran, on passe automatiquement en mode auto
	if (cParams.GetScreen() == NOSCREEN or cParams.GetKeyboard() == NOKEYBOARD)
		bAutoMode = true;
	// On fixe le mode enregistrement Auto
	cParams.SetRecMode(AUTO);
	// Si mode auto, on force le fonctionnement en monitoring auto
	if (bAutoMode)
	{
		cParams.SetModeAuto( true);
		cParams.SetRecMode( AUTO);
	}
}

//-------------------------------------------------------------------------
// Destructeur
PiBatScheduler::~PiBatScheduler()
{
}

//-------------------------------------------------------------------------
// Séquence d'initialisation du programme
void PiBatScheduler::Init()
{
	// Reset de la carte audio
	char temp[255];
	strcpy( temp, cParams.GetWolfsonShPath());
	strcat( temp, "Reset_paths.sh -q");
	system(temp);

	// On essai de monter la clé USB
	cParams.MountUSBKey();
	// Init des répertoires de sauvegarde sur la clé ou en local
	cParams.SetSavePaths();
}

//-------------------------------------------------------------------------
// Boucle principale du programme
// Lecture du clavier et gestion des modes de fonctionnement
void PiBatScheduler::Run()
{
	// Par défaut, le gestionnaire courant est l'init
	pCurrentGest = &GestInit;
	pCurrentGest->BeginMode();
	// Mémorisation de l'heure du dernier affichage
	// pour cadencer l'affichage sur 200ms
	unsigned int timeAff;
	timeAff = millis();
	// Mémorisation de l'heure de la derniere modif hétrodyne
	// pour cadencer le changement de fréquence hétérodyne
	unsigned int timeHet;
	timeHet = millis();
	// Mémorisation de l'heure du dernier clic clavier
	// pour autoriser la même touche après 500ms
	unsigned int timeKey;
	timeKey = millis();
	// Si pas de frappe clavier pendant 10s
	// on passe en mode économie avec une attente de 1000ms
	unsigned int timelastKey;
	timelastKey = millis();
	// Boucle principale de lecture du clavier, affichage, changement de mode...
	while (!bQuit)
	{
		//printf("timeNow %d (%dms)\n", millis(), millis() - timeNow);
		// Mémo de l'heure courante
		timeNow = millis();
		// On force l'affichage toutes les 200ms
		if ((timeNow - timeAff) > 200)
		{
			// On mémorise l'heure courante
			timeAff = timeNow;
			// On affiche le mode courant
			pCurrentGest->PrintMode();
		}

		// On teste le clavier toutes les 50ms
		if ((timeNow - timeKey) > 50)
		{
			// Lecture du clavier et appel systématique d ela fonction ManageKey
			// car des timers sont éventuellement gérés dans cette méthode
			int iKey = KScreen.GetKey();
			if (iKey != K_NO)
				timelastKey = timeNow;
			int iNewMode = pCurrentGest->ManageKey(iKey);
			if (iNewMode != NOMODE)
				// Changement de mode de fonctionnement
				bQuit = SetMode(iNewMode);
		}		
		// Si pas de frappe clavier pendant 10s
		// on passe en mode économie avec une attente de 1000ms
		if ((timeNow - timelastKey) > 10000)
			delay(1000);
	}
	// Si shutdown demandé
	if (GestQuit.IsWithHalt())
	{	
		LogFile::AddLog(LLOG, "PiBatScheduler go to halt system");
		system("sudo halt");
	}
	else
		LogFile::AddLog(LLOG, "PiBatScheduler go to end without halt\n");		
}

//-------------------------------------------------------------------------
// Change le mode courant
// Retourne true si on doit quitter le programme
bool PiBatScheduler::SetMode( int newMode)
{
	if (newMode != iCurrentMode)
	{
		// Traitement d'un changement de mode
		bQuit = false;
		// Fin du mode précédent
		pCurrentGest->EndMode();
		// Init du pointeur sur le nouveau mode
		switch (newMode)
		{
		case INIT:
			LogFile::AddLog(LLOG, "PiBatScheduler go to INIT");
			pCurrentGest = &GestInit;
			break;
		case MENU:
			LogFile::AddLog(LLOG, "PiBatScheduler go to MENU");
			pCurrentGest = &GestModeParams;
			break;
		case PLAY:
			LogFile::AddLog(LLOG, "PiBatScheduler go to PLAY");
			pCurrentGest = &GestPlay;
			break;
		case MONITORING:
			LogFile::AddLog(LLOG, "PiBatScheduler go to SCHEDULING");
			pCurrentGest = &GestScheduler;
			break;
		case QUIT:
			LogFile::AddLog(LLOG, "PiBatScheduler go to QUIT");
			pCurrentGest = &GestQuit;
			// Mémo du mode précédent
			GestQuit.SetOldMode((MODEFONC)iCurrentMode);
			break;
		case END:
			LogFile::AddLog(LLOG, "PiBatScheduler go to END");
			pCurrentGest = &GestFin;
			break;
		case ERROR:
			LogFile::AddLog(LLOG, "PiBatScheduler go to ERROR");
			pCurrentGest = &GestModeParams;
			break;
		case EXIT:
		default:
			LogFile::AddLog(LLOG, "PiBatScheduler go to EXIT");
			bQuit = true;
		}
		if (!bQuit)
			// Début du nouveau mode
			pCurrentGest->BeginMode();
		// Mémo du mode courant
		iCurrentMode = newMode;
	}
	
	return bQuit;
}

