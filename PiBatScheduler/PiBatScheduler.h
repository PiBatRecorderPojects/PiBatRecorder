/* 
 * File:   PiBatScheduler.h
 * Author: Jean-Do
 *
 * Created on 17 août 2015, 21:13
 */

#include "ModePlay.h"
#include "KeyOledManager.h"
#include "CParameters.h"
#include "SchedulerModeInit.h"
#include "SchedulerModeParametres.h"
#include "QuitMode.h"
#include "SchedulerModeEnd.h"
#include "ModeScheduler.h"

#ifndef PIBATSCHEDULER_H
#define	PIBATSCHEDULER_H

/*-----------------------------------------------------------------------------
 Classe principale du PiBatScheduler
 Gestion du clavier et de l'afficheur LCD
-----------------------------------------------------------------------------*/
class PiBatScheduler
{
public:
	//-------------------------------------------------------------------------
	// Constructeur
	PiBatScheduler();
	
	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~PiBatScheduler();

	//-------------------------------------------------------------------------
	// Change le mode courant
	// Retourne true si on doit quitter le programme
	bool SetMode( int newMode);

	//-------------------------------------------------------------------------
	// Séquence d'initialisation du programme
	void Init();

	//-------------------------------------------------------------------------
	// Boucle principale du programme
	// Lecture du clavier et gestion des modes de fonctionnement
	void Run();
	
	//-------------------------------------------------------------------------
	// Monte la clé USB
	// Retourne True si la clé USB est montée
	bool MountUSBKey();
	
	//-------------------------------------------------------------------------
	// Traitement du lancement et de l'arrêt du monitoring auto
	void TraiteMonitoringAuto();
	
private:
	// Gestionnaire clavier/LCD
	KeyOledManager KScreen;
	// Indique si un mode entièrement auto est demandé
	bool bAutoMode;
	// Indique si une sortie est en cours
	bool bQuit;
	// Heure courante
	unsigned int timeNow;
	// Mode courant
	int iCurrentMode;
	// Mémorisation des paramètres
	CParameters cParams;
	// Gestionnaire d'init
	SchedulerModeInit GestInit;
	// Gestionnaire de menus
	SchedulerModeParametres GestModeParams;
	// Gestionnaire de lecture
	ModePlay GestPlay;
	// Gestionnaire de planification
	ModeScheduler GestScheduler;
	// Gestionnaire de sortie
	QuitMode GestQuit;
	// Gestionnaire de fin
	SchedulerModeEnd GestFin;
	// Pointeur sur le gestionnaire de l'état courant
	GenericMode *pCurrentGest;
};

#endif	/* PIBATSCHEDULER_H */

