/* 
 * File:   ScheduleTypeParam.cpp
 * Author: Jean-Do
 * 
 * Created on 5 janvier 2016, 22:22
 */
//-------------------------------------------------------------------------
// Classe de modification du type de planification

#include <stdio.h>
#include <stdlib.h>
#include "CParameters.h"
#include "ScheduleTypeParam.h"

//-------------------------------------------------------------------------
// Constructeur (initialisation des paramètres aux valeurs par défaut)
ScheduleTypeParam::ScheduleTypeParam(
	KeyOledManager	*pKOled,	// Pointeur sur l'afficheur/clavier
	CParameters		*pPar		// Pointeur sur les paramètres
	)
	:GenericParam(pKOled, pPar)
{
	//                   123456789012345678901
	//                    Type planific Soleil
	//                    Type planific Heure
	//                    Schedule type Sun
	//                    Schedule type Hour
	strcpy( indicModif, "0000000000000001xxxxx");
	// Mémo de la valeur courante
	iOldMode = pParams->GetScheduleType();
	iNewMode = iOldMode;
}

//-------------------------------------------------------------------------
// Destructeur
ScheduleTypeParam::~ScheduleTypeParam()
{
}

//-------------------------------------------------------------------------
// Traitement d'une touche clavier
void ScheduleTypeParam::ReceiveKey(
	int iKey	// Touche à traiter
	)
{
	// Traitement du clavier
	if (bModif)
	{
		switch (iKey)
		{
		case K_PLUS:
		case K_R:
			// On incrémente le digit
			iNewMode++;
			if (iNewMode > SUN)
				iNewMode = HOUR;
			break;
		case K_MINUS:
		case K_L:
			// On décrémente le digit
			iNewMode--;
			if (iNewMode < HOUR)
				iNewMode = SUN;
			break;
		case K_SQUARE:
			// Init de la valeur min
			iNewMode = HOUR;
			break;
		case K_CIRCLE:
			// Init de la valeur max
			iNewMode = SUN;
			break;
		case K_X:
			// Annulation de la modif
			bModif = false;
			iNewMode = iOldMode;
			break;
		default:
			GenericParam::ReceiveKey(iKey);
		}
	}
	else
	{
		GenericParam::ReceiveKey(iKey);
	}
}

//-------------------------------------------------------------------------
// Affichage du paramètre
void ScheduleTypeParam::AffParam(
	int iLine,		// Indice de la ligne ou afficher le paramètre
	bool bActive	// Inidque si la ligne est active
	)
{
	if (!bModif)
		iNewMode = pParams->GetScheduleType();
	//                   012345678901234567890
	//                    Type planific Soleil
	//                    Type planific Heure
	//                    Schedule type Sun
	//                    Schedule type Hour
	switch (iNewMode)
	{
	case HOUR:
		if (pParams->GetLangue() == FR)
			sprintf(lineParam, " Type planific Heure ");
		else
			sprintf(lineParam, " Schedule type Hour  ");
		break;
	case SUN:
	default:
		if (pParams->GetLangue() == FR)
			sprintf(lineParam, " Type planific Soleil");
		else
			sprintf(lineParam, " Schedule type Sun   ");
	}
	// Affichage de la ligne
	GenericParam::AffParam( iLine, bActive);
}

//-------------------------------------------------------------------------
// Start la modification du paramètre
void ScheduleTypeParam::StartModif()
{
	// Appel de la méthode parente
	GenericParam::StartModif();
	// Mémo de la valeur courante
	iOldMode = pParams->GetScheduleType();
	iNewMode = iOldMode;
}

//-------------------------------------------------------------------------
// Mise en jour du paramètre
void ScheduleTypeParam::SetParam()
{
	// Vérification du paramètre
	if (iNewMode < HOUR or iNewMode > SUN)
		// Valeur précédente
		iNewMode = iOldMode;
	pParams->SetScheduleType((SCHEDULETYPE)iNewMode);
	bModif = false;
	// Appel de la méthode parente
	GenericParam::SetParam();
}

