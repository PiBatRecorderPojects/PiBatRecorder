/* 
 * File:   SchedulerModeEnd.cpp
 * Author: Jean-Do
 * 
 * Created on 11 décembre 2015, 21:54
 */

#include "SchedulerModeEnd.h"
#include "logo_oled_nb_triste.h"
#include "SchedulerVersion.h"

/*-----------------------------------------------------------------------------
 Classe de geston du mode Fin
 Se contente d'afficher le logo du logiciel et quitte le logiciel
-----------------------------------------------------------------------------*/


//-------------------------------------------------------------------------
// Constructeur
SchedulerModeEnd::SchedulerModeEnd(
	CParameters *pPar,					// Pointeur sur les paramètres
	KeyOledManager *pKeyScreenManager,	// Pointeur sur le gestionnaire du clavier et de l'écran
	bool bAutoMode						// True si le logiciel fonctionne entièrement en mode automatique
										// Si oui, le clavier et l'écran ne sont pas gérés
	):GenericMode(pPar, pKeyScreenManager, bAutoMode)
{
}

//-------------------------------------------------------------------------
// Destructeur
SchedulerModeEnd::~SchedulerModeEnd()
{
}

//-------------------------------------------------------------------------
// Début du mode
void SchedulerModeEnd::BeginMode()
{
	LogFile::AddLog(LDEBUG, "SchedulerModeEnd::BeginMode");
	// Effacement de l'écran
	pKeyOledManager->ClearScreen();
	// Affichage du nom du logiciel en taille 2
	pKeyOledManager->DrawString((char *)"  PiBat"  , 0, L1T2, true, 2);
	pKeyOledManager->DrawString((char *)"Scheduler", 0, L2T2, true, 2);
	// Affichage de la version
	pKeyOledManager->DrawString((char *)VERSION,     0, L3T2, true, 2);
	// Affichage de la fin
	pKeyOledManager->DrawString((char *)"Bye...",    0, L7T1, true, 1);
	pKeyOledManager->DisplayScreen();
	// Mémorisation du temps de maintenant pour le timeout
	timeBegin = millis();
}

//-------------------------------------------------------------------------
// Fin du mode
void SchedulerModeEnd::EndMode()
{
	LogFile::AddLog(LDEBUG, "SchedulerModeEnd::EndMode");
	/*if (!bModeAuto)
	{*/
		// Effacement de l'écran
		pKeyOledManager->ClearScreen();
		pKeyOledManager->DisplayScreen();
	//}
}
	
//-------------------------------------------------------------------------
// Affichage du mode sur l'écran
// Cette méthodes est appelée régulièrement par la classe PiBatManager
// à charge des différents modes d'afficher les informations nécessaires
void SchedulerModeEnd::PrintMode()
{
	// Ne fait rien
}

//-------------------------------------------------------------------------
// Traitement des ordres claviers
// Retourne éventuellement un mode demandé (NOMODE sinon)
// Cette méthodes est appelée régulièrement par la classe PiBatManager
// à charge des différents modes de traiter les actions opérateurs
int SchedulerModeEnd::ManageKey(
	unsigned short key	// Touche sollicitée par l'opérateur
	)
{
	// Mémorise le temps de maintenant
	unsigned int timeNow = millis();
	MODEFONC newMode = NOMODE;
	
	// Test si le timeout est passé (3s)
	if ((timeNow - timeBegin) > 3000)
		// On quitte le logiciel
		newMode = EXIT;

	return newMode;
}

