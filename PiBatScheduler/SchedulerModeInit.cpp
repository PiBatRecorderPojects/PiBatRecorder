/* 
 * File:   SchedulerModeInit.cpp
 * Author: Jean-Do
 * 
 * Created on 10 décembre 2015, 14:39
 */

#include "SchedulerModeInit.h"
#include "logo_oled_nb_heureuse.h"
#include "SchedulerVersion.h"

//-------------------------------------------------------------------------
// Constructeur
SchedulerModeInit::SchedulerModeInit(
	CParameters *pPar,					// Pointeur sur les paramètres
	KeyOledManager *pKeyScreenManager,	// Pointeur sur le gestionnaire du clavier et de l'écran
	bool bAutoMode						// True si le logiciel fonctionne entièrement en mode automatique
										// Si oui, le clavier et l'écran ne sont pas gérés
	):GenericMode(pPar, pKeyScreenManager, bAutoMode)
{
}

//-------------------------------------------------------------------------
// Destructeur
SchedulerModeInit::~SchedulerModeInit()
{
}

//-------------------------------------------------------------------------
// Début du mode
void SchedulerModeInit::BeginMode()
{
	LogFile::AddLog(LDEBUG, "SchedulerModeInit::BeginMode");
	// Effacement de l'écran
	pKeyOledManager->ClearScreen();
	// Affichage du nom du logiciel en taille 2
	pKeyOledManager->DrawString((char *)"  PiBat"  , 0, L1T2, true, 2);
	pKeyOledManager->DrawString((char *)"Scheduler", 0, L2T2, true, 2);
	// Affichage de la version
	pKeyOledManager->DrawString((char *)VERSION,     0, L3T2, true, 2);
	// Affichage du début
	pKeyOledManager->DrawString((char *)"Hello",     0, L7T1, true, 1);
	pKeyOledManager->DisplayScreen();
	// Mémorisation du temps de maintenant pour le timeout
	timeBegin = millis();
}

//-------------------------------------------------------------------------
// Affichage du mode sur l'écran
// Cette méthodes est appelée régulièrement par la classe PiBatManager
// à charge des différents modes d'afficher les informations nécessaires
void SchedulerModeInit::PrintMode()
{
	// Ne fait rien
}

//-------------------------------------------------------------------------
// Traitement des ordres claviers
// Retourne éventuellement un mode demandé (NOMODE sinon)
// Cette méthodes est appelée régulièrement par la classe PiBatManager
// à charge des différents modes de traiter les actions opérateurs
int SchedulerModeInit::ManageKey(
	unsigned short key	// Touche sollicitée par l'opérateur
	)
{
	// Pas de traitement des touches
	// Mémorise le temps de maintenant
	unsigned int timeNow = millis();
	MODEFONC newMode = NOMODE;
	
	// Test si le timeout est passé (2s)
	if ((timeNow - timeBegin) > 2000)
		// On passe dans le mode schedule
		newMode = MONITORING;

	return newMode;
}

