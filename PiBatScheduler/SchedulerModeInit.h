/* 
 * File:   SchedulerModeInit.h
 * Author: Jean-Do
 *
 * Created on 10 décembre 2015, 14:39
 */
#include "GenericMode.h"

#ifndef SCHEDULERMODEINIT_H
#define	SCHEDULERMODEINIT_H

/*-----------------------------------------------------------------------------
 Classe de geston du mode Init
 Se contente d'afficher le logo du logiciel et passe en mode Monitoring
-----------------------------------------------------------------------------*/
class SchedulerModeInit : public GenericMode
{
public:
	//-------------------------------------------------------------------------
	// Constructeur
	SchedulerModeInit(
		CParameters *pPar,					// Pointeur sur les paramètres
		KeyOledManager *pKeyScreenManager,	// Pointeur sur le gestionnaire du clavier et de l'écran
		bool bAutoMode=false				// True si le logiciel fonctionne entièrement en mode automatique
			                                // Si oui, le clavier et l'écran ne sont pas gérés
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~SchedulerModeInit();
	
	//-------------------------------------------------------------------------
	// Début du mode
	virtual void BeginMode();
	
	//-------------------------------------------------------------------------
	// Affichage du mode sur l'écran
	// Cette méthode est appelée régulièrement par la classe PiBatManager
	// à charge des différents modes d'afficher les informations nécessaires
	virtual void PrintMode();
	
	//-------------------------------------------------------------------------
	// Traitement des ordres claviers
	// Si la touche est une touche de changement de mode, retourne le mode demandé
	// Cette méthode est appelée régulièrement par la classe PiBatManager
	// à charge des différents modes de traiter les actions opérateurs
	virtual int ManageKey(
		unsigned short key	// Touche sollicitée par l'opérateur
		);

private:
	// Mémorisation du temps en début de mode pour le timeout
	unsigned int timeBegin;
};

#endif	/* SCHEDULERMODEINIT_H */

