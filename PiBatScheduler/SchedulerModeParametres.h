/* 
 * File:   ModeParametres.h
 * Author: Jean-Do
 *
 * Created on 12 décembre 2015, 09:34
 */

#include <vector>
#include "CParameters.h"
#include "GenericParam.h"
#include "GenericMode.h"

#ifndef SCHEDULEMODEPARAMETRES_H
#define	SCHEDULEMODEPARAMETRES_H

/*-----------------------------------------------------------------------------
 Classe de geston du mode de modification des paramètres
 Traite les commande et appelle les fonctions des paramètres affichés et
 du paramètre encourt de modification
-----------------------------------------------------------------------------*/

// Liste des menus dans l'ordre d'apparition
// Seuil détect		Seuil de détection en dB
// Frequ Min		Fréquence d'intéret min
// Frequ Max		Fréquence d'intéret max
// Volume Record	Gain d'enregistrement
// Min Record		Temps min d'enregistrement
// Max Record		Temps max d'enregistrement
// Type record		Type d'enregistrement (WAV/LOG/ACT)
// Mode Record		Mode d'enregistrement (Manu/Auto)
// Mode planif		Mode planification (Soleil ou Heure)
// Si Heure
// Début Record		Heure de début d'enregistrement
// Fin Record		Heure de fin d'enregistrement
// Sinon Si Sun
// Décalage coucher	Déclagae par rapport au coucher et lever du soleil
// Latitude			Latitude du site
// Longitude		Longitude du site
// Fin Si
// Prefixe wave		Préfixe des fichiers wav
// Volume Play		Valide en mode play
// Output			Sortie du lecteur (HEADSET, LINE ou LOUDSPEAKER, HEADSET par défaut)
// Date				Date de l'horloge
// Heure			Heure de l'horloge
// Langue			Langue d'affichage
// RAZ              RAZ des fichiers wav
// Mount USB		Montage/Démontage clé USB
// A propos         A propos du logiciel

class SchedulerModeParametres : public GenericMode
{
public:
	//-------------------------------------------------------------------------
	// Constructeur
	SchedulerModeParametres(
		CParameters *pPar,					// Pointeur sur les paramètres
		KeyOledManager *pKeyScreenManager,	// Pointeur sur le gestionnaire du clavier et de l'écran
		bool bAutoMode=false				// True si le logiciel fonctionne entièrement en mode automatique
			                                // Si oui, le clavier et l'écran ne sont pas gérés
		);

	//-------------------------------------------------------------------------
	// Destructeur
	virtual ~SchedulerModeParametres();
	
	//-------------------------------------------------------------------------
	// Début du mode
	virtual void BeginMode();
	
	//-------------------------------------------------------------------------
	// Affichage du mode sur l'écran
	// Cette méthode est appelée régulièrement par la classe PiBatManager
	// à charge des différents modes d'afficher les informations nécessaires
	virtual void PrintMode();
	
	//-------------------------------------------------------------------------
	// Traitement des ordres claviers
	// Retourne éventuellement un mode demandé (NOMODE sinon)
	// Cette méthode est appelée régulièrement par la classe PiBatManager
	// à charge des différents modes de traiter les actions opérateurs
	int ManageKey(
		unsigned short key	// Touche sollicitée par l'opérateur
		);

	//-------------------------------------------------------------------------
	// Initialisation des paramètres valides
	void InitValidParams();
	
private:
	// Indice du menu courant
	int iMenu;
	
	// Indice du menu du haut
	int iMenuUp;
	
	// Indice du menu du bas
	int iMenuDown;
	
	// Pour repasser une ligne en modif après validation d'une modification
	bool bModifOnNextAff;
	unsigned int timeAff;
	
	// Liste des Modificateurs de paramètres
	std::vector<GenericParam *> LstModifParams;
	
	// Liste des modificateurs valide
	std::vector<GenericParam *> LstValidParams;
	
	// Mémo du pointeur sur RAZParam pour initialiser la liste de fichiers
	GenericParam *pRAZParam;
	
	// Mémo des pointeurs des paramètres HDeb, HFin, Lat, Long et ShiftH
	// pour les valider ou non en fonction du contexte
	GenericParam *pScheduleType;
	GenericParam *pHDeb;
	GenericParam *pHFin;
	GenericParam *pLat;
	GenericParam *pLon;
	GenericParam *pShift;
};

#endif	/* SCHEDULEMODEPARAMETRES_H */

