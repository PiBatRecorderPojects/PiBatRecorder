/* 
 * File:   main.cpp
 * Author: Jean-Do
 *
 * Created on 5 janvier 2016, 19:06
 */
//-------------------------------------------------------------------------
// Point d'entrée du programme PiBatScheduler
// Permet de modifier les paramètres d'un PiBatAutomaticRecorder
// Et de lancer l'enregistrement aux heures programmées

#include <cstdlib>
#include "Version.h"
#include "PiBatScheduler.h"

//using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
	strcpy(LogFile::softName, "PiBatScheduler");
	LogFile::bConsole = true;
	LogFile::LogLevel = LINFO;
	bool bVersion = false;
	// Récupération du répertoire de lancement
	char pathExec[512];
	char path[512];
	char *p;
	strcpy(path, argv[0]);
	strcpy(pathExec, argv[0]);
	p = strrchr((char *)path, '/');
	if (p != NULL)
		p[1] = 0;
	//printf("Path d'exécution [%s]\n", path, pathExec);	
	if (argc > 1)
	{
		// Test de l'option
		if (strstr(argv[1], "-v") != NULL or strstr(argv[1], "-V") != NULL)
		{
			// Appel juste pour indiquer la version
			bVersion = true;
			printf("PiBatScheduler Version %s\n", VERSION);
		}
		/*else
			printf("Paramètre [%s] invalide !\n", argv[1]);*/
	}

	if (!bVersion)
	{
		// Mémo du démarrage de PiBatScheduler dans le log
		LogFile::AddLog(LLOG, "Start");
		// Création du manager
		PiBatScheduler manager;
		manager.Init();
		// Lancement de la boucle principale
		manager.Run();
		// Mémo de la fin de PiBatScheduler dans le log
		LogFile::AddLog(LLOG, "Stop");
	}
	// Sortie du logiciel
	return 0;
}

