#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/869457397/AboutParam.o \
	${OBJECTDIR}/_ext/869457397/CParameters.o \
	${OBJECTDIR}/_ext/869457397/CPlay.o \
	${OBJECTDIR}/_ext/869457397/CWaveFile.o \
	${OBJECTDIR}/_ext/869457397/DateParam.o \
	${OBJECTDIR}/_ext/869457397/GenericMode.o \
	${OBJECTDIR}/_ext/869457397/GenericParam.o \
	${OBJECTDIR}/_ext/869457397/HourParam.o \
	${OBJECTDIR}/_ext/869457397/KeyOledManager.o \
	${OBJECTDIR}/_ext/869457397/LanguageParam.o \
	${OBJECTDIR}/_ext/869457397/LogFile.o \
	${OBJECTDIR}/_ext/869457397/MaxFreqParam.o \
	${OBJECTDIR}/_ext/869457397/MaxRecParam.o \
	${OBJECTDIR}/_ext/869457397/MinFreqParam.o \
	${OBJECTDIR}/_ext/869457397/MinRecParam.o \
	${OBJECTDIR}/_ext/869457397/ModePlay.o \
	${OBJECTDIR}/_ext/869457397/MountUSBKey.o \
	${OBJECTDIR}/_ext/869457397/OutputParam.o \
	${OBJECTDIR}/_ext/869457397/PrefixParam.o \
	${OBJECTDIR}/_ext/869457397/QuitMode.o \
	${OBJECTDIR}/_ext/869457397/RAZParam.o \
	${OBJECTDIR}/_ext/869457397/RecVolumeParam.o \
	${OBJECTDIR}/_ext/869457397/RecordTypeParam.o \
	${OBJECTDIR}/_ext/869457397/StartHourParam.o \
	${OBJECTDIR}/_ext/869457397/StopHourParam.o \
	${OBJECTDIR}/_ext/869457397/ThresholdParam.o \
	${OBJECTDIR}/_ext/869457397/VolumeParam.o \
	${OBJECTDIR}/HourShiftParam.o \
	${OBJECTDIR}/LatitudeParam.o \
	${OBJECTDIR}/LongitudeParam.o \
	${OBJECTDIR}/ModeScheduler.o \
	${OBJECTDIR}/PiBatScheduler.o \
	${OBJECTDIR}/ScheduleTypeParam.o \
	${OBJECTDIR}/SchedulerModeEnd.o \
	${OBJECTDIR}/SchedulerModeInit.o \
	${OBJECTDIR}/SchedulerModeParametres.o \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lportaudio -lwiringPi -lArduiPi_OLED

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pibatscheduler

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pibatscheduler: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pibatscheduler ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/869457397/AboutParam.o: ../PiBatRecorder/AboutParam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/AboutParam.o ../PiBatRecorder/AboutParam.cpp

${OBJECTDIR}/_ext/869457397/CParameters.o: ../PiBatRecorder/CParameters.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/CParameters.o ../PiBatRecorder/CParameters.cpp

${OBJECTDIR}/_ext/869457397/CPlay.o: ../PiBatRecorder/CPlay.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/CPlay.o ../PiBatRecorder/CPlay.cpp

${OBJECTDIR}/_ext/869457397/CWaveFile.o: ../PiBatRecorder/CWaveFile.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/CWaveFile.o ../PiBatRecorder/CWaveFile.cpp

${OBJECTDIR}/_ext/869457397/DateParam.o: ../PiBatRecorder/DateParam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/DateParam.o ../PiBatRecorder/DateParam.cpp

${OBJECTDIR}/_ext/869457397/GenericMode.o: ../PiBatRecorder/GenericMode.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/GenericMode.o ../PiBatRecorder/GenericMode.cpp

${OBJECTDIR}/_ext/869457397/GenericParam.o: ../PiBatRecorder/GenericParam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/GenericParam.o ../PiBatRecorder/GenericParam.cpp

${OBJECTDIR}/_ext/869457397/HourParam.o: ../PiBatRecorder/HourParam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/HourParam.o ../PiBatRecorder/HourParam.cpp

${OBJECTDIR}/_ext/869457397/KeyOledManager.o: ../PiBatRecorder/KeyOledManager.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/KeyOledManager.o ../PiBatRecorder/KeyOledManager.cpp

${OBJECTDIR}/_ext/869457397/LanguageParam.o: ../PiBatRecorder/LanguageParam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/LanguageParam.o ../PiBatRecorder/LanguageParam.cpp

${OBJECTDIR}/_ext/869457397/LogFile.o: ../PiBatRecorder/LogFile.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/LogFile.o ../PiBatRecorder/LogFile.cpp

${OBJECTDIR}/_ext/869457397/MaxFreqParam.o: ../PiBatRecorder/MaxFreqParam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/MaxFreqParam.o ../PiBatRecorder/MaxFreqParam.cpp

${OBJECTDIR}/_ext/869457397/MaxRecParam.o: ../PiBatRecorder/MaxRecParam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/MaxRecParam.o ../PiBatRecorder/MaxRecParam.cpp

${OBJECTDIR}/_ext/869457397/MinFreqParam.o: ../PiBatRecorder/MinFreqParam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/MinFreqParam.o ../PiBatRecorder/MinFreqParam.cpp

${OBJECTDIR}/_ext/869457397/MinRecParam.o: ../PiBatRecorder/MinRecParam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/MinRecParam.o ../PiBatRecorder/MinRecParam.cpp

${OBJECTDIR}/_ext/869457397/ModePlay.o: ../PiBatRecorder/ModePlay.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/ModePlay.o ../PiBatRecorder/ModePlay.cpp

${OBJECTDIR}/_ext/869457397/MountUSBKey.o: ../PiBatRecorder/MountUSBKey.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/MountUSBKey.o ../PiBatRecorder/MountUSBKey.cpp

${OBJECTDIR}/_ext/869457397/OutputParam.o: ../PiBatRecorder/OutputParam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/OutputParam.o ../PiBatRecorder/OutputParam.cpp

${OBJECTDIR}/_ext/869457397/PrefixParam.o: ../PiBatRecorder/PrefixParam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/PrefixParam.o ../PiBatRecorder/PrefixParam.cpp

${OBJECTDIR}/_ext/869457397/QuitMode.o: ../PiBatRecorder/QuitMode.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/QuitMode.o ../PiBatRecorder/QuitMode.cpp

${OBJECTDIR}/_ext/869457397/RAZParam.o: ../PiBatRecorder/RAZParam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/RAZParam.o ../PiBatRecorder/RAZParam.cpp

${OBJECTDIR}/_ext/869457397/RecVolumeParam.o: ../PiBatRecorder/RecVolumeParam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/RecVolumeParam.o ../PiBatRecorder/RecVolumeParam.cpp

${OBJECTDIR}/_ext/869457397/RecordTypeParam.o: ../PiBatRecorder/RecordTypeParam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/RecordTypeParam.o ../PiBatRecorder/RecordTypeParam.cpp

${OBJECTDIR}/_ext/869457397/StartHourParam.o: ../PiBatRecorder/StartHourParam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/StartHourParam.o ../PiBatRecorder/StartHourParam.cpp

${OBJECTDIR}/_ext/869457397/StopHourParam.o: ../PiBatRecorder/StopHourParam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/StopHourParam.o ../PiBatRecorder/StopHourParam.cpp

${OBJECTDIR}/_ext/869457397/ThresholdParam.o: ../PiBatRecorder/ThresholdParam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/ThresholdParam.o ../PiBatRecorder/ThresholdParam.cpp

${OBJECTDIR}/_ext/869457397/VolumeParam.o: ../PiBatRecorder/VolumeParam.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/869457397
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/869457397/VolumeParam.o ../PiBatRecorder/VolumeParam.cpp

${OBJECTDIR}/HourShiftParam.o: HourShiftParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/HourShiftParam.o HourShiftParam.cpp

${OBJECTDIR}/LatitudeParam.o: LatitudeParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/LatitudeParam.o LatitudeParam.cpp

${OBJECTDIR}/LongitudeParam.o: LongitudeParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/LongitudeParam.o LongitudeParam.cpp

${OBJECTDIR}/ModeScheduler.o: ModeScheduler.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ModeScheduler.o ModeScheduler.cpp

${OBJECTDIR}/PiBatScheduler.o: PiBatScheduler.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/PiBatScheduler.o PiBatScheduler.cpp

${OBJECTDIR}/ScheduleTypeParam.o: ScheduleTypeParam.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ScheduleTypeParam.o ScheduleTypeParam.cpp

${OBJECTDIR}/SchedulerModeEnd.o: SchedulerModeEnd.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SchedulerModeEnd.o SchedulerModeEnd.cpp

${OBJECTDIR}/SchedulerModeInit.o: SchedulerModeInit.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SchedulerModeInit.o SchedulerModeInit.cpp

${OBJECTDIR}/SchedulerModeParametres.o: SchedulerModeParametres.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SchedulerModeParametres.o SchedulerModeParametres.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../PiBatRecorder -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/pibatscheduler

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
