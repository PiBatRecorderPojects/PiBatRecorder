/* 
 * File:   main.cpp
 * Author: Jean-Do
 *
 * Created on 18 janvier 2016, 20:12
 */
//-------------------------------------------------------------------------
// Logiciel de gestion des mises à jours automatique des logiciels
// PiBatRecorder et PiBatScheduler
// PiBatUpdater est lancé au démarrage du système Raspbian
// Il monte la clé USB et si un répertoire "PiBatUpdater" est présent,
// il copie les exécutables dans le répertoire d'exécution
// puis s'arrête.

#include <cstdlib>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include "Version.h"
#include "LogFile.h"

using namespace std;

// Gestionnaire du fichier LOG
LogFile log;

//-------------------------------------------------------------------------
// Test de présence de la clé USB
// Retourne True si la clé USB est montée
bool isUSBKey(
	const char* pathUSBkey	// Répertoire de base de la clé USB
	)
{
	bool bOK = false;
	// Commande df pour savoir ou est monté /mnt/usbkey
	char sCmd[255];
	sprintf(sCmd, "sudo df %s > /home/pi/testusbkey.txt", pathUSBkey);
	LogFile::AddLog(LDEBUG, "isUSBKey %s\n", sCmd);
	system(sCmd);
	// Ligne de lecture
	char sLine[255];
	// Ouverture du fichier de résultat
	FILE *f = fopen ("/home/pi/testusbkey.txt", "r");
	if (f != NULL)
	{
		// Vérification présence /dev/sda1
		while (fgets(sLine, 255, f) != NULL)
		{
			LogFile::AddLog(LDEBUG, "line [%s]\n", sLine);
			if (strstr(sLine, "/dev/sda1") != NULL)
			{
				LogFile::AddLog(LDEBUG, "USB trouvée\n");
				bOK = true;
				break;
			}
		}
		// Fermeture du fichier
		fclose(f);
		system("sudo rm /home/pi/testusbkey.txt");
	}
	return bOK;
}

//-------------------------------------------------------------------------
// Monte la clé USB
// Retourne True si la clé USB est montée
bool MountUSBKey(
	const char* pathUSBkey	// Répertoire de base de la clé USB
	)
{
	bool bOK = true;
	// Si pas déjà montée
	if (not isUSBKey(pathUSBkey))
	{
		LogFile::AddLog(LDEBUG, "MountUSBKey [%s]\n", pathUSBkey);
		char temp[255];
		//sprintf( temp, "sudo mount -o uid=pi,gid=pi /dev/sda1 %s", pathUSBkey);
		system(temp);
		if (!isUSBKey(pathUSBkey))
		{
			LogFile::AddLog(LDEBUG, "CParameters::MountUSBKey clé [%s] absente !\n", pathUSBkey),
			bOK = false;
		}
		else
			LogFile::AddLog(LDEBUG, "CParameters::MountUSBKey clé [%s] montée\n", pathUSBkey);
	}
	return bOK;
}

//-------------------------------------------------------------------------
// Retourne true si le répertoire existe
bool isUpdateDir(
	const char* pathUSBKey	// Répertoire de base de la clé USB
	)
{
	LogFile::AddLog(LDEBUG, "isUpdateDir(%s)\n", pathUSBKey);
    DIR * rep = opendir(pathUSBKey);
    bool bOK = false;
 
    if (rep != NULL)
    {
        struct dirent * ent;
        while ((ent = readdir(rep)) != NULL)
        {
			LogFile::AddLog(LDEBUG, "Dir %s\n", ent->d_name);
            if (strcmp("PiBatUpdater", ent->d_name) == 0)
			{
				// Le répertoire PiBatUpdater existe sur la clé USB
                bOK = true;
				break;
			}
        }
        closedir(rep);
    }
	else
		LogFile::AddLog(LDEBUG, "Dir %s introuvable\n", pathUSBKey);
 
    return bOK;
}

//-------------------------------------------------------------------------
// Mise à jour éventuelle d'un fichier
// Retourne true si le fichier est mis à jour
bool UpdateFile(
	const char* pathUpdateDir,	// Répertoire contenant les éventuels fichiers de mise à jour
	const char* filename		// Nom du fichier à mettre à jour
	)
{
	char cmd[255];
	bool bOK = false;
    DIR * rep = opendir(pathUpdateDir);
 
    if (rep != NULL)
    {
        struct dirent * ent;
        while ((ent = readdir(rep)) != NULL)
        {
            if (strcmp( filename, ent->d_name) == 0)
			{
				// Le fichier est présent
				// Copie du fichier
				sprintf( cmd, "cp %s/%s /home/pi/PiBatRecorder", pathUpdateDir, filename);
				LogFile::AddLog(LDEBUG, cmd);
				system ( cmd);
				if (strstr(filename, ".txt") == NULL)
				{
					// Rend le fichier exécutable
					sprintf( cmd, "sudo chmod +x /home/pi/PiBatRecorder/%s", filename);
					LogFile::AddLog(LDEBUG, cmd);
					system ( cmd);
				}
				bOK = true;
				break;
			}
        }
        closedir(rep);
    }
	return bOK;
}

/*
 * 
 */
int main(int argc, char** argv)
{
	strcpy(LogFile::softName, "PiBatUpdater");
	LogFile::bConsole = true;
	LogFile::LogLevel = LINFO;
	//LogFile::LogLevel = LDEBUG;
	// Récupération du répertoire de montage de la clé USB
	if (argc > 1)
	{
		// Test de l'option
		if (strstr(argv[1], "-v") != NULL or strstr(argv[1], "-V") != NULL)
		{
			// Appel juste pour indiquer la version
			printf("PiBatUpdater Version %s\n", VERSION);
		}
		else if (strlen(argv[1]) == 0)
			LogFile::AddLog(LLOG, "USB path argument empty !");
		else
		{
			LogFile::AddLog(LLOG, "Start");
			// Copie du répertoire
			char pathUSB[255];
			strcpy( pathUSB, argv[1]);
			LogFile::AddLog(LLOG, "Argument [%s]\n", argv[1]);
			if (pathUSB[strlen(pathUSB)-1] == '/')
				pathUSB[strlen(pathUSB)-1] = 0;
			// Montage de la clé USB
			if (MountUSBKey(pathUSB))
			{
				if (isUpdateDir(pathUSB))
				{
					bool bOK = false;
					// Préparation du chemin du répertoire avec les fichiers à mettre éventuellement à jour
					char updatePath[255];
					strcpy(updatePath, pathUSB);
					strcat(updatePath, "/PiBatUpdater");

					// Mise à jour éventuelle de PiBatRecorder
					if (UpdateFile( updatePath, "pibatrecorder"))
					{
						LogFile::AddLog(LLOG, "update pibatrecorder OK");
						bOK = true;
					}

					// Mise à jour éventuelle de PiBatScheduler
					if (UpdateFile( updatePath, "pibatscheduler"))
					{
						LogFile::AddLog(LLOG, "update pibatscheduler OK");
						bOK = true;
					}

					// Mise à jour éventuelle du fichier de configuration
					if (UpdateFile( updatePath, "PiBatConfig.txt"))
					{
						LogFile::AddLog(LLOG, "update PiBatConfig.txt OK");
						bOK = true;
					}

					// Mise à jour éventuelle du ficher des paramètres
					if (UpdateFile( updatePath, "PiBatParams.txt"))
					{
						LogFile::AddLog(LLOG, "update PiBatParams.txt OK");
						bOK = true;
					}

					if (!bOK)
						LogFile::AddLog(LLOG, "no update to do");
				}
				else
					LogFile::AddLog(LLOG, "no update directory");
				LogFile::AddLog(LLOG, "Stop");
			}
			else
				LogFile::AddLog(LLOG, "no USB key !");
		}
	}
	else
		LogFile::AddLog(LLOG, "USB path argument empty !!");
	return 0;
}

