Avertissement
=
Le projet BiPatRecorder n'est plus supporté ! La carte audio nécessaire au projet n'est plus commercialisée.
Les projets Teensy Recorder remplacent désormais le projet PiBatRecorder.
Environ 15 exemplaires de PiBatRecorder ont été construit, ce site reste pour une éventuelle maintenance. 

Présentation du projet
=

**Plus d'informations sur le site officiel du projet : http://pibatrecorder.ardechelibre.org**

**PiBatRecorder** est un détecteur/enregistreur de chauve-souris libre et économique  basé sur un Raspberry Pi qui permet d’enregistrer les ultrasons jusqu’à 96kHz.

Porté bénévolement par un groupe informel de chiro-bricolos, ce projet a été fait pour une fabrication à réaliser soi-même.

Les objectifs
-

- Être **ouvert** (dans le sens des communs). Les logiciels ainsi que leurs codes source, les schémas matériels (montages et circuits électroniques) sont mis à disposition de tous gratuitement et librement afin de permettre à chacun de fabriquer son détecteur.
- Être **facilement reproductible** pour faciliter la réalisation par toute personne intéressée.
- Être **économique** pour faciliter l’accès à un matériel d’étude de qualité (environ 200-250€).
- Être **pertinent pour l’étude des chauve-souris**, en couvrant une part importante de la bande de fréquence et en ayant des résultats comparables aux autres détecteurs couramment utilisés.

4 modèles
-

- Le **PiBatManualRecorder** pour une utilisation manuelle avec écoute en hétérodyne, réécoute en expansion de temps et enregistrement manuel ou automatique au format WAV (96kHz). Il permet également un fonctionnement automatique sur une nuit. Interface d’utilisation par un écran et un  clavier tactile.
- Le **PiBatAutomaticRecorder** pour une utilisation automatique avec enregistrement de fichiers audio wav sur une ou plusieurs nuits.
- Le **PiBatFixedRecorder** pour une utilisation automatique en point fixe sur de longues durées avec transmission des données par internet.
- Le **PiBatLogger** pour une surveillance automatique sur plusieurs semaines de l’activité d’espèces faciles à détecter (grand rhinolophe par exemple).

Pour l’heure, le projet le plus avancé est le PiBatManualRecorder dont le premier prototype est fonctionnel depuis quelques mois.

Un détecteur/enregistreur économique
-

Le coeur de ce détecteur est constitué de :
- un nano-ordinateurs populaire et économique, le **Raspberry Pi** (A+ ou ZERO) ;
- une **carte son Cirrus Logic** permettant des échantillonnages jusqu’à une fréquence de **96kHz** ;
- un **microphone amplifié adapté aux ultrasons**. Fabriqués par Knowles, ces capteurs sont identiques à ceux des détecteurs du commerce (modèles electret de la série FG ou modèles MEMS).
- une **batterie LiIon** (le dispositif a une autonomie de plusieurs nuits, 2 nuits pour une batterie de 6600mAh).
- un **écran OLED** et un **clavier tactile** ont été ajoutés.

L’ensemble du matériel nécessaire à la conception de ce détecteur vous reviendra à environ **150€**.

**Plus d'informations sur le site officiel du projet : http://pibatrecorder.ardechelibre.org**